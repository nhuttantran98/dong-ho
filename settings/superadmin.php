<?php  
  $adminSettings = array (
  'name' => '',
  'name_en' => '',
  'image_resolution_product_text' => '737x737',
  'image_resolution_collection_text' => '',
  'image_resolution_article_text' => '737x737',
  'image_resolution_page_text' => '1903x447',
  'image_resolution_blog_text' => '',
  'image_resolution_testimonial_text' => '',
  'image_resolution_client_text' => '158x65',
  'image_resolution_services_text' => '',
  'image_resolution_icon_services_text' => '',
  'image_resolution_slider_text' => '1903x943',
  'slug_type' => '2',
  'slug_type_2_product_vi' => 'product',
  'slug_type_2_product_en' => 'product',
  'slug_type_2_collection_vi' => 'collection',
  'slug_type_2_collection_en' => 'collection',
  'slug_type_2_article_vi' => 'tin-tuc-&-su-kien',
  'slug_type_2_article_en' => 'article',
  'slug_type_2_blog_vi' => 'tin-tuc-va-su-kien',
  'slug_type_2_blog_en' => 'blog',
  'slug_type_2_page_vi' => 'about-us',
  'slug_type_2_page_en' => 'page',
  'slug_type_2_gallery_vi' => 'gallery',
  'slug_type_2_gallery_en' => 'gallery',
  'slug_lv_1_product_vi' => '',
  'slug_lv_1_product_en' => '',
  'slug_lv_1_collection_vi' => '',
  'slug_lv_1_collection_en' => '',
  'slug_lv_1_article_vi' => '',
  'slug_lv_1_article_en' => '',
  'slug_lv_1_blog_vi' => '',
  'slug_lv_1_blog_en' => '',
  'slug_lv_1_page_vi' => '',
  'slug_lv_1_page_en' => '',
  'slug_lv_1_gallery_vi' => '',
  'slug_lv_1_gallery_en' => '',
  'slug_lv_2_product_vi' => '',
  'slug_lv_2_product_en' => '',
  'slug_lv_2_collection_vi' => '',
  'slug_lv_2_collection_en' => '',
  'slug_lv_2_article_vi' => '',
  'slug_lv_2_article_en' => '',
  'slug_lv_2_blog_vi' => '',
  'slug_lv_2_blog_en' => '',
  'slug_lv_2_page_vi' => '',
  'slug_lv_2_page_en' => '',
  'slug_lv_2_gallery_vi' => '',
  'slug_lv_2_gallery_en' => '',
  'mailgun_api_key' => 'key-a435c7c000868856db662554bced44cc',
  'mailgun_domain' => 'mail.eye-solution.vn',
  'mailgun_user' => 'postmaster@mail.eye-solution.vn',
  'fb_app_id' => '',
  'fb_app_secret' => '',
  'gg_project_id' => '',
  'gg_client_id' => '',
  'gg_client_secret' => '',
  'telegram_key' => '',
  'telegram_group_id' => '',
  'kiotviet_client_id' => '',
  'kiotviet_client_secret' => '',
  'custom_field' => 
  array (
  ),
  'setting_collection_perpage' => '4',
  'setting_blog_perpage' => '4',
  'setting_search_product_perpage' => '94',
  'setting_search_article_perpage' => '4',
  'setting_photo_perpage' => '',
  'maintain_website' => 0,
  'maintain_website_content' => '',
  'image_resolution_variant_text' => '',
  'image_resolution_photo_text' => '',
  'image_resolution_favicon_text' => '',
  'image_resolution_logo_header_text' => '',
  'image_resolution_logo_footer_text' => '',
  'fb_login_url' => '',
  'image_resolution_menu_text' => ' ',
  'image_resolution_seo_optimize_text' => '470x264',
  'image_resolution_custom_field_text' => '737x498',
  'image_resolution_custom_field_article_banner_text' => '737x498',
  'image_resolution_custom_field_blog_banner_text' => '1903x192',
  'image_resolution_custom_field_page_right_image_text' => '750x458',
  'status' => '1',
  'mailgun_email_address' => '',
  'ghtk_client_token' => '',
  'ghtk_client_environment' => '',
  'freshdesk_domain' => '',
  'freshdesk_api_ley' => '',
  'pick_name' => '',
  'pick_phone' => '',
  'pick_address' => '',
  'pick_district' => '',
  'pick_province' => '',
);