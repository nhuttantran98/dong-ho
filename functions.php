<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CustomAdminController extends Controller {
  public function helloWorld(Request $request, Response $response) {
    return $response->write('Hello World');
  }
}


class CustomStoreFrontController extends Controller {
  public function helloWorld(Request $request, Response $response) {
    // return this->view->render($response, 'view-twig-file', $data);
    return $response->write('Hello World');
  }
}


// TODO: add custom admin routes
function add_custom_admin_routes($app) {

}

// TODO: add custom store front routes
function add_custom_store_front_routes($app) {
  $app->get('/hello', '\CustomStoreFrontController:helloWorld');

}
registerCustomField("Tiêu đề", "page", "input");
registerCustomField("Mô tả", "page", "editor");
registerCustomField("Hình phải", "page", "image");

// registerCustomField("Tiêu đề phải", "blog", "input");
registerCustomField("Người viết", "blog", "input");
registerCustomField("Banner", "blog", "image");
registerCustomField("Tiêu đề banner", "blog", "editor");

// registerCustomField("Tiêu đề phải", "article", "input");
registerCustomField("Banner", "article", "image");
registerCustomField("Người viết", "article", "input");

