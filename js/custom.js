var url = '';
var query = '';

$('#loadMore').click(function (event) {
  event.preventDefault();
  var blogList = $('.blog-list');
  var count = 1;
  blogList.find('.entry-blog.hidden').each(function (index, el) {
    if (count < 5) {
      $(this).removeClass('hidden');
      count++;
    }
    if (!blogList.find('.entry-blog.hidden').length) {
      $('#loadMore').addClass('hidden');
    }
  });
});

$('.searchform-header').submit(function (event) {
  event.preventDefault();
  $('input').removeClass('error');
  var self = $(this);
  var q = self.find('input[name="q"]').val();
  if (!q) {
    toastr.error('Vui lòng nhập từ khóa tìm kiếm');
    self.find('input[name="q"]').addClass('error');
    return;
  }
  location.href = '/search?q=' + q;
});

$(document).on('click', '.btn-add-to-cart', function (event) {
  event.preventDefault();
  var form = $(this).closest('form.cart');
  var qtyTemp = form.find('input[name="quantity"]').val();
  var variant_id = $(this).attr('data-id');
  var quantity = qtyTemp ? qtyTemp : 1;

  StoreAPI.addItem(variant_id, quantity, function () {
    StoreAPI.getCart(function (json) {
      if (!json.code) {
        var cart = json.cart;
        var items = cart.items;
        var htmlItem = '';
        var popupCart = $('.widget_shopping_cart_content');
        $('.cart_total').text(cart.total_items);
        $.each(items, function (index, el) {
          htmlItem += tmpl('item-cart', el);
        });
        var obj = {
          listItem: htmlItem,
          total: cart.total
        }
        var blockCart = tmpl('cart-header', obj)
        popupCart.html(blockCart);
      }
    });
    toastr.success("Thêm vào giỏ hàng thành công");
  });
});

$('.mini-cart').on('click', 'a.remove', function () {
  var self = $(this);
  var id = self.attr('data-id');
  StoreAPI.removeItem(id, function (json) {
    if (!json.code) {
      toastr.success('Xóa sản phẩm khỏi giỏ hàng thành công');
      self.closest('.mini_cart_item').remove();
      StoreAPI.getCart(function (json) {
        if (!json.code) {
          var cart = json.cart;
          var popupCart = $('.widget_shopping_cart_content');
          popupCart.find('.total').html('<strong>Subtotal: </strong>' + formatMoney(cart.total) + ' VNĐ');
          $('.cart_total').text(cart.total_items);
        }
      });
    }
  })
});

$('.shop-cart').on('click', 'a.remove', function () {
  var self = $(this);
  var id = self.attr('data-id');
  StoreAPI.removeItem(id, function (json) {
    if (!json.code) {
      toastr.success('Xóa sản phẩm khỏi giỏ hàng thành công');
      self.closest('.product_list_widget').remove();
      location.reload();
    }
  })
});

$(document).on('change', '.order-product', function () {
  var collection_id = $('#sl-collection').val();
  var collection_present = $('input[name="collection_id"]').val();
  if (!collection_id) {
    collection_id = collection_present;
  }
  query = $('input[name="query"]').val();

  var minPrice = $('#min_price').val();
  var maxPrice = $('#max_price').val();
  var url = '/api/search?view=collection_filter&type=product&filter=min_price=' + minPrice + '%26%26max_price=' + maxPrice;

  if (query) {
    url += encodeURIComponent('&&') + 'product.title**' + query;
  }
  if (collection_id) {
    url += '&collection_id=' + collection_id;
  }
  url += '&sortby=' + $(this).val();
  ajaxLoad(url);
});
$('.order-product option').each(function (i, val) {
  var orderBy = $('.order-product').attr('data-value');
  if (orderBy == val.value) {
    $(this).attr('selected', 'selected')
  }
})

$(document).on('submit', '.searchform-collection', function (event) {
  event.preventDefault();
  $('input').removeClass('error');
  query = $('input[name="query"]').val();
  if (!query) {
    toastr.error('Vui lòng nhập tìm kiếm');
    $('input[name="query"]').addClass('error');
    return
  }
  var collection_id = $('#sl-collection').val();
  var order_by = $('.order-product').val();
  url = '/api/search?view=collection_filter&type=product&filter=product.title**' + query + '&sortby=' + order_by;
  if (collection_id) {
    url += '&collection_id=' + collection_id;
  }
  ajaxLoad(url);
});

$(document).on('click', '.btn-submit-price', function (event) {
  event.preventDefault();
  var collectionID = $('input[name="collection_id"]').val();
  var minPrice = $('#min_price').val();
  var maxPrice = $('#max_price').val();
  url = '/api/search?view=collection_filter&type=product&filter=min_price=' + minPrice + '%26%26max_price=' + maxPrice;
  if (collectionID) {
    url += '&collection_id=' + collectionID;
  }
  ajaxLoad(url);
});

$('.btn-checkout').click(function (event) {
  event.preventDefault();
  $('input').removeClass('error');
  var data = {};
  var firstName = $('input[name="first_name"]').val();
  var lastName = $('input[name="last_name"]').val();
  if (!firstName) {
    toastr.error('Vui lòng nhập tên');
    $('input[name="first_name"]').addClass('error');
    return;
  }
  if (!lastName) {
    toastr.error('Vui lòng nhập họ');
    $('input[name="last_name"]').addClass('error');
    return;
  }
  data.name = firstName + ' ' + lastName;
  data.address = $('input[name="address"]').val();
  if (!data.address) {
    toastr.error('Vui lòng nhập địa chỉ giao hàng');
    $('input[name="address"]').addClass('error');
    return;
  }
  data.email = $('input[name="email"]').val();
  if (!data.email) {
    toastr.error('Vui lòng nhập email');
    $('input[name="email"]').addClass('error');
    return;
  }
  if (!isEmail(data.email)) {
    toastr.error('Vui lòng nhập đúng định dạng email');
    $('input[name="email"]').addClass('error');
    return;
  }
  data.phone = $('input[name="phone"]').val();
  if (!data.phone) {
    toastr.error('Vui lòng nhập số điện thoại');
    $('input[name="phone"]').addClass('error');
    return;
  }
  if (!isPhone(data.phone)) {
    toastr.error('Vui lòng nhập đúng định dạng số điện thoại');
    $('input[name="phone"]').addClass('error');
    return;
  }

  data.metafield = $('input[name="company_name"]').val();
  data.notes = $('textarea[name="order-notes"]').val();
  data.payment_method = $('input[name="payment_method"]:checked').val();
  data.shipping_price = $('input[name="shipping_price"]').val();

  var couponObj = JSON.parse(localStorage.getItem('coupon'));
  if (couponObj) {
    data.coupon = couponObj.coupon.code;
    data.coupon_discount = couponObj.discount;
  }

  StoreAPI.checkout(data, function (json) {
    if (!json.code) {
      localStorage.clear();
      toastr.success('Đặt hàng thành công!');
      location.href = '/order-success';
      return;
    } else {
      toastr.error(json.message);
    }
  })
});

$(document).on('click', '.btn-attribute', function (event) {
  event.preventDefault();
  var self = $(this);
  var name = self.attr('name');
  var value = self.attr('data-value');
  var metafield = name + '**' + value;
  var url = '/api/search?type=product&view=collection_filter&metafield=' + metafield;
  ajaxLoad(url);
});

var page_number, total;
page_number = $(document).find('#pagination').data('page_number');
total = $(document).find('#pagination').data('total');

if (total && parseInt(total) > 1) {
  $(document).find('#pagination').twbsPagination('destroy');
  $(document).find('#pagination').twbsPagination({
    totalPages: parseInt(total),
    visiblePages: 3,
    startPage: parseInt(page_number),
    initiateStartPageClick: false,
    first: false,
    prev: '<i class="fa fa-angle-left"></i>',
    next: '<i class="fa fa-angle-right"></i>',
    activeClass: 'custom-active',
    pageClass: 'custom-page',
    anchorClass: 'custom-page custom-anchor',
    prevClass: 'custom-page',
    nextClass: 'custom-page',
    last: false,
    href: false,

    onPageClick: function (event, page) {
      var href = location.href;
      var s = location.search;
      if (!s || !s.indexOf('?page=')) {
        if (page == 1)
          href = location.pathname;
        else
          href = location.pathname + '?page=' + page;
      } else {
        if (href.indexOf('&page=') > -1)
          href = href.substring(0, href.indexOf('&page='));
        if (page > 1)
          href = href + '&page=' + page;
      }
      location.href = href;
    }
  });
}

function paginateSearch() {
  var page_number, total;
  page_number = $(document).find('#pagination').data('page_number');
  total = $(document).find('#pagination').data('total');

  if (total && parseInt(total) > 1) {
    $(document).find('#pagination').twbsPagination('destroy');
    $(document).find('#pagination').twbsPagination({
      totalPages: parseInt(total),
      visiblePages: 3,
      startPage: parseInt(page_number),
      initiateStartPageClick: false,
      first: false,
      prev: '<i class="fa fa-angle-left"></i>',
      next: '<i class="fa fa-angle-right"></i>',
      activeClass: 'custom-active',
      pageClass: 'custom-page',
      anchorClass: 'custom-page custom-anchor',
      prevClass: 'custom-page',
      nextClass: 'custom-page',
      last: false,
      href: false,

      onPageClick: function (event, page) {
        var href = url;
        var s = location.search;
        if (!s || !s.indexOf('&page=')) {
          if (page == 1)
            href = href;
          else
            href = href + '&page=' + page;
        } else {
          if (href.indexOf('&page=') > -1)
            href = href.substring(0, href.indexOf('&page='));
          if (page > 1)
            href = href + '&page=' + page;
        }
        $.ajax({
          url: href,
          type: 'GET',
          success: function (html) {
            $('.product-list').html(html);
            var collection_id = $('#sl-collection').val();
            var order_by = $('.order-product').val();
            $('input[name="query"]').val(query);
            $('.order-product option').each(function (i, val) {
              var orderBy = $('.order-product').attr('data-value');
              if (orderBy == val.value) {
                $(this).attr('selected', 'selected')
              }
            })
            $(document).find('#sl-collection option').each(function (index, el) {
              if (collection_id == el.value) {
                $(this).attr('selected', 'selected')
              }
            });
            $('#pagination').attr('data-url', url);
            paginateSearch();
          }
        })
      }
    });
  }
}

function ajaxLoad(url) {
  $.ajax({
    url: url,
    type: 'GET',
    success: function (html) {
      $('.product-list').html(html);
      $('input[name="query"]').val(query);
      $('#sl-collection').each(function (i, val) {
        var collection_id = $('#sl-collection').val();
        if (collection_id == val.value) {
          $(this).attr('selected', 'selected')
        }
      })
      $('.order-product option').each(function (i, val) {
        var orderBy = $('.order-product').attr('data-value');
        if (orderBy == val.value) {
          $(this).attr('selected', 'selected')
        }
      })
      // $(document).find('select[name="sl-collection"] option').each(function(index, el) {
      //   if (collection_id == el.value) {
      //     $(this).attr('selected', 'selected')
      //   }
      // });
      $('#pagination').attr('data-url', url);
      paginateSearch();
      if ($('span.key_search').length) {
        $('span.key_search').text(query);
      }
    }
  })
}

function formatMoney(num) {
  if (num) num = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  return num;
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9])+$/;
  return regex.test(email);
}

function isPhone(phone) {
  var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  return regex.test(phone);
}