function mobile_menu() {
    var e = $(window).width(),
        t = $(".navigation-left").find(".main-navigation ul").html(),
        o = $(".navigation-right").find(".main-navigation ul").html();
    1024 >= e ? ($(".header-not-mobile-default").addClass("header-mobile"), $(".navigation").addClass("mobile-nav"), $(".navigation-left").length > 0 && $(".navigation-right").length > 0 && $(".navigation").find(".main-navigation ul").html(t + o)) : ($(".header-not-mobile-default").removeClass("header-mobile"), $(".navigation").removeClass("mobile-nav"), $(".navigation-left").length > 0 && $(".navigation-right").length > 0 && $(".navigation.navigation-right").find(".main-navigation ul").html(o))
}

function fullScreenHeight() {
    var e = $(window).height();
    $(".section-fullscreen").css({
        height: e
    })
}

function Countdown() {
    $(".pl-clock").length > 0 && $(".pl-clock").each(function() {
        var e = $(this).attr("data-time");
        $(this).countdown(e, function(e) {
            $(this).html(e.strftime('<div class="countdown-item"><div class="countdown-item-value">%D</div><div class="countdown-item-label">Days</div></div><div class="countdown-item"><div class="countdown-item-value">%H</div><div class="countdown-item-label">Hours</div></div><div class="countdown-item"><div class="countdown-item-value">%M</div><div class="countdown-item-label">Minutes</div></div><div class="countdown-item"><div class="countdown-item-value">%S</div><div class="countdown-item-label">Seconds</div></div>'))
        })
    })
}

function menu_touched_side() {
    var e = $(".main-navigation");
    e.children("ul").children("li").each(function() {
        var e = $(this).children("ul");
        e.length > 0 && (e.offset().left + e.width() > $(window).width() ? e.addClass("back") : e.offset().left < 0 && e.addClass("back"))
    })
}

function owlCarousel() {
    $(".project-carousel").length > 0 && ($(".project-carousel").each(function() {
        $(this).owlCarousel({
            items: 2,
            loop: !0,
            mouseDrag: !1,
            navigation: !0,
            dots: !1,
            pagination: !1,
            autoplay: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            itemsDesktop: [1199, 2],
            itemsDesktopSmall: [979, 2],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],
            addClassActive: !0
        })
    }), changeFirstLast(), $(".project-carousel .owl-prev, .project-carousel .owl-next").on("click", function(e) {
        changeFirstLast()
    })),$(".home-slide").length > 0 && ($(".home-slide").each(function() {
        $(this).owlCarousel({
            items: 1,
            loop: !0,
            mouseDrag: !0,
            navigation: !0,
            dots: !1,
            pagination: !0,
            autoplay: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            // navigationText: ["<img src='../images/slider/back.png'>","<img src='../images/slider/next.png'>"],
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],
            addClassActive: !0
        })
    }), changeFirstLast(), $(".home-slide .owl-prev, .home-slide .owl-next").on("click", function(e) {
        changeFirstLast()
    })), $(".client-carousel").length > 0 && $(".client-carousel").each(function() {
        var e = "true" === $(this).attr("data-auto-play") ? !0 : !1;
        $(this).owlCarousel({
            items: $(this).attr("data-desktop"),
            loop: !0,
            mouseDrag: !0,
            navigation: !1,
            dots: !1,
            pagination: !1,
            autoPlay: e,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            itemsDesktop: [1199, $(this).attr("data-desktop")],
            itemsDesktopSmall: [979, $(this).attr("data-laptop")],
            itemsTablet: [768, $(this).attr("data-tablet")],
            itemsMobile: [479, $(this).attr("data-mobile")]
        })
    }), $(".team-carousel").length > 0 && $(".team-carousel").each(function() {
        var e = "true" === $(this).attr("data-auto-play") ? !0 : !1;
        $(this).owlCarousel({
            items: $(this).attr("data-desktop"),
            loop: !0,
            mouseDrag: !0,
            navigation: !1,
            dots: !1,
            pagination: !1,
            autoPlay: e,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            itemsDesktop: [1199, $(this).attr("data-desktop")],
            itemsDesktopSmall: [979, $(this).attr("data-laptop")],
            itemsTablet: [768, $(this).attr("data-tablet")],
            itemsMobile: [479, $(this).attr("data-mobile")]
        })
    }), $(".testimonial-carousel").length > 0 && $(".testimonial-carousel").each(function() {
        $(this).owlCarousel({
            items: 1,
            loop: !0,
            mouseDrag: !0,
            navigation: !0,
            dots: !1,
            pagination: !1,
            autoplay: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            navigationText: ['<i class="pe-7s-left-arrow"></i>', '<i class="pe-7s-right-arrow"></i>'],
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1]
        })
    }), $(".portfolio-carousel").length > 0 && $(".portfolio-carousel").each(function() {
        var e = "true" === $(this).attr("data-auto-play") ? !0 : !1;
        $(this).owlCarousel({
            items: $(this).attr("data-desktop"),
            loop: !0,
            mouseDrag: !0,
            navigation: !0,
            dots: !1,
            pagination: !1,
            autoPlay: e,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            itemsDesktop: [1199, $(this).attr("data-desktop")],
            itemsDesktopSmall: [979, $(this).attr("data-laptop")],
            itemsTablet: [768, $(this).attr("data-tablet")],
            itemsMobile: [479, $(this).attr("data-mobile")]
        })
    }), $(".banner-carousel").length > 0 && $(".banner-carousel").each(function() {
        $(this).owlCarousel({
            items: 1,
            loop: !0,
            mouseDrag: !0,
            navigation: !1,
            dots: !1,
            pagination: !1,
            autoplay: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],
            transitionStyle: "fadeUp"
        }), $(".next").on("click", function() {
            $(".banner-carousel").trigger("owl.next")
        }), $(".prev").on("click", function() {
            $(".banner-carousel").trigger("owl.prev")
        })
    }), $(".process-carousel").length > 0 && $(".process-carousel").each(function() {
        var e = "true" === $(this).attr("data-auto-play") ? !0 : !1;
        $(this).owlCarousel({
            items: $(this).attr("data-desktop"),
            loop: !0,
            mouseDrag: !0,
            navigation: !1,
            dots: !0,
            pagination: !0,
            autoPlay: e,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            itemsDesktop: [1199, $(this).attr("data-desktop")],
            itemsDesktopSmall: [979, $(this).attr("data-laptop")],
            itemsTablet: [768, $(this).attr("data-tablet")],
            itemsMobile: [479, $(this).attr("data-mobile")]
        })
    }), $(".relate-blog-carousel").length > 0 && $(".relate-blog-carousel").each(function() {
        $(this).owlCarousel({
            items: 3,
            loop: !0,
            mouseDrag: !0,
            navigation: !1,
            dots: !0,
            pagination: !1,
            autoplay: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 2],
            itemsMobile: [479, 1]
        })
    }), $(".blog-carousel").length > 0 && $(".blog-carousel").each(function() {
        $(this).owlCarousel({
            items: 3,
            loop: !0,
            mouseDrag: !0,
            navigation: !1,
            dots: !0,
            pagination: !0,
            autoPlay: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            smartSpeed: 1e3,
            autoplayHoverPause: !0,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 2],
            itemsMobile: [479, 1]
        })
    })
}

function autoPlayYouTubeModal() {
    var e = $("body").find('[data-toggle="modal"]');
    e.click(function() {
        var e = $(this).data("target"),
            t = $(this).attr("data-theVideo"),
            o = t + "?autoplay=1";
        $(e + " iframe").attr("src", o), $(e + " button.close").click(function() {
            $(e + " iframe").attr("src", t)
        })
    }), $("body").on("hidden.bs.modal", function() {
        $("body iframe").removeAttr("src")
    })
}

function GoogleMap() {
    function e() {
        var e = {
                zoom: 11,
                scrollwheel: !1,
                center: new google.maps.LatLng(40.6, -73.94),
                styles: [{
                    featureType: "all",
                    elementType: "labels.text.fill",
                    stylers: [{
                        saturation: 36
                    }, {
                        color: "#000000"
                    }, {
                        lightness: 40
                    }]
                }, {
                    featureType: "all",
                    elementType: "labels.text.stroke",
                    stylers: [{
                        visibility: "on"
                    }, {
                        color: "#000000"
                    }, {
                        lightness: 16
                    }]
                }, {
                    featureType: "all",
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "administrative",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 20
                    }]
                }, {
                    featureType: "administrative",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 17
                    }, {
                        weight: 1.2
                    }]
                }, {
                    featureType: "landscape",
                    elementType: "geometry",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 20
                    }]
                }, {
                    featureType: "poi",
                    elementType: "geometry",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 21
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 17
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 29
                    }, {
                        weight: .2
                    }]
                }, {
                    featureType: "road.arterial",
                    elementType: "geometry",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 18
                    }]
                }, {
                    featureType: "road.local",
                    elementType: "geometry",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 16
                    }]
                }, {
                    featureType: "transit",
                    elementType: "geometry",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 19
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{
                        color: "#000000"
                    }, {
                        lightness: 17
                    }]
                }]
            },
            o = document.getElementById("map"),
            a = new google.maps.Map(o, e);
        new google.maps.Marker({
            position: new google.maps.LatLng(40.6, -73.94),
            map: a,
            title: "Location 1",
            icon: t
        })
    }
    var t = "../images/map-marker.png";
    $("#map").length > 0 && (void 0 != $("#map").attr("data-marker-image") && (t = $("#map").attr("data-marker-image")), google.maps.event.addDomListener(window, "load", e))
}

function RevolutionInit() {
    $("#rev_slider").attr("data-overlay");
    $("#rev_slider").show().revolution({
        sliderType: "standard",
        sliderLayout: "fullscreen",
        dottedOverlay: "twoxtwo",
        delay: 9e3,
        navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            touch: {
                touchenabled: "on",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: !1
            },
            arrows: {
                style: "nito-agency-03",
                enable: !0,
                hide_onmobile: !1,
                hide_onleave: !0,
                hide_delay: 200,
                hide_delay_mobile: 1200,
                tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-iwrapper">				<div class="tp-arr-titleholder"></div>		<div class="tp-arr-subtitleholder"></div>	</div></div>',
                left: {
                    h_align: "left",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                },
                right: {
                    h_align: "right",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                }
            }
        },
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: 1240,
        gridheight: 868,
        lazyType: "none",
        parallax: {
            type: "mouse",
            origo: "enterpoint",
            speed: 400,
            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
            type: "mouse"
        },
        shadow: 0,
        spinner: "spinner4",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        fullScreenAutoWidth: "off",
        fullScreenAlignForce: "off",
        fullScreenOffsetContainer: "",
        fullScreenOffset: "",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: !1,
        fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: !1
        }
    })
}

function RevolutionInitVideo() {
    $("#rev_slider_3").attr("data-overlay");
    $("#rev_slider_3").show().revolution({
        sliderType: "standard",
        sliderLayout: "fullwidth",
        dottedOverlay: "twoxtwo",
        delay: 9e3,
        navigation: {
            onHoverStop: "off"
        },
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: 1920,
        gridheight: 1080,
        lazyType: "none",
        shadow: 0,
        spinner: "spinner0",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: !1,
        fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: !1
        }
    })
}

function RevolutionInitWider() {
    $("#rev_slider_2").attr("data-overlay");
    $("#rev_slider_2").show().revolution({
        sliderType: "standard",
        sliderLayout: "auto",
        dottedOverlay: "none",
        delay: 9e3,
        navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            arrows: {
                style: "nito-agency-03-3013",
                enable: !0,
                hide_onmobile: !1,
                hide_onleave: !0,
                hide_delay: 200,
                hide_delay_mobile: 1200,
                tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-iwrapper">				<div class="tp-arr-titleholder"></div>		<div class="tp-arr-subtitleholder"></div>	</div></div>',
                left: {
                    h_align: "left",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                },
                right: {
                    h_align: "right",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                }
            }
        },
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: 1920,
        gridheight: 996,
        lazyType: "none",
        shadow: 0,
        spinner: "spinner0",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: !1,
        fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: !1
        }
    })
}

function RevolutionInitMini() {
    $("#rev_slider_4").attr("data-overlay");
    $("#rev_slider_4").show().revolution({
        sliderType: "standard",
        sliderLayout: "auto",
        dottedOverlay: "twoxtwo",
        delay: 9e3,
        navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            arrows: {
                style: "nito-agency-03-3846",
                enable: !0,
                hide_onmobile: !1,
                hide_onleave: !0,
                hide_delay: 200,
                hide_delay_mobile: 1200,
                tmp: '<div class="tp-arr-allwrapper"><div class="tp-arr-iwrapper"><div class="tp-arr-titleholder"></div>		<div class="tp-arr-subtitleholder"></div></div></div>',
                left: {
                    h_align: "left",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                },
                right: {
                    h_align: "right",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                }
            }
        },
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: 1920,
        gridheight: 998,
        lazyType: "none",
        shadow: 0,
        spinner: "spinner0",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: !1,
        fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: !1
        }
    })
}

function RevolutionInitSidebar() {
    $("#rev_slider_5").attr("data-overlay");
    $("#rev_slider_5").show().revolution({
        sliderType: "standard",
        sliderLayout: "auto",
        dottedOverlay: "none",
        delay: 9e3,
        navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            arrows: {
                style: "nito-agency-03-0244",
                enable: !0,
                hide_onmobile: !1,
                hide_onleave: !0,
                hide_delay: 200,
                hide_delay_mobile: 1200,
                tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-iwrapper">				<div class="tp-arr-titleholder"></div>		<div class="tp-arr-subtitleholder"></div>	</div></div>',
                left: {
                    h_align: "left",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                },
                right: {
                    h_align: "right",
                    v_align: "center",
                    h_offset: 20,
                    v_offset: 0
                }
            }
        },
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: 1543,
        gridheight: 1080,
        lazyType: "none",
        shadow: 0,
        spinner: "spinner0",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: !1,
        fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: !1
        }
    })
}

function RevolutionInitFashion() {
    $("#rev_slider_6").attr("data-overlay");
    $("#rev_slider_6").show().revolution({
        sliderType: "standard",
        sliderLayout: "auto",
        dottedOverlay: "twoxtwo",
        delay: 9e3,
        navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            arrows: {
                style: "nito-02",
                enable: !0,
                hide_onmobile: !1,
                hide_onleave: !1,
                tmp: "",
                left: {
                    h_align: "left",
                    v_align: "bottom",
                    h_offset: 105,
                    v_offset: 80
                },
                right: {
                    h_align: "left",
                    v_align: "bottom",
                    h_offset: 170,
                    v_offset: 80
                }
            }
        },
        responsiveLevels: [1240, 1024, 778, 480],
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: [1920, 1024, 778, 480],
        gridheight: [1080, 768, 960, 720],
        lazyType: "none",
        minHeight: "100vh",
        shadow: 0,
        spinner: "spinner0",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: !1,
        fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: !1
        }
    })
}

function changeFirstLast() {
    var e = 0;
    $(".project-carousel .owl-item").each(function() {
        var t = $(this);
        t.removeClass("first").removeClass("init"), t.hasClass("active") && 0 == e && (t.addClass("first"), e += 1)
    })
}! function(e, t, o) {
    "use strict";
    o(e).on("load", function() {
        menu_touched_side(), autoPlayYouTubeModal(), o(".popup-video-wrap .modal").appendTo("body"), o(".noo-spinner").fadeOut(500, function() {
            o(".noo-spinner").remove()
        })
    }), o(e).on("resize", function(e, t) {
        menu_touched_side()
    }), o(t).ready(function(o) {
        var a;
        /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? (a = !0, o("html").addClass("mobile")) : (a = !1, o("html").addClass("no-mobile")), mobile_menu(), o("a.prettyphoto").length > 0 && (o("a[data-rel^='prettyPhoto']").prettyPhoto(), o("a.prettyphoto").prettyPhoto(), o("a[data-rel^='prettyPhoto']").prettyPhoto({
            hook: "data-rel",
            social_tools: !1,
            theme: "pp_default",
            horizontal_padding: 20,
            opacity: .8,
            deeplinking: !1
        })), fullScreenHeight(), GoogleMap(), o("#rev_slider").length > 0 && RevolutionInit(), o("#rev_slider_2").length > 0 && RevolutionInitWider(), o("#rev_slider_3").length > 0 && RevolutionInitVideo(), o("#rev_slider_4").length > 0 && RevolutionInitMini(), o("#rev_slider_5").length > 0 && RevolutionInitSidebar(), o("#rev_slider_6").length > 0 && RevolutionInitFashion(), Countdown(), o("#anchor").length > 0 && o("#anchor").singlePageNav({
            offset: 0,
            filter: ".onepage"
        }), o(".restaurant-filter").length > 0 && o(".restaurant-filter").singlePageNav({
            offset: 0,
            filter: ".onepage"
        }), o(".onepage-menu").length > 0 && o(".onepage-menu").singlePageNav({
            offset: 0,
            filter: ".onepage"
        }), o("body").on("click", function(e) {
            var t = o(e.target);
            0 != t.parents(".mini-tools").length || t.hasClass("mini-search") || o(".mini-search,.mini-cart,.tools").removeClass("active").hide()
        }), o(".mini-search,.mini-cart,.mini-tools").on("click", function(e) {
            e.stopPropagation()
        }), o(".header-popup [data-display]").on("click", function(t) {
            var a = o(this).parents(".header-popup");
            t.stopPropagation();
            var i = o(this),
                l = i.attr("data-display"),
                s = i.attr("data-no-display");
            if (o(l, a).css({
                    right: 0
                }), o(l, a).hasClass("active")) o(l, a).removeClass("active").hide();
            else {
                o(l, a).addClass("active").show().css("display", "block"), o(s, a).removeClass("active").hide();
                var t = o(l, a).offset().left + o(l).outerWidth() - o(e).width();
                t > 0 ? o(l, a).css({
                    left: 0 - t
                }) : o(l, a).css({
                    right: 0
                })
            }
        }), o(".header-popup .header-icon").on("click", function() {
            o(".popup .searchform .s").focus()
        }), o(t).on("show.bs.collapse hide.bs.collapse", ".accordion", function(e) {
            var t = o(e.target);
            "show" == e.type && t.prev(".accordion-heading").addClass("active"), "hide" == e.type && t.prev(".accordion-heading").removeClass("active")
        }), owlCarousel(), o(e).scroll(function() {
            o(this).scrollTop() > 500 ? o("#backtotop").addClass("on") : o("#backtotop").removeClass("on"), o(this).scrollTop() > 150 ? o(".header-scroll").addClass("header-sticky") : o(".header-scroll").removeClass("header-sticky")
        }), o("body").on("click", "#backtotop", function() {
            return o("html, body").animate({
                scrollTop: 0
            }, 800), !1
        }), o(".group-progressbar").each(function() {
            var e = o(this);
            e.waypoint({
                handler: function(t) {
                    e.find(".progressbar").progressbar({
                        display_text: "center"
                    })
                },
                offset: "80%"
            })
        }), o(".counter-wraper").length > 0 && o(".counter-wraper").each(function(e) {
            var t = o(this);
            t.waypoint({
                handler: function(e) {
                    t.find(".counter-digit:not(.counted)").countTo().addClass("counted")
                },
                offset: "90%"
            })
        }), o("#multiScroll").length > 0 && o(e).width() > 768 && (o("#multiScroll").multiscroll({
            sectionsColor: [],
            menu: !1,
            navigation: !0,
            loopBottom: !0,
            loopTop: !0
        }), o("#multiscroll-nav > ul > li ").each(function(e) {
            o(this).children("a").attr("href", "javascript:void()")
        })), o(".banner-carousel .slide-item").each(function() {
            o(this).css("background-image", 'url("' + o(this).attr("data-src") + '")')
        }), o(".portfolio-grid .masonry-item").each(function() {
            void 0 !== o(this).attr("data-src") && o(this).css("background-image", 'url("' + o(this).attr("data-src") + '")')
        }), o(".menu-mobile, .mobile-close").on("click", function() {
            o(".header-mobile .navigation").toggleClass("open")
        }), o(".header-mobile .menu-toggle").on("click", function() {
            o(this).siblings("ul").toggleClass("submenu-open"), o(this).toggleClass("open")
        }), o(".equalheight").length > 0 && o(".equalheight").equalHeights(), o(e).width() >= 1024 && 0 == a && o(".parallax").length > 0 && o(".parallax").each(function(e) {
            o(this).parallax("50%", o(this).attr("data-ratio"))
        })
    })
}(window, document, jQuery);


// ---------------------------------------//
/* Laziweb JS */
// ---------------------------------------//
jQuery(document).ready(function($) {
    $(".slidebar-menu li").click(function(event) {
        $(this).children('.children').slideToggle();
        $(this).toggleClass('show-children');;
    });
});

$(document).ready(function () {
    $(".entry-content table , .text-box-editor table").wrap("<div class='table-responsive'></div>");
});