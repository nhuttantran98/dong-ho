-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 08, 2018 at 08:58 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `realtrading`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1000',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `author` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `view` int(11) NOT NULL DEFAULT '0',
  `publish_date` datetime DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `priority`, `description`, `content`, `image`, `tags`, `author`, `status`, `view`, `publish_date`, `template`, `created_at`, `updated_at`) VALUES
(1, 'Chính sách Đại lý', 1000, 'Real Trading hoan nghênh Quý khách hàng, đối tác trở thành Đại lý phân phối các sản phẩm của chúng tôi. ', '<p>C&aacute;m ơn qu&yacute; kh&aacute;ch đ&atilde; quan t&acirc;m đến c&aacute;c sản phẩm của C&ocirc;ng ty Real Trading.</p>\n<p>Qu&yacute; kh&aacute;ch vui l&ograve;ng li&ecirc;n hệ ch&uacute;ng t&ocirc;i để biết th&ecirc;m th&ocirc;ng tin chi tiết.</p>\n<p>&nbsp;</p>\n<p>&nbsp;Li&ecirc;n hệ: Mrs. Ruby ( 0903 688 183)</p>', '1533887438_blog_1.jpg', '#abcd#custom#', '', 'active', 29, '2018-08-07 00:00:00', '', '2018-08-10 07:50:42', '2018-08-26 19:42:07'),
(2, 'Spotlight on Geordie Willis', 1000, 'Ladyship it daughter securing procured or am moreover mr. Put sir she exercise vicinity cheerful wondered. Continual say suspicion provision you neglected sir curiosity unwilling. Simplicity end themselves increasing led day sympathize yet. General windows effects not are drawing man garrets. Common indeed garden you his ladies out yet. Preference imprudence contrasted to remarkably in on. Taken now you him trees tears any. Her object giving end sister except oppose. ', '', '1533887438_blog_1_(1)-min_1541558316.jpg', '', '', 'active', 37, '2018-08-07 00:00:00', 'ht-khach-hang', '2018-08-10 07:51:00', '2018-11-07 02:41:32'),
(3, 'Chính sách bảo hành', 1000, 'Chính sách Bảo hành sản phẩm nội thất', '<p>C&ocirc;ng ty Real Trading lu&ocirc;n mong muốn mang đến cho qu&yacute; kh&aacute;ch h&agrave;ng sự h&agrave;i l&ograve;ng trước v&agrave; sau khi mua h&agrave;ng bằng những ch&iacute;nh s&aacute;ch chăm s&oacute;c kh&aacute;ch h&agrave;ng chu đ&aacute;o, tậm t&acirc;m v&agrave; kịp thời. Do đ&oacute;, trong thời gian bảo h&agrave;nh sản phầm/linh kiện hoặc bất k&igrave; hư hỏng/ trục trặc trong điều kiện sử dụng b&igrave;nh thường hoặc do lỗi kỹ thuật/ lỗi thuộc về sản xuất sẽ được Real Trading sửa chữa hoặc thay thế mới với sản phẩm/linh kiện c&ugrave;ng loại hoặc tương tự.</p>\n<ol>\n<li><strong> &nbsp;Thời gian bảo h&agrave;nh</strong></li>\n</ol>\n<p>Tất cả c&aacute;c sản phẩm do C&ocirc;ng ty Real Trading sản xuất hoặc nhập khẩu ph&acirc;n phối đều được &aacute;p dụng c&aacute;c ch&iacute;nh s&aacute;ch bảo h&agrave;nh theo đ&uacute;ng ch&iacute;nh s&aacute;ch bảo h&agrave;nh của c&ocirc;ng ty. Thời hạn bảo h&agrave;nh c&aacute;c sản phẩm t&ugrave;y thuộc v&agrave;o thương hiệu sản phẩm như sau:</p>\n<p>&nbsp;</p>\n<table style=\"height: 215px; width: 510px;\">\n<tbody>\n<tr>\n<td style=\"width: 225px;\">\n<p><strong>Thương hiệu sản phẩm</strong></p>\n</td>\n<td style=\"width: 273px;\">\n<p><strong>Thời gian bảo h&agrave;nh</strong></p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 225px;\">\n<p>Thương hiệu Locker &amp; Lock</p>\n</td>\n<td style=\"width: 273px;\">\n<p>Bảo h&agrave;nh 1 năm</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 225px;\">\n<p>Thương hiệu Real Locks</p>\n</td>\n<td style=\"width: 273px;\">\n<p>Bảo h&agrave;nh 1 năm</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 225px;\">\n<p>Thương hiệu &nbsp;Toyo Taper</p>\n</td>\n<td style=\"width: 273px;\">\n<p>Bảo h&agrave;nh 2 năm</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>&nbsp;</p>\n<ol start=\"2\">\n<li><strong>Trường hợp n&agrave;o kh&ocirc;ng được bảo h&agrave;nh?</strong></li>\n</ol>\n<p>- &nbsp;Sản phẩm/ linh kiện kh&ocirc;ng phải do Real Trading sản xuất, nhập khẩu v&agrave; ph&acirc;n phối.</p>\n<p>- &nbsp;Sản phẩm c&oacute; sự hao m&ograve;n tự nhi&ecirc;n do sử dụng l&acirc;u ng&agrave;y hoặc do thường xuy&ecirc;n tiếp x&uacute;c trực tiếp dưới &aacute;nh nắng mặt trời.</p>\n<p>- &nbsp;Bất cứ việc tự &yacute; thay đổi, sửa chữa n&agrave;o từ ph&iacute;a kh&aacute;ch h&agrave;ng m&agrave; kh&ocirc;ng do Real Trading hướng dẫn cũng kh&ocirc;ng được bảo h&agrave;nh.</p>\n<p>- &nbsp;Sản phẩm/ linh kiện bị hư/ trục trặc do kh&aacute;ch h&agrave;ng lắp đặt, sử dụng, bảo quản v&agrave; vệ sinh sản phẩm kh&ocirc;ng tu&acirc;n theo c&aacute;c hướng dẫn của nh&agrave; sản xuất.</p>\n<p>- &nbsp;Sản phẩm bị trầy xước, r&aacute;ch, vỡ, mẻ, rỉ s&eacute;t. Ngoại trừ trường hợp vỡ b&aacute;nh xe, vỡ ch&acirc;n ghế sẽ được bảo h&agrave;nh.</p>\n<p>- &nbsp;Sản phẩm bị hư hỏng do bất cẩn, va đập, vận chuyển kh&ocirc;ng cẩn thận, tai nạn, hỏa hoạn, thi&ecirc;n tai.</p>', '1533887438_blog_1.jpg', '', '', 'active', 16, '2018-08-07 00:00:00', '', '2018-08-10 07:51:26', '2018-08-26 19:37:39'),
(4, 'Spotlight on Geordie Willis', 1000, 'Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd [...]', '', '1533887438_blog_1.jpg', '', '', 'active', 8, '2018-08-07 00:00:00', '', '2018-08-10 07:51:38', '2018-08-10 07:51:41'),
(5, 'Brunch on Saturday', 1000, 'Ladyship it daughter securing procured or am moreover mr. Put sir she exercise vicinity cheerful wondered. Continual say suspicion provision you neglected sir curiosity unwilling. Simplicity end themselves increasing led day sympathize yet. General windows effects not are drawing man garrets. Common indeed garden you his ladies out yet. Preference imprudence contrasted to remarkably in on. Taken now you him trees tears any. Her object giving end sister except oppose. ', '', '1533887438_blog_1-min_1541558316.jpg', '', '', 'active', 12, '2018-08-07 00:00:00', '', '2018-08-10 07:51:46', '2018-11-07 02:41:43'),
(6, 'Brunch on Saturday', 1000, 'For a little sophistication on a Saturday morning, head to the design centre of the capital for me contained explained my education', '', '1533887543_blog_3.jpg', '', '', 'active', 7, '2018-08-07 00:00:00', '', '2018-08-10 07:52:38', '2018-09-11 04:36:04'),
(7, 'Spotlight on Geordie Willis', 1000, 'Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. & Rudd in St. James’s Street, Pall Mall. Founded in 1698, it is the UK’s oldest family-run wine merchant and supplier of wines', '', '1533887438_blog_1.jpg', '#custom#anh#yêu#hay#', '', 'active', 25, '2018-08-07 00:00:00', 'ht-khach-hang', '2018-08-10 08:40:39', '2018-11-07 07:40:37'),
(8, 'Hướng dẫn mua hàng, giao hàng và lắp đặt', 1000, 'Chúng tôi sẽ sớm cập nhật các hướng dẫn mua hàng chi tiết cho Quý khách', '<p>Ch&uacute;ng t&ocirc;i sẽ sớm cập nhật c&aacute;c hướng dẫn mua h&agrave;ng chi tiết cho Qu&yacute; kh&aacute;ch</p>', '1533890475_blog_2.jpg', '#custom#', '', 'active', 25, '2018-08-07 00:00:00', '', '2018-08-10 08:40:54', '2018-08-26 19:48:10'),
(9, 'Supersonic import', 1000, 'No more time wasting in data import. We rocket its speed by improving the progress which strikingly reduces your waiting time.', '', '1533978972_project_4.jpg', '#test#anh#abcd#custom#', '', 'active', 17, '2018-08-07 00:00:00', 'what-we-do', '2018-08-11 07:58:29', '2018-09-04 04:21:35'),
(10, 'Effortless customization', 1000, 'Never run into trouble when customizing your website. We created Nito with a mindset of ease your process.', '', '1533979002_project_5.jpg', '#custom#', '', 'active', 20, '2018-08-07 00:00:00', 'what-we-do', '2018-08-11 07:58:55', '2018-11-07 07:41:09'),
(11, 'Fast loading speed', 1000, 'The bad news is time flies. The good news is you\'re the pilot. Be a wise pilot to choose Nito\'s fast loading speed and be outstanding.', '<p><img src=\"http://realtrading.demo3.laziweb.com/uploads/1535311710_airport.jpg\" alt=\"\" width=\"5472\" height=\"3648\" />The bad news is time flies. The good news is you\'re the pilot. Be a wise pilot to choose Nito\'s fast loading speed and be outstanding.</p>', '1533979030_project_6.jpg', '', '', 'active', 9, '2018-08-07 00:00:00', 'what-we-do', '2018-08-11 08:02:05', '2018-11-07 08:19:19'),
(12, 'Easy page builder', 1000, 'You don\'t need to any code to build your page. Visual Composer will do it for you. Just drag & crop elements & enjoy the process.', '<p>You don\'t need to any code to build your page. Visual Composer will do it for you. Just drag &amp; crop elements &amp; enjoy the process.</p>', '1533981138_blog_detail.jpg', '', '', 'active', 22, '2018-08-07 00:00:00', 'what-we-do', '2018-08-11 08:02:57', '2018-11-07 08:09:52'),
(13, 'Fully Responsive', 1000, 'We cannot deny the importance of mobile in interacting with customer. Responding to that requirement, Nito is fully responsive.', '<p>We cannot deny the importance of mobile in interacting with customer. Responding to that requirement, Nito is fully responsive.</p>', '1533961058_bg_nito_2.jpg', '', '', 'active', 10, '2018-08-07 00:00:00', '', '2018-08-11 08:04:39', '2018-11-07 08:17:34'),
(14, 'Powerful Shortcodes', 1000, 'There may be some complicated establishing tasks. But no worry as all of those have been solved by Nito\'s effective shortcodes system.', '<p>There may be some complicated establishing tasks. But no worry as all of those have been solved by Nito\'s effective shortcodes system.</p>', '1533869703_introducing_1.png', '', '', 'active', 20, '2018-08-07 00:00:00', '', '2018-08-11 08:05:23', '2018-11-07 08:05:32'),
(15, 'A perfect day in New York', 1000, 'Creative strategy', '', '1533978841_project_1.jpg', '', '', 'active', 1, '2018-08-07 00:00:00', '', '2018-08-11 09:14:05', '2018-08-11 09:17:34'),
(16, 'Giving back to your fans', 1000, 'Art direction', '', '1533978874_project_2.jpg', '', '', 'active', 7, '2018-08-07 00:00:00', '', '2018-08-11 09:14:22', '2018-08-11 09:17:41'),
(17, 'Reintroducing a key tech media publisher', 1000, 'Custom application', '', '1533978932_project_3.jpg', '', '', 'active', 7, '2018-08-07 00:00:00', '', '2018-08-11 09:15:00', '2018-08-11 09:17:49'),
(18, 'Unifying a brand identity and experience', 1000, 'Art direction', '', '1533978972_project_4.jpg', '#custom#', '', 'active', 17, '2018-08-07 00:00:00', '', '2018-08-11 09:15:52', '2018-08-16 03:02:35'),
(19, 'Explore nature', 1000, 'Art direction', '', '1533979002_project_5.jpg', '', '', 'active', 12, '2018-08-07 00:00:00', '', '2018-08-11 09:16:31', '2018-08-11 09:16:55'),
(20, 'The end of social media?', 1000, 'Creative strategy', '', '1533979030_project_6.jpg', '', '', 'active', 4, '2018-08-07 00:00:00', '', '2018-08-11 09:17:03', '2018-08-11 09:17:22'),
(21, 'test', 1000, 'test', '<p>test</p>', '1533979030_project_6.jpg', '#test#abcd#custom#', '', 'delete', 17, '2018-08-07 00:00:00', '', '2018-08-16 02:21:14', '2018-08-18 08:32:44'),
(22, 'test', 1000, 'test', '<p>test</p>', '', '', '', 'delete', 6, '2018-08-07 00:00:00', 'what-we-do', '2018-08-16 02:35:44', '2018-08-16 02:42:51'),
(23, 'what we do', 1000, 'test', '<p>test</p>', '', '', '', 'delete', 2, '2018-08-07 00:00:00', 'what-we-do', '2018-08-16 02:54:17', '2018-08-16 02:54:45'),
(24, 'test', 1000, '1111', '<p>111111</p>', '', '', '', 'delete', 1, '2018-08-07 00:00:00', 'what-we-do', '2018-08-16 02:55:10', '2018-08-16 03:01:13'),
(25, 'Brunch on Saturday', 1000, 'For a little sophistication on a Saturday morning, head to the design centre of the capital for pleasantly fussy dishes and an eclectic mix of cocktails at Clerkenwell’s The Modern Pantry. Run by Anna Hansen', '<p>Nội dung đang cập nhật</p>', '1533887543_blog_3.jpg', '#custom#real#', '', 'active', 8, '2018-08-07 00:00:00', 'ht-khach-hang', '2018-08-16 12:08:24', '2018-11-07 08:53:19'),
(26, 'Câu hỏi thường gặp', 1000, 'Những câu hỏi thường gặp sẽ được chúng tôi giải đáp tại đây. Nếu có câu hỏi Quý khách vui lòng liên hệ để được giải đáp trong thời gian sớm nhất.', '<p>Những c&acirc;u hỏi thường gặp sẽ được ch&uacute;ng t&ocirc;i giải đ&aacute;p tại đ&acirc;y. Nếu c&oacute; c&acirc;u hỏi Qu&yacute; kh&aacute;ch vui l&ograve;ng li&ecirc;n hệ để được giải đ&aacute;p trong thời gian sớm nhất.</p>', '1533978841_project_1.jpg', '#Q&A#', '', 'active', 26, '2018-08-07 00:00:00', '', '2018-08-22 03:52:30', '2018-08-26 19:45:27'),
(27, 'test bai viet 2', 1000, '', '', '', '', '', 'delete', 4, '2018-08-07 00:00:00', '', '2018-08-22 04:54:45', '2018-08-22 08:17:30'),
(28, 'Spotlight on Geordie Willis', 1000, 'Giải pháp mới', '<table style=\"width: 772px; height: 196px;\" border=\"1\" cellspacing=\"1\" cellpadding=\"1\">\n<tbody>\n<tr>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n</tr>\n<tr>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n</tr>\n<tr>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n</tr>\n<tr>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n</tr>\n<tr>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n</tr>\n<tr>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n</tr>\n<tr>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 103px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n<td style=\"width: 104px;\">Perpetual</td>\n</tr>\n</tbody>\n</table>\n<p>&nbsp;<img src=\"http://realtrading.demo3.laziweb.com/uploads/1536070433_quality_from_pretigious_brand.png\" alt=\"\" width=\"1903\" height=\"943\" /></p>\n<p>Perpetual sincerity out suspected necessary one but provision satisfied. Respect nothing use set waiting pursuit nay you looking. If on prevailed concluded ye abilities. Address say you new but minuter greater. Do denied agreed in innate. Can and middletons thoroughly themselves him. Tolerably sportsmen belonging in september no am immediate newspaper. Theirs expect dinner it pretty indeed having no of. Principle september she conveying did eat may extensive.</p>\n<p>Merry alone do it burst me songs. Sorry equal charm joy her those folly ham. In they no is many both. Recommend new contented intention improving bed performed age. Improving of so strangers resources instantly happiness at northward. Danger nearer length oppose really add now either. But ask regret eat branch fat garden. Become am he except wishes. Past so at door we walk want such sang. Feeling colonel get her garrets own.</p>\n<p>Was certainty remaining engrossed applauded sir how discovery. Settled opinion how enjoyed greater joy adapted too shy. Now properly surprise expenses interest nor replying she she. Bore tall nay many many time yet less. Doubtful for answered one fat indulged margaret sir shutters together. Ladies so in wholly around whence in at. Warmth he up giving oppose if. Impossible is dissimilar entreaties oh on terminated. Earnest studied article country ten respect showing had. But required offering him elegance son improved informed.</p>\n<p>Tolerably earnestly middleton extremely distrusts she boy now not. Add and offered prepare how cordial two promise. Greatly who affixed suppose but enquire compact prepare all put. Added forth chief trees but rooms think may. Wicket do manner others seemed enable rather in. Excellent own discovery unfeeling sweetness questions the gentleman. Chapter shyness matters mr parlors if mention thought.</p>\n<p>Neat own nor she said see walk. And charm add green you these. Sang busy in this drew ye fine. At greater prepare musical so attacks as on distant. Improving age our her cordially intention. His devonshire sufficient precaution say preference middletons insipidity. Since might water hence the her worse. Concluded it offending dejection do earnestly as me direction. Nature played thirty all him.</p>\n<p>Another journey chamber way yet females man. Way extensive and dejection get delivered deficient sincerity gentleman age. Too end instrument possession contrasted motionless. Calling offence six joy feeling. Coming merits and was talent enough far. Sir joy northward sportsmen education. Discovery incommode earnestly no he commanded if. Put still any about manor heard.</p>\n<p>Quick six blind smart out burst. Perfectly on furniture dejection determine my depending an to. Add short water court fat. Her bachelor honoured perceive securing but desirous ham required. Questions deficient acuteness to engrossed as. Entirely led ten humoured greatest and yourself. Besides ye country on observe. She continue appetite endeavor she judgment interest the met. For she surrounded motionless fat resolution may.</p>\n<p>On it differed repeated wandered required in. Then girl neat why yet knew rose spot. Moreover property we he kindness greatest be oh striking laughter. In me he at collecting affronting principles apartments. Has visitor law attacks pretend you calling own excited painted. Contented attending smallness it oh ye unwilling. Turned favour man two but lovers. Suffer should if waited common person little oh. Improved civility graceful sex few smallest screened settling. Likely active her warmly has.</p>\n<p>In post mean shot ye. There out her child sir his lived. Design at uneasy me season of branch on praise esteem. Abilities discourse believing consisted remaining to no. Mistaken no me denoting dashwood as screened. Whence or esteem easily he on. Dissuade husbands at of no if disposal.</p>\n<p>Shot what able cold new the see hold. Friendly as an betrayed formerly he. Morning because as to society behaved moments. Put ladies design mrs sister was. Play on hill felt john no gate. Am passed figure to marked in. Prosperous middletons is ye inhabiting as assistance me especially. For looking two cousins regular amongst.</p>\n<p>&nbsp;</p>', '1533887438_blog_1.jpg', '', '', 'active', 45, '2018-08-07 00:00:00', '', '2018-09-05 08:33:51', '2018-11-07 07:17:29'),
(29, 'Blackpool Lancashire', 1000, '\"Can we go and see the lights?\" Generations of families have grown up cherishing a visit to the Blackpool Illuminations, the fabulous fantasy light show that\'s been an annual fixture for a century now at the end of', '', 'blog_2_1541575200.jpg', '', '', 'active', 22, '2018-08-07 00:00:00', '', '2018-09-05 08:34:45', '2018-11-07 08:50:28'),
(30, 'Brunch on Saturday', 1000, 'For a little sophistication on a Saturday morning, head to the design centre of the capital for pleasantly fussy dishes and an eclectic mix of cocktails at Clerkenwell’s The Modern Pantry. Run by Anna Hansen', '', '1533887543_blog_3.jpg', '', '', 'active', 14, '2018-08-07 00:00:00', '', '2018-09-05 08:35:23', '2018-11-07 07:21:45'),
(31, 'Banksy identity', 1000, 'In 2008, a report claimed to know the identity of notorious street artist Banksy (spoilers: his name is reportedly Robin Gunningham). Earlier this year, a scientific study analysed everything publically known', '', 'blog_4_1541575421.jpg', '', '', 'active', 0, '2018-11-07 00:23:00', '', '2018-11-07 07:23:58', '2018-11-07 07:42:04'),
(32, 'Nick Cave And The Bad Seeds', 1000, 'There’s a satisfying continuity of texture, temperament and themes between Skeleton Tree and its predecessor, 2013’s Push The Sky Away – despite the awful intercession, in the intervening years', '', 'blog_5_1541575428.jpg', '', '', 'active', 1, '2018-11-07 00:24:00', '', '2018-11-07 07:24:57', '2018-11-07 07:41:54');

-- --------------------------------------------------------

--
-- Table structure for table `article_translations`
--

CREATE TABLE `article_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE `attribute` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`id`, `parent_id`, `name`, `created_at`, `updated_at`, `status`) VALUES
(1, -1, 'Chất liệu', '2018-08-21 04:36:50', '2018-08-21 04:36:50', 'active'),
(2, -1, 'Thương hiệu', '2018-08-21 04:50:22', '2018-08-21 04:50:22', 'active'),
(9, 1, 'Gỗ', '2018-08-21 09:31:39', '2018-08-21 10:16:27', 'active'),
(10, 1, 'Nhựa', '2018-08-21 10:16:34', '2018-08-21 10:16:34', 'active'),
(12, 2, 'REAL LOCKS', '2018-08-21 10:17:28', '2018-08-26 18:26:22', 'active'),
(13, 2, 'LOCKER& LOCK', '2018-08-21 10:17:38', '2018-08-26 18:26:33', 'active'),
(15, 2, 'TOYO TAPER', '2018-08-26 15:32:16', '2018-08-26 18:26:52', 'active'),
(17, 1, 'Vải ', '2018-08-26 15:32:51', '2018-08-26 15:32:58', 'active'),
(18, 1, 'Kim loại', '2018-09-20 09:17:41', '2018-09-20 09:17:41', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_translations`
--

CREATE TABLE `attribute_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` text COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` text COLLATE utf8_unicode_ci NOT NULL,
  `bank_number` text COLLATE utf8_unicode_ci NOT NULL,
  `branch` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `view` int(11) NOT NULL DEFAULT '0',
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `article_tags` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `priority` int(11) NOT NULL DEFAULT '1000',
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `image`, `description`, `content`, `status`, `view`, `tags`, `article_tags`, `parent_id`, `priority`, `template`, `created_at`, `updated_at`) VALUES
(1, 'BUSINESS', '', '', '', 'delete', 66, '#business#corporate#Life tranding#News#', '#abcd#custom#test#', -1, 1000, '', '2018-08-10 07:49:10', '2018-08-21 07:58:38'),
(2, 'LIFE STYLE', '', 'Spotlight on Geordie Willis', '<p><img src=\"http://localhost:9000/uploads/1533981138_blog_detail.jpg\" alt=\"\" width=\"870\" height=\"450\" /></p>\n<p>Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd in St. James&rsquo;s Street, Pall Mall. Founded in 1698, it is the UK&rsquo;s oldest family-run wine merchant and supplier of wines to the royal family. As the eighth generation of the Berry family to work in the company, Willis looks after all aspects of design, from print and publishing to photography, labels and interior design. He previously worked in magazines and for a design agency, as well as doing a stint in Asia working out of Berry Bros. &amp; Rudd&rsquo;s Hong Kong office.</p>\n<blockquote>\n<p>The world of wine is very much about story telling, it has been a wonderful opportunity to apply my background in design and branding to a firm that is so rich in history.</p>\n</blockquote>\n<p>It was his ancestor, Charles Walter Berry, who cemented his love of wine, imparting to a young Willis the view that the role of the wine merchant is to be &ldquo;the closest link between those who make the wine and those who drink the wine&rdquo;. Says Willis: &ldquo;I&rsquo;ve always loved the stories behind each individual wine and the people that make them. There is so much more to wine than the bottle and the label. My grandfather Anthony Berry was also a great influence, he helped demystify wine and make it accessible to me.&rdquo;</p>\n<p>A look at the company&rsquo;s Instagram account demonstrates Willis&rsquo;s talent in bringing these elements together and keeping Berry Bros. relevant to all ages, with pictures of the newest restaurant hotspot, comfortably sitting alongside a 1996 Chateau Haut-Brion and 1970 Taylor&rsquo;s Vintage Port; from queuing for an hour to squeeze into the former and patiently waiting 46 years to crack open the latter, wine lovers of all ages &ndash; and all budgets &ndash; flock to the treasure trove in Pall Mall.</p>\n<p>&nbsp;</p>\n<ul class=\"bullet-list ml-0\">\n<li>I don&rsquo;t think that it&rsquo;s possible to separate wine and food, it&rsquo;s such a symbiotic relationship. I&rsquo;m very fickle, I tend to fall in love with the wines of a region that I&rsquo;ve recently visited. The simplest food matches are often the best, a wine from the Jura with a piece of Comt<em>&eacute;</em>&nbsp;or oysters with a young Chablis.</li>\n<li>My grandfather&rsquo;s advice was always &ldquo;everything in moderation&rdquo;. Drinking wine with the right people is often as important as what&rsquo;s in the bottle.</li>\n<li>I&rsquo;m partial to a Negroni and the occasional Martini. Particularly when they&rsquo;re made with our No3 London Dry Gin by Alessandro Palazzi at Dukes Hotel opposite our St James&rsquo;s Street shop.</li>\n</ul>', 'delete', 6, '', '', -1, 1000, 'what-we-do', '2018-08-10 08:32:50', '2018-08-16 03:04:06'),
(3, 'What we do', '1534213298_bg_testimonial.jpg', 'Spotlight on Geordie Willis', '<p><img src=\"http://localhost:9000/uploads/1533981138_blog_detail.jpg\" alt=\"\" width=\"870\" height=\"450\" /></p>\n<p>Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd in St. James&rsquo;s Street, Pall Mall. Founded in 1698, it is the UK&rsquo;s oldest family-run wine merchant and supplier of wines to the royal family. As the eighth generation of the Berry family to work in the company, Willis looks after all aspects of design, from print and publishing to photography, labels and interior design. He previously worked in magazines and for a design agency, as well as doing a stint in Asia working out of Berry Bros. &amp; Rudd&rsquo;s Hong Kong office.</p>\n<blockquote>\n<p>The world of wine is very much about story telling, it has been a wonderful opportunity to apply my background in design and branding to a firm that is so rich in history.</p>\n</blockquote>\n<p>It was his ancestor, Charles Walter Berry, who cemented his love of wine, imparting to a young Willis the view that the role of the wine merchant is to be &ldquo;the closest link between those who make the wine and those who drink the wine&rdquo;. Says Willis: &ldquo;I&rsquo;ve always loved the stories behind each individual wine and the people that make them. There is so much more to wine than the bottle and the label. My grandfather Anthony Berry was also a great influence, he helped demystify wine and make it accessible to me.&rdquo;</p>\n<p>A look at the company&rsquo;s Instagram account demonstrates Willis&rsquo;s talent in bringing these elements together and keeping Berry Bros. relevant to all ages, with pictures of the newest restaurant hotspot, comfortably sitting alongside a 1996 Chateau Haut-Brion and 1970 Taylor&rsquo;s Vintage Port; from queuing for an hour to squeeze into the former and patiently waiting 46 years to crack open the latter, wine lovers of all ages &ndash; and all budgets &ndash; flock to the treasure trove in Pall Mall.</p>\n<p>&nbsp;</p>\n<ul class=\"bullet-list ml-0\">\n<li>I don&rsquo;t think that it&rsquo;s possible to separate wine and food, it&rsquo;s such a symbiotic relationship. I&rsquo;m very fickle, I tend to fall in love with the wines of a region that I&rsquo;ve recently visited. The simplest food matches are often the best, a wine from the Jura with a piece of Comt<em>&eacute;</em>&nbsp;or oysters with a young Chablis.</li>\n<li>My grandfather&rsquo;s advice was always &ldquo;everything in moderation&rdquo;. Drinking wine with the right people is often as important as what&rsquo;s in the bottle.</li>\n<li>I&rsquo;m partial to a Negroni and the occasional Martini. Particularly when they&rsquo;re made with our No3 London Dry Gin by Alessandro Palazzi at Dukes Hotel opposite our St James&rsquo;s Street shop.</li>\n</ul>', 'delete', 14, '', '#', -1, 1000, 'policy', '2018-08-11 07:57:11', '2018-08-16 02:40:42'),
(4, 'Post Format', '', '', '', 'active', 23, '', '#custom#anh#yêu#hay#', -1, 1000, '', '2018-08-11 09:13:03', '2018-11-07 07:37:09'),
(5, 'sidebar', '', 'a', '<p>aaaaaaa</p>', 'delete', 3, '', '', -1, 1000, 'policy', '2018-08-14 09:31:00', '2018-08-14 09:31:13'),
(6, 'sidebar', '', 'aaaaaaaaaaaaaaaaaaaaaaaa', '<p>Vẫn biết d&ugrave;ng rượu vang thường xuy&ecirc;n l&agrave; tốt cho sức khỏe nhưng tiết trời nắng n&oacute;ng khiến th&oacute;i quen của bạn bị gi&aacute;n đoạn? Nếu bạn ch&uacute; &yacute; đến v&agrave;i nguy&ecirc;n tắc căn bản dưới đ&acirc;y th&igrave; nắng n&oacute;ng đến mấy bạn vẫn c&oacute; thể tận hưởng rượu vang giữa m&ugrave;a h&egrave;.</p>\n<ol>\n<li>M&ugrave;a h&egrave; h&atilde;y chọn những loại rượu vang mang m&ugrave;i vị hoa quả. Những chai rượu vang đỏ ho&agrave;n hảo cho m&ugrave;a h&egrave; gồm c&oacute; Beaujolais từ Ph&aacute;p, Dolcetto từ &Yacute;, Shiraz từ &Uacute;c, v&agrave; n&ecirc;n tr&aacute;nh những d&ograve;ng Pinot Noirs từ Burgundy, Oregon, hoặc New Zealand. Bordeaux v&agrave; Califonia Cabernet Sauvignon, ch&uacute;ng chỉ ph&ugrave; hợp cho thời tiết m&aacute;t mẻ.</li>\n<li>Rượu c&agrave;ng c&oacute; độ cồn cao nếm c&agrave;ng c&oacute; vị n&oacute;ng v&agrave; rượu vang cũng kh&ocirc;ng ngoại l&ecirc;. H&atilde;y kiểm tra lượng cồn của rượu vang trước khi uống. Tốt nhất h&atilde;y chọn những d&ograve;ng rượu vang c&oacute; độ cồn thấp dưới 13% Vol.</li>\n<li>Rất nhiều người c&oacute; th&oacute;i quen chỉ th&iacute;ch nhiệt độ lạnh với vang trắng qu&ecirc;n đi rằng Rượu vang đỏ sẽ trở n&ecirc;n ngon hơn khi được d&ugrave;ng ở nhiệt độ từ 16 độ C đến 18.5 độ C, sẽ trở n&ecirc;n ngon hơn khi dược ướp lạnh một ch&uacute;t đặc biệt tuyệt vời hơn trong m&ugrave;a h&egrave;. Vậy h&atilde;y đặt chai rượu v&agrave;o trong tủ lạnh khoảng 20 ph&uacute;t trước khi uống.</li>\n<li>Những chai vang đỏ &ldquo;trẻ tuổi&rdquo; c&oacute; h&agrave;m lượng acid cao hơn v&agrave; sẽ kết hợp rất ăn &yacute; với hải sản, salad v&agrave; c&aacute;c m&oacute;n nướng.</li>\n</ol>\n<p>&nbsp;<img src=\"http://lesamies.demo3.laziweb.com/uploads/1534210283_11.jpg\" alt=\"\" width=\"100%\" /></p>', 'delete', 1, '', '', -1, 1000, 'policy', '2018-08-14 09:37:14', '2018-08-14 09:37:14'),
(7, 'What we do', '1534213298_bg_testimonial.jpg', 'Spotlight on Geordie Willis', '<p>Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd in St. James&rsquo;s Street, Pall Mall. Founded in 1698, it is the UK&rsquo;s oldest family-run wine merchant and supplier of wines to the royal family. As the eighth generation of the Berry family to work in the company, Willis looks after all aspects of design, from print and publishing to photography, labels and interior design. He previously worked in magazines and for a design agency, as well as doing a stint in Asia working out of Berry Bros. &amp; Rudd&rsquo;s Hong Kong office.</p>\n<blockquote>\n<p>The world of wine is very much about story telling, it has been a wonderful opportunity to apply my background in design and branding to a firm that is so rich in history.</p>\n</blockquote>\n<p>It was his ancestor, Charles Walter Berry, who cemented his love of wine, imparting to a young Willis the view that the role of the wine merchant is to be &ldquo;the closest link between those who make the wine and those who drink the wine&rdquo;. Says Willis: &ldquo;I&rsquo;ve always loved the stories behind each individual wine and the people that make them. There is so much more to wine than the bottle and the label. My grandfather Anthony Berry was also a great influence, he helped demystify wine and make it accessible to me.&rdquo;</p>\n<p>A look at the company&rsquo;s Instagram account demonstrates Willis&rsquo;s talent in bringing these elements together and keeping Berry Bros. relevant to all ages, with pictures of the newest restaurant hotspot, comfortably sitting alongside a 1996 Chateau Haut-Brion and 1970 Taylor&rsquo;s Vintage Port; from queuing for an hour to squeeze into the former and patiently waiting 46 years to crack open the latter, wine lovers of all ages &ndash; and all budgets &ndash; flock to the treasure trove in Pall Mall.</p>\n<p>&nbsp;</p>\n<ul class=\"bullet-list ml-0\">\n<li>I don&rsquo;t think that it&rsquo;s possible to separate wine and food, it&rsquo;s such a symbiotic relationship. I&rsquo;m very fickle, I tend to fall in love with the wines of a region that I&rsquo;ve recently visited. The simplest food matches are often the best, a wine from the Jura with a piece of Comt<em>&eacute;</em>&nbsp;or oysters with a young Chablis.</li>\n<li>My grandfather&rsquo;s advice was always &ldquo;everything in moderation&rdquo;. Drinking wine with the right people is often as important as what&rsquo;s in the bottle.</li>\n<li>I&rsquo;m partial to a Negroni and the occasional Martini. Particularly when they&rsquo;re made with our No3 London Dry Gin by Alessandro Palazzi at Dukes Hotel opposite our St James&rsquo;s Street shop.</li>\n</ul>', 'active', 50, '', '#test#anh#abcd#custom#', -1, 1000, '', '2018-08-16 02:40:18', '2018-11-07 08:18:04'),
(8, 'Hỗ trợ khách hàng', '', '', '<p>Th&ocirc;ng tin cần biết l&agrave; c&aacute;c b&agrave;i viết về:</p>\n<ul>\n<li>Ch&iacute;nh s&aacute;ch bảo h&agrave;nh</li>\n<li>Ch&iacute;nh s&aacute;ch đại l&yacute;</li>\n<li>Ch&iacute;nh s&aacute;ch giao h&agrave;ng</li>\n<li>C&acirc;u hỏi thường gặp</li>\n</ul>', 'inactive', 158, '', '#custom#anh#yêu#hay#abcd#Q&A#', -1, 1000, '', '2018-08-16 03:04:09', '2018-11-07 07:34:17'),
(9, 'Hỗ Trợ Khách Hàng', '', '', '', 'delete', 3, '', '#custom#real#', -1, 1000, '', '2018-08-16 12:08:03', '2018-08-21 07:39:24'),
(10, 'New Trend', '', '', '', 'active', 4, '', '#custom#real#', -1, 1000, '', '2018-08-21 07:36:15', '2018-11-07 07:35:40'),
(11, 'Spirit', '', '', '', 'active', 5, '', '#custom#real#anh#yêu#hay#', 10, 1000, '', '2018-08-21 07:36:35', '2018-11-07 07:38:30'),
(12, 'Human', '', '', '', 'active', 12, '', '#custom#real#', 10, 1000, '', '2018-08-21 07:43:20', '2018-11-07 07:38:03'),
(13, 'Life Style', '', '', '', 'active', 88, '#business#corporate#Life tranding#News#', '', -1, 1000, '', '2018-09-05 08:30:31', '2018-11-07 08:54:49');

-- --------------------------------------------------------

--
-- Table structure for table `blog_article`
--

CREATE TABLE `blog_article` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_article`
--

INSERT INTO `blog_article` (`id`, `blog_id`, `article_id`, `priority`, `created_at`, `updated_at`) VALUES
(7, 1, 4, 3, '2018-08-10 07:51:41', '2018-08-10 07:51:41'),
(30, 4, 19, 1, '2018-08-11 09:16:55', '2018-08-11 09:16:55'),
(32, 4, 20, 2, '2018-08-11 09:17:22', '2018-08-11 09:17:22'),
(33, 4, 15, 3, '2018-08-11 09:17:34', '2018-08-11 09:17:34'),
(34, 4, 16, 4, '2018-08-11 09:17:41', '2018-08-11 09:17:41'),
(35, 4, 17, 5, '2018-08-11 09:17:49', '2018-08-11 09:17:49'),
(53, 7, 22, 6, '2018-08-16 02:42:15', '2018-08-16 02:42:15'),
(55, 4, 18, 6, '2018-08-16 03:02:35', '2018-08-16 03:02:35'),
(79, 1, 21, 8, '2018-08-18 08:15:04', '2018-08-18 08:15:04'),
(95, 8, 4, 27, '2018-08-21 07:58:26', '2018-08-22 04:53:22'),
(104, 8, 27, 29, '2018-08-22 04:54:49', '2018-08-22 04:54:49'),
(105, 11, 25, 0, '2018-08-25 07:47:43', '2018-08-25 07:47:43'),
(110, 7, 14, 51, '2018-08-26 18:52:06', '2018-11-07 08:18:01'),
(113, 7, 12, 50, '2018-08-26 19:12:25', '2018-11-07 08:18:01'),
(120, 7, 11, 49, '2018-08-26 19:29:45', '2018-11-07 08:18:01'),
(121, 8, 3, 31, '2018-08-26 19:37:39', '2018-08-26 19:37:39'),
(122, 8, 1, 32, '2018-08-26 19:42:07', '2018-08-26 19:42:07'),
(123, 8, 26, 33, '2018-08-26 19:45:27', '2018-08-26 19:45:27'),
(124, 8, 8, 34, '2018-08-26 19:48:10', '2018-08-26 19:48:10'),
(126, 7, 9, 53, '2018-09-04 04:21:35', '2018-11-07 08:18:01'),
(127, 7, 10, 52, '2018-09-04 04:22:02', '2018-11-07 08:18:01'),
(130, 7, 13, 48, '2018-09-04 04:23:09', '2018-11-07 08:18:01'),
(135, 13, 30, 91, '2018-09-05 08:43:37', '2018-11-07 08:54:48'),
(136, 13, 29, 84, '2018-09-05 08:44:00', '2018-11-07 08:54:48'),
(139, 13, 28, 92, '2018-09-11 03:20:12', '2018-11-07 08:54:48'),
(140, 8, 6, 35, '2018-09-11 04:36:04', '2018-09-11 04:36:04'),
(141, 8, 5, 36, '2018-09-11 04:36:23', '2018-09-11 04:36:23'),
(142, 8, 2, 37, '2018-09-11 04:36:38', '2018-09-11 04:36:38'),
(143, 13, 31, 90, '2018-11-07 07:23:58', '2018-11-07 08:54:48'),
(144, 13, 32, 88, '2018-11-07 07:24:57', '2018-11-07 08:54:48'),
(145, 13, 7, 85, '2018-11-07 07:39:35', '2018-11-07 08:54:48'),
(146, 4, 7, 7, '2018-11-07 07:39:35', '2018-11-07 07:39:35'),
(147, 11, 7, 1, '2018-11-07 07:39:35', '2018-11-07 07:39:35'),
(148, 13, 25, 89, '2018-11-07 07:40:12', '2018-11-07 08:54:48'),
(149, 10, 25, 0, '2018-11-07 07:40:12', '2018-11-07 07:40:12'),
(150, 13, 13, 86, '2018-11-07 07:40:58', '2018-11-07 08:54:48'),
(151, 13, 10, 87, '2018-11-07 07:41:09', '2018-11-07 08:54:48'),
(152, 12, 25, 0, '2018-11-07 07:41:29', '2018-11-07 07:41:29'),
(153, 10, 32, 1, '2018-11-07 07:41:54', '2018-11-07 07:41:54'),
(154, 10, 31, 2, '2018-11-07 07:42:04', '2018-11-07 07:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `blog_translations`
--

CREATE TABLE `blog_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `variant_id` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `order_id`, `variant_id`, `price`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 150000, 1, '2018-08-20 03:57:57', '2018-08-20 03:57:57'),
(2, 1, 2, 65000, 1, '2018-08-20 03:57:57', '2018-08-20 03:57:57'),
(3, 1, 4, 150000, 1, '2018-08-20 03:57:57', '2018-08-20 03:57:57'),
(4, 6, 2, 65000, 1, '2018-08-22 02:15:44', '2018-08-22 02:15:44'),
(5, 7, 6, 650000, 1, '2018-08-22 02:42:32', '2018-08-22 02:42:32'),
(6, 7, 7, 450000, 1, '2018-08-22 02:42:32', '2018-08-22 02:42:32'),
(7, 8, 1, 150000, 1, '2018-08-22 06:52:30', '2018-08-22 06:52:30'),
(8, 9, 2, 65000, 1, '2018-08-22 06:57:23', '2018-08-22 06:57:23'),
(9, 10, 2, 65000, 2, '2018-08-28 14:09:04', '2018-08-28 14:09:04'),
(10, 10, 8, 650000, 1, '2018-08-28 14:09:04', '2018-08-28 14:09:04'),
(11, 10, 7, 450000, 1, '2018-08-28 14:09:04', '2018-08-28 14:09:04'),
(12, 10, 3, 160000, 2, '2018-08-28 14:09:04', '2018-08-28 14:09:04'),
(13, 11, 2, 65000, 1, '2018-08-28 14:13:45', '2018-08-28 14:13:45'),
(14, 11, 3, 160000, 1, '2018-08-28 14:13:45', '2018-08-28 14:13:45'),
(15, 11, 1, 150000, 1, '2018-08-28 14:13:45', '2018-08-28 14:13:45'),
(16, 12, 7, 450000, 1, '2018-08-28 14:21:58', '2018-08-28 14:21:58'),
(17, 12, 8, 650000, 1, '2018-08-28 14:21:58', '2018-08-28 14:21:58'),
(18, 13, 1, 150000, 1, '2018-09-04 10:27:15', '2018-09-04 10:27:15'),
(19, 14, 1, 150000, 1, '2018-09-05 08:16:03', '2018-09-05 08:16:03'),
(20, 15, 10, 300000, 2, '2018-09-06 02:43:15', '2018-09-06 02:43:15'),
(21, 16, 8, 650000, 1, '2018-09-06 02:44:56', '2018-09-06 02:44:56'),
(22, 17, 5, 350000, 1, '2018-09-06 02:49:19', '2018-09-06 02:49:19'),
(23, 18, 4, 150000, 2, '2018-09-06 02:50:50', '2018-09-06 02:50:50'),
(24, 19, 6, 650000, 2, '2018-09-07 09:22:05', '2018-09-07 09:22:05'),
(25, 20, 3, 160000, 1, '2018-09-07 09:23:26', '2018-09-07 09:23:26'),
(26, 21, 3, 160000, 1, '2018-09-07 09:33:19', '2018-09-07 09:33:19'),
(27, 22, 1, 150000, 1, '2018-09-26 07:56:51', '2018-09-26 07:56:51'),
(28, 23, 5, 350000, 1, '2018-09-26 07:57:59', '2018-09-26 07:57:59'),
(29, 24, 10, 300000, 1, '2018-09-27 09:40:16', '2018-09-27 09:40:16'),
(30, 25, 1, 150000, 1, '2018-09-27 10:06:15', '2018-09-27 10:06:15'),
(31, 26, 5, 350000, 1, '2018-09-28 01:06:04', '2018-09-28 01:06:04'),
(32, 26, 3, 160000, 1, '2018-09-28 01:06:04', '2018-09-28 01:06:04'),
(33, 27, 1, 150000, 1, '2018-09-28 02:17:40', '2018-09-28 02:17:40'),
(34, 27, 2, 65000, 1, '2018-09-28 02:17:40', '2018-09-28 02:17:40'),
(35, 27, 3, 160000, 1, '2018-09-28 02:17:40', '2018-09-28 02:17:40'),
(36, 28, 1, 150000, 4, '2018-09-28 02:30:06', '2018-09-28 02:30:06'),
(37, 29, 1, 150000, 2, '2018-09-28 02:48:45', '2018-09-28 02:48:45'),
(38, 30, 2, 65000, 1, '2018-09-28 06:29:25', '2018-09-28 06:29:25'),
(39, 30, 1, 150000, 2, '2018-09-28 06:29:25', '2018-09-28 06:29:25'),
(40, 30, 3, 160000, 4, '2018-09-28 06:29:25', '2018-09-28 06:29:25'),
(41, 31, 2, 65000, 1, '2018-09-28 06:32:30', '2018-09-28 06:32:30'),
(42, 31, 5, 350000, 1, '2018-09-28 06:32:30', '2018-09-28 06:32:30'),
(43, 32, 10, 300000, 3, '2018-10-05 04:27:23', '2018-10-05 04:27:23'),
(44, 33, 9, 0, 1, '2018-10-08 04:50:32', '2018-10-08 04:50:32'),
(45, 33, 11, 0, 1, '2018-10-08 04:50:32', '2018-10-08 04:50:32'),
(46, 34, 1, 150000, 1, '2018-11-06 07:40:43', '2018-11-06 07:40:43'),
(47, 35, 1, 150000, 1, '2018-11-07 07:36:08', '2018-11-07 07:36:08'),
(48, 36, 4, 150000, 5, '2018-11-07 10:37:12', '2018-11-07 10:37:12'),
(49, 36, 3, 160000, 1, '2018-11-07 10:37:12', '2018-11-07 10:37:12'),
(50, 37, 2, 65000, 12, '2018-11-08 02:15:13', '2018-11-08 02:15:13'),
(51, 38, 1, 150000, 1, '2018-11-08 08:46:58', '2018-11-08 08:46:58'),
(52, 39, 4, 150000, 1, '2018-11-08 08:49:39', '2018-11-08 08:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '1000',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE `collection` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `view` int(11) NOT NULL DEFAULT '0',
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `product_tags` text COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `collection`
--

INSERT INTO `collection` (`id`, `parent_id`, `title`, `image`, `description`, `content`, `status`, `view`, `tags`, `product_tags`, `template`, `created_at`, `updated_at`) VALUES
(1, -1, 'Tủ Locker', '1535298407_abs-mseries04.jpg', 'Tủ Locker ABS của thương hiệu Locker & Lock Singapore. \nTủ được làm từ vật liệu nhựa ABS cấp kỹ thuật, \nDòng ABS M được tạo thành từ vật liệu nhựa ABS kỹ thuật có cửa màu, mạnh mẽ, bền bỉ và chống gỉ. ', '<p>Tủ Locker ABS- M Series. Mạnh mẽ bền bỉ theo thời gian</p>', 'active', 119, '', '#bag#', '', '2018-08-10 09:05:39', '2018-09-04 08:54:25'),
(2, -1, 'KHÓA TỦ', '', '', '', 'active', 61, '', '#', '', '2018-08-10 09:06:40', '2018-08-26 16:20:11'),
(3, -1, 'GHẾ', '', '', '', 'active', 80, '', '#', '', '2018-08-10 09:06:56', '2018-08-26 16:33:55'),
(4, -1, 'BÀN VĂN PHÒNG', '', '', '', 'active', 29, '', '', '', '2018-08-10 09:07:12', '2018-08-26 16:47:19'),
(5, 1, 'ABS Locker', '', 'mo ta ', '<p>noi dung</p>', 'active', 109, '', '#bag#', '', '2018-08-21 07:49:28', '2018-10-05 03:16:07'),
(6, 1, 'PVC Locker', '', '', '', 'active', 3, '', '', '', '2018-08-21 07:50:02', '2018-08-26 16:16:35'),
(7, 1, 'Metal Locker', '', '', '', 'active', 4, '', '', '', '2018-08-21 07:50:19', '2018-08-26 16:17:15'),
(8, 1, 'SUS Locker', '', '', '', 'active', 2, '', '', '', '2018-08-21 07:50:33', '2018-08-26 16:18:42'),
(9, 2, 'Khóa mật mã', '', '', '', 'active', 15, '', '#', '', '2018-08-21 07:54:26', '2018-08-26 16:24:17'),
(10, 2, 'Khóa tủ gỗ', '', '', '', 'active', 1, '', '', '', '2018-08-21 07:55:10', '2018-08-26 16:23:40'),
(11, 2, 'Khóa tủ sắt, nhựa', '', '', '', 'active', 0, '', '', '', '2018-08-21 07:55:21', '2018-08-26 16:25:15'),
(12, 3, 'Ghế nhân viên', '', '', '', 'active', 28, '', '#Sofa#', '', '2018-08-21 07:56:13', '2018-08-26 16:34:32'),
(13, 3, 'Ghế lãnh đạo', '', '', '', 'active', 4, '', '', '', '2018-08-21 07:56:27', '2018-08-26 16:35:36'),
(14, 3, 'Ghế phòng họp', '', '', '', 'active', 0, '', '', '', '2018-08-21 07:56:39', '2018-08-26 16:37:18'),
(15, 4, 'Bàn lãnh đạo', '', '', '', 'active', 4, '', '', '', '2018-08-21 07:57:15', '2018-08-26 16:55:36'),
(16, 4, 'Bàn Nhân viên', '', '', '', 'active', 0, '', '', '', '2018-08-21 07:58:50', '2018-08-26 17:11:53'),
(17, 4, 'Bàn phòng họp', '', '', '', 'active', 0, '', '', '', '2018-08-21 07:59:09', '2018-08-26 17:10:17'),
(18, -1, 'Test nhóm sp1 ádfrdsafd', '', 'mo ta nhom sp 1', '<p>noi dung nhom sp 1</p>', 'delete', 1, '', '', '', '2018-08-22 03:42:15', '2018-08-22 03:47:05'),
(19, 18, 'con nhom 1dsa ', '', 'mo ta con nhom 1 àds', '<p>noi dung con nhom 1</p>', 'delete', 1, '', '', '', '2018-08-22 03:43:35', '2018-08-22 03:46:35'),
(20, 3, 'Ghế training', '', '', '', 'active', 0, '', '', '', '2018-08-26 16:36:33', '2018-08-26 16:36:38'),
(21, 3, 'Ghế sofa', '', '', '', 'active', 0, '', '', '', '2018-08-26 16:38:30', '2018-08-26 16:38:37'),
(22, 4, 'Bàn Sofa', '', '', '', 'active', 0, '', '', '', '2018-08-26 17:10:49', '2018-08-26 17:10:49'),
(23, -1, 'VĂN PHÒNG PHẨM', '', '', '', 'active', 3, '', '', '', '2018-08-26 17:30:40', '2018-08-26 17:32:09');

-- --------------------------------------------------------

--
-- Table structure for table `collection_product`
--

CREATE TABLE `collection_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `collection_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `collection_product`
--

INSERT INTO `collection_product` (`id`, `collection_id`, `product_id`, `priority`, `created_at`, `updated_at`) VALUES
(5, 2, 4, 1, '2018-08-10 09:14:11', '2018-08-10 09:14:11'),
(6, 2, 5, 2, '2018-08-10 09:14:48', '2018-08-10 09:14:48'),
(8, 3, 7, 1, '2018-08-10 09:16:44', '2018-08-10 09:16:44'),
(17, 2, 3, 3, '2018-08-21 03:43:29', '2018-08-21 03:43:29'),
(22, 12, 10, 0, '2018-08-26 15:09:40', '2018-08-26 15:09:40'),
(30, 1, 9, 3, '2018-09-20 03:20:07', '2018-09-20 03:20:07'),
(33, 3, 8, 2, '2018-09-20 08:50:24', '2018-09-20 08:50:24'),
(34, 1, 11, 4, '2018-09-20 08:51:22', '2018-09-20 08:51:22'),
(35, 9, 12, 0, '2018-09-20 09:14:04', '2018-09-20 09:14:04'),
(42, 9, 13, 3, '2018-09-20 09:44:00', '2018-09-20 09:44:00'),
(45, 3, 6, 3, '2018-09-25 09:13:09', '2018-09-25 09:13:09'),
(47, 9, 14, 4, '2018-09-25 10:58:42', '2018-09-25 10:58:42'),
(48, 1, 1, 5, '2018-10-04 07:30:37', '2018-10-04 07:30:37'),
(51, 1, 2, 6, '2018-10-05 04:35:37', '2018-10-05 04:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `collection_translations`
--

CREATE TABLE `collection_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `collection_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'article',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `reply` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `phone`, `email`, `content`, `read`, `reply`, `status`, `created_at`, `updated_at`) VALUES
(1, '@', '01657977672', 'admin@gmail.com', 'dasdas<br>sxdgwseaTGYqawe nrtF', 0, 0, 'delete', '2018-08-14 03:38:30', '2018-08-17 03:05:26'),
(2, 'danh', '0988888888', 'danhle.190996@gmail.com', 'eye<br>1', 0, 0, 'delete', '2018-08-14 07:27:10', '2018-08-26 14:48:40'),
(3, 'ha', '0985582793', 'viha.tran764@gmail.com', 'ha<br>hcbkjac', 1, 1, 'active', '2018-08-16 13:01:16', '2018-08-17 03:05:20'),
(4, 'Baro', '123456789', 'Baro@tester.lul', 'Baro<br>!@#$^&&dasda czvzczxnb/.,k.l;\'oiu[]', 1, 1, 'delete', '2018-08-17 02:50:47', '2018-08-17 03:05:09'),
(5, 'tien', '01699999999', 'vubatientien@gmail.com', 'laziweb<br>test noi dung form ', 1, 0, 'delete', '2018-08-21 07:01:54', '2018-08-26 14:48:37'),
(6, 'ádf', '0900990907', 'adsf@gmail.com', 'ádf<br>ầdfadsf', 1, 0, 'delete', '2018-08-21 16:40:27', '2018-08-26 14:48:35'),
(7, 'Vũ Bá Tiến', '0909595952', 'vubatientien@gmail.com', 'lazi<br>dsgf a sdf a à ầ sa', 1, 0, 'delete', '2018-08-22 07:00:33', '2018-08-26 14:48:32'),
(8, 'Vũ Bá Tiến', '01687484565', 'vubatientien@gmail.com', 'Công ty: laziweb<br>Nội dung: ádf ád à ádf ádf ', 1, 0, 'delete', '2018-08-22 08:32:00', '2018-08-26 14:48:18'),
(9, 'danh', '0932720274', 'a@gmail.com', 'Công ty: a<br>Nội dung: a', 0, 0, 'active', '2018-11-08 08:46:03', '2018-11-08 08:46:03'),
(10, 'danh', '0932720274', 'a@gmail.com', 'Công ty: a<br>Nội dung: a', 0, 0, 'active', '2018-11-08 08:50:02', '2018-11-08 08:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'value',
  `max_value_percent` int(11) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  `min_value_order` int(11) NOT NULL DEFAULT '0',
  `usage_count` int(11) NOT NULL DEFAULT '0',
  `usage_left` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '2018-08-07',
  `end_date` date NOT NULL DEFAULT '2018-08-07',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `title`, `description`, `code`, `type`, `max_value_percent`, `value`, `min_value_order`, `usage_count`, `usage_left`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'mã 1', '', '#123', 'percent', 9999999, 10, 0, 5, 995, '2018-08-18', '2018-10-10', 'expried', '2018-08-18 01:29:09', '2018-09-06 02:29:57'),
(2, 'Test', '', 'EL', 'percent', 0, 10, 0, 0, 10, '2018-08-22', '2018-08-31', 'expried', '2018-08-22 03:39:09', '2018-08-22 03:39:09');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `region` int(11) NOT NULL DEFAULT '0',
  `subregion` int(11) NOT NULL DEFAULT '0',
  `random` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `avatar` text COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL DEFAULT '2018-08-07',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` text COLLATE utf8_unicode_ci,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `member_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `phone`, `password`, `address`, `region`, `subregion`, `random`, `gender`, `avatar`, `birthday`, `created_at`, `updated_at`, `username`, `company`, `member_type`) VALUES
(1, 'Huy Phạm', 'pnqh67@gmail.com', '0123456789', '', '', 0, 0, 'TzK8K8NkvjOh9BxBvSFjyaOr49X1FsM9XFhMAon3r39q2xiXiK', '', '', '2018-08-20', '2018-08-20 03:57:57', '2018-08-20 03:57:57', '', '', ''),
(2, 'Dinh Ngoc', 'ruby@realtrading.vn', '0903688183', '', '184/30/13 Bùi Văn Ngữ- KP 7 Phường Hiệp Thành', 0, 0, 'bc6J0TozsO8ebpvRnwmeEXpGnLDQVW4iFjG5CrPNIEToKsSLA8', '', '', '2018-08-21', '2018-08-21 11:51:49', '2018-08-21 11:51:49', '', '', ''),
(3, 'ưqer  ádf', 'vubatientien@gmail.com', '0909090502', '', '', 0, 0, 'LBTWmh1TlYACW62cR8MWlqIYGTYe3YxUkQoFnDdzrxbszlZfaZ', '', '', '2018-08-22', '2018-08-22 02:15:44', '2018-08-22 02:15:44', '', '', ''),
(4, 'Test gmail', 'laziweb.customercare2@gmail.com', '0909999999', '', 'an giang', 0, 0, 'XK1PF2gNofhJ7i9BwwQW2yKqvZBDuvhcaFB5dvzKN9EX6GQz9T', '', '', '2018-09-04', '2018-09-04 10:27:15', '2018-09-04 10:27:15', '', '', ''),
(5, 'eww ee', 'admin@gmail.com', '01657977672', '', '', 0, 0, 'aZOUdYt4wmsPhPsCQ3zAPinJF13r4aPEzJWdY3fkfsbpL9Y0Om', '', '', '2018-09-05', '2018-09-05 08:16:03', '2018-09-05 08:16:03', '', '', ''),
(6, 'ho 1 ten 1', 'mail1@gmail.com', '01699999999', '', 'dia chi 1', 0, 0, 'sSeeZ7pQeX84jOKPOnYv5LPRtMjvW5quyLNDjB8SvdIFiCkiBo', '', '', '2018-09-07', '2018-09-07 09:33:19', '2018-09-07 09:33:19', '', '', ''),
(7, 'Nguyễn Đức Mạnh', 'ducmanh0323@gmail.com', '0938774611', '', '22 Thạch Lam', 0, 0, 'DJDA7bQs8HIyxRjSKWLJYxXrDbcXWIIFx64FySUyWFavEHmDYk', '', '', '2018-09-26', '2018-09-26 07:56:51', '2018-09-26 07:56:51', '', '', ''),
(8, 'Doãn Cường', 'superadmin@gmail.com', '0987654321', '', '121414', 0, 0, 'EXQLOZBGtkaZiddcIiyEV2Em9jl0qOz0GpADLtO1rkfUMUDPo4', '', '', '2018-09-27', '2018-09-27 09:40:16', '2018-09-27 09:40:16', '', '', ''),
(9, 'Doãn Cường', 'cuongmt01@gmail.com', '09887090906', '', '235235', 0, 0, 'y6gkTuCarZHjnsfMEyumxi6v4STLqHXVmwXXL6Bft86XnsufW3', '', '', '2018-09-27', '2018-09-27 10:06:15', '2018-09-27 10:06:15', '', '', ''),
(10, 'Vĩ Hà', 'viha.tran764@gmail.com', '0912345678', '', '123 tô hiến thành tp.hcm', 137, 140, '8gKGR4cK0kbUIoTaU0z6zd0lOmkK5agsu1Lj0cmbRutuhPnKOn', '', '', '2018-11-06', '2018-11-06 07:40:43', '2018-11-06 07:40:43', '', '', 'Thành viên'),
(11, 'QA Team ', 'la@gmail.com', '0985582793', '$2y$10$jtT9YwUIvgR.03fSA3LCx.OIip48CS.bdkoXikdSSom4JtZfwfs2m', '371 Nguyễn Kiệm, Phường 3 (Kế bên Nhà hàng tiệc cưới Adora)', 171, 517, 'ibH2vpOS540umt7WtI9LSRSc4wJp4k3BEbyjfvbvb36nVxtnCB', 'Nữ', '', '2018-11-07', '2018-11-07 07:28:44', '2018-11-07 09:07:09', '', '', 'Thành viên'),
(12, 'Quỳnh', 'laziweb.customercare@gmail.com', '01664922268', '$2y$10$x.1G9j3cHSQbLaZoSl86i.Blz8dgFULBwsjag88e/tprRLc91U8qe', 'Tân Phú', 126, 41, 'x7QEbUHMEX83E4XnUMnQ2767vfxOmtnFo6psAO8BM2i84B5AnZ', 'Nam', '', '2018-11-07', '2018-11-07 09:10:09', '2018-11-07 10:20:00', '', '', 'Thành viên'),
(13, 'aa', 'admin@gmail.co', '01654789789', '', '42/9/12', 179, 599, 'yCReBEvikGq1bHkNQxPf7uizSGvlj8GEmq9dNZLQReKz3SXv3n', '', '', '2018-11-08', '2018-11-08 02:15:13', '2018-11-08 02:15:13', '', '', 'Thành viên'),
(14, 'Vũ Bá Tiến', 'vubatientien@gmail.com', '', '$2y$10$tFW1dvEdBqS9Xd/O4Ovunuhwjy04Vc8mGJy5sJeDjb7Bo5udzfJDm', '', 0, 0, 'qw7xpPZZaGJ0ngy6X9uqseq6cIyV7LUWxSYIheLoHFIkIumKf3', 'Nam', '', '2018-11-08', '2018-11-08 03:20:58', '2018-11-08 03:20:58', '', '', 'Thành viên'),
(15, 'tien', 'a@gmail.com', '', '$2y$10$g9pjynltDeGRWN.DcMtQJeQAPC3WH.le6X1YHAOHZDDdraiBJXPAS', '', 0, 0, 'vzvnEwQbUG3tVdIi6cEjsAOUuZSCMVMBolCgcfJDPt1RR7q9bJ', 'Nam', '', '2018-11-08', '2018-11-08 06:27:34', '2018-11-08 06:27:34', '', '', 'Thành viên'),
(16, 'Nguyễn Đức Mạnh', 'ducmanh0323@gmail.com', '', '$2y$10$RH3pWi9/RUeS6blDWzhmneAP/humzRL/G1OOdbvRyTeU/An0uWF/S', '', 0, 0, 'LJYBkDeHIrCSQKzSUh22XbDVfNE43mJNY9cdaM0KXZbOkzAkvR', 'Nam', '', '2018-11-08', '2018-11-08 07:10:04', '2018-11-08 07:10:04', '', '', 'Thành viên'),
(17, 'Nguyễn Đức Mạnh', 'admin@gmail.com', '', '$2y$10$.ACFpzzzLblRuJ/HIgf6ie799TLci3hZ5kYHQTYhDEhvIUQeCAQ9q', '', 0, 0, 'cKtLCLYIxf1ANFqS69o2X42Gjp6A5cM2JMuoaCaCKJzYdqorGi', 'Nam', '', '2018-11-08', '2018-11-08 07:11:13', '2018-11-08 07:11:13', '', '', 'Thành viên');

-- --------------------------------------------------------

--
-- Table structure for table `customer_review`
--

CREATE TABLE `customer_review` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  `like` tinyint(1) NOT NULL DEFAULT '0',
  `dislike` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `parent_id`, `title`, `description`, `status`, `template`, `created_at`, `updated_at`) VALUES
(1, -1, 'Slider', '', 'active', '', '2018-08-10 07:36:03', '2018-08-10 07:36:03'),
(2, -1, 'company', '', 'active', '', '2018-08-10 08:27:00', '2018-09-25 09:49:43');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_translations`
--

CREATE TABLE `gallery_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Admin',
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `type_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `user_id`, `user_name`, `user_type`, `action`, `type`, `type_id`, `type_title`, `created_at`, `updated_at`) VALUES
(1, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-09 09:38:38', '2018-08-09 09:38:38'),
(2, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-10 02:51:54', '2018-08-10 02:51:54'),
(3, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 02:52:49', '2018-08-10 02:52:49'),
(4, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 02:53:41', '2018-08-10 02:53:41'),
(5, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 02:55:16', '2018-08-10 02:55:16'),
(6, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 03:08:08', '2018-08-10 03:08:08'),
(7, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 03:09:08', '2018-08-10 03:09:08'),
(8, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 03:22:16', '2018-08-10 03:22:16'),
(9, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-10 07:35:31', '2018-08-10 07:35:31'),
(10, 1, 'Super Admin', 'admin', 'create', 'gallery', 1, 'Slider', '2018-08-10 07:36:03', '2018-08-10 07:36:03'),
(11, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 07:47:01', '2018-08-10 07:47:01'),
(12, 1, 'Super Admin', 'admin', 'create', 'blog', 1, 'BUSINESS', '2018-08-10 07:49:10', '2018-08-10 07:49:10'),
(13, 1, 'Super Admin', 'admin', 'create', 'article', 1, 'Spotlight on Geordie Willis', '2018-08-10 07:50:42', '2018-08-10 07:50:42'),
(14, 1, 'Super Admin', 'admin', 'create', 'article', 2, 'Spotlight on Geordie Willis', '2018-08-10 07:51:00', '2018-08-10 07:51:00'),
(15, 1, 'Super Admin', 'admin', 'update', 'article', 2, 'Spotlight on Geordie Willis', '2018-08-10 07:51:05', '2018-08-10 07:51:05'),
(16, 1, 'Super Admin', 'admin', 'create', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-10 07:51:26', '2018-08-10 07:51:26'),
(17, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-10 07:51:28', '2018-08-10 07:51:28'),
(18, 1, 'Super Admin', 'admin', 'create', 'article', 4, 'Spotlight on Geordie Willis', '2018-08-10 07:51:38', '2018-08-10 07:51:38'),
(19, 1, 'Super Admin', 'admin', 'update', 'article', 4, 'Spotlight on Geordie Willis', '2018-08-10 07:51:41', '2018-08-10 07:51:41'),
(20, 1, 'Super Admin', 'admin', 'create', 'article', 5, 'Nhân bản của Spotlight on Geordie Willis', '2018-08-10 07:51:46', '2018-08-10 07:51:46'),
(21, 1, 'Super Admin', 'admin', 'update', 'article', 5, 'Brunch on Saturday', '2018-08-10 07:52:31', '2018-08-10 07:52:31'),
(22, 1, 'Super Admin', 'admin', 'create', 'article', 6, 'Brunch on Saturday', '2018-08-10 07:52:38', '2018-08-10 07:52:38'),
(23, 1, 'Super Admin', 'admin', 'update', 'article', 6, 'Brunch on Saturday', '2018-08-10 07:52:40', '2018-08-10 07:52:40'),
(24, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 07:57:04', '2018-08-10 07:57:04'),
(25, 1, 'Super Admin', 'admin', 'create', 'gallery', 2, 'company', '2018-08-10 08:27:00', '2018-08-10 08:27:00'),
(26, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 08:29:42', '2018-08-10 08:29:42'),
(27, 1, 'Super Admin', 'admin', 'create', 'blog', 2, 'LIFE STYLE', '2018-08-10 08:32:50', '2018-08-10 08:32:50'),
(28, 1, 'Super Admin', 'admin', 'create', 'article', 7, 'Spotlight on Geordie Willis', '2018-08-10 08:40:39', '2018-08-10 08:40:39'),
(29, 1, 'Super Admin', 'admin', 'create', 'article', 8, 'Blackpool Lancashire', '2018-08-10 08:40:54', '2018-08-10 08:40:54'),
(30, 1, 'Super Admin', 'admin', 'update', 'article', 8, 'Blackpool Lancashire', '2018-08-10 08:41:18', '2018-08-10 08:41:18'),
(31, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 08:56:53', '2018-08-10 08:56:53'),
(32, 1, 'Super Admin', 'admin', 'create', 'collection', 1, 'Men', '2018-08-10 09:05:39', '2018-08-10 09:05:39'),
(33, 1, 'Super Admin', 'admin', 'update', 'collection', 1, 'Men', '2018-08-10 09:06:24', '2018-08-10 09:06:24'),
(34, 1, 'Super Admin', 'admin', 'create', 'collection', 2, 'Women', '2018-08-10 09:06:40', '2018-08-10 09:06:40'),
(35, 1, 'Super Admin', 'admin', 'create', 'collection', 3, 'Bag', '2018-08-10 09:06:56', '2018-08-10 09:06:56'),
(36, 1, 'Super Admin', 'admin', 'create', 'collection', 4, 'Shoes', '2018-08-10 09:07:12', '2018-08-10 09:07:12'),
(37, 1, 'Super Admin', 'admin', 'create', 'product', 1, 'Fort Collins Men’s Cotton Coat', '2018-08-10 09:09:07', '2018-08-10 09:09:07'),
(38, 1, 'Super Admin', 'admin', 'create', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-08-10 09:09:31', '2018-08-10 09:09:31'),
(39, 1, 'Super Admin', 'admin', 'update', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-08-10 09:09:47', '2018-08-10 09:09:47'),
(40, 1, 'Super Admin', 'admin', 'update', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-08-10 09:10:27', '2018-08-10 09:10:27'),
(41, 1, 'Super Admin', 'admin', 'create', 'product', 3, 'Black Tote', '2018-08-10 09:12:26', '2018-08-10 09:12:26'),
(42, 1, 'Super Admin', 'admin', 'update', 'product', 1, 'Fort Collins Men’s Cotton Coat', '2018-08-10 09:12:41', '2018-08-10 09:12:41'),
(43, 1, 'Super Admin', 'admin', 'update', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-08-10 09:13:01', '2018-08-10 09:13:01'),
(44, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-10 09:13:12', '2018-08-10 09:13:12'),
(45, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-10 09:13:22', '2018-08-10 09:13:22'),
(46, 1, 'Super Admin', 'admin', 'create', 'product', 4, 'Double Breasted Coat', '2018-08-10 09:14:11', '2018-08-10 09:14:11'),
(47, 1, 'Super Admin', 'admin', 'create', 'product', 5, 'Utsukushii Women’s Handbag', '2018-08-10 09:14:48', '2018-08-10 09:14:48'),
(48, 1, 'Super Admin', 'admin', 'create', 'product', 6, 'Women’s Handbag', '2018-08-10 09:15:39', '2018-08-10 09:15:39'),
(49, 1, 'Super Admin', 'admin', 'create', 'product', 7, 'Handbag White', '2018-08-10 09:16:44', '2018-08-10 09:16:44'),
(50, 1, 'Super Admin', 'admin', 'create', 'product', 8, 'Daphne Women’s Handbag', '2018-08-10 09:18:38', '2018-08-10 09:18:38'),
(51, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-10 09:20:44', '2018-08-10 09:20:44'),
(52, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-10 09:21:33', '2018-08-10 09:21:33'),
(53, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-10 09:24:50', '2018-08-10 09:24:50'),
(54, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-10 09:56:51', '2018-08-10 09:56:51'),
(55, 1, 'Super Admin', 'admin', 'update', 'product', 8, 'Daphne Women’s Handbag', '2018-08-10 09:58:47', '2018-08-10 09:58:47'),
(56, 1, 'Super Admin', 'admin', 'update', 'product', 8, 'Daphne Women’s Handbag', '2018-08-10 09:58:55', '2018-08-10 09:58:55'),
(57, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-11 02:37:43', '2018-08-11 02:37:43'),
(58, 1, 'Super Admin', 'admin', 'create', 'menu', 1, 'Header', '2018-08-11 02:38:14', '2018-08-11 02:38:14'),
(59, 1, 'Super Admin', 'admin', 'create', 'menu', 2, 'Trang chủ', '2018-08-11 02:38:34', '2018-08-11 02:38:34'),
(60, 1, 'Super Admin', 'admin', 'create', 'menu', 3, 'Giới thiệu', '2018-08-11 02:38:53', '2018-08-11 02:38:53'),
(61, 1, 'Super Admin', 'admin', 'create', 'menu', 4, 'Sản phẩm', '2018-08-11 02:39:29', '2018-08-11 02:39:29'),
(62, 1, 'Super Admin', 'admin', 'create', 'menu', 5, 'Sản phẩm 1', '2018-08-11 02:40:01', '2018-08-11 02:40:01'),
(63, 1, 'Super Admin', 'admin', 'create', 'menu', 6, 'Sản phẩm 2', '2018-08-11 02:40:21', '2018-08-11 02:40:21'),
(64, 1, 'Super Admin', 'admin', 'create', 'menu', 7, 'Sản phẩm 3', '2018-08-11 02:40:54', '2018-08-11 02:40:54'),
(65, 1, 'Super Admin', 'admin', 'create', 'menu', 8, 'Tin tức', '2018-08-11 02:41:29', '2018-08-11 02:41:29'),
(66, 1, 'Super Admin', 'admin', 'create', 'menu', 9, 'Tin tức 1', '2018-08-11 02:41:51', '2018-08-11 02:41:51'),
(67, 1, 'Super Admin', 'admin', 'create', 'menu', 10, 'Tin tức 2', '2018-08-11 02:42:10', '2018-08-11 02:42:10'),
(68, 1, 'Super Admin', 'admin', 'create', 'menu', 11, 'Tin tức 3', '2018-08-11 02:42:28', '2018-08-11 02:42:28'),
(69, 1, 'Super Admin', 'admin', 'create', 'menu', 12, 'Khách hàng', '2018-08-11 02:42:51', '2018-08-11 02:42:51'),
(70, 1, 'Super Admin', 'admin', 'create', 'menu', 13, 'Liên hệ', '2018-08-11 02:43:09', '2018-08-11 02:43:09'),
(71, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-11 03:51:40', '2018-08-11 03:51:40'),
(72, 1, 'Super Admin', 'admin', 'create', 'page', 1, 'Contact', '2018-08-11 03:52:31', '2018-08-11 03:52:31'),
(73, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-11 03:52:36', '2018-08-11 03:52:36'),
(74, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-11 04:17:42', '2018-08-11 04:17:42'),
(75, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-11 04:20:13', '2018-08-11 04:20:13'),
(76, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-11 04:22:51', '2018-08-11 04:22:51'),
(77, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-11 04:26:38', '2018-08-11 04:26:38'),
(78, 1, 'Super Admin', 'admin', 'update', 'menu', 13, 'Liên hệ', '2018-08-11 04:26:54', '2018-08-11 04:26:54'),
(79, 1, 'Super Admin', 'admin', 'update', 'menu', 1, 'Header', '2018-08-11 04:26:56', '2018-08-11 04:26:56'),
(80, 1, 'Super Admin', 'admin', 'create', 'page', 2, 'Giới thiệu', '2018-08-11 04:53:05', '2018-08-11 04:53:05'),
(81, 1, 'Super Admin', 'admin', 'update', 'menu', 3, 'Giới thiệu', '2018-08-11 04:53:23', '2018-08-11 04:53:23'),
(82, 1, 'Super Admin', 'admin', 'update', 'menu', 1, 'Header', '2018-08-11 04:53:25', '2018-08-11 04:53:25'),
(83, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-11 07:08:43', '2018-08-11 07:08:43'),
(84, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-11 07:19:22', '2018-08-11 07:19:22'),
(85, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-11 07:35:40', '2018-08-11 07:35:40'),
(86, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-11 07:37:19', '2018-08-11 07:37:19'),
(87, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-11 07:40:07', '2018-08-11 07:40:07'),
(88, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-11 07:43:19', '2018-08-11 07:43:19'),
(89, 1, 'Super Admin', 'admin', 'create', 'blog', 3, 'What we do', '2018-08-11 07:57:11', '2018-08-11 07:57:11'),
(90, 1, 'Super Admin', 'admin', 'create', 'article', 9, 'Supersonic import', '2018-08-11 07:58:29', '2018-08-11 07:58:29'),
(91, 1, 'Super Admin', 'admin', 'create', 'article', 10, 'Effortless customization', '2018-08-11 07:58:55', '2018-08-11 07:58:55'),
(92, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-11 07:59:09', '2018-08-11 07:59:09'),
(93, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-11 08:00:33', '2018-08-11 08:00:33'),
(94, 1, 'Super Admin', 'admin', 'create', 'article', 11, 'Powerful shortcodes', '2018-08-11 08:02:05', '2018-08-11 08:02:05'),
(95, 1, 'Super Admin', 'admin', 'update', 'article', 11, 'Powerful shortcodes', '2018-08-11 08:02:24', '2018-08-11 08:02:24'),
(96, 1, 'Super Admin', 'admin', 'create', 'article', 12, 'Easy page builder', '2018-08-11 08:02:57', '2018-08-11 08:02:57'),
(97, 1, 'Super Admin', 'admin', 'update', 'article', 12, 'Easy page builder', '2018-08-11 08:03:41', '2018-08-11 08:03:41'),
(98, 1, 'Super Admin', 'admin', 'create', 'article', 13, 'Fast loading speed', '2018-08-11 08:04:39', '2018-08-11 08:04:39'),
(99, 1, 'Super Admin', 'admin', 'update', 'article', 13, 'Fast loading speed', '2018-08-11 08:04:51', '2018-08-11 08:04:51'),
(100, 1, 'Super Admin', 'admin', 'create', 'article', 14, 'Fully Responsive', '2018-08-11 08:05:23', '2018-08-11 08:05:23'),
(101, 1, 'Super Admin', 'admin', 'update', 'article', 14, 'Fully Responsive', '2018-08-11 08:05:30', '2018-08-11 08:05:30'),
(102, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-11 08:39:25', '2018-08-11 08:39:25'),
(103, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-11 08:39:58', '2018-08-11 08:39:58'),
(104, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-11 09:06:59', '2018-08-11 09:06:59'),
(105, 1, 'Super Admin', 'admin', 'create', 'blog', 4, 'Our projects', '2018-08-11 09:13:03', '2018-08-11 09:13:03'),
(106, 1, 'Super Admin', 'admin', 'create', 'article', 15, 'A perfect day in New York', '2018-08-11 09:14:05', '2018-08-11 09:14:05'),
(107, 1, 'Super Admin', 'admin', 'create', 'article', 16, 'Giving back to your fans', '2018-08-11 09:14:22', '2018-08-11 09:14:22'),
(108, 1, 'Super Admin', 'admin', 'update', 'article', 16, 'Giving back to your fans', '2018-08-11 09:14:37', '2018-08-11 09:14:37'),
(109, 1, 'Super Admin', 'admin', 'create', 'article', 17, 'Reintroducing a key tech media publisher', '2018-08-11 09:15:00', '2018-08-11 09:15:00'),
(110, 1, 'Super Admin', 'admin', 'update', 'article', 17, 'Reintroducing a key tech media publisher', '2018-08-11 09:15:35', '2018-08-11 09:15:35'),
(111, 1, 'Super Admin', 'admin', 'create', 'article', 18, 'Unifying a brand identity and experience', '2018-08-11 09:15:52', '2018-08-11 09:15:52'),
(112, 1, 'Super Admin', 'admin', 'update', 'article', 18, 'Unifying a brand identity and experience', '2018-08-11 09:16:04', '2018-08-11 09:16:04'),
(113, 1, 'Super Admin', 'admin', 'update', 'article', 18, 'Unifying a brand identity and experience', '2018-08-11 09:16:15', '2018-08-11 09:16:15'),
(114, 1, 'Super Admin', 'admin', 'create', 'article', 19, 'Explore nature', '2018-08-11 09:16:31', '2018-08-11 09:16:31'),
(115, 1, 'Super Admin', 'admin', 'update', 'article', 19, 'Explore nature', '2018-08-11 09:16:55', '2018-08-11 09:16:55'),
(116, 1, 'Super Admin', 'admin', 'create', 'article', 20, 'The end of social media?', '2018-08-11 09:17:03', '2018-08-11 09:17:03'),
(117, 1, 'Super Admin', 'admin', 'update', 'article', 20, 'The end of social media?', '2018-08-11 09:17:22', '2018-08-11 09:17:22'),
(118, 1, 'Super Admin', 'admin', 'update', 'article', 15, 'A perfect day in New York', '2018-08-11 09:17:34', '2018-08-11 09:17:34'),
(119, 1, 'Super Admin', 'admin', 'update', 'article', 16, 'Giving back to your fans', '2018-08-11 09:17:41', '2018-08-11 09:17:41'),
(120, 1, 'Super Admin', 'admin', 'update', 'article', 17, 'Reintroducing a key tech media publisher', '2018-08-11 09:17:49', '2018-08-11 09:17:49'),
(121, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-11 09:18:10', '2018-08-11 09:18:10'),
(122, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-11 09:49:33', '2018-08-11 09:49:33'),
(123, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-11 09:50:09', '2018-08-11 09:50:09'),
(124, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-11 09:52:22', '2018-08-11 09:52:22'),
(125, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-14 02:06:51', '2018-08-14 02:06:51'),
(126, 1, 'Super Admin', 'admin', 'create', 'menu', 14, 'Menu right', '2018-08-14 02:07:56', '2018-08-14 02:07:56'),
(127, 1, 'Super Admin', 'admin', 'update', 'menu', 14, 'Our service', '2018-08-14 02:08:36', '2018-08-14 02:08:36'),
(128, 1, 'Super Admin', 'admin', 'create', 'menu', 15, 'Life Style', '2018-08-14 02:09:25', '2018-08-14 02:09:25'),
(129, 1, 'Super Admin', 'admin', 'create', 'menu', 16, 'New Trend', '2018-08-14 02:09:36', '2018-08-14 02:09:36'),
(130, 1, 'Super Admin', 'admin', 'create', 'menu', 17, 'Post Format', '2018-08-14 02:09:49', '2018-08-14 02:09:49'),
(131, 1, 'Super Admin', 'admin', 'create', 'menu', 18, 'What we do', '2018-08-14 02:10:00', '2018-08-14 02:10:00'),
(132, 1, 'Super Admin', 'admin', 'update', 'menu', 14, 'Our service', '2018-08-14 02:10:05', '2018-08-14 02:10:05'),
(133, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-14 02:21:44', '2018-08-14 02:21:44'),
(134, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-14 02:32:36', '2018-08-14 02:32:36'),
(135, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-14 02:32:55', '2018-08-14 02:32:55'),
(136, 1, 'Super Admin', 'admin', 'create', 'menu', 19, 'What we do', '2018-08-14 02:34:13', '2018-08-14 02:34:13'),
(137, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-14 02:35:28', '2018-08-14 02:35:28'),
(138, 1, 'Super Admin', 'admin', 'create', 'menu', 20, 'What we do', '2018-08-14 02:35:46', '2018-08-14 02:35:46'),
(139, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-14 02:36:16', '2018-08-14 02:36:16'),
(140, 1, 'Super Admin', 'admin', 'create', 'menu', 21, 'What we do', '2018-08-14 02:37:06', '2018-08-14 02:37:06'),
(141, 1, 'Super Admin', 'admin', 'update', 'blog', 2, 'LIFE STYLE', '2018-08-14 02:38:46', '2018-08-14 02:38:46'),
(142, 1, 'Super Admin', 'admin', 'create', 'menu', 22, 'Right wwd', '2018-08-14 02:41:08', '2018-08-14 02:41:08'),
(143, 1, 'Super Admin', 'admin', 'create', 'menu', 23, 'con1', '2018-08-14 02:41:44', '2018-08-14 02:41:44'),
(144, 1, 'Super Admin', 'admin', 'create', 'menu', 24, 'con2', '2018-08-14 02:41:55', '2018-08-14 02:41:55'),
(145, 1, 'Super Admin', 'admin', 'create', 'menu', 25, 'con3', '2018-08-14 02:42:07', '2018-08-14 02:42:07'),
(146, 1, 'Super Admin', 'admin', 'create', 'menu', 26, 'con4', '2018-08-14 02:42:21', '2018-08-14 02:42:21'),
(147, 1, 'Super Admin', 'admin', 'update', 'menu', 22, 'Right wwd', '2018-08-14 02:42:24', '2018-08-14 02:42:24'),
(148, 1, 'Super Admin', 'admin', 'update', 'blog', 2, 'LIFE STYLE', '2018-08-14 02:46:41', '2018-08-14 02:46:41'),
(149, 1, 'Super Admin', 'admin', 'update', 'blog', 2, 'LIFE STYLE', '2018-08-14 02:47:03', '2018-08-14 02:47:03'),
(150, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-14 04:01:51', '2018-08-14 04:01:51'),
(151, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 04:02:08', '2018-08-14 04:02:08'),
(152, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-14 07:12:55', '2018-08-14 07:12:55'),
(153, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:28:22', '2018-08-14 07:28:22'),
(154, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:28:54', '2018-08-14 07:28:54'),
(155, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:29:25', '2018-08-14 07:29:25'),
(156, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:29:39', '2018-08-14 07:29:39'),
(157, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:31:52', '2018-08-14 07:31:52'),
(158, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:35:33', '2018-08-14 07:35:33'),
(159, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:35:53', '2018-08-14 07:35:53'),
(160, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:38:01', '2018-08-14 07:38:01'),
(161, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:38:14', '2018-08-14 07:38:14'),
(162, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:40:44', '2018-08-14 07:40:44'),
(163, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:40:57', '2018-08-14 07:40:57'),
(164, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:41:17', '2018-08-14 07:41:17'),
(165, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 07:41:26', '2018-08-14 07:41:26'),
(166, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:42:53', '2018-08-14 07:42:53'),
(167, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:43:06', '2018-08-14 07:43:06'),
(168, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:43:29', '2018-08-14 07:43:29'),
(169, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:43:37', '2018-08-14 07:43:37'),
(170, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'a', '2018-08-14 07:43:43', '2018-08-14 07:43:43'),
(171, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:43:52', '2018-08-14 07:43:52'),
(172, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:44:13', '2018-08-14 07:44:13'),
(173, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:44:31', '2018-08-14 07:44:31'),
(174, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:44:44', '2018-08-14 07:44:44'),
(175, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:44:55', '2018-08-14 07:44:55'),
(176, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:45:13', '2018-08-14 07:45:13'),
(177, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-14 07:45:25', '2018-08-14 07:45:25'),
(178, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-14 07:51:49', '2018-08-14 07:51:49'),
(179, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-14 07:52:12', '2018-08-14 07:52:12'),
(180, 1, 'Super Admin', 'admin', 'update', 'blog', 4, 'Our projects', '2018-08-14 07:53:21', '2018-08-14 07:53:21'),
(181, 1, 'Super Admin', 'admin', 'update', 'blog', 4, 'Our projects', '2018-08-14 07:53:40', '2018-08-14 07:53:40'),
(182, 1, 'Super Admin', 'admin', 'update', 'gallery', 2, 'company', '2018-08-14 07:56:02', '2018-08-14 07:56:02'),
(183, 1, 'Super Admin', 'admin', 'update', 'article', 18, 'a', '2018-08-14 07:58:12', '2018-08-14 07:58:12'),
(184, 1, 'Super Admin', 'admin', 'update', 'article', 18, 'a', '2018-08-14 07:58:34', '2018-08-14 07:58:34'),
(185, 1, 'Super Admin', 'admin', 'update', 'article', 18, 'Unifying a brand identity and experience', '2018-08-14 07:58:54', '2018-08-14 07:58:54'),
(186, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'a', '2018-08-14 07:59:49', '2018-08-14 07:59:49'),
(187, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-14 08:00:03', '2018-08-14 08:00:03'),
(188, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-14 08:00:09', '2018-08-14 08:00:09'),
(189, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-14 08:00:23', '2018-08-14 08:00:23'),
(190, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-14 08:01:09', '2018-08-14 08:01:09'),
(191, 1, 'Super Admin', 'admin', 'update', 'blog', 3, 'What we do', '2018-08-14 08:03:40', '2018-08-14 08:03:40'),
(192, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'a', '2018-08-14 08:12:14', '2018-08-14 08:12:14'),
(193, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-14 08:12:28', '2018-08-14 08:12:28'),
(194, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-14 08:12:35', '2018-08-14 08:12:35'),
(195, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-14 08:12:57', '2018-08-14 08:12:57'),
(196, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Contact', '2018-08-14 08:13:06', '2018-08-14 08:13:06'),
(197, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:15:16', '2018-08-14 08:15:16'),
(198, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:15:45', '2018-08-14 08:15:45'),
(199, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:16:20', '2018-08-14 08:16:20'),
(200, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:16:41', '2018-08-14 08:16:41'),
(201, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:17:07', '2018-08-14 08:17:07'),
(202, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:17:37', '2018-08-14 08:17:37'),
(203, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:18:19', '2018-08-14 08:18:19'),
(204, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:18:43', '2018-08-14 08:18:43'),
(205, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:28:52', '2018-08-14 08:28:52'),
(206, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:29:05', '2018-08-14 08:29:05'),
(207, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:35:00', '2018-08-14 08:35:00'),
(208, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:36:22', '2018-08-14 08:36:22'),
(209, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:36:41', '2018-08-14 08:36:41'),
(210, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:39:47', '2018-08-14 08:39:47'),
(211, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:39:58', '2018-08-14 08:39:58'),
(212, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:40:17', '2018-08-14 08:40:17'),
(213, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:40:26', '2018-08-14 08:40:26'),
(214, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:40:40', '2018-08-14 08:40:40'),
(215, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:41:05', '2018-08-14 08:41:05'),
(216, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:41:21', '2018-08-14 08:41:21'),
(217, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:41:38', '2018-08-14 08:41:38'),
(218, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:42:11', '2018-08-14 08:42:11'),
(219, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:42:21', '2018-08-14 08:42:21'),
(220, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:42:44', '2018-08-14 08:42:44'),
(221, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:47:22', '2018-08-14 08:47:22'),
(222, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:47:41', '2018-08-14 08:47:41'),
(223, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:47:49', '2018-08-14 08:47:49'),
(224, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:48:12', '2018-08-14 08:48:12'),
(225, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:48:20', '2018-08-14 08:48:20'),
(226, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:48:31', '2018-08-14 08:48:31'),
(227, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:48:52', '2018-08-14 08:48:52'),
(228, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:49:15', '2018-08-14 08:49:15'),
(229, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:51:04', '2018-08-14 08:51:04'),
(230, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:51:40', '2018-08-14 08:51:40'),
(231, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:51:50', '2018-08-14 08:51:50'),
(232, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:52:02', '2018-08-14 08:52:02'),
(233, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:52:15', '2018-08-14 08:52:15'),
(234, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:52:26', '2018-08-14 08:52:26'),
(235, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 08:52:58', '2018-08-14 08:52:58'),
(236, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 09:13:18', '2018-08-14 09:13:18'),
(237, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 09:13:33', '2018-08-14 09:13:33'),
(238, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 09:13:46', '2018-08-14 09:13:46'),
(239, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 09:13:59', '2018-08-14 09:13:59'),
(240, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-14 09:15:15', '2018-08-14 09:15:15'),
(241, 1, 'Super Admin', 'admin', 'update', 'menu', 1, 'Header', '2018-08-14 09:23:20', '2018-08-14 09:23:20'),
(242, 1, 'Super Admin', 'admin', 'create', 'blog', 5, 'sidebar', '2018-08-14 09:31:00', '2018-08-14 09:31:00'),
(243, 1, 'Super Admin', 'admin', 'update', 'blog', 5, 'sidebar', '2018-08-14 09:31:13', '2018-08-14 09:31:13'),
(244, 1, 'Super Admin', 'admin', 'delete', 'blog', 5, 'sidebar', '2018-08-14 09:32:21', '2018-08-14 09:32:21'),
(245, 1, 'Super Admin', 'admin', 'create', 'blog', 6, 'sidebar', '2018-08-14 09:37:14', '2018-08-14 09:37:14'),
(246, 1, 'Super Admin', 'admin', 'delete', 'blog', 6, 'sidebar', '2018-08-14 09:38:00', '2018-08-14 09:38:00'),
(247, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-15 04:11:28', '2018-08-15 04:11:28'),
(248, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 02:10:28', '2018-08-16 02:10:28'),
(249, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 02:13:21', '2018-08-16 02:13:21'),
(250, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 02:19:06', '2018-08-16 02:19:06'),
(251, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 02:19:40', '2018-08-16 02:19:40'),
(252, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 02:20:16', '2018-08-16 02:20:16'),
(253, 1, 'Super Admin', 'admin', 'create', 'article', 21, 'test', '2018-08-16 02:21:14', '2018-08-16 02:21:14'),
(254, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-16 02:24:34', '2018-08-16 02:24:34'),
(255, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 02:28:17', '2018-08-16 02:28:17'),
(256, 1, 'Super Admin', 'admin', 'update', 'menu', 8, 'Tin tức', '2018-08-16 02:29:01', '2018-08-16 02:29:01'),
(257, 1, 'Super Admin', 'admin', 'delete', 'article', 21, 'test', '2018-08-16 02:35:15', '2018-08-16 02:35:15'),
(258, 1, 'Super Admin', 'admin', 'create', 'article', 22, 'test', '2018-08-16 02:35:44', '2018-08-16 02:35:44'),
(259, 1, 'Super Admin', 'admin', 'update', 'article', 22, 'test', '2018-08-16 02:36:16', '2018-08-16 02:36:16'),
(260, 1, 'Super Admin', 'admin', 'create', 'blog', 7, 'What we do', '2018-08-16 02:40:18', '2018-08-16 02:40:18'),
(261, 1, 'Super Admin', 'admin', 'delete', 'blog', 3, 'What we do', '2018-08-16 02:40:42', '2018-08-16 02:40:42'),
(262, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-16 02:41:04', '2018-08-16 02:41:04'),
(263, 1, 'Super Admin', 'admin', 'update', 'article', 14, 'Fully Responsive', '2018-08-16 02:41:32', '2018-08-16 02:41:32'),
(264, 1, 'Super Admin', 'admin', 'update', 'article', 13, 'Fast loading speed', '2018-08-16 02:41:39', '2018-08-16 02:41:39'),
(265, 1, 'Super Admin', 'admin', 'update', 'article', 12, 'Easy page builder', '2018-08-16 02:41:45', '2018-08-16 02:41:45'),
(266, 1, 'Super Admin', 'admin', 'update', 'article', 11, 'Powerful shortcodes', '2018-08-16 02:41:53', '2018-08-16 02:41:53'),
(267, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-16 02:41:58', '2018-08-16 02:41:58'),
(268, 1, 'Super Admin', 'admin', 'update', 'article', 22, 'test', '2018-08-16 02:42:15', '2018-08-16 02:42:15'),
(269, 1, 'Super Admin', 'admin', 'delete', 'article', 22, 'test', '2018-08-16 02:42:51', '2018-08-16 02:42:51'),
(270, 1, 'Super Admin', 'admin', 'update', 'blog', 7, 'What we do', '2018-08-16 02:43:25', '2018-08-16 02:43:25'),
(271, 1, 'Super Admin', 'admin', 'update', 'blog', 4, 'Our projects', '2018-08-16 02:44:00', '2018-08-16 02:44:00'),
(272, 1, 'Super Admin', 'admin', 'update', 'blog', 4, 'Our projects', '2018-08-16 02:49:08', '2018-08-16 02:49:08'),
(273, 1, 'Super Admin', 'admin', 'create', 'article', 23, 'what we do', '2018-08-16 02:54:17', '2018-08-16 02:54:17'),
(274, 1, 'Super Admin', 'admin', 'delete', 'article', 23, 'what we do', '2018-08-16 02:54:45', '2018-08-16 02:54:45'),
(275, 1, 'Super Admin', 'admin', 'create', 'article', 24, 'test', '2018-08-16 02:55:10', '2018-08-16 02:55:10'),
(276, 1, 'Super Admin', 'admin', 'delete', 'article', 24, 'test', '2018-08-16 03:01:13', '2018-08-16 03:01:13'),
(277, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-16 03:02:25', '2018-08-16 03:02:25'),
(278, 1, 'Super Admin', 'admin', 'update', 'article', 18, 'Unifying a brand identity and experience', '2018-08-16 03:02:35', '2018-08-16 03:02:35'),
(279, 1, 'Super Admin', 'admin', 'delete', 'blog', 2, 'LIFE STYLE', '2018-08-16 03:04:06', '2018-08-16 03:04:06'),
(280, 1, 'Super Admin', 'admin', 'create', 'blog', 8, 'LIFE STYLE', '2018-08-16 03:04:09', '2018-08-16 03:04:09'),
(281, 1, 'Super Admin', 'admin', 'update', 'article', 8, 'Blackpool Lancashire', '2018-08-16 03:04:23', '2018-08-16 03:04:23'),
(282, 1, 'Super Admin', 'admin', 'update', 'article', 7, 'Spotlight on Geordie Willis', '2018-08-16 03:04:33', '2018-08-16 03:04:33'),
(283, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 03:05:07', '2018-08-16 03:05:07'),
(284, 1, 'Super Admin', 'admin', 'update', 'article', 7, 'Spotlight on Geordie Willis', '2018-08-16 03:05:15', '2018-08-16 03:05:15'),
(285, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 03:07:39', '2018-08-16 03:07:39'),
(286, 1, 'Super Admin', 'admin', 'update', 'article', 7, 'Spotlight on Geordie Willis', '2018-08-16 03:07:58', '2018-08-16 03:07:58'),
(287, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 10:17:38', '2018-08-16 10:17:38'),
(288, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 11:30:38', '2018-08-16 11:30:38'),
(289, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 11:31:11', '2018-08-16 11:31:11'),
(290, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 11:39:53', '2018-08-16 11:39:53'),
(291, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 11:40:16', '2018-08-16 11:40:16'),
(292, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 11:43:00', '2018-08-16 11:43:00'),
(293, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-16 11:45:36', '2018-08-16 11:45:36'),
(294, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-16 11:49:51', '2018-08-16 11:49:51'),
(295, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-16 11:50:36', '2018-08-16 11:50:36'),
(296, 1, 'Super Admin', 'admin', 'update', 'article', 11, 'Powerful shortcodes', '2018-08-16 11:52:59', '2018-08-16 11:52:59'),
(297, 1, 'Super Admin', 'admin', 'update', 'article', 11, 'Powerful shortcodes', '2018-08-16 11:53:24', '2018-08-16 11:53:24'),
(298, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 11:59:17', '2018-08-16 11:59:17'),
(299, 1, 'Super Admin', 'admin', 'update', 'article', 11, 'Powerful shortcodes', '2018-08-16 12:00:29', '2018-08-16 12:00:29'),
(300, 1, 'Super Admin', 'admin', 'update', 'article', 11, 'Powerful shortcodes', '2018-08-16 12:00:44', '2018-08-16 12:00:44'),
(301, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-16 12:01:47', '2018-08-16 12:01:47'),
(302, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-16 12:02:40', '2018-08-16 12:02:40'),
(303, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-16 12:02:58', '2018-08-16 12:02:58'),
(304, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-16 12:03:08', '2018-08-16 12:03:08'),
(305, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 12:03:31', '2018-08-16 12:03:31'),
(306, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 12:03:38', '2018-08-16 12:03:38'),
(307, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-16 12:03:56', '2018-08-16 12:03:56'),
(308, 1, 'Super Admin', 'admin', 'create', 'blog', 9, 'Hỗ Trợ Khách Hàng', '2018-08-16 12:08:03', '2018-08-16 12:08:03'),
(309, 1, 'Super Admin', 'admin', 'create', 'article', 25, 'Hỗ trợ 1', '2018-08-16 12:08:24', '2018-08-16 12:08:24'),
(310, 1, 'Super Admin', 'admin', 'update', 'article', 25, 'Hỗ trợ 1', '2018-08-16 12:09:06', '2018-08-16 12:09:06'),
(311, 1, 'Super Admin', 'admin', 'update', 'article', 25, 'Hỗ trợ 1', '2018-08-16 12:10:13', '2018-08-16 12:10:13'),
(312, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 12:11:28', '2018-08-16 12:11:28'),
(313, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-16 12:12:02', '2018-08-16 12:12:02'),
(314, 1, 'Super Admin', 'admin', 'update', 'article', 7, 'Spotlight on Geordie Willis', '2018-08-16 12:12:21', '2018-08-16 12:12:21'),
(315, 1, 'Super Admin', 'admin', 'update', 'article', 7, 'Spotlight on Geordie Willis', '2018-08-16 12:13:15', '2018-08-16 12:13:15'),
(316, 1, 'Super Admin', 'admin', 'update', 'article', 8, 'Blackpool Lancashire', '2018-08-16 12:13:47', '2018-08-16 12:13:47'),
(317, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-16 12:20:09', '2018-08-16 12:20:09'),
(318, 1, 'Super Admin', 'admin', 'update', 'menu', 12, 'Khách hàng', '2018-08-16 12:30:17', '2018-08-16 12:30:17'),
(319, 1, 'Super Admin', 'admin', 'update', 'menu', 1, 'Header', '2018-08-16 12:30:26', '2018-08-16 12:30:26'),
(320, 1, 'Super Admin', 'admin', 'update', 'product', 8, 'Daphne Women’s Handbag', '2018-08-16 12:51:41', '2018-08-16 12:51:41'),
(321, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-17 02:14:12', '2018-08-17 02:14:12'),
(322, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-17 02:14:24', '2018-08-17 02:14:24'),
(323, 1, 'Super Admin', 'admin', 'update', 'menu', 23, 'Title 1', '2018-08-17 02:15:36', '2018-08-17 02:15:36'),
(324, 1, 'Super Admin', 'admin', 'update', 'menu', 24, 'Tiltle', '2018-08-17 02:15:43', '2018-08-17 02:15:43'),
(325, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 02:29:12', '2018-08-17 02:29:12'),
(326, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-17 02:52:27', '2018-08-17 02:52:27'),
(327, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:53:13', '2018-08-17 02:53:13'),
(328, 1, 'Super Admin', 'admin', 'restore', 'settingrestore', 0, 'file admin_2018_08_16_19_12_01.php', '2018-08-17 02:54:16', '2018-08-17 02:54:16'),
(329, 1, 'Super Admin', 'admin', 'restore', 'settingrestore', 0, 'file admin_2018_08_17_09_53_12.php', '2018-08-17 02:54:34', '2018-08-17 02:54:34'),
(330, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:55:20', '2018-08-17 02:55:20'),
(331, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:55:59', '2018-08-17 02:55:59'),
(332, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:56:33', '2018-08-17 02:56:33'),
(333, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:56:43', '2018-08-17 02:56:43'),
(334, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:57:24', '2018-08-17 02:57:24'),
(335, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:57:30', '2018-08-17 02:57:30'),
(336, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:58:01', '2018-08-17 02:58:01'),
(337, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 02:58:32', '2018-08-17 02:58:32'),
(338, 1, 'Super Admin', 'admin', 'restore', 'article', 21, 'test', '2018-08-17 03:02:35', '2018-08-17 03:02:35'),
(339, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-17 03:03:40', '2018-08-17 03:03:40'),
(340, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-17 03:03:51', '2018-08-17 03:03:51'),
(341, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-17 03:04:16', '2018-08-17 03:04:16'),
(342, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-17 03:26:51', '2018-08-17 03:26:51'),
(343, 1, 'Super Admin', 'admin', 'create', 'page', 3, 'Spotlight on Geordie Willis', '2018-08-17 03:28:37', '2018-08-17 03:28:37'),
(344, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-17 03:43:16', '2018-08-17 03:43:16'),
(345, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-17 03:48:52', '2018-08-17 03:48:52'),
(346, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 03:49:10', '2018-08-17 03:49:10'),
(347, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 03:49:16', '2018-08-17 03:49:16'),
(348, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 03:51:45', '2018-08-17 03:51:45'),
(349, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 03:52:04', '2018-08-17 03:52:04'),
(350, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 03:54:18', '2018-08-17 03:54:18'),
(351, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 03:54:57', '2018-08-17 03:54:57'),
(352, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 03:56:16', '2018-08-17 03:56:16'),
(353, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-17 04:05:00', '2018-08-17 04:05:00'),
(354, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-17 04:42:02', '2018-08-17 04:42:02'),
(355, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-17 04:42:50', '2018-08-17 04:42:50'),
(356, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-18 01:26:23', '2018-08-18 01:26:23'),
(357, 2, 'Admin', 'admin', 'create', 'coupon', 1, 'mã 1', '2018-08-18 01:29:09', '2018-08-18 01:29:09'),
(358, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-18 03:35:53', '2018-08-18 03:35:53'),
(359, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-18 03:42:47', '2018-08-18 03:42:47'),
(360, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-18 03:43:10', '2018-08-18 03:43:10'),
(361, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-18 03:46:34', '2018-08-18 03:46:34'),
(362, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-18 04:44:56', '2018-08-18 04:44:56'),
(363, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-18 04:45:30', '2018-08-18 04:45:30'),
(364, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-18 08:06:45', '2018-08-18 08:06:45'),
(365, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-18 08:07:00', '2018-08-18 08:07:00'),
(366, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-18 08:07:08', '2018-08-18 08:07:08'),
(367, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-18 08:08:08', '2018-08-18 08:08:08'),
(368, 1, 'Super Admin', 'admin', 'update', 'article', 1, 'Spotlight on Geordie Willis', '2018-08-18 08:09:24', '2018-08-18 08:09:24'),
(369, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-18 08:09:32', '2018-08-18 08:09:32'),
(370, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-18 08:12:42', '2018-08-18 08:12:42'),
(371, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-18 08:14:37', '2018-08-18 08:14:37'),
(372, 1, 'Super Admin', 'admin', 'update', 'article', 21, 'test', '2018-08-18 08:15:04', '2018-08-18 08:15:04'),
(373, 1, 'Super Admin', 'admin', 'update', 'article', 12, 'Easy page builder', '2018-08-18 08:15:43', '2018-08-18 08:15:43'),
(374, 1, 'Super Admin', 'admin', 'delete', 'article', 21, 'test', '2018-08-18 08:32:44', '2018-08-18 08:32:44'),
(375, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-18 08:33:21', '2018-08-18 08:33:21'),
(376, 1, 'Super Admin', 'admin', 'update', 'article', 12, 'Easy page builder', '2018-08-18 08:33:52', '2018-08-18 08:33:52'),
(377, 1, 'Super Admin', 'admin', 'update', 'article', 12, 'Easy page builder', '2018-08-18 08:34:40', '2018-08-18 08:34:40'),
(378, 1, 'Super Admin', 'admin', 'update', 'article', 12, 'Easy page builder', '2018-08-18 08:35:02', '2018-08-18 08:35:02'),
(379, 1, 'Super Admin', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-18 08:36:05', '2018-08-18 08:36:05'),
(380, 1, 'Super Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-18 08:36:21', '2018-08-18 08:36:21'),
(381, 1, 'Super Admin', 'admin', 'update', 'article', 13, 'Fast loading speed', '2018-08-18 08:37:09', '2018-08-18 08:37:09'),
(382, 1, 'Super Admin', 'admin', 'update', 'article', 14, 'Fully Responsive', '2018-08-18 08:37:25', '2018-08-18 08:37:25'),
(383, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-20 02:59:39', '2018-08-20 02:59:39'),
(384, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-20 03:50:41', '2018-08-20 03:50:41'),
(385, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 03:00:20', '2018-08-21 03:00:20'),
(386, 2, 'Admin', 'admin', 'logout', 'user', 0, '', '2018-08-21 03:13:56', '2018-08-21 03:13:56'),
(387, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 03:14:03', '2018-08-21 03:14:03'),
(388, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-21 03:14:29', '2018-08-21 03:14:29'),
(389, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 03:40:28', '2018-08-21 03:40:28'),
(390, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 03:40:43', '2018-08-21 03:40:43'),
(391, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-21 03:42:18', '2018-08-21 03:42:18'),
(392, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-21 03:42:29', '2018-08-21 03:42:29'),
(393, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-21 03:42:55', '2018-08-21 03:42:55'),
(394, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-08-21 03:43:29', '2018-08-21 03:43:29'),
(395, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 04:10:38', '2018-08-21 04:10:38'),
(396, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 04:13:27', '2018-08-21 04:13:27'),
(397, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 04:33:04', '2018-08-21 04:33:04'),
(398, 1, 'Super Admin', 'admin', 'update', 'menu', 5, 'Sản phẩm 1', '2018-08-21 04:33:17', '2018-08-21 04:33:17'),
(399, 1, 'Super Admin', 'admin', 'update', 'menu', 6, 'Sản phẩm 2', '2018-08-21 04:33:22', '2018-08-21 04:33:22'),
(400, 1, 'Super Admin', 'admin', 'update', 'menu', 7, 'Sản phẩm 3', '2018-08-21 04:33:28', '2018-08-21 04:33:28'),
(401, 1, 'Super Admin', 'admin', 'update', 'menu', 9, 'Tin tức 1', '2018-08-21 04:33:38', '2018-08-21 04:33:38'),
(402, 1, 'Super Admin', 'admin', 'update', 'menu', 10, 'Tin tức 2', '2018-08-21 04:33:45', '2018-08-21 04:33:45'),
(403, 1, 'Super Admin', 'admin', 'update', 'menu', 11, 'Tin tức 3', '2018-08-21 04:33:52', '2018-08-21 04:33:52'),
(404, 1, 'Super Admin', 'admin', 'update', 'menu', 1, 'Header', '2018-08-21 04:33:55', '2018-08-21 04:33:55'),
(405, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 04:36:15', '2018-08-21 04:36:15'),
(406, 1, 'Super Admin', 'admin', 'logout', 'user', 0, '', '2018-08-21 04:36:17', '2018-08-21 04:36:17'),
(407, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 04:36:19', '2018-08-21 04:36:19'),
(408, 1, 'Super Admin', 'admin', 'create', 'attribute', 1, 'Chất liệu', '2018-08-21 04:36:50', '2018-08-21 04:36:50'),
(409, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 04:37:12', '2018-08-21 04:37:12'),
(410, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 04:48:32', '2018-08-21 04:48:32'),
(411, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 04:48:34', '2018-08-21 04:48:34'),
(412, 1, 'Super Admin', 'admin', 'create', 'attribute', 2, 'Thương hiệu', '2018-08-21 04:50:22', '2018-08-21 04:50:22'),
(413, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 06:35:37', '2018-08-21 06:35:37'),
(414, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-21 07:19:53', '2018-08-21 07:19:53'),
(415, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 07:28:24', '2018-08-21 07:28:24'),
(416, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 07:29:02', '2018-08-21 07:29:02'),
(417, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-21 07:29:30', '2018-08-21 07:29:30'),
(418, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-21 07:31:43', '2018-08-21 07:31:43'),
(419, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 07:32:00', '2018-08-21 07:32:00'),
(420, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-21 07:32:10', '2018-08-21 07:32:10'),
(421, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 07:32:42', '2018-08-21 07:32:42'),
(422, 1, 'Super Admin', 'admin', 'create', 'blog', 10, 'New Trend', '2018-08-21 07:36:15', '2018-08-21 07:36:15');
INSERT INTO `history` (`id`, `user_id`, `user_name`, `user_type`, `action`, `type`, `type_id`, `type_title`, `created_at`, `updated_at`) VALUES
(423, 1, 'Super Admin', 'admin', 'create', 'blog', 11, 'Human', '2018-08-21 07:36:35', '2018-08-21 07:36:35'),
(424, 1, 'Super Admin', 'admin', 'update', 'blog', 7, 'What we do', '2018-08-21 07:37:22', '2018-08-21 07:37:22'),
(425, 1, 'Super Admin', 'admin', 'delete', 'blog', 9, 'Hỗ Trợ Khách Hàng', '2018-08-21 07:39:24', '2018-08-21 07:39:24'),
(426, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 07:41:00', '2018-08-21 07:41:00'),
(427, 1, 'Super Admin', 'admin', 'create', 'blog', 12, 'Spirit', '2018-08-21 07:43:20', '2018-08-21 07:43:20'),
(428, 1, 'Super Admin', 'admin', 'update', 'blog', 1, 'BUSINESS', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(429, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 07:46:55', '2018-08-21 07:46:55'),
(430, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 07:46:55', '2018-08-21 07:46:55'),
(431, 1, 'Super Admin', 'admin', 'update', 'collection', 1, 'Fashion', '2018-08-21 07:47:41', '2018-08-21 07:47:41'),
(432, 1, 'Super Admin', 'admin', 'update', 'collection', 2, 'Flowers', '2018-08-21 07:47:58', '2018-08-21 07:47:58'),
(433, 1, 'Super Admin', 'admin', 'create', 'attribute', 3, 'daisy', '2018-08-21 07:48:12', '2018-08-21 07:48:12'),
(434, 1, 'Super Admin', 'admin', 'create', 'attribute', 4, 'Rose', '2018-08-21 07:48:27', '2018-08-21 07:48:27'),
(435, 1, 'Super Admin', 'admin', 'update', 'collection', 3, 'Interior', '2018-08-21 07:48:28', '2018-08-21 07:48:28'),
(436, 1, 'Super Admin', 'admin', 'create', 'attribute', 5, 'Tulip', '2018-08-21 07:48:37', '2018-08-21 07:48:37'),
(437, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-21 07:48:54', '2018-08-21 07:48:54'),
(438, 1, 'Super Admin', 'admin', 'update', 'collection', 4, 'Restaurant', '2018-08-21 07:49:00', '2018-08-21 07:49:00'),
(439, 1, 'Super Admin', 'admin', 'create', 'collection', 5, 'Bag', '2018-08-21 07:49:28', '2018-08-21 07:49:28'),
(440, 1, 'Super Admin', 'admin', 'create', 'attribute', 6, 'Chair', '2018-08-21 07:49:48', '2018-08-21 07:49:48'),
(441, 1, 'Super Admin', 'admin', 'update', 'collection', 5, 'Bag', '2018-08-21 07:49:48', '2018-08-21 07:49:48'),
(442, 1, 'Super Admin', 'admin', 'update', 'collection', 5, 'Bag', '2018-08-21 07:49:48', '2018-08-21 07:49:48'),
(443, 1, 'Super Admin', 'admin', 'create', 'attribute', 7, 'Lamp', '2018-08-21 07:49:57', '2018-08-21 07:49:57'),
(444, 1, 'Super Admin', 'admin', 'create', 'collection', 6, 'Men', '2018-08-21 07:50:02', '2018-08-21 07:50:02'),
(445, 1, 'Super Admin', 'admin', 'create', 'attribute', 8, 'Table', '2018-08-21 07:50:08', '2018-08-21 07:50:08'),
(446, 1, 'Super Admin', 'admin', 'create', 'collection', 7, 'Women', '2018-08-21 07:50:19', '2018-08-21 07:50:19'),
(447, 1, 'Super Admin', 'admin', 'create', 'collection', 8, 'Shoes', '2018-08-21 07:50:33', '2018-08-21 07:50:33'),
(448, 1, 'Super Admin', 'admin', 'update', 'collection', 8, 'Shoes', '2018-08-21 07:53:28', '2018-08-21 07:53:28'),
(449, 1, 'Super Admin', 'admin', 'create', 'collection', 9, 'daisy', '2018-08-21 07:54:26', '2018-08-21 07:54:26'),
(450, 1, 'Super Admin', 'admin', 'create', 'collection', 10, 'rose', '2018-08-21 07:55:10', '2018-08-21 07:55:10'),
(451, 1, 'Super Admin', 'admin', 'create', 'collection', 11, 'tulip', '2018-08-21 07:55:21', '2018-08-21 07:55:21'),
(452, 1, 'Super Admin', 'admin', 'create', 'collection', 12, 'chair', '2018-08-21 07:56:13', '2018-08-21 07:56:13'),
(453, 1, 'Super Admin', 'admin', 'create', 'collection', 13, 'lamp', '2018-08-21 07:56:27', '2018-08-21 07:56:27'),
(454, 1, 'Super Admin', 'admin', 'create', 'collection', 14, 'table', '2018-08-21 07:56:39', '2018-08-21 07:56:39'),
(455, 1, 'Super Admin', 'admin', 'create', 'collection', 15, 'Appetizer', '2018-08-21 07:57:15', '2018-08-21 07:57:15'),
(456, 1, 'Super Admin', 'admin', 'update', 'collection', 9, 'Daisy', '2018-08-21 07:57:27', '2018-08-21 07:57:27'),
(457, 1, 'Super Admin', 'admin', 'update', 'collection', 10, 'Rose', '2018-08-21 07:57:35', '2018-08-21 07:57:35'),
(458, 1, 'Super Admin', 'admin', 'update', 'collection', 11, 'Tulip', '2018-08-21 07:57:46', '2018-08-21 07:57:46'),
(459, 1, 'Super Admin', 'admin', 'update', 'collection', 12, 'Chair', '2018-08-21 07:57:56', '2018-08-21 07:57:56'),
(460, 1, 'Super Admin', 'admin', 'update', 'collection', 13, 'Lamp', '2018-08-21 07:58:05', '2018-08-21 07:58:05'),
(461, 1, 'Super Admin', 'admin', 'update', 'collection', 14, 'Table', '2018-08-21 07:58:15', '2018-08-21 07:58:15'),
(462, 1, 'Super Admin', 'admin', 'delete', 'blog', 1, 'BUSINESS', '2018-08-21 07:58:38', '2018-08-21 07:58:38'),
(463, 1, 'Super Admin', 'admin', 'create', 'collection', 16, 'Chef Recommends', '2018-08-21 07:58:50', '2018-08-21 07:58:50'),
(464, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 07:58:56', '2018-08-21 07:58:56'),
(465, 1, 'Super Admin', 'admin', 'create', 'collection', 17, 'Main Course', '2018-08-21 07:59:09', '2018-08-21 07:59:09'),
(466, 1, 'Super Admin', 'admin', 'update', 'menu', 8, 'Tin tức', '2018-08-21 08:08:32', '2018-08-21 08:08:32'),
(467, 1, 'Super Admin', 'admin', 'update', 'menu', 1, 'Header', '2018-08-21 08:08:36', '2018-08-21 08:08:36'),
(468, 1, 'Super Admin', 'admin', 'update', 'blog', 8, 'LIFE STYLE', '2018-08-21 08:09:09', '2018-08-21 08:09:09'),
(469, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 08:10:53', '2018-08-21 08:10:53'),
(470, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 08:12:22', '2018-08-21 08:12:22'),
(471, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 08:38:25', '2018-08-21 08:38:25'),
(472, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 09:22:37', '2018-08-21 09:22:37'),
(473, 2, 'Admin', 'admin', 'create', 'attribute', 9, 'Test thuộc tính', '2018-08-21 09:31:39', '2018-08-21 09:31:39'),
(474, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 10:15:25', '2018-08-21 10:15:25'),
(475, 1, 'Super Admin', 'admin', 'create', 'attribute', 10, 'Nhựa', '2018-08-21 10:16:34', '2018-08-21 10:16:34'),
(476, 1, 'Super Admin', 'admin', 'create', 'attribute', 11, 'Inox', '2018-08-21 10:17:05', '2018-08-21 10:17:05'),
(477, 1, 'Super Admin', 'admin', 'create', 'attribute', 12, 'Thương hiệu 1', '2018-08-21 10:17:28', '2018-08-21 10:17:28'),
(478, 1, 'Super Admin', 'admin', 'create', 'attribute', 13, 'Thương hiệu 2', '2018-08-21 10:17:38', '2018-08-21 10:17:38'),
(479, 1, 'Super Admin', 'admin', 'create', 'attribute', 14, 'Thương hiệu 3', '2018-08-21 10:17:46', '2018-08-21 10:17:46'),
(480, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-21 10:18:41', '2018-08-21 10:18:41'),
(481, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-21 10:20:12', '2018-08-21 10:20:12'),
(482, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 10:22:20', '2018-08-21 10:22:20'),
(483, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-21 10:23:46', '2018-08-21 10:23:46'),
(484, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-21 10:24:15', '2018-08-21 10:24:15'),
(485, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-21 10:25:30', '2018-08-21 10:25:30'),
(486, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 11:05:02', '2018-08-21 11:05:02'),
(487, 2, 'Admin', 'admin', 'logout', 'user', 0, '', '2018-08-21 11:05:07', '2018-08-21 11:05:07'),
(488, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 11:05:22', '2018-08-21 11:05:22'),
(489, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 11:07:03', '2018-08-21 11:07:03'),
(490, 1, 'Super Admin', 'admin', 'logout', 'user', 0, '', '2018-08-21 11:07:13', '2018-08-21 11:07:13'),
(491, 3, 'Loan', 'admin', 'login', 'user', 0, '', '2018-08-21 11:07:20', '2018-08-21 11:07:20'),
(492, 3, 'Loan', 'admin', 'logout', 'user', 0, '', '2018-08-21 11:07:24', '2018-08-21 11:07:24'),
(493, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 11:07:47', '2018-08-21 11:07:47'),
(494, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 16:12:47', '2018-08-21 16:12:47'),
(495, 1, 'Super Admin', 'admin', 'update', 'product', 8, 'Daphne Women’s Handbag', '2018-08-21 16:13:05', '2018-08-21 16:13:05'),
(496, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-21 16:40:57', '2018-08-21 16:40:57'),
(497, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 16:41:38', '2018-08-21 16:41:38'),
(498, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 16:42:04', '2018-08-21 16:42:04'),
(499, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 16:42:16', '2018-08-21 16:42:16'),
(500, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-21 16:42:39', '2018-08-21 16:42:39'),
(501, 1, 'Super Admin', 'admin', 'create', 'product', 9, 'Sản phẩm 1', '2018-08-21 16:46:06', '2018-08-21 16:46:06'),
(502, 1, 'Super Admin', 'admin', 'update', 'product', 9, 'Sản phẩm 1', '2018-08-21 16:46:29', '2018-08-21 16:46:29'),
(503, 1, 'Super Admin', 'admin', 'update', 'product', 9, 'Sản phẩm 1', '2018-08-21 16:46:55', '2018-08-21 16:46:55'),
(504, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-21 16:52:19', '2018-08-21 16:52:19'),
(505, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 00:58:14', '2018-08-22 00:58:14'),
(506, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 02:41:26', '2018-08-22 02:41:26'),
(507, 1, 'Super Admin', 'admin', 'update', 'order', 7, '', '2018-08-22 02:43:52', '2018-08-22 02:43:52'),
(508, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-22 03:12:22', '2018-08-22 03:12:22'),
(509, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 03:25:23', '2018-08-22 03:25:23'),
(510, 1, 'Super Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-08-22 03:31:26', '2018-08-22 03:31:26'),
(511, 1, 'Super Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-08-22 03:32:55', '2018-08-22 03:32:55'),
(512, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 03:38:11', '2018-08-22 03:38:11'),
(513, 2, 'Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-08-22 03:38:42', '2018-08-22 03:38:42'),
(514, 2, 'Admin', 'admin', 'create', 'coupon', 2, 'Test', '2018-08-22 03:39:09', '2018-08-22 03:39:09'),
(515, 1, 'Super Admin', 'admin', 'create', 'collection', 18, 'Test nhóm sp1', '2018-08-22 03:42:15', '2018-08-22 03:42:15'),
(516, 1, 'Super Admin', 'admin', 'update', 'collection', 18, 'Test nhóm sp1', '2018-08-22 03:43:00', '2018-08-22 03:43:00'),
(517, 1, 'Super Admin', 'admin', 'create', 'collection', 19, 'con nhom 1', '2018-08-22 03:43:35', '2018-08-22 03:43:35'),
(518, 1, 'Super Admin', 'admin', 'update', 'collection', 18, 'Test nhóm sp1 ádfrdsafd', '2018-08-22 03:45:58', '2018-08-22 03:45:58'),
(519, 1, 'Super Admin', 'admin', 'update', 'collection', 19, 'con nhom 1dsa ', '2018-08-22 03:46:20', '2018-08-22 03:46:20'),
(520, 1, 'Super Admin', 'admin', 'delete', 'collection', 19, 'con nhom 1dsa ', '2018-08-22 03:46:35', '2018-08-22 03:46:35'),
(521, 1, 'Super Admin', 'admin', 'delete', 'collection', 18, 'Test nhóm sp1 ádfrdsafd', '2018-08-22 03:47:05', '2018-08-22 03:47:05'),
(522, 1, 'Super Admin', 'admin', 'update', 'collection', 1, 'Fashion', '2018-08-22 03:48:23', '2018-08-22 03:48:23'),
(523, 1, 'Super Admin', 'admin', 'create', 'article', 26, 'Test bài viết 1', '2018-08-22 03:52:30', '2018-08-22 03:52:30'),
(524, 1, 'Super Admin', 'admin', 'update', 'article', 26, 'Test bài viết 1', '2018-08-22 03:54:21', '2018-08-22 03:54:21'),
(525, 1, 'Super Admin', 'admin', 'update', 'article', 26, 'Test bài viết 1', '2018-08-22 04:13:50', '2018-08-22 04:13:50'),
(526, 1, 'Super Admin', 'admin', 'update', 'article', 26, 'Test bài viết 1 sadf ', '2018-08-22 04:27:14', '2018-08-22 04:27:14'),
(527, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 04:30:50', '2018-08-22 04:30:50'),
(528, 1, 'Super Admin', 'admin', 'update', 'article', 26, 'Test bài viết 1 sadf ', '2018-08-22 04:31:04', '2018-08-22 04:31:04'),
(529, 1, 'Super Admin', 'admin', 'update', 'article', 26, 'Test bài viết 1 sadf ', '2018-08-22 04:39:17', '2018-08-22 04:39:17'),
(530, 1, 'Super Admin', 'admin', 'update', 'blog', 8, 'LIFE STYLE', '2018-08-22 04:45:53', '2018-08-22 04:45:53'),
(531, 1, 'Super Admin', 'admin', 'update', 'blog', 8, 'LIFE STYLE', '2018-08-22 04:49:29', '2018-08-22 04:49:29'),
(532, 1, 'Super Admin', 'admin', 'update', 'article', 26, 'Test bài viết 1 sadf ', '2018-08-22 04:50:25', '2018-08-22 04:50:25'),
(533, 1, 'Super Admin', 'admin', 'update', 'blog', 8, 'LIFE STYLE', '2018-08-22 04:53:24', '2018-08-22 04:53:24'),
(534, 1, 'Super Admin', 'admin', 'create', 'article', 27, 'test bai viet 2', '2018-08-22 04:54:45', '2018-08-22 04:54:45'),
(535, 1, 'Super Admin', 'admin', 'update', 'article', 27, 'test bai viet 2', '2018-08-22 04:54:49', '2018-08-22 04:54:49'),
(536, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-22 06:44:05', '2018-08-22 06:44:05'),
(537, 1, 'Super Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-08-22 06:48:18', '2018-08-22 06:48:18'),
(538, 1, 'Super Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-08-22 06:48:54', '2018-08-22 06:48:54'),
(539, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 06:59:17', '2018-08-22 06:59:17'),
(540, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 07:56:12', '2018-08-22 07:56:12'),
(541, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-22 07:57:17', '2018-08-22 07:57:17'),
(542, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-22 07:57:44', '2018-08-22 07:57:44'),
(543, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-22 07:59:05', '2018-08-22 07:59:05'),
(544, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-22 07:59:13', '2018-08-22 07:59:13'),
(545, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 08:05:14', '2018-08-22 08:05:14'),
(546, 1, 'Super Admin', 'admin', 'delete', 'article', 27, 'test bai viet 2', '2018-08-22 08:17:30', '2018-08-22 08:17:30'),
(547, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 08:32:48', '2018-08-22 08:32:48'),
(548, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 08:32:48', '2018-08-22 08:32:48'),
(549, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 09:33:01', '2018-08-22 09:33:01'),
(550, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-22 09:36:57', '2018-08-22 09:36:57'),
(551, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-22 09:41:41', '2018-08-22 09:41:41'),
(552, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-23 02:49:31', '2018-08-23 02:49:31'),
(553, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-24 01:12:14', '2018-08-24 01:12:14'),
(554, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-25 04:40:05', '2018-08-25 04:40:05'),
(555, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-25 04:41:32', '2018-08-25 04:41:32'),
(556, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-25 04:41:55', '2018-08-25 04:41:55'),
(557, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-25 04:44:29', '2018-08-25 04:44:29'),
(558, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-25 05:00:58', '2018-08-25 05:00:58'),
(559, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-25 07:47:21', '2018-08-25 07:47:21'),
(560, 1, 'Super Admin', 'admin', 'update', 'article', 25, 'Hỗ trợ 1', '2018-08-25 07:47:43', '2018-08-25 07:47:43'),
(561, 1, 'Super Admin', 'admin', 'update', 'article', 1, 'Spotlight on Geordie Willis', '2018-08-25 07:47:51', '2018-08-25 07:47:51'),
(562, 1, 'Super Admin', 'admin', 'update', 'article', 3, 'Spotlight on Geordie Willis', '2018-08-25 07:48:00', '2018-08-25 07:48:00'),
(563, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-08-26 14:47:57', '2018-08-26 14:47:57'),
(564, 4, 'Ngọc', 'admin', 'delete', 'contact', 8, 'Vũ Bá Tiến', '2018-08-26 14:48:18', '2018-08-26 14:48:18'),
(565, 4, 'Ngọc', 'admin', 'delete', 'contact', 7, 'Vũ Bá Tiến', '2018-08-26 14:48:32', '2018-08-26 14:48:32'),
(566, 4, 'Ngọc', 'admin', 'delete', 'contact', 6, 'ádf', '2018-08-26 14:48:35', '2018-08-26 14:48:35'),
(567, 4, 'Ngọc', 'admin', 'delete', 'contact', 5, 'tien', '2018-08-26 14:48:37', '2018-08-26 14:48:37'),
(568, 4, 'Ngọc', 'admin', 'delete', 'contact', 2, 'danh', '2018-08-26 14:48:40', '2018-08-26 14:48:40'),
(569, 4, 'Ngọc', 'admin', 'create', 'product', 10, 'Ghế Sofa 3 seater', '2018-08-26 14:51:40', '2018-08-26 14:51:40'),
(570, 4, 'Ngọc', 'admin', 'update', 'product', 10, 'Ghế Sofa 3 seater', '2018-08-26 14:54:47', '2018-08-26 14:54:47'),
(571, 4, 'Ngọc', 'admin', 'update', 'product', 10, 'Ghế Sofa 3 seater', '2018-08-26 15:09:40', '2018-08-26 15:09:40'),
(572, 4, 'Ngọc', 'admin', 'update', 'collection', 2, 'REAL LOCKS', '2018-08-26 15:13:28', '2018-08-26 15:13:28'),
(573, 4, 'Ngọc', 'admin', 'update', 'collection', 9, 'Keyless Lock', '2018-08-26 15:15:15', '2018-08-26 15:15:15'),
(574, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 15:21:33', '2018-08-26 15:21:33'),
(575, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 15:24:17', '2018-08-26 15:24:17'),
(576, 4, 'Ngọc', 'admin', 'create', 'attribute', 15, 'Toyo Taper', '2018-08-26 15:32:16', '2018-08-26 15:32:16'),
(577, 4, 'Ngọc', 'admin', 'create', 'attribute', 16, 'Thép', '2018-08-26 15:32:42', '2018-08-26 15:32:42'),
(578, 4, 'Ngọc', 'admin', 'create', 'attribute', 17, 'Vải bố', '2018-08-26 15:32:51', '2018-08-26 15:32:51'),
(579, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 15:42:08', '2018-08-26 15:42:08'),
(580, 4, 'Ngọc', 'admin', 'update', 'collection', 1, 'LOCKER', '2018-08-26 15:48:29', '2018-08-26 15:48:29'),
(581, 4, 'Ngọc', 'admin', 'update', 'collection', 1, 'LOCKER', '2018-08-26 15:50:05', '2018-08-26 15:50:05'),
(582, 4, 'Ngọc', 'admin', 'update', 'collection', 1, 'LOCKER', '2018-08-26 15:51:39', '2018-08-26 15:51:39'),
(583, 4, 'Ngọc', 'admin', 'update', 'collection', 1, 'LOCKER', '2018-08-26 15:51:44', '2018-08-26 15:51:44'),
(584, 4, 'Ngọc', 'admin', 'create', 'product', 11, 'ABS Locker M series', '2018-08-26 15:54:49', '2018-08-26 15:54:49'),
(585, 4, 'Ngọc', 'admin', 'update', 'product', 11, 'ABS Locker M series', '2018-08-26 15:54:55', '2018-08-26 15:54:55'),
(586, 4, 'Ngọc', 'admin', 'update', 'product', 11, 'ABS Locker M series', '2018-08-26 16:06:31', '2018-08-26 16:06:31'),
(587, 4, 'Ngọc', 'admin', 'update', 'product', 9, 'Locker', '2018-08-26 16:10:43', '2018-08-26 16:10:43'),
(588, 4, 'Ngọc', 'admin', 'update', 'product', 9, 'Locker', '2018-08-26 16:10:48', '2018-08-26 16:10:48'),
(589, 4, 'Ngọc', 'admin', 'update', 'product', 9, 'Locker', '2018-08-26 16:11:53', '2018-08-26 16:11:53'),
(590, 4, 'Ngọc', 'admin', 'update', 'product', 9, 'Locker', '2018-08-26 16:12:46', '2018-08-26 16:12:46'),
(591, 4, 'Ngọc', 'admin', 'update', 'collection', 5, 'ABS Locker', '2018-08-26 16:13:56', '2018-08-26 16:13:56'),
(592, 4, 'Ngọc', 'admin', 'update', 'collection', 5, 'ABS Locker', '2018-08-26 16:14:50', '2018-08-26 16:14:50'),
(593, 4, 'Ngọc', 'admin', 'update', 'collection', 6, 'PVC Locker', '2018-08-26 16:16:35', '2018-08-26 16:16:35'),
(594, 4, 'Ngọc', 'admin', 'update', 'collection', 7, 'Metal Locker', '2018-08-26 16:17:15', '2018-08-26 16:17:15'),
(595, 4, 'Ngọc', 'admin', 'update', 'collection', 8, 'SUS Locker', '2018-08-26 16:18:42', '2018-08-26 16:18:42'),
(596, 4, 'Ngọc', 'admin', 'update', 'collection', 2, 'KHÓA TỦ', '2018-08-26 16:20:11', '2018-08-26 16:20:11'),
(597, 4, 'Ngọc', 'admin', 'update', 'collection', 9, 'Khóa số', '2018-08-26 16:22:01', '2018-08-26 16:22:01'),
(598, 4, 'Ngọc', 'admin', 'update', 'collection', 10, 'Khóa chìa ', '2018-08-26 16:22:49', '2018-08-26 16:22:49'),
(599, 4, 'Ngọc', 'admin', 'update', 'collection', 10, 'Khóa tủ gỗ', '2018-08-26 16:23:40', '2018-08-26 16:23:40'),
(600, 4, 'Ngọc', 'admin', 'update', 'collection', 9, 'Khóa mật mã', '2018-08-26 16:24:17', '2018-08-26 16:24:17'),
(601, 4, 'Ngọc', 'admin', 'update', 'collection', 11, 'Khóa tủ sắt, nhựa', '2018-08-26 16:25:15', '2018-08-26 16:25:15'),
(602, 4, 'Ngọc', 'admin', 'update', 'collection', 3, 'NỘI THẤT VĂN PHÒNG', '2018-08-26 16:27:47', '2018-08-26 16:27:47'),
(603, 4, 'Ngọc', 'admin', 'update', 'collection', 12, 'Ghế văn phòng', '2018-08-26 16:29:49', '2018-08-26 16:29:49'),
(604, 4, 'Ngọc', 'admin', 'update', 'collection', 3, 'GHẾ', '2018-08-26 16:31:35', '2018-08-26 16:31:35'),
(605, 4, 'Ngọc', 'admin', 'update', 'collection', 3, 'GHẾ', '2018-08-26 16:33:21', '2018-08-26 16:33:21'),
(606, 4, 'Ngọc', 'admin', 'update', 'collection', 3, 'GHẾ', '2018-08-26 16:33:55', '2018-08-26 16:33:55'),
(607, 4, 'Ngọc', 'admin', 'update', 'collection', 12, 'Ghế nhân viên', '2018-08-26 16:34:32', '2018-08-26 16:34:32'),
(608, 4, 'Ngọc', 'admin', 'update', 'collection', 13, 'Ghế lãnh đạo', '2018-08-26 16:35:36', '2018-08-26 16:35:36'),
(609, 4, 'Ngọc', 'admin', 'create', 'collection', 20, 'Ghế training', '2018-08-26 16:36:33', '2018-08-26 16:36:33'),
(610, 4, 'Ngọc', 'admin', 'update', 'collection', 20, 'Ghế training', '2018-08-26 16:36:38', '2018-08-26 16:36:38'),
(611, 4, 'Ngọc', 'admin', 'update', 'collection', 14, 'Ghế phòng họp', '2018-08-26 16:37:18', '2018-08-26 16:37:18'),
(612, 4, 'Ngọc', 'admin', 'create', 'collection', 21, 'Ghế sofa', '2018-08-26 16:38:30', '2018-08-26 16:38:30'),
(613, 4, 'Ngọc', 'admin', 'update', 'collection', 21, 'Ghế sofa', '2018-08-26 16:38:37', '2018-08-26 16:38:37'),
(614, 4, 'Ngọc', 'admin', 'update', 'collection', 4, 'BÀN', '2018-08-26 16:44:51', '2018-08-26 16:44:51'),
(615, 4, 'Ngọc', 'admin', 'update', 'collection', 4, 'BÀN', '2018-08-26 16:45:07', '2018-08-26 16:45:07'),
(616, 4, 'Ngọc', 'admin', 'update', 'collection', 4, 'BÀN VĂN PHÒNG', '2018-08-26 16:47:19', '2018-08-26 16:47:19'),
(617, 4, 'Ngọc', 'admin', 'update', 'collection', 16, 'Bàn đơn Nhân viên', '2018-08-26 16:49:01', '2018-08-26 16:49:01'),
(618, 4, 'Ngọc', 'admin', 'update', 'collection', 15, 'Bàn lãnh đạo', '2018-08-26 16:55:36', '2018-08-26 16:55:36'),
(619, 4, 'Ngọc', 'admin', 'update', 'collection', 17, 'Bàn lãnh đạo', '2018-08-26 17:09:28', '2018-08-26 17:09:28'),
(620, 4, 'Ngọc', 'admin', 'update', 'collection', 17, 'Bàn phòng họp', '2018-08-26 17:10:17', '2018-08-26 17:10:17'),
(621, 4, 'Ngọc', 'admin', 'create', 'collection', 22, 'Bàn Sofa', '2018-08-26 17:10:49', '2018-08-26 17:10:49'),
(622, 4, 'Ngọc', 'admin', 'update', 'collection', 16, 'Bàn Nhân viên', '2018-08-26 17:11:53', '2018-08-26 17:11:53'),
(623, 4, 'Ngọc', 'admin', 'update', 'menu', 5, 'Locker', '2018-08-26 17:18:32', '2018-08-26 17:18:32'),
(624, 4, 'Ngọc', 'admin', 'update', 'menu', 6, 'Lock', '2018-08-26 17:19:27', '2018-08-26 17:19:27'),
(625, 4, 'Ngọc', 'admin', 'update', 'menu', 7, 'Ghế', '2018-08-26 17:20:55', '2018-08-26 17:20:55'),
(626, 4, 'Ngọc', 'admin', 'update', 'menu', 5, 'Locker', '2018-08-26 17:21:38', '2018-08-26 17:21:38'),
(627, 4, 'Ngọc', 'admin', 'update', 'menu', 6, 'Lock', '2018-08-26 17:22:06', '2018-08-26 17:22:06'),
(628, 4, 'Ngọc', 'admin', 'update', 'menu', 1, 'Header', '2018-08-26 17:22:33', '2018-08-26 17:22:33'),
(629, 4, 'Ngọc', 'admin', 'create', 'menu', 27, 'Bàn văn phòng', '2018-08-26 17:24:35', '2018-08-26 17:24:35'),
(630, 4, 'Ngọc', 'admin', 'update', 'menu', 5, 'Tủ Locker', '2018-08-26 17:25:43', '2018-08-26 17:25:43'),
(631, 4, 'Ngọc', 'admin', 'update', 'menu', 6, 'Khóa Tủ', '2018-08-26 17:26:00', '2018-08-26 17:26:00'),
(632, 4, 'Ngọc', 'admin', 'create', 'menu', 28, 'Đồ uống', '2018-08-26 17:29:59', '2018-08-26 17:29:59'),
(633, 4, 'Ngọc', 'admin', 'create', 'collection', 23, 'ĐỒ UỐNG', '2018-08-26 17:30:40', '2018-08-26 17:30:40'),
(634, 4, 'Ngọc', 'admin', 'update', 'collection', 23, 'ĐỒ UỐNG', '2018-08-26 17:30:43', '2018-08-26 17:30:43'),
(635, 4, 'Ngọc', 'admin', 'update', 'collection', 23, 'VĂN PHÒNG PHẨM', '2018-08-26 17:32:09', '2018-08-26 17:32:09'),
(636, 4, 'Ngọc', 'admin', 'update', 'collection', 1, 'TỦ LOCKER', '2018-08-26 17:32:39', '2018-08-26 17:32:39'),
(637, 4, 'Ngọc', 'admin', 'update', 'menu', 28, 'Văn phòng phẩm', '2018-08-26 17:33:33', '2018-08-26 17:33:33'),
(638, 4, 'Ngọc', 'admin', 'update', 'menu', 9, 'Sản phẩm- Giải pháp mới', '2018-08-26 17:37:28', '2018-08-26 17:37:28'),
(639, 4, 'Ngọc', 'admin', 'update', 'menu', 10, 'Sự kiện', '2018-08-26 17:41:53', '2018-08-26 17:41:53'),
(640, 4, 'Ngọc', 'admin', 'update', 'menu', 11, 'Tin chuyên ngành', '2018-08-26 17:42:17', '2018-08-26 17:42:17'),
(641, 4, 'Ngọc', 'admin', 'update', 'menu', 10, 'Hoạt động & Sự kiện', '2018-08-26 17:44:39', '2018-08-26 17:44:39'),
(642, 4, 'Ngọc', 'admin', 'update', 'menu', 11, 'Kiến thức chuyên ngành', '2018-08-26 17:45:16', '2018-08-26 17:45:16'),
(643, 4, 'Ngọc', 'admin', 'update', 'menu', 9, 'Giải pháp mới', '2018-08-26 17:48:39', '2018-08-26 17:48:39'),
(644, 4, 'Ngọc', 'admin', 'update', 'menu', 9, 'Ý tưởng & Giải pháp mới', '2018-08-26 17:49:22', '2018-08-26 17:49:22'),
(645, 4, 'Ngọc', 'admin', 'update', 'menu', 9, 'Giải pháp mới', '2018-08-26 17:49:37', '2018-08-26 17:49:37'),
(646, 4, 'Ngọc', 'admin', 'update', 'menu', 9, 'Giải pháp mới', '2018-08-26 17:49:46', '2018-08-26 17:49:46'),
(647, 4, 'Ngọc', 'admin', 'update', 'menu', 10, 'Hoạt động & Sự kiện', '2018-08-26 17:49:55', '2018-08-26 17:49:55'),
(648, 4, 'Ngọc', 'admin', 'update', 'menu', 10, 'Hoạt động & Sự kiện', '2018-08-26 17:50:04', '2018-08-26 17:50:04'),
(649, 4, 'Ngọc', 'admin', 'update', 'menu', 11, 'Tin chuyên ngành', '2018-08-26 17:50:56', '2018-08-26 17:50:56'),
(650, 4, 'Ngọc', 'admin', 'update', 'menu', 13, 'Liên hệ', '2018-08-26 17:51:29', '2018-08-26 17:51:29'),
(651, 4, 'Ngọc', 'admin', 'update', 'menu', 12, 'Customer Support', '2018-08-26 17:52:04', '2018-08-26 17:52:04'),
(652, 4, 'Ngọc', 'admin', 'update', 'menu', 13, 'Contact Us', '2018-08-26 17:52:18', '2018-08-26 17:52:18'),
(653, 4, 'Ngọc', 'admin', 'update', 'menu', 8, 'News & Event', '2018-08-26 17:52:36', '2018-08-26 17:52:36'),
(654, 4, 'Ngọc', 'admin', 'update', 'menu', 4, 'PRODUCTS', '2018-08-26 17:53:12', '2018-08-26 17:53:12'),
(655, 4, 'Ngọc', 'admin', 'update', 'menu', 3, 'ABOUT US', '2018-08-26 17:53:31', '2018-08-26 17:53:31'),
(656, 4, 'Ngọc', 'admin', 'update', 'menu', 2, 'HOME', '2018-08-26 17:53:40', '2018-08-26 17:53:40'),
(657, 4, 'Ngọc', 'admin', 'create', 'menu', 29, 'LOCATION', '2018-08-26 17:54:46', '2018-08-26 17:54:46'),
(658, 4, 'Ngọc', 'admin', 'update', 'menu', 15, 'Working memo', '2018-08-26 17:57:37', '2018-08-26 17:57:37'),
(659, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 18:11:11', '2018-08-26 18:11:11'),
(660, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 18:12:42', '2018-08-26 18:12:42'),
(661, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 18:22:49', '2018-08-26 18:22:49'),
(662, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 18:23:54', '2018-08-26 18:23:54'),
(663, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-26 18:24:30', '2018-08-26 18:24:30'),
(664, 4, 'Ngọc', 'admin', 'update', 'blog', 8, 'Thông tin cần biết', '2018-08-26 18:40:08', '2018-08-26 18:40:08'),
(665, 4, 'Ngọc', 'admin', 'update', 'blog', 10, 'Xu hướng và Giải pháp mới', '2018-08-26 18:42:07', '2018-08-26 18:42:07'),
(666, 4, 'Ngọc', 'admin', 'update', 'blog', 11, 'Xu hướng mới', '2018-08-26 18:43:31', '2018-08-26 18:43:31'),
(667, 4, 'Ngọc', 'admin', 'update', 'blog', 12, 'Giải pháp mới', '2018-08-26 18:43:54', '2018-08-26 18:43:54'),
(668, 4, 'Ngọc', 'admin', 'update', 'blog', 8, 'Hỗ trợ khách hàng', '2018-08-26 18:46:18', '2018-08-26 18:46:18'),
(669, 4, 'Ngọc', 'admin', 'update', 'article', 14, 'Office supplies', '2018-08-26 18:49:39', '2018-08-26 18:49:39'),
(670, 4, 'Ngọc', 'admin', 'update', 'article', 14, 'Office supplies', '2018-08-26 18:51:10', '2018-08-26 18:51:10'),
(671, 4, 'Ngọc', 'admin', 'update', 'article', 14, 'Office supplies', '2018-08-26 18:52:06', '2018-08-26 18:52:06'),
(672, 4, 'Ngọc', 'admin', 'update', 'article', 12, 'Factory supplies', '2018-08-26 18:55:17', '2018-08-26 18:55:17'),
(673, 4, 'Ngọc', 'admin', 'update', 'article', 12, 'Factory supplies', '2018-08-26 19:11:23', '2018-08-26 19:11:23'),
(674, 4, 'Ngọc', 'admin', 'update', 'article', 12, 'Factory supplies', '2018-08-26 19:12:25', '2018-08-26 19:12:25'),
(675, 4, 'Ngọc', 'admin', 'update', 'article', 13, 'Specialized supplies', '2018-08-26 19:20:01', '2018-08-26 19:20:01'),
(676, 4, 'Ngọc', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-26 19:22:22', '2018-08-26 19:22:22'),
(677, 4, 'Ngọc', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-08-26 19:22:49', '2018-08-26 19:22:49'),
(678, 4, 'Ngọc', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-08-26 19:23:11', '2018-08-26 19:23:11'),
(679, 4, 'Ngọc', 'admin', 'update', 'article', 11, 'Powerful shortcodes', '2018-08-26 19:23:25', '2018-08-26 19:23:25'),
(680, 4, 'Ngọc', 'admin', 'update', 'article', 11, 'Hotel Supplies', '2018-08-26 19:28:56', '2018-08-26 19:28:56'),
(681, 4, 'Ngọc', 'admin', 'update', 'article', 11, 'Hotel Supplies', '2018-08-26 19:29:45', '2018-08-26 19:29:45'),
(682, 4, 'Ngọc', 'admin', 'update', 'article', 3, 'Chính sách bảo hành', '2018-08-26 19:37:39', '2018-08-26 19:37:39'),
(683, 4, 'Ngọc', 'admin', 'update', 'article', 1, 'Chính sách Đại lý', '2018-08-26 19:42:07', '2018-08-26 19:42:07'),
(684, 4, 'Ngọc', 'admin', 'update', 'article', 26, 'Câu hỏi thường gặp', '2018-08-26 19:45:27', '2018-08-26 19:45:27'),
(685, 4, 'Ngọc', 'admin', 'update', 'article', 8, 'Hướng dẫn mua hàng, giao hàng và lắp đặt', '2018-08-26 19:48:10', '2018-08-26 19:48:10'),
(686, 3, 'Loan', 'admin', 'login', 'user', 0, '', '2018-08-27 02:47:20', '2018-08-27 02:47:20'),
(687, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-08-27 06:35:46', '2018-08-27 06:35:46'),
(688, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-27 08:49:06', '2018-08-27 08:49:06'),
(689, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-27 08:49:31', '2018-08-27 08:49:31'),
(690, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-27 08:55:27', '2018-08-27 08:55:27'),
(691, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-08-27 08:58:05', '2018-08-27 08:58:05'),
(692, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-27 09:00:08', '2018-08-27 09:00:08'),
(693, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-08-27 09:01:27', '2018-08-27 09:01:27'),
(694, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'Liên hệ', '2018-08-27 09:02:08', '2018-08-27 09:02:08'),
(695, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-27 10:02:14', '2018-08-27 10:02:14'),
(696, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-27 10:03:30', '2018-08-27 10:03:30'),
(697, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-08-28 00:59:59', '2018-08-28 00:59:59'),
(698, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-08-28 14:09:57', '2018-08-28 14:09:57'),
(699, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-08-29 15:59:35', '2018-08-29 15:59:35'),
(700, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-29 16:06:07', '2018-08-29 16:06:07'),
(701, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-29 17:46:02', '2018-08-29 17:46:02'),
(702, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-29 17:48:02', '2018-08-29 17:48:02'),
(703, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-29 17:48:40', '2018-08-29 17:48:40'),
(704, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-29 18:38:20', '2018-08-29 18:38:20'),
(705, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-29 18:39:26', '2018-08-29 18:39:26'),
(706, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-29 18:45:16', '2018-08-29 18:45:16'),
(707, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-29 18:45:58', '2018-08-29 18:45:58'),
(708, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-29 18:47:01', '2018-08-29 18:47:01'),
(709, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-30 05:02:02', '2018-08-30 05:02:02'),
(710, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'Giới thiệu', '2018-08-30 05:24:48', '2018-08-30 05:24:48'),
(711, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-08-30 05:25:39', '2018-08-30 05:25:39'),
(712, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-08-30 05:31:37', '2018-08-30 05:31:37'),
(713, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-30 05:37:16', '2018-08-30 05:37:16'),
(714, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-30 05:53:43', '2018-08-30 05:53:43'),
(715, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-08-30 05:54:39', '2018-08-30 05:54:39'),
(716, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-08-30 06:56:51', '2018-08-30 06:56:51'),
(717, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-08-30 07:25:25', '2018-08-30 07:25:25'),
(718, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-30 09:56:31', '2018-08-30 09:56:31'),
(719, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-30 10:24:39', '2018-08-30 10:24:39'),
(720, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 05:16:51', '2018-08-31 05:16:51'),
(721, 3, 'Loan', 'admin', 'login', 'user', 0, '', '2018-08-31 07:02:31', '2018-08-31 07:02:31'),
(722, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 07:03:55', '2018-08-31 07:03:55'),
(723, 3, 'Loan', 'admin', 'login', 'user', 0, '', '2018-08-31 07:29:25', '2018-08-31 07:29:25'),
(724, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 07:29:50', '2018-08-31 07:29:50'),
(725, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 07:30:30', '2018-08-31 07:30:30'),
(726, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 07:30:51', '2018-08-31 07:30:51'),
(727, 2, 'Admin', 'admin', 'update', 'menu', 9, 'Giải pháp mới', '2018-08-31 10:34:38', '2018-08-31 10:34:38'),
(728, 2, 'Admin', 'admin', 'update', 'menu', 9, 'Giải pháp mới', '2018-08-31 10:40:17', '2018-08-31 10:40:17'),
(729, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:14:31', '2018-08-31 21:14:31'),
(730, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:15:30', '2018-08-31 21:15:30'),
(731, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:27:24', '2018-08-31 21:27:24'),
(732, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:31:08', '2018-08-31 21:31:08'),
(733, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:31:29', '2018-08-31 21:31:29'),
(734, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:33:01', '2018-08-31 21:33:01'),
(735, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:38:09', '2018-08-31 21:38:09'),
(736, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:39:09', '2018-08-31 21:39:09'),
(737, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-08-31 21:41:39', '2018-08-31 21:41:39'),
(738, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-01 05:38:24', '2018-09-01 05:38:24'),
(739, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 05:40:27', '2018-09-01 05:40:27'),
(740, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 05:42:14', '2018-09-01 05:42:14'),
(741, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 05:42:35', '2018-09-01 05:42:35'),
(742, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-01 09:19:00', '2018-09-01 09:19:00'),
(743, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 09:19:42', '2018-09-01 09:19:42'),
(744, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 09:20:03', '2018-09-01 09:20:03'),
(745, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-01 09:24:33', '2018-09-01 09:24:33'),
(746, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 09:37:39', '2018-09-01 09:37:39'),
(747, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 09:38:34', '2018-09-01 09:38:34'),
(748, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 09:39:38', '2018-09-01 09:39:38'),
(749, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 09:41:05', '2018-09-01 09:41:05'),
(750, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-01 09:41:24', '2018-09-01 09:41:24'),
(751, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-01 13:21:53', '2018-09-01 13:21:53'),
(752, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-01 13:22:48', '2018-09-01 13:22:48'),
(753, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-01 13:24:29', '2018-09-01 13:24:29'),
(754, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 04:56:34', '2018-09-02 04:56:34'),
(755, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 04:58:45', '2018-09-02 04:58:45'),
(756, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 06:37:32', '2018-09-02 06:37:32'),
(757, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 06:59:39', '2018-09-02 06:59:39'),
(758, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 07:20:31', '2018-09-02 07:20:31'),
(759, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 07:30:46', '2018-09-02 07:30:46'),
(760, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 07:32:03', '2018-09-02 07:32:03'),
(761, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 07:32:38', '2018-09-02 07:32:38'),
(762, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 07:43:29', '2018-09-02 07:43:29'),
(763, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 07:51:18', '2018-09-02 07:51:18'),
(764, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-02 07:51:52', '2018-09-02 07:51:52'),
(765, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-02 08:26:07', '2018-09-02 08:26:07'),
(766, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-02 08:26:53', '2018-09-02 08:26:53'),
(767, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-02 08:28:10', '2018-09-02 08:28:10'),
(768, 4, 'Ngọc', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-02 08:29:12', '2018-09-02 08:29:12'),
(769, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'Liên hệ', '2018-09-02 14:13:02', '2018-09-02 14:13:02'),
(770, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'NICE TO MEET YOU', '2018-09-02 14:13:34', '2018-09-02 14:13:34'),
(771, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'NICE TO MEET YOU', '2018-09-02 14:20:57', '2018-09-02 14:20:57'),
(772, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'NICE TO MEET YOU', '2018-09-02 14:21:50', '2018-09-02 14:21:50'),
(773, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 14:27:56', '2018-09-02 14:27:56'),
(774, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 15:55:40', '2018-09-02 15:55:40'),
(775, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 15:56:33', '2018-09-02 15:56:33'),
(776, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 15:57:25', '2018-09-02 15:57:25'),
(777, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 15:58:39', '2018-09-02 15:58:39'),
(778, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 15:59:32', '2018-09-02 15:59:32'),
(779, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 16:00:07', '2018-09-02 16:00:07'),
(780, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-02 16:00:15', '2018-09-02 16:00:15'),
(781, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-04 00:13:16', '2018-09-04 00:13:16'),
(782, 2, 'Admin', 'admin', 'logout', 'user', 0, '', '2018-09-04 00:14:04', '2018-09-04 00:14:04'),
(783, 3, 'Loan', 'admin', 'login', 'user', 0, '', '2018-09-04 04:15:02', '2018-09-04 04:15:02'),
(784, 3, 'Loan', 'admin', 'active', 'article', 11, 'Hotel Supplies', '2018-09-04 04:20:09', '2018-09-04 04:20:09'),
(785, 3, 'Loan', 'admin', 'active', 'article', 9, 'Supersonic import', '2018-09-04 04:20:09', '2018-09-04 04:20:09'),
(786, 3, 'Loan', 'admin', 'active', 'article', 10, 'Effortless customization', '2018-09-04 04:20:09', '2018-09-04 04:20:09'),
(787, 3, 'Loan', 'admin', 'update', 'article', 13, 'Specialized supplies', '2018-09-04 04:21:03', '2018-09-04 04:21:03'),
(788, 3, 'Loan', 'admin', 'update', 'article', 9, 'Supersonic import', '2018-09-04 04:21:35', '2018-09-04 04:21:35'),
(789, 3, 'Loan', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-09-04 04:22:02', '2018-09-04 04:22:02'),
(790, 3, 'Loan', 'admin', 'update', 'article', 13, 'Specialized supplies', '2018-09-04 04:22:16', '2018-09-04 04:22:16'),
(791, 3, 'Loan', 'admin', 'update', 'article', 13, 'Specialized supplies', '2018-09-04 04:23:03', '2018-09-04 04:23:03'),
(792, 3, 'Loan', 'admin', 'update', 'article', 13, 'Specialized supplies', '2018-09-04 04:23:09', '2018-09-04 04:23:09'),
(793, 3, 'Loan', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-04 04:24:28', '2018-09-04 04:24:28'),
(794, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-04 04:30:30', '2018-09-04 04:30:30'),
(795, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-04 04:41:31', '2018-09-04 04:41:31'),
(796, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-04 05:18:58', '2018-09-04 05:18:58'),
(797, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-04 06:15:35', '2018-09-04 06:15:35'),
(798, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-04 06:33:07', '2018-09-04 06:33:07'),
(799, 3, 'Loan', 'admin', 'login', 'user', 0, '', '2018-09-04 08:47:19', '2018-09-04 08:47:19'),
(800, 3, 'Loan', 'admin', 'update', 'collection', 1, 'Tủ Locker', '2018-09-04 08:54:25', '2018-09-04 08:54:25'),
(801, 3, 'Loan', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-04 08:57:25', '2018-09-04 08:57:25'),
(802, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-09-04 08:57:48', '2018-09-04 08:57:48'),
(803, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-09-04 14:12:38', '2018-09-04 14:12:38'),
(804, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-05 07:16:17', '2018-09-05 07:16:17'),
(805, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-05 07:17:16', '2018-09-05 07:17:16'),
(806, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-05 07:18:32', '2018-09-05 07:18:32'),
(807, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-05 07:19:26', '2018-09-05 07:19:26'),
(808, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-05 07:23:01', '2018-09-05 07:23:01'),
(809, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-05 07:24:45', '2018-09-05 07:24:45'),
(810, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-05 07:34:58', '2018-09-05 07:34:58'),
(811, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-05 07:35:28', '2018-09-05 07:35:28'),
(812, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-05 07:36:25', '2018-09-05 07:36:25'),
(813, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-05 07:36:52', '2018-09-05 07:36:52'),
(814, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-05 07:38:51', '2018-09-05 07:38:51'),
(815, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-05 07:38:52', '2018-09-05 07:38:52'),
(816, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-05 08:29:09', '2018-09-05 08:29:09'),
(817, 1, 'Super Admin', 'admin', 'create', 'blog', 13, 'News & Event', '2018-09-05 08:30:31', '2018-09-05 08:30:31'),
(818, 1, 'Super Admin', 'admin', 'update', 'blog', 13, 'News & Event', '2018-09-05 08:31:09', '2018-09-05 08:31:09'),
(819, 1, 'Super Admin', 'admin', 'create', 'article', 28, 'Giải pháp mới', '2018-09-05 08:33:51', '2018-09-05 08:33:51'),
(820, 1, 'Super Admin', 'admin', 'create', 'article', 29, 'Hoạt động & Sự kiện', '2018-09-05 08:34:45', '2018-09-05 08:34:45'),
(821, 1, 'Super Admin', 'admin', 'update', 'article', 29, 'Hoạt động & Sự kiện', '2018-09-05 08:35:13', '2018-09-05 08:35:13'),
(822, 1, 'Super Admin', 'admin', 'create', 'article', 30, 'Tin chuyên ngành', '2018-09-05 08:35:23', '2018-09-05 08:35:23'),
(823, 1, 'Super Admin', 'admin', 'update', 'menu', 8, 'News & Event', '2018-09-05 08:35:49', '2018-09-05 08:35:49'),
(824, 1, 'Super Admin', 'admin', 'update', 'menu', 9, 'Giải pháp mới', '2018-09-05 08:36:11', '2018-09-05 08:36:11'),
(825, 1, 'Super Admin', 'admin', 'update', 'menu', 10, 'Hoạt động & Sự kiện', '2018-09-05 08:36:32', '2018-09-05 08:36:32'),
(826, 1, 'Super Admin', 'admin', 'update', 'menu', 11, 'Tin chuyên ngành', '2018-09-05 08:36:41', '2018-09-05 08:36:41'),
(827, 1, 'Super Admin', 'admin', 'update', 'menu', 1, 'Header', '2018-09-05 08:36:45', '2018-09-05 08:36:45'),
(828, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-05 08:40:35', '2018-09-05 08:40:35'),
(829, 1, 'Super Admin', 'admin', 'update', 'article', 30, 'Tin chuyên ngành', '2018-09-05 08:43:37', '2018-09-05 08:43:37'),
(830, 1, 'Super Admin', 'admin', 'update', 'article', 29, 'Hoạt động & Sự kiện', '2018-09-05 08:44:00', '2018-09-05 08:44:00'),
(831, 1, 'Super Admin', 'admin', 'update', 'article', 28, 'Giải pháp mới', '2018-09-05 08:44:33', '2018-09-05 08:44:33'),
(832, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 02:28:41', '2018-09-06 02:28:41'),
(833, 2, 'Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-09-06 02:29:23', '2018-09-06 02:29:23'),
(834, 2, 'Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-09-06 02:29:40', '2018-09-06 02:29:40'),
(835, 2, 'Admin', 'admin', 'update', 'coupon', 1, 'mã 1', '2018-09-06 02:29:57', '2018-09-06 02:29:57'),
(836, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-06 02:33:10', '2018-09-06 02:33:10'),
(837, 2, 'Admin', 'admin', 'logout', 'user', 0, '', '2018-09-06 02:43:54', '2018-09-06 02:43:54'),
(838, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 02:43:59', '2018-09-06 02:43:59'),
(839, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 03:11:25', '2018-09-06 03:11:25'),
(840, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 03:11:27', '2018-09-06 03:11:27'),
(841, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 04:45:01', '2018-09-06 04:45:01'),
(842, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 07:29:27', '2018-09-06 07:29:27'),
(843, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 09:53:59', '2018-09-06 09:53:59'),
(844, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 10:17:03', '2018-09-06 10:17:03'),
(845, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-06 10:18:02', '2018-09-06 10:18:02'),
(846, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-06 10:18:15', '2018-09-06 10:18:15'),
(847, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'NICE TO MEET YOU', '2018-09-06 10:18:56', '2018-09-06 10:18:56'),
(848, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-06 10:19:51', '2018-09-06 10:19:51'),
(849, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 10:35:46', '2018-09-06 10:35:46'),
(850, 1, 'Super Admin', 'admin', 'update', 'page', 2, 'ABOUT US', '2018-09-06 10:36:30', '2018-09-06 10:36:30'),
(851, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-06 11:00:11', '2018-09-06 11:00:11'),
(852, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 01:24:19', '2018-09-07 01:24:19'),
(853, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 01:57:04', '2018-09-07 01:57:04'),
(854, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 03:09:10', '2018-09-07 03:09:10'),
(855, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-07 03:09:34', '2018-09-07 03:09:34'),
(856, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-07 03:09:56', '2018-09-07 03:09:56'),
(857, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-07 03:10:18', '2018-09-07 03:10:18'),
(858, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 03:43:08', '2018-09-07 03:43:08'),
(859, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 03:44:31', '2018-09-07 03:44:31'),
(860, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 04:23:35', '2018-09-07 04:23:35'),
(861, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-07 04:34:41', '2018-09-07 04:34:41'),
(862, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 04:36:01', '2018-09-07 04:36:01'),
(863, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 09:20:54', '2018-09-07 09:20:54'),
(864, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-07 09:50:28', '2018-09-07 09:50:28'),
(865, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-07 09:51:54', '2018-09-07 09:51:54'),
(866, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-07 09:54:20', '2018-09-07 09:54:20'),
(867, 4, 'Ngọc', 'admin', 'update', 'menu', 12, 'Customer Support', '2018-09-07 16:05:42', '2018-09-07 16:05:42'),
(868, 4, 'Ngọc', 'admin', 'update', 'menu', 12, 'Customer Support', '2018-09-07 16:06:11', '2018-09-07 16:06:11'),
(869, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-07 16:09:04', '2018-09-07 16:09:04');
INSERT INTO `history` (`id`, `user_id`, `user_name`, `user_type`, `action`, `type`, `type_id`, `type_title`, `created_at`, `updated_at`) VALUES
(870, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-07 16:09:32', '2018-09-07 16:09:32'),
(871, 4, 'Ngọc', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-07 16:09:59', '2018-09-07 16:09:59'),
(872, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-10 02:04:58', '2018-09-10 02:04:58'),
(873, 2, 'Admin', 'admin', 'update', 'menu', 12, 'Customer Support', '2018-09-10 02:05:42', '2018-09-10 02:05:42'),
(874, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-10 02:06:48', '2018-09-10 02:06:48'),
(875, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-11 03:18:48', '2018-09-11 03:18:48'),
(876, 1, 'Super Admin', 'admin', 'update', 'article', 28, 'Giải pháp mới', '2018-09-11 03:19:52', '2018-09-11 03:19:52'),
(877, 1, 'Super Admin', 'admin', 'update', 'article', 28, 'Giải pháp mới', '2018-09-11 03:20:12', '2018-09-11 03:20:12'),
(878, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-11 04:35:17', '2018-09-11 04:35:17'),
(879, 1, 'Super Admin', 'admin', 'update', 'article', 6, 'Brunch on Saturday', '2018-09-11 04:36:04', '2018-09-11 04:36:04'),
(880, 1, 'Super Admin', 'admin', 'update', 'article', 5, 'Brunch on Saturday', '2018-09-11 04:36:23', '2018-09-11 04:36:23'),
(881, 1, 'Super Admin', 'admin', 'update', 'article', 2, 'Spotlight on Geordie Willis', '2018-09-11 04:36:38', '2018-09-11 04:36:38'),
(882, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-11 10:19:13', '2018-09-11 10:19:13'),
(883, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-11 10:20:10', '2018-09-11 10:20:10'),
(884, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-11 10:20:39', '2018-09-11 10:20:39'),
(885, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-11 11:51:39', '2018-09-11 11:51:39'),
(886, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-11 11:52:39', '2018-09-11 11:52:39'),
(887, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-12 02:15:14', '2018-09-12 02:15:14'),
(888, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-12 02:26:10', '2018-09-12 02:26:10'),
(889, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-12 05:59:30', '2018-09-12 05:59:30'),
(890, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-12 06:00:39', '2018-09-12 06:00:39'),
(891, 1, 'Super Admin', 'admin', 'update', 'page', 1, 'CONTACT US', '2018-09-12 06:00:57', '2018-09-12 06:00:57'),
(892, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-14 04:49:42', '2018-09-14 04:49:42'),
(893, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-14 08:34:33', '2018-09-14 08:34:33'),
(894, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 08:35:08', '2018-09-14 08:35:08'),
(895, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 08:35:27', '2018-09-14 08:35:27'),
(896, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 08:38:55', '2018-09-14 08:38:55'),
(897, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 08:47:12', '2018-09-14 08:47:12'),
(898, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 08:47:56', '2018-09-14 08:47:56'),
(899, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 08:48:44', '2018-09-14 08:48:44'),
(900, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:13:12', '2018-09-14 09:13:12'),
(901, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-14 09:24:34', '2018-09-14 09:24:34'),
(902, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:35:09', '2018-09-14 09:35:09'),
(903, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:36:46', '2018-09-14 09:36:46'),
(904, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:37:16', '2018-09-14 09:37:16'),
(905, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:38:39', '2018-09-14 09:38:39'),
(906, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:39:09', '2018-09-14 09:39:09'),
(907, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:40:59', '2018-09-14 09:40:59'),
(908, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:42:39', '2018-09-14 09:42:39'),
(909, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:43:12', '2018-09-14 09:43:12'),
(910, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:43:36', '2018-09-14 09:43:36'),
(911, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-14 09:55:34', '2018-09-14 09:55:34'),
(912, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-09-14 21:44:22', '2018-09-14 21:44:22'),
(913, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-14 21:45:26', '2018-09-14 21:45:26'),
(914, 4, 'Ngọc', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-14 21:46:50', '2018-09-14 21:46:50'),
(915, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-15 07:53:42', '2018-09-15 07:53:42'),
(916, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-15 07:56:38', '2018-09-15 07:56:38'),
(917, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-15 08:01:03', '2018-09-15 08:01:03'),
(918, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-17 04:31:03', '2018-09-17 04:31:03'),
(919, 2, 'Admin', 'admin', 'logout', 'user', 0, '', '2018-09-17 04:31:16', '2018-09-17 04:31:16'),
(920, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-17 04:31:25', '2018-09-17 04:31:25'),
(921, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-17 04:32:30', '2018-09-17 04:32:30'),
(922, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-17 04:33:15', '2018-09-17 04:33:15'),
(923, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-17 04:34:52', '2018-09-17 04:34:52'),
(924, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-17 07:29:59', '2018-09-17 07:29:59'),
(925, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-09-17 08:03:21', '2018-09-17 08:03:21'),
(926, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-17 08:06:33', '2018-09-17 08:06:33'),
(927, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-17 08:12:27', '2018-09-17 08:12:27'),
(928, 1, 'Super Admin', 'admin', 'update', 'menu', 13, 'Contact Us', '2018-09-17 08:14:58', '2018-09-17 08:14:58'),
(929, 1, 'Super Admin', 'admin', 'delete', 'page', 1, 'CONTACT US', '2018-09-17 08:15:25', '2018-09-17 08:15:25'),
(930, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-09-20 03:14:08', '2018-09-20 03:14:08'),
(931, 4, 'Ngọc', 'admin', 'update', 'product', 9, 'Locker', '2018-09-20 03:20:07', '2018-09-20 03:20:07'),
(932, 4, 'Ngọc', 'admin', 'update', 'product', 11, 'ABS Locker M series', '2018-09-20 03:20:31', '2018-09-20 03:20:31'),
(933, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-09-20 06:43:10', '2018-09-20 06:43:10'),
(934, 4, 'Ngọc', 'admin', 'update', 'product', 8, 'Daphne Women’s Handbag', '2018-09-20 06:47:08', '2018-09-20 06:47:08'),
(935, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-20 06:50:44', '2018-09-20 06:50:44'),
(936, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-20 07:16:36', '2018-09-20 07:16:36'),
(937, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-20 07:39:01', '2018-09-20 07:39:01'),
(938, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-20 08:07:44', '2018-09-20 08:07:44'),
(939, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-20 08:09:37', '2018-09-20 08:09:37'),
(940, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-20 08:10:11', '2018-09-20 08:10:11'),
(941, 4, 'Ngọc', 'admin', 'update', 'product', 8, 'Daphne Women’s Handbag', '2018-09-20 08:50:24', '2018-09-20 08:50:24'),
(942, 4, 'Ngọc', 'admin', 'update', 'product', 11, 'ABS Locker M series', '2018-09-20 08:51:22', '2018-09-20 08:51:22'),
(943, 4, 'Ngọc', 'admin', 'create', 'product', 12, 'RL-10347 Real Keyless Lock', '2018-09-20 09:08:30', '2018-09-20 09:08:30'),
(944, 4, 'Ngọc', 'admin', 'update', 'product', 12, 'RL-10347 Real Keyless Lock', '2018-09-20 09:11:40', '2018-09-20 09:11:40'),
(945, 4, 'Ngọc', 'admin', 'update', 'product', 12, 'RL-10347 Real Keyless Lock', '2018-09-20 09:14:04', '2018-09-20 09:14:04'),
(946, 4, 'Ngọc', 'admin', 'create', 'attribute', 18, 'Kim loại', '2018-09-20 09:17:41', '2018-09-20 09:17:41'),
(947, 4, 'Ngọc', 'admin', 'create', 'product', 13, 'RL-10347 Real Keyless Lock ( Black)', '2018-09-20 09:21:42', '2018-09-20 09:21:42'),
(948, 4, 'Ngọc', 'admin', 'update', 'product', 13, 'RL-10347 Real Keyless Lock ( Black)', '2018-09-20 09:22:31', '2018-09-20 09:22:31'),
(949, 4, 'Ngọc', 'admin', 'create', 'product', 14, 'RL-9041 Real Keyless Lock', '2018-09-20 09:27:40', '2018-09-20 09:27:40'),
(950, 4, 'Ngọc', 'admin', 'update', 'product', 14, 'RL-9041 Real Keyless Lock', '2018-09-20 09:39:36', '2018-09-20 09:39:36'),
(951, 4, 'Ngọc', 'admin', 'update', 'product', 13, 'RL-10347 Real Keyless Lock ( Black)', '2018-09-20 09:40:47', '2018-09-20 09:40:47'),
(952, 4, 'Ngọc', 'admin', 'update', 'product', 13, 'RL-10347 Real Keyless Lock ( Black)', '2018-09-20 09:42:14', '2018-09-20 09:42:14'),
(953, 4, 'Ngọc', 'admin', 'update', 'product', 13, 'RL-10347 Real Keyless Lock ( Black)', '2018-09-20 09:44:00', '2018-09-20 09:44:00'),
(954, 4, 'Ngọc', 'admin', 'update', 'product', 14, 'RL-9041 Real Keyless Lock', '2018-09-20 09:46:23', '2018-09-20 09:46:23'),
(955, 4, 'Ngọc', 'admin', 'update', 'product', 14, 'RL-9041 Real Keyless Lock', '2018-09-20 09:47:33', '2018-09-20 09:47:33'),
(956, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-21 09:33:05', '2018-09-21 09:33:05'),
(957, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-21 09:34:38', '2018-09-21 09:34:38'),
(958, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-24 07:35:54', '2018-09-24 07:35:54'),
(959, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-25 08:21:21', '2018-09-25 08:21:21'),
(960, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-25 08:31:29', '2018-09-25 08:31:29'),
(961, 1, 'Super Admin', 'admin', 'update', 'product', 6, 'Women’s Handbag', '2018-09-25 09:13:09', '2018-09-25 09:13:09'),
(962, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-25 09:16:09', '2018-09-25 09:16:09'),
(963, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-25 09:16:40', '2018-09-25 09:16:40'),
(964, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-25 09:49:28', '2018-09-25 09:49:28'),
(965, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-25 09:49:29', '2018-09-25 09:49:29'),
(966, 1, 'Super Admin', 'admin', 'update', 'gallery', 2, 'company', '2018-09-25 09:49:43', '2018-09-25 09:49:43'),
(967, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-25 09:51:05', '2018-09-25 09:51:05'),
(968, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-25 10:21:31', '2018-09-25 10:21:31'),
(969, 2, 'Admin', 'admin', 'update', 'product', 14, 'RL-9041 Real Keyless Lock', '2018-09-25 10:50:21', '2018-09-25 10:50:21'),
(970, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-25 10:52:56', '2018-09-25 10:52:56'),
(971, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-25 10:53:08', '2018-09-25 10:53:08'),
(972, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-25 10:53:31', '2018-09-25 10:53:31'),
(973, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-25 10:53:52', '2018-09-25 10:53:52'),
(974, 2, 'Admin', 'admin', 'update', 'product', 14, 'RL-9041 Real Keyless Lock', '2018-09-25 10:58:42', '2018-09-25 10:58:42'),
(975, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-26 03:16:13', '2018-09-26 03:16:13'),
(976, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-26 03:42:27', '2018-09-26 03:42:27'),
(977, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-26 03:42:40', '2018-09-26 03:42:40'),
(978, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-09-26 07:53:42', '2018-09-26 07:53:42'),
(979, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-27 09:40:28', '2018-09-27 09:40:28'),
(980, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-27 10:07:55', '2018-09-27 10:07:55'),
(981, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-28 03:29:59', '2018-09-28 03:29:59'),
(982, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-09-28 06:30:43', '2018-09-28 06:30:43'),
(983, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-09-28 06:31:23', '2018-09-28 06:31:23'),
(984, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-04 02:31:55', '2018-10-04 02:31:55'),
(985, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-04 07:26:50', '2018-10-04 07:26:50'),
(986, 2, 'Admin', 'admin', 'update', 'product', 1, 'Fort Collins Men’s Cotton Coat', '2018-10-04 07:30:37', '2018-10-04 07:30:37'),
(987, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:13:57', '2018-10-05 03:13:57'),
(988, 2, 'Admin', 'admin', 'update', 'menu', 5, 'Tủ Locker', '2018-10-05 03:14:19', '2018-10-05 03:14:19'),
(989, 2, 'Admin', 'admin', 'update', 'collection', 5, 'ABS Locker', '2018-10-05 03:16:07', '2018-10-05 03:16:07'),
(990, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:30:41', '2018-10-05 03:30:41'),
(991, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:30:47', '2018-10-05 03:30:47'),
(992, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:31:15', '2018-10-05 03:31:15'),
(993, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:34:58', '2018-10-05 03:34:58'),
(994, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:34:59', '2018-10-05 03:34:59'),
(995, 2, 'Admin', 'admin', 'logout', 'user', 0, '', '2018-10-05 03:35:12', '2018-10-05 03:35:12'),
(996, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:35:17', '2018-10-05 03:35:17'),
(997, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-10-05 03:35:42', '2018-10-05 03:35:42'),
(998, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:37:35', '2018-10-05 03:37:35'),
(999, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:37:36', '2018-10-05 03:37:36'),
(1000, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-10-05 03:37:56', '2018-10-05 03:37:56'),
(1001, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:55:19', '2018-10-05 03:55:19'),
(1002, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:55:19', '2018-10-05 03:55:19'),
(1003, 1, 'Super Admin', 'admin', 'update', 'article', 2, 'Spotlight on Geordie Willis', '2018-10-05 03:55:54', '2018-10-05 03:55:54'),
(1004, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-10-05 03:56:20', '2018-10-05 03:56:20'),
(1005, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 03:59:22', '2018-10-05 03:59:22'),
(1006, 1, 'Super Admin', 'admin', 'update', 'article', 2, 'Spotlight on Geordie Willis', '2018-10-05 04:03:27', '2018-10-05 04:03:27'),
(1007, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-10-05 04:05:02', '2018-10-05 04:05:02'),
(1008, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-10-05 04:05:41', '2018-10-05 04:05:41'),
(1009, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 04:33:02', '2018-10-05 04:33:02'),
(1010, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 04:33:03', '2018-10-05 04:33:03'),
(1011, 2, 'Admin', 'admin', 'update', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-10-05 04:33:35', '2018-10-05 04:33:35'),
(1012, 2, 'Admin', 'admin', 'update', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-10-05 04:35:09', '2018-10-05 04:35:09'),
(1013, 2, 'Admin', 'admin', 'update', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-10-05 04:35:38', '2018-10-05 04:35:38'),
(1014, 2, 'Admin', 'admin', 'update', 'menu', 27, 'Bàn văn phòng', '2018-10-05 06:31:14', '2018-10-05 06:31:14'),
(1015, 2, 'Admin', 'admin', 'update', 'menu', 28, 'Văn phòng phẩm', '2018-10-05 06:31:31', '2018-10-05 06:31:31'),
(1016, 2, 'Admin', 'admin', 'update', 'article', 2, 'Spotlight on Geordie Willis', '2018-10-05 06:33:47', '2018-10-05 06:33:47'),
(1017, 2, 'Admin', 'admin', 'update', 'article', 2, 'Spotlight on Geordie Willis', '2018-10-05 06:34:05', '2018-10-05 06:34:05'),
(1018, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 07:30:40', '2018-10-05 07:30:40'),
(1019, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-05 07:43:58', '2018-10-05 07:43:58'),
(1020, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-10-05 07:44:18', '2018-10-05 07:44:18'),
(1021, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-10-06 21:11:46', '2018-10-06 21:11:46'),
(1022, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-08 04:46:36', '2018-10-08 04:46:36'),
(1023, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-10-13 04:49:02', '2018-10-13 04:49:02'),
(1024, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-10-13 04:49:35', '2018-10-13 04:49:35'),
(1025, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-10-13 04:51:08', '2018-10-13 04:51:08'),
(1026, 4, 'Ngọc', 'admin', 'login', 'user', 0, '', '2018-10-28 03:42:30', '2018-10-28 03:42:30'),
(1027, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-10-29 10:23:14', '2018-10-29 10:23:14'),
(1028, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-11-01 03:46:50', '2018-11-01 03:46:50'),
(1029, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-11-02 07:08:51', '2018-11-02 07:08:51'),
(1030, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-06 07:48:48', '2018-11-06 07:48:48'),
(1031, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-11-07 02:18:21', '2018-11-07 02:18:21'),
(1032, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-07 02:37:56', '2018-11-07 02:37:56'),
(1033, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 02:40:49', '2018-11-07 02:40:49'),
(1034, 1, 'Super Admin', 'admin', 'update', 'article', 2, 'Spotlight on Geordie Willis', '2018-11-07 02:41:32', '2018-11-07 02:41:32'),
(1035, 1, 'Super Admin', 'admin', 'update', 'article', 5, 'Brunch on Saturday', '2018-11-07 02:41:43', '2018-11-07 02:41:43'),
(1036, 1, 'Super Admin', 'admin', 'update', 'product', 2, 'Fort Collins Men’s Cotton Coat', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(1037, 1, 'Super Admin', 'admin', 'update', 'product', 3, 'Black Tote', '2018-11-07 02:44:25', '2018-11-07 02:44:25'),
(1038, 1, 'Super Admin', 'admin', 'update', 'product', 4, 'Double Breasted Coat', '2018-11-07 02:44:39', '2018-11-07 02:44:39'),
(1039, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-11-07 06:39:18', '2018-11-07 06:39:18'),
(1040, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 06:47:50', '2018-11-07 06:47:50'),
(1041, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 06:48:14', '2018-11-07 06:48:14'),
(1042, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 06:48:38', '2018-11-07 06:48:38'),
(1043, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 06:50:52', '2018-11-07 06:50:52'),
(1044, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 06:55:57', '2018-11-07 06:55:57'),
(1045, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 07:00:31', '2018-11-07 07:00:31'),
(1046, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 07:01:20', '2018-11-07 07:01:20'),
(1047, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 07:01:36', '2018-11-07 07:01:36'),
(1048, 2, 'Admin', 'admin', 'update', 'menu', 2, 'TRANG CHỦ', '2018-11-07 07:02:35', '2018-11-07 07:02:35'),
(1049, 2, 'Admin', 'admin', 'update', 'menu', 3, 'GIỚI THIỆU', '2018-11-07 07:02:51', '2018-11-07 07:02:51'),
(1050, 2, 'Admin', 'admin', 'update', 'menu', 4, 'SẢN PHẨM', '2018-11-07 07:03:08', '2018-11-07 07:03:08'),
(1051, 2, 'Admin', 'admin', 'update', 'menu', 8, 'TIN TỨC', '2018-11-07 07:03:24', '2018-11-07 07:03:24'),
(1052, 2, 'Admin', 'admin', 'update', 'menu', 12, 'KHÁCH HÀNG', '2018-11-07 07:03:40', '2018-11-07 07:03:40'),
(1053, 2, 'Admin', 'admin', 'update', 'menu', 13, 'LIÊN HỆ', '2018-11-07 07:03:54', '2018-11-07 07:03:54'),
(1054, 2, 'Admin', 'admin', 'update', 'article', 28, 'Spotlight on Geordie Willis', '2018-11-07 07:14:21', '2018-11-07 07:14:21'),
(1055, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 07:16:50', '2018-11-07 07:16:50'),
(1056, 2, 'Admin', 'admin', 'update', 'article', 28, 'Spotlight on Geordie Willis', '2018-11-07 07:17:29', '2018-11-07 07:17:29'),
(1057, 2, 'Admin', 'admin', 'update', 'article', 29, 'Blackpool Illuminations, Lancashire', '2018-11-07 07:18:33', '2018-11-07 07:18:33'),
(1058, 2, 'Admin', 'admin', 'update', 'article', 29, 'Blackpool Illuminations, Lancashire', '2018-11-07 07:19:20', '2018-11-07 07:19:20'),
(1059, 2, 'Admin', 'admin', 'update', 'article', 29, 'Blackpool Illuminations, Lancashire', '2018-11-07 07:20:11', '2018-11-07 07:20:11'),
(1060, 2, 'Admin', 'admin', 'update', 'article', 30, 'Brunch on Saturday', '2018-11-07 07:21:45', '2018-11-07 07:21:45'),
(1061, 2, 'Admin', 'admin', 'create', 'article', 31, 'Banksy identity', '2018-11-07 07:23:58', '2018-11-07 07:23:58'),
(1062, 2, 'Admin', 'admin', 'create', 'article', 32, 'Nick Cave And The Bad Seeds', '2018-11-07 07:24:57', '2018-11-07 07:24:57'),
(1063, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 07:25:17', '2018-11-07 07:25:17'),
(1064, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 07:26:56', '2018-11-07 07:26:56'),
(1065, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 07:27:41', '2018-11-07 07:27:41'),
(1066, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 07:33:12', '2018-11-07 07:33:12'),
(1067, 2, 'Admin', 'admin', 'inactive', 'blog', 8, 'Hỗ trợ khách hàng', '2018-11-07 07:34:17', '2018-11-07 07:34:17'),
(1068, 2, 'Admin', 'admin', 'update', 'blog', 10, 'New Trend', '2018-11-07 07:35:40', '2018-11-07 07:35:40'),
(1069, 2, 'Admin', 'admin', 'update', 'blog', 4, 'Post Format', '2018-11-07 07:37:09', '2018-11-07 07:37:09'),
(1070, 2, 'Admin', 'admin', 'update', 'blog', 12, 'Human', '2018-11-07 07:38:03', '2018-11-07 07:38:03'),
(1071, 2, 'Admin', 'admin', 'update', 'blog', 11, 'Spirit', '2018-11-07 07:38:30', '2018-11-07 07:38:30'),
(1072, 2, 'Admin', 'admin', 'update', 'article', 7, 'Spotlight on Geordie Willis', '2018-11-07 07:39:35', '2018-11-07 07:39:35'),
(1073, 2, 'Admin', 'admin', 'update', 'article', 25, 'Hỗ trợ 1', '2018-11-07 07:40:12', '2018-11-07 07:40:12'),
(1074, 2, 'Admin', 'admin', 'update', 'article', 7, 'Spotlight on Geordie Willis', '2018-11-07 07:40:37', '2018-11-07 07:40:37'),
(1075, 2, 'Admin', 'admin', 'update', 'article', 13, 'Specialized supplies', '2018-11-07 07:40:59', '2018-11-07 07:40:59'),
(1076, 2, 'Admin', 'admin', 'update', 'article', 10, 'Effortless customization', '2018-11-07 07:41:09', '2018-11-07 07:41:09'),
(1077, 2, 'Admin', 'admin', 'update', 'article', 25, 'Hỗ trợ 1', '2018-11-07 07:41:29', '2018-11-07 07:41:29'),
(1078, 2, 'Admin', 'admin', 'update', 'article', 32, 'Nick Cave And The Bad Seeds', '2018-11-07 07:41:54', '2018-11-07 07:41:54'),
(1079, 2, 'Admin', 'admin', 'update', 'article', 31, 'Banksy identity', '2018-11-07 07:42:05', '2018-11-07 07:42:05'),
(1080, 2, 'Admin', 'admin', 'update', 'menu', 5, 'Sản phẩm 1', '2018-11-07 07:43:43', '2018-11-07 07:43:43'),
(1081, 2, 'Admin', 'admin', 'update', 'menu', 6, 'Sản phẩm 2', '2018-11-07 07:43:52', '2018-11-07 07:43:52'),
(1082, 2, 'Admin', 'admin', 'update', 'menu', 7, 'Sản phẩm 3', '2018-11-07 07:44:03', '2018-11-07 07:44:03'),
(1083, 2, 'Admin', 'admin', 'update', 'menu', 9, 'Tin tức 1', '2018-11-07 07:44:30', '2018-11-07 07:44:30'),
(1084, 2, 'Admin', 'admin', 'update', 'menu', 10, 'Tin tức 2', '2018-11-07 07:44:42', '2018-11-07 07:44:42'),
(1085, 2, 'Admin', 'admin', 'update', 'menu', 11, 'Tin tức 3', '2018-11-07 07:44:51', '2018-11-07 07:44:51'),
(1086, 2, 'Admin', 'admin', 'update', 'page', 2, 'GIỚI THIỆU', '2018-11-07 07:52:06', '2018-11-07 07:52:06'),
(1087, 2, 'Admin', 'admin', 'update', 'article', 14, 'Powerful Shortcodes', '2018-11-07 08:05:32', '2018-11-07 08:05:32'),
(1088, 2, 'Admin', 'admin', 'update', 'article', 12, 'Easy page builder', '2018-11-07 08:09:52', '2018-11-07 08:09:52'),
(1089, 2, 'Admin', 'admin', 'update', 'article', 11, 'Fast loading speed', '2018-11-07 08:13:19', '2018-11-07 08:13:19'),
(1090, 2, 'Admin', 'admin', 'update', 'article', 13, 'Fully Responsive', '2018-11-07 08:17:34', '2018-11-07 08:17:34'),
(1091, 2, 'Admin', 'admin', 'update', 'blog', 7, 'What we do', '2018-11-07 08:18:04', '2018-11-07 08:18:04'),
(1092, 2, 'Admin', 'admin', 'update', 'article', 11, 'Fast loading speed', '2018-11-07 08:19:00', '2018-11-07 08:19:00'),
(1093, 2, 'Admin', 'admin', 'update', 'article', 11, 'Fast loading speed', '2018-11-07 08:19:19', '2018-11-07 08:19:19'),
(1094, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 08:31:33', '2018-11-07 08:31:33'),
(1095, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 08:34:01', '2018-11-07 08:34:01'),
(1096, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 08:37:30', '2018-11-07 08:37:30'),
(1097, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 08:40:33', '2018-11-07 08:40:33'),
(1098, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 08:43:18', '2018-11-07 08:43:18'),
(1099, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 08:48:24', '2018-11-07 08:48:24'),
(1100, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 08:49:49', '2018-11-07 08:49:49'),
(1101, 2, 'Admin', 'admin', 'update', 'article', 29, 'Blackpool Lancashire', '2018-11-07 08:50:28', '2018-11-07 08:50:28'),
(1102, 2, 'Admin', 'admin', 'update', 'article', 25, 'Brunch on Saturday', '2018-11-07 08:52:49', '2018-11-07 08:52:49'),
(1103, 2, 'Admin', 'admin', 'update', 'article', 25, 'Brunch on Saturday', '2018-11-07 08:53:19', '2018-11-07 08:53:19'),
(1104, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 08:54:20', '2018-11-07 08:54:20'),
(1105, 2, 'Admin', 'admin', 'update', 'blog', 13, 'Life Style', '2018-11-07 08:54:49', '2018-11-07 08:54:49'),
(1106, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-07 08:55:54', '2018-11-07 08:55:54'),
(1107, 2, 'Admin', 'admin', 'create', 'product', 15, 'Product 1', '2018-11-07 08:58:31', '2018-11-07 08:58:31'),
(1108, 2, 'Admin', 'admin', 'create', 'product', 16, 'Product 2', '2018-11-07 08:58:47', '2018-11-07 08:58:47'),
(1109, 2, 'Admin', 'admin', 'create', 'product', 17, 'Product 3', '2018-11-07 08:59:49', '2018-11-07 08:59:49'),
(1110, 2, 'Admin', 'admin', 'update', 'product', 15, 'Product 1', '2018-11-07 09:00:08', '2018-11-07 09:00:08'),
(1111, 2, 'Admin', 'admin', 'update', 'product', 16, 'Product 2', '2018-11-07 09:00:14', '2018-11-07 09:00:14'),
(1112, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 09:00:49', '2018-11-07 09:00:49'),
(1113, 2, 'Admin', 'admin', 'update', 'product', 16, 'Product 2', '2018-11-07 09:01:09', '2018-11-07 09:01:09'),
(1114, 2, 'Admin', 'admin', 'update', 'product', 17, 'Product 3', '2018-11-07 09:01:14', '2018-11-07 09:01:14'),
(1115, 2, 'Admin', 'admin', 'update', 'product', 16, 'Product 2', '2018-11-07 09:01:41', '2018-11-07 09:01:41'),
(1116, 2, 'Admin', 'admin', 'update', 'product', 17, 'Product 3', '2018-11-07 09:01:44', '2018-11-07 09:01:44'),
(1117, 2, 'Admin', 'admin', 'update', 'product', 16, 'Product 2', '2018-11-07 09:01:59', '2018-11-07 09:01:59'),
(1118, 2, 'Admin', 'admin', 'update', 'product', 17, 'Product 3', '2018-11-07 09:02:03', '2018-11-07 09:02:03'),
(1119, 2, 'Admin', 'admin', 'update', 'product', 16, 'Product 2', '2018-11-07 09:03:23', '2018-11-07 09:03:23'),
(1120, 2, 'Admin', 'admin', 'update', 'product', 17, 'Product 3', '2018-11-07 09:04:00', '2018-11-07 09:04:00'),
(1121, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 09:06:07', '2018-11-07 09:06:07'),
(1122, 2, 'Admin', 'admin', 'update', 'product', 15, 'Product 1', '2018-11-07 09:08:25', '2018-11-07 09:08:25'),
(1123, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 09:10:29', '2018-11-07 09:10:29'),
(1124, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 09:11:39', '2018-11-07 09:11:39'),
(1125, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 09:12:39', '2018-11-07 09:12:39'),
(1126, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-11-07 09:18:03', '2018-11-07 09:18:03'),
(1127, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 09:18:35', '2018-11-07 09:18:35'),
(1128, 2, 'Admin', 'admin', 'login', 'user', 0, '', '2018-11-07 10:30:27', '2018-11-07 10:30:27'),
(1129, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 10:31:24', '2018-11-07 10:31:24'),
(1130, 2, 'Admin', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-07 10:32:15', '2018-11-07 10:32:15'),
(1131, 2, 'Admin', 'admin', 'update', 'order', 36, '', '2018-11-07 10:38:16', '2018-11-07 10:38:16'),
(1132, 2, 'Admin', 'admin', 'update', 'order', 36, '', '2018-11-07 10:38:33', '2018-11-07 10:38:33'),
(1133, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-08 07:00:23', '2018-11-08 07:00:23'),
(1134, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-08 07:45:28', '2018-11-08 07:45:28'),
(1135, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-08 08:13:00', '2018-11-08 08:13:00'),
(1136, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-11-08 08:17:42', '2018-11-08 08:17:42'),
(1137, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-08 08:18:25', '2018-11-08 08:18:25'),
(1138, 1, 'Super Admin', 'admin', 'login', 'user', 0, '', '2018-11-08 08:19:28', '2018-11-08 08:19:28'),
(1139, 1, 'Super Admin', 'admin', 'logout', 'user', 0, '', '2018-11-08 08:19:33', '2018-11-08 08:19:33'),
(1140, 6, 'danh', 'admin', 'login', 'user', 0, '', '2018-11-08 08:19:39', '2018-11-08 08:19:39'),
(1141, 1, 'Super Admin', 'admin', 'update', 'role', 1, 'Quản trị viên cấp cao', '2018-11-08 08:25:18', '2018-11-08 08:25:18'),
(1142, 1, 'Super Admin', 'admin', 'update', 'role', 1, 'Quản trị viên cấp cao', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(1143, 6, 'danh', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-08 08:43:38', '2018-11-08 08:43:38'),
(1144, 6, 'danh', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-08 08:44:02', '2018-11-08 08:44:02'),
(1145, 6, 'danh', 'admin', 'update', 'setting', 0, 'Update Setting', '2018-11-08 08:44:33', '2018-11-08 08:44:33'),
(1146, 1, 'Super Admin', 'admin', 'update', 'setting', 0, 'Update Super Admin Setting', '2018-11-08 08:45:25', '2018-11-08 08:45:25');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `name`, `type`, `type_id`, `created_at`, `updated_at`) VALUES
(10, '1533892472_shop_17.jpg', 'product', '5', '2018-08-10 09:14:48', '2018-08-10 09:14:48'),
(12, '1533892590_shop_15.jpg', 'product', '7', '2018-08-10 09:16:44', '2018-08-10 09:16:44'),
(30, '1535295014_3_seater_sofa.png', 'product', '10', '2018-08-26 15:09:40', '2018-08-26 15:09:40'),
(31, '1535295169_black_desk.png', 'product', '10', '2018-08-26 15:09:40', '2018-08-26 15:09:40'),
(39, '1535298407_abs-mseries04.jpg', 'product', '9', '2018-09-20 03:20:07', '2018-09-20 03:20:07'),
(42, '1537430919_product-try.png', 'product', '8', '2018-09-20 08:50:24', '2018-09-20 08:50:24'),
(43, '1535298407_abs-mseries04.jpg', 'product', '11', '2018-09-20 08:51:22', '2018-09-20 08:51:22'),
(46, '1537434467_rl10347-02_trang.jpg', 'product', '12', '2018-09-20 09:14:04', '2018-09-20 09:14:04'),
(53, '1537436424_rl-10347_combination_locks_06.jpg', 'product', '13', '2018-09-20 09:44:00', '2018-09-20 09:44:00'),
(56, '1533892526_shop_14.jpg', 'product', '6', '2018-09-25 09:13:09', '2018-09-25 09:13:09'),
(58, '1537435576_rl9041.jpg', 'product', '14', '2018-09-25 10:58:42', '2018-09-25 10:58:42'),
(59, '1533892138_shop_11.jpg', 'product', '1', '2018-10-04 07:30:37', '2018-10-04 07:30:37'),
(60, '1533892182_shop_13.jpg', 'product', '1', '2018-10-04 07:30:37', '2018-10-04 07:30:37'),
(61, '1533892437_shop_12.jpg', 'product', '1', '2018-10-04 07:30:37', '2018-10-04 07:30:37'),
(78, '1533892437_shop_12.jpg', 'product', '2', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(79, '1533892138_shop_11.jpg', 'product', '2', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(80, '1533892655_shop_16.jpg', 'product', '2', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(81, '1533892590_shop_15.jpg', 'product', '2', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(82, '1533892526_shop_14.jpg', 'product', '2', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(83, '1537434467_rl10347-02_trang.jpg', 'product', '2', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(84, '1533892182_shop_13-min_1541558316.jpg', 'product', '2', '2018-11-07 02:43:57', '2018-11-07 02:43:57'),
(85, '1533892336_shop_10-min_1541558316.jpg', 'product', '3', '2018-11-07 02:44:25', '2018-11-07 02:44:25'),
(86, '1533892437_shop_12-min_1541558316.jpg', 'product', '4', '2018-11-07 02:44:39', '2018-11-07 02:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '1000',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `title`, `handle`, `link`, `link_type`, `priority`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, -1, 'Header', 'header', '', '', 0, '', 'active', '2018-08-11 02:38:14', '2018-09-05 08:36:45'),
(2, 1, 'TRANG CHỦ', 'trang-chu', '/', 'custom', 65, '', 'active', '2018-08-11 02:38:34', '2018-11-07 07:02:35'),
(3, 1, 'GIỚI THIỆU', 'gioi-thieu', '2', 'page', 66, '', 'active', '2018-08-11 02:38:53', '2018-11-07 07:02:51'),
(4, 1, 'SẢN PHẨM', 'san-pham', '/collection/all', 'custom', 67, '', 'active', '2018-08-11 02:39:29', '2018-11-07 07:03:08'),
(5, 4, 'Sản phẩm 1', 'san-pham-1', '5', 'collection', 45, '', 'active', '2018-08-11 02:40:01', '2018-11-07 07:43:43'),
(6, 4, 'Sản phẩm 2', 'san-pham-2', '5', 'collection', 46, '', 'active', '2018-08-11 02:40:21', '2018-11-07 07:43:52'),
(7, 4, 'Sản phẩm 3', 'san-pham-3', '3', 'collection', 47, '', 'active', '2018-08-11 02:40:54', '2018-11-07 07:44:03'),
(8, 1, 'TIN TỨC', 'tin-tuc', '13', 'blog', 68, '', 'active', '2018-08-11 02:41:29', '2018-11-07 07:03:24'),
(9, 8, 'Tin tức 1', 'tin-tuc-1', '28', 'article', 28, '', 'active', '2018-08-11 02:41:51', '2018-11-07 07:44:30'),
(10, 8, 'Tin tức 2', 'tin-tuc-2', '29', 'article', 29, '', 'active', '2018-08-11 02:42:10', '2018-11-07 07:44:42'),
(11, 8, 'Tin tức 3', 'tin-tuc-3', '30', 'article', 30, '', 'active', '2018-08-11 02:42:28', '2018-11-07 07:44:51'),
(12, 1, 'KHÁCH HÀNG', 'khach-hang', '8', 'blog', 69, '', 'active', '2018-08-11 02:42:51', '2018-11-07 07:03:40'),
(13, 1, 'LIÊN HỆ', 'lien-he', '/lien-he', 'custom', 70, '', 'active', '2018-08-11 02:43:09', '2018-11-07 07:03:54'),
(14, -1, 'Our service', 'our-service', '', '', 1, '', 'active', '2018-08-14 02:07:56', '2018-08-14 02:10:05'),
(15, 14, 'Working memo', 'working-memo', '/', 'custom', 0, '', 'active', '2018-08-14 02:09:25', '2018-08-26 17:57:37'),
(16, 14, 'New Trend', 'new-trend', '/', 'custom', 1, '', 'active', '2018-08-14 02:09:36', '2018-08-14 02:09:36'),
(17, 14, 'Post Format', 'post-format', '/', 'custom', 2, '', 'active', '2018-08-14 02:09:49', '2018-08-14 02:09:49'),
(18, 14, 'What we do', 'what-we-do', '/', 'custom', 3, '', 'active', '2018-08-14 02:10:00', '2018-08-14 02:10:00'),
(22, -1, 'Right wwd', 'right-wwd', '', '', 2, '', 'active', '2018-08-14 02:41:08', '2018-08-14 02:42:24'),
(23, 22, 'Title 1', 'title-1', '/', 'custom', 0, '', 'active', '2018-08-14 02:41:44', '2018-08-17 02:15:36'),
(24, 22, 'Tiltle', 'tiltle', '/', 'custom', 1, '', 'active', '2018-08-14 02:41:55', '2018-08-17 02:15:43'),
(25, 22, 'con3', 'con3', '/', 'custom', 2, '', 'active', '2018-08-14 02:42:07', '2018-08-14 02:42:07'),
(26, 22, 'con4', 'con4', '/', 'custom', 3, '', 'active', '2018-08-14 02:42:21', '2018-08-14 02:42:21');

-- --------------------------------------------------------

--
-- Table structure for table `menu_translations`
--

CREATE TABLE `menu_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `metafield`
--

CREATE TABLE `metafield` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'product',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `metafield`
--

INSERT INTO `metafield` (`id`, `title`, `handle`, `value`, `post_id`, `post_type`, `created_at`, `updated_at`) VALUES
(1, 'Tiêu đề dưới', 'tieu-de-duoi', '& Reliability', 1, 'photo', '2018-08-10 07:37:27', '2018-09-10 02:10:00'),
(2, 'Tiêu đề dưới', 'tieu-de-duoi', 'With Right Supplies', 2, 'photo', '2018-08-10 07:38:07', '2018-09-10 02:09:45'),
(3, 'Tiêu đề dưới', 'tieu-de-duoi', 'From Prestigious Brands', 3, 'photo', '2018-08-10 07:38:58', '2018-09-10 02:10:11'),
(4, 'Tiêu đề dưới', 'tieu-de-duoi', 'With Customer Care', 4, 'photo', '2018-08-10 07:39:25', '2018-09-10 02:10:34'),
(5, 'Tiêu đề dưới', 'tieu-de-duoi', '', 5, 'photo', '2018-08-10 08:27:28', '2018-08-10 08:27:28'),
(6, 'Tiêu đề dưới', 'tieu-de-duoi', '', 6, 'photo', '2018-08-10 08:27:54', '2018-08-10 08:27:54'),
(7, 'Tiêu đề dưới', 'tieu-de-duoi', '', 7, 'photo', '2018-08-10 08:28:11', '2018-08-10 08:28:11'),
(8, 'Tiêu đề dưới', 'tieu-de-duoi', '', 8, 'photo', '2018-08-10 08:28:25', '2018-08-10 08:28:25'),
(9, 'Tiêu đề dưới', 'tieu-de-duoi', '', 9, 'photo', '2018-08-10 08:28:38', '2018-08-10 08:28:38'),
(10, 'Tiêu đề dưới', 'tieu-de-duoi', '', 10, 'photo', '2018-08-10 08:28:57', '2018-08-10 08:28:57'),
(11, 'Tiêu đề dưới', 'tieu-de-duoi', '', 11, 'photo', '2018-08-10 08:29:10', '2018-08-10 08:29:10'),
(12, 'Sale', 'sale', 'true', 3, 'product', '2018-08-10 09:20:45', '2018-08-10 09:56:51'),
(13, 'Sale', 'sale', 'false', 8, 'product', '2018-08-10 09:58:47', '2018-08-10 09:58:55'),
(14, 'Tiêu đề', 'tieu-de', 'ABOUT REAL TRADING', 2, 'page', '2018-08-11 07:40:07', '2018-11-07 07:52:06'),
(15, 'Mô tả', 'mo-ta', '<h2 class=\"sub-heading\"><span class=\"f2\">Rethinking</span>&nbsp;<em><span class=\"f1\">our relationship</span></em><br /><span class=\"f2\">with technology.</span></h2>', 2, 'page', '2018-08-11 07:40:08', '2018-11-07 07:52:06'),
(16, 'Hình phải', 'hinh-phai', '1533973396_about3.png', 2, 'page', '2018-08-11 07:43:20', '2018-11-07 07:52:06'),
(17, 'Mô tả', 'mo-ta', '', 1, 'page', '2018-08-14 07:51:49', '2018-09-12 06:00:58'),
(18, 'Hình phải', 'hinh-phai', '1535898047_customer_support_793_x826_(1).png', 1, 'page', '2018-08-14 07:51:49', '2018-09-12 06:00:58'),
(19, 'Tiêu đề', 'tieu-de', 'NICE TO MEET YOU', 1, 'page', '2018-08-14 07:51:49', '2018-09-12 06:00:58'),
(20, 'Người viết', 'nguoi-viet', '', 4, 'blog', '2018-08-14 07:53:21', '2018-11-07 07:37:10'),
(21, 'Tiêu đề phải', 'tieu-de-phai', '', 4, 'blog', '2018-08-14 07:53:21', '2018-08-16 02:49:08'),
(22, 'Banner', 'banner', '1534213298_bg_testimonial.jpg', 4, 'blog', '2018-08-14 07:53:21', '2018-11-07 07:37:10'),
(23, 'Tiêu đề dưới', 'tieu-de-duoi', '', 12, 'photo', '2018-08-14 07:55:17', '2018-08-14 07:55:17'),
(24, 'Người viết', 'nguoi-viet', '', 3, 'blog', '2018-08-14 08:01:09', '2018-08-14 08:03:40'),
(25, 'Banner', 'banner', '', 3, 'blog', '2018-08-14 08:01:09', '2018-08-14 08:03:40'),
(26, 'Tiêu đề phải', 'tieu-de-phai', '', 3, 'blog', '2018-08-14 08:01:09', '2018-08-14 08:03:40'),
(27, 'Người viết', 'nguoi-viet', '', 5, 'blog', '2018-08-14 09:31:00', '2018-08-14 09:31:14'),
(28, 'Tiêu đề phải', 'tieu-de-phai', '', 5, 'blog', '2018-08-14 09:31:00', '2018-08-14 09:31:13'),
(29, 'Banner', 'banner', '', 5, 'blog', '2018-08-14 09:31:00', '2018-08-14 09:31:13'),
(30, 'Tiêu đề phải', 'tieu-de-phai', '', 6, 'blog', '2018-08-14 09:37:14', '2018-08-14 09:37:14'),
(31, 'Người viết', 'nguoi-viet', '', 6, 'blog', '2018-08-14 09:37:14', '2018-08-14 09:37:14'),
(32, 'Banner', 'banner', '', 6, 'blog', '2018-08-14 09:37:14', '2018-08-14 09:37:14'),
(33, 'Tiêu đề phải', 'tieu-de-phai', 'test', 21, 'article', '2018-08-16 02:21:14', '2018-08-18 08:15:04'),
(34, 'Người viết', 'nguoi-viet', 'test', 21, 'article', '2018-08-16 02:21:14', '2018-08-18 08:15:04'),
(35, 'Banner', 'banner', '1533981138_blog_detail.jpg', 21, 'article', '2018-08-16 02:21:14', '2018-08-18 08:15:04'),
(36, 'Bài viết liên quan', 'bai-viet-lien-quan', 'test', 21, 'article', '2018-08-16 02:21:14', '2018-08-18 08:15:04'),
(37, 'Tiêu đề phải', 'tieu-de-phai', '', 22, 'article', '2018-08-16 02:35:44', '2018-08-16 02:42:15'),
(38, 'Banner', 'banner', '', 22, 'article', '2018-08-16 02:35:44', '2018-08-16 02:42:15'),
(39, 'Người viết', 'nguoi-viet', '', 22, 'article', '2018-08-16 02:35:44', '2018-08-16 02:42:15'),
(40, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 22, 'article', '2018-08-16 02:35:44', '2018-08-16 02:42:15'),
(41, 'Tiêu đề phải', 'tieu-de-phai', '', 7, 'blog', '2018-08-16 02:40:18', '2018-08-21 07:37:22'),
(42, 'Người viết', 'nguoi-viet', '', 7, 'blog', '2018-08-16 02:40:18', '2018-11-07 08:18:04'),
(43, 'Tiêu đề banner', 'tieu-de-banner', '', 7, 'blog', '2018-08-16 02:40:18', '2018-11-07 08:18:04'),
(44, 'Banner', 'banner', '', 7, 'blog', '2018-08-16 02:40:18', '2018-11-07 08:18:04'),
(45, 'Tiêu đề phải', 'tieu-de-phai', '', 9, 'article', '2018-08-16 02:41:05', '2018-08-18 08:36:05'),
(46, 'Người viết', 'nguoi-viet', '', 9, 'article', '2018-08-16 02:41:05', '2018-09-04 04:21:35'),
(47, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 9, 'article', '2018-08-16 02:41:05', '2018-09-04 04:21:35'),
(48, 'Banner', 'banner', '', 9, 'article', '2018-08-16 02:41:05', '2018-09-04 04:21:35'),
(49, 'Tiêu đề phải', 'tieu-de-phai', '', 14, 'article', '2018-08-16 02:41:32', '2018-08-18 08:37:25'),
(50, 'Banner', 'banner', '', 14, 'article', '2018-08-16 02:41:32', '2018-11-07 08:05:32'),
(51, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 14, 'article', '2018-08-16 02:41:32', '2018-08-26 18:52:07'),
(52, 'Người viết', 'nguoi-viet', '', 14, 'article', '2018-08-16 02:41:32', '2018-11-07 08:05:32'),
(53, 'Tiêu đề phải', 'tieu-de-phai', '', 13, 'article', '2018-08-16 02:41:39', '2018-08-18 08:37:09'),
(54, 'Người viết', 'nguoi-viet', '', 13, 'article', '2018-08-16 02:41:39', '2018-11-07 08:17:34'),
(55, 'Banner', 'banner', '', 13, 'article', '2018-08-16 02:41:39', '2018-11-07 08:17:34'),
(56, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 13, 'article', '2018-08-16 02:41:39', '2018-09-04 04:23:10'),
(57, 'Tiêu đề phải', 'tieu-de-phai', '', 12, 'article', '2018-08-16 02:41:45', '2018-08-18 08:35:02'),
(58, 'Banner', 'banner', '1534213298_bg_testimonial.jpg', 12, 'article', '2018-08-16 02:41:45', '2018-11-07 08:09:52'),
(59, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 12, 'article', '2018-08-16 02:41:45', '2018-08-26 19:12:25'),
(60, 'Người viết', 'nguoi-viet', '', 12, 'article', '2018-08-16 02:41:45', '2018-11-07 08:09:52'),
(61, 'Tiêu đề phải', 'tieu-de-phai', '', 11, 'article', '2018-08-16 02:41:53', '2018-08-16 12:00:44'),
(62, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 11, 'article', '2018-08-16 02:41:53', '2018-08-26 19:29:46'),
(63, 'Banner', 'banner', '', 11, 'article', '2018-08-16 02:41:53', '2018-11-07 08:19:20'),
(64, 'Người viết', 'nguoi-viet', '', 11, 'article', '2018-08-16 02:41:53', '2018-11-07 08:19:20'),
(65, 'Banner', 'banner', '1533981138_blog_detail.jpg', 10, 'article', '2018-08-16 02:41:58', '2018-11-07 07:41:09'),
(66, 'Tiêu đề phải', 'tieu-de-phai', 'CATEGORIES', 10, 'article', '2018-08-16 02:41:58', '2018-08-18 08:36:21'),
(67, 'Người viết', 'nguoi-viet', 'Admin', 10, 'article', '2018-08-16 02:41:58', '2018-11-07 07:41:09'),
(68, 'Bài viết liên quan', 'bai-viet-lien-quan', 'RELATED POSTS', 10, 'article', '2018-08-16 02:41:58', '2018-09-04 04:22:02'),
(69, 'Tiêu đề banner', 'tieu-de-banner', '', 4, 'blog', '2018-08-16 02:44:00', '2018-11-07 07:37:10'),
(70, 'Tiêu đề phải', 'tieu-de-phai', '', 23, 'article', '2018-08-16 02:54:17', '2018-08-16 02:54:17'),
(71, 'Banner', 'banner', '', 23, 'article', '2018-08-16 02:54:17', '2018-08-16 02:54:17'),
(72, 'Người viết', 'nguoi-viet', '', 23, 'article', '2018-08-16 02:54:17', '2018-08-16 02:54:17'),
(73, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 23, 'article', '2018-08-16 02:54:17', '2018-08-16 02:54:17'),
(74, 'Tiêu đề phải', 'tieu-de-phai', '', 24, 'article', '2018-08-16 02:55:10', '2018-08-16 02:55:10'),
(75, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 24, 'article', '2018-08-16 02:55:10', '2018-08-16 02:55:10'),
(76, 'Banner', 'banner', '', 24, 'article', '2018-08-16 02:55:10', '2018-08-16 02:55:10'),
(77, 'Người viết', 'nguoi-viet', '', 24, 'article', '2018-08-16 02:55:10', '2018-08-16 02:55:10'),
(78, 'Banner', 'banner', '', 18, 'article', '2018-08-16 03:02:35', '2018-08-16 03:02:35'),
(79, 'Người viết', 'nguoi-viet', '', 18, 'article', '2018-08-16 03:02:35', '2018-08-16 03:02:35'),
(80, 'Tiêu đề phải', 'tieu-de-phai', '', 18, 'article', '2018-08-16 03:02:35', '2018-08-16 03:02:35'),
(81, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 18, 'article', '2018-08-16 03:02:35', '2018-08-16 03:02:35'),
(82, 'Người viết', 'nguoi-viet', '', 8, 'blog', '2018-08-16 03:04:09', '2018-08-26 18:46:18'),
(83, 'Banner', 'banner', '', 8, 'blog', '2018-08-16 03:04:09', '2018-08-26 18:46:18'),
(84, 'Tiêu đề phải', 'tieu-de-phai', '', 8, 'blog', '2018-08-16 03:04:09', '2018-08-22 04:53:24'),
(85, 'Tiêu đề banner', 'tieu-de-banner', '', 8, 'blog', '2018-08-16 03:04:09', '2018-08-26 18:46:18'),
(86, 'Tiêu đề phải', 'tieu-de-phai', 'Categories', 8, 'article', '2018-08-16 03:04:23', '2018-08-16 12:13:47'),
(87, 'Banner', 'banner', '1534213298_bg_testimonial.jpg', 8, 'article', '2018-08-16 03:04:23', '2018-08-26 19:48:11'),
(88, 'Bài viết liên quan', 'bai-viet-lien-quan', 'Related Post', 8, 'article', '2018-08-16 03:04:23', '2018-08-26 19:48:11'),
(89, 'Người viết', 'nguoi-viet', 'Admin', 8, 'article', '2018-08-16 03:04:23', '2018-08-26 19:48:11'),
(90, 'Tiêu đề phải', 'tieu-de-phai', 'Categories', 7, 'article', '2018-08-16 03:04:33', '2018-08-16 12:13:15'),
(91, 'Banner', 'banner', '1534213298_bg_testimonial.jpg', 7, 'article', '2018-08-16 03:04:33', '2018-11-07 07:40:37'),
(92, 'Bài viết liên quan', 'bai-viet-lien-quan', 'Related Post', 7, 'article', '2018-08-16 03:04:33', '2018-08-16 12:13:15'),
(93, 'Người viết', 'nguoi-viet', 'Admin', 7, 'article', '2018-08-16 03:04:33', '2018-11-07 07:40:37'),
(94, 'Tiêu đề phải', 'tieu-de-phai', '', 9, 'blog', '2018-08-16 12:08:03', '2018-08-16 12:08:03'),
(95, 'Người viết', 'nguoi-viet', '', 9, 'blog', '2018-08-16 12:08:03', '2018-08-16 12:08:03'),
(96, 'Banner', 'banner', '', 9, 'blog', '2018-08-16 12:08:03', '2018-08-16 12:08:03'),
(97, 'Tiêu đề banner', 'tieu-de-banner', '', 9, 'blog', '2018-08-16 12:08:03', '2018-08-16 12:08:03'),
(98, 'Tiêu đề phải', 'tieu-de-phai', 'Categories', 25, 'article', '2018-08-16 12:08:24', '2018-08-16 12:10:13'),
(99, 'Banner', 'banner', '1534213298_bg_testimonial.jpg', 25, 'article', '2018-08-16 12:08:24', '2018-11-07 08:53:20'),
(100, 'Người viết', 'nguoi-viet', 'Admin', 25, 'article', '2018-08-16 12:08:25', '2018-11-07 08:53:20'),
(101, 'Bài viết liên quan', 'bai-viet-lien-quan', 'Related Post', 25, 'article', '2018-08-16 12:08:25', '2018-08-25 07:47:43'),
(102, 'Tiêu đề', 'tieu-de', '', 3, 'page', '2018-08-17 03:28:37', '2018-08-17 03:28:37'),
(103, 'Mô tả', 'mo-ta', '', 3, 'page', '2018-08-17 03:28:37', '2018-08-17 03:28:37'),
(104, 'Hình phải', 'hinh-phai', '', 3, 'page', '2018-08-17 03:28:37', '2018-08-17 03:28:37'),
(105, 'Tiêu đề phải', 'tieu-de-phai', '', 1, 'article', '2018-08-18 08:09:24', '2018-08-18 08:09:24'),
(106, 'Banner', 'banner', '', 1, 'article', '2018-08-18 08:09:24', '2018-08-26 19:42:07'),
(107, 'Người viết', 'nguoi-viet', '', 1, 'article', '2018-08-18 08:09:24', '2018-08-26 19:42:07'),
(108, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 1, 'article', '2018-08-18 08:09:24', '2018-08-26 19:42:07'),
(109, 'Tiêu đề phải', 'tieu-de-phai', '', 3, 'article', '2018-08-18 08:09:32', '2018-08-21 07:48:54'),
(110, 'Banner', 'banner', '1534419775_bg_whatwedo.jpg', 3, 'article', '2018-08-18 08:09:32', '2018-08-26 19:37:39'),
(111, 'Người viết', 'nguoi-viet', '', 3, 'article', '2018-08-18 08:09:32', '2018-08-26 19:37:39'),
(112, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 3, 'article', '2018-08-18 08:09:32', '2018-08-26 19:37:39'),
(113, 'Tiêu đề phải', 'tieu-de-phai', '', 10, 'blog', '2018-08-21 07:36:15', '2018-08-21 07:36:15'),
(114, 'Người viết', 'nguoi-viet', '', 10, 'blog', '2018-08-21 07:36:15', '2018-11-07 07:35:40'),
(115, 'Banner', 'banner', '', 10, 'blog', '2018-08-21 07:36:15', '2018-11-07 07:35:40'),
(116, 'Tiêu đề banner', 'tieu-de-banner', '', 10, 'blog', '2018-08-21 07:36:15', '2018-11-07 07:35:40'),
(117, 'Tiêu đề phải', 'tieu-de-phai', '', 11, 'blog', '2018-08-21 07:36:35', '2018-08-21 07:36:35'),
(118, 'Người viết', 'nguoi-viet', '', 11, 'blog', '2018-08-21 07:36:35', '2018-11-07 07:38:30'),
(119, 'Banner', 'banner', '', 11, 'blog', '2018-08-21 07:36:35', '2018-11-07 07:38:30'),
(120, 'Tiêu đề banner', 'tieu-de-banner', '', 11, 'blog', '2018-08-21 07:36:35', '2018-11-07 07:38:30'),
(121, 'Tiêu đề phải', 'tieu-de-phai', '', 12, 'blog', '2018-08-21 07:43:20', '2018-08-21 07:43:20'),
(122, 'Người viết', 'nguoi-viet', '', 12, 'blog', '2018-08-21 07:43:20', '2018-11-07 07:38:03'),
(123, 'Banner', 'banner', '', 12, 'blog', '2018-08-21 07:43:20', '2018-11-07 07:38:03'),
(124, 'Tiêu đề banner', 'tieu-de-banner', '', 12, 'blog', '2018-08-21 07:43:20', '2018-11-07 07:38:03'),
(125, 'Tiêu đề phải', 'tieu-de-phai', '', 1, 'blog', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(126, 'Người viết', 'nguoi-viet', '', 1, 'blog', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(127, 'Banner', 'banner', '', 1, 'blog', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(128, 'Tiêu đề banner', 'tieu-de-banner', '', 1, 'blog', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(129, 'Chất liệu', 'chat-lieu', '[]', 8, 'product_attribute', '2018-08-21 16:13:05', '2018-09-20 08:50:24'),
(130, 'Chất liệu', 'chat-lieu', '', 3, 'collection_attribute', NULL, '2018-09-25 09:13:10'),
(131, 'Thương hiệu', 'thuong-hieu', '[]', 8, 'product_attribute', '2018-08-21 16:13:05', '2018-09-20 08:50:24'),
(132, 'Thương hiệu', 'thuong-hieu', '', 3, 'collection_attribute', NULL, '2018-09-25 09:13:10'),
(133, 'Chất liệu', 'chat-lieu', '[\"Nhựa\"]', 9, 'product_attribute', '2018-08-21 16:46:06', '2018-09-20 03:20:07'),
(134, 'Thương hiệu', 'thuong-hieu', '[]', 9, 'product_attribute', '2018-08-21 16:46:06', '2018-09-20 03:20:07'),
(135, 'Chất liệu', 'chat-lieu', 'Nhựa', 5, 'collection_attribute', NULL, '2018-08-26 16:10:48'),
(136, 'Thương hiệu', 'thuong-hieu', 'Locker & Lock', 5, 'collection_attribute', NULL, '2018-08-26 16:10:48'),
(137, 'Tiêu đề phải', 'tieu-de-phai', '', 26, 'article', '2018-08-22 03:52:30', '2018-08-22 04:50:25'),
(138, 'Banner', 'banner', '1534213298_bg_testimonial.jpg', 26, 'article', '2018-08-22 03:52:30', '2018-08-26 19:45:27'),
(139, 'Người viết', 'nguoi-viet', '', 26, 'article', '2018-08-22 03:52:30', '2018-08-26 19:45:27'),
(140, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 26, 'article', '2018-08-22 03:52:30', '2018-08-26 19:45:27'),
(141, 'Tiêu đề phải', 'tieu-de-phai', '', 27, 'article', '2018-08-22 04:54:45', '2018-08-22 04:54:49'),
(142, 'Banner', 'banner', '', 27, 'article', '2018-08-22 04:54:45', '2018-08-22 04:54:49'),
(143, 'Người viết', 'nguoi-viet', '', 27, 'article', '2018-08-22 04:54:45', '2018-08-22 04:54:49'),
(144, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 27, 'article', '2018-08-22 04:54:45', '2018-08-22 04:54:49'),
(145, 'Chất liệu', 'chat-lieu', '[]', 10, 'product_attribute', '2018-08-26 14:51:40', '2018-08-26 15:09:41'),
(146, 'Thương hiệu', 'thuong-hieu', '[\"Thương hiệu 1\"]', 10, 'product_attribute', '2018-08-26 14:51:40', '2018-08-26 15:09:41'),
(147, 'Chất liệu', 'chat-lieu', '', 12, 'collection_attribute', NULL, '2018-08-26 15:09:41'),
(148, 'Thương hiệu', 'thuong-hieu', 'Thương hiệu 1', 12, 'collection_attribute', NULL, '2018-08-26 15:09:41'),
(149, 'Chất liệu', 'chat-lieu', '[\"Nhựa\"]', 11, 'product_attribute', '2018-08-26 15:54:49', '2018-09-20 08:51:23'),
(150, 'Chất liệu', 'chat-lieu', 'Nhựa', 1, 'collection_attribute', NULL, '2018-11-07 02:43:57'),
(151, 'Thương hiệu', 'thuong-hieu', '[]', 11, 'product_attribute', '2018-08-26 15:54:49', '2018-09-20 08:51:23'),
(152, 'Thương hiệu', 'thuong-hieu', '', 1, 'collection_attribute', NULL, '2018-11-07 02:43:57'),
(153, 'Người viết', 'nguoi-viet', '', 13, 'blog', '2018-09-05 08:30:31', '2018-11-07 08:54:49'),
(154, 'Banner', 'banner', '1534213298_bg_testimonial.jpg', 13, 'blog', '2018-09-05 08:30:31', '2018-11-07 08:54:49'),
(155, 'Tiêu đề banner', 'tieu-de-banner', '', 13, 'blog', '2018-09-05 08:30:31', '2018-11-07 08:54:49'),
(156, 'Banner', 'banner', '', 28, 'article', '2018-09-05 08:33:51', '2018-11-07 07:17:29'),
(157, 'Người viết', 'nguoi-viet', '', 28, 'article', '2018-09-05 08:33:51', '2018-11-07 07:17:29'),
(158, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 28, 'article', '2018-09-05 08:33:51', '2018-09-11 03:20:12'),
(159, 'Banner', 'banner', '', 29, 'article', '2018-09-05 08:34:45', '2018-11-07 08:50:28'),
(160, 'Người viết', 'nguoi-viet', '', 29, 'article', '2018-09-05 08:34:45', '2018-11-07 08:50:28'),
(161, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 29, 'article', '2018-09-05 08:34:45', '2018-09-05 08:44:00'),
(162, 'Banner', 'banner', '', 30, 'article', '2018-09-05 08:35:23', '2018-11-07 07:21:45'),
(163, 'Người viết', 'nguoi-viet', '', 30, 'article', '2018-09-05 08:35:23', '2018-11-07 07:21:45'),
(164, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 30, 'article', '2018-09-05 08:35:23', '2018-09-05 08:43:37'),
(165, 'Banner', 'banner', '', 6, 'article', '2018-09-11 04:36:04', '2018-09-11 04:36:04'),
(166, 'Người viết', 'nguoi-viet', '', 6, 'article', '2018-09-11 04:36:04', '2018-09-11 04:36:04'),
(167, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 6, 'article', '2018-09-11 04:36:04', '2018-09-11 04:36:04'),
(168, 'Banner', 'banner', '', 5, 'article', '2018-09-11 04:36:23', '2018-11-07 02:41:43'),
(169, 'Người viết', 'nguoi-viet', '', 5, 'article', '2018-09-11 04:36:23', '2018-11-07 02:41:43'),
(170, 'Bài viết liên quan', 'bai-viet-lien-quan', '', 5, 'article', '2018-09-11 04:36:23', '2018-09-11 04:36:23'),
(171, 'Banner', 'banner', '', 2, 'article', '2018-09-11 04:36:38', '2018-11-07 02:41:32'),
(172, 'Người viết', 'nguoi-viet', '', 2, 'article', '2018-09-11 04:36:38', '2018-11-07 02:41:32'),
(173, 'Bài viết liên quan', 'bai-viet-lien-quan', 'Bài viết liên quan 111', 2, 'article', '2018-09-11 04:36:38', '2018-10-05 06:34:05'),
(174, 'Chất liệu', 'chat-lieu', '[\"Thép\"]', 12, 'product_attribute', '2018-09-20 09:08:30', '2018-09-20 09:14:05'),
(175, 'Thương hiệu', 'thuong-hieu', '[\"REAL LOCKS\"]', 12, 'product_attribute', '2018-09-20 09:08:30', '2018-09-20 09:14:05'),
(176, 'Chất liệu', 'chat-lieu', 'Thép,Kim loại', 9, 'collection_attribute', NULL, '2018-09-25 10:58:42'),
(177, 'Thương hiệu', 'thuong-hieu', 'REAL LOCKS', 9, 'collection_attribute', NULL, '2018-09-25 10:58:42'),
(178, 'Chất liệu', 'chat-lieu', '[\"Kim loại\"]', 13, 'product_attribute', '2018-09-20 09:21:42', '2018-09-20 09:44:01'),
(179, 'Thương hiệu', 'thuong-hieu', '[\"REAL LOCKS\"]', 13, 'product_attribute', '2018-09-20 09:21:42', '2018-09-20 09:44:01'),
(180, 'Chất liệu', 'chat-lieu', '[\"Kim loại\"]', 14, 'product_attribute', '2018-09-20 09:27:40', '2018-09-25 10:58:42'),
(181, 'Thương hiệu', 'thuong-hieu', '[\"REAL LOCKS\"]', 14, 'product_attribute', '2018-09-20 09:27:40', '2018-09-25 10:58:42'),
(182, 'Chất liệu', 'chat-lieu', '[]', 6, 'product_attribute', '2018-09-25 09:13:10', '2018-09-25 09:13:10'),
(183, 'Thương hiệu', 'thuong-hieu', '[]', 6, 'product_attribute', '2018-09-25 09:13:10', '2018-09-25 09:13:10'),
(184, 'Thông tin thêm', 'more-information', 'eye', 28, 'order', '2018-09-28 02:30:06', '2018-09-28 02:30:06'),
(185, 'Chất liệu', 'chat-lieu', '[]', 1, 'product_attribute', '2018-10-04 07:30:37', '2018-10-04 07:30:37'),
(186, 'Thương hiệu', 'thuong-hieu', '[]', 1, 'product_attribute', '2018-10-04 07:30:37', '2018-10-04 07:30:37'),
(187, 'Chất liệu', 'chat-lieu', '[]', 2, 'product_attribute', '2018-10-05 04:33:35', '2018-11-07 02:43:57'),
(188, 'Thương hiệu', 'thuong-hieu', '[]', 2, 'product_attribute', '2018-10-05 04:33:35', '2018-11-07 02:43:57'),
(189, 'Thông tin thêm', 'more-information', 'laziweb', 33, 'order', '2018-10-08 04:50:32', '2018-10-08 04:50:32'),
(190, 'Chất liệu', 'chat-lieu', '[]', 3, 'product_attribute', '2018-11-07 02:44:25', '2018-11-07 02:44:25'),
(191, 'Chất liệu', 'chat-lieu', '', 2, 'collection_attribute', NULL, '2018-11-07 02:44:39'),
(192, 'Thương hiệu', 'thuong-hieu', '[]', 3, 'product_attribute', '2018-11-07 02:44:25', '2018-11-07 02:44:25'),
(193, 'Thương hiệu', 'thuong-hieu', '', 2, 'collection_attribute', NULL, '2018-11-07 02:44:39'),
(194, 'Chất liệu', 'chat-lieu', '[]', 4, 'product_attribute', '2018-11-07 02:44:39', '2018-11-07 02:44:39'),
(195, 'Thương hiệu', 'thuong-hieu', '[]', 4, 'product_attribute', '2018-11-07 02:44:39', '2018-11-07 02:44:39'),
(196, 'Banner', 'banner', '', 31, 'article', '2018-11-07 07:23:58', '2018-11-07 07:42:05'),
(197, 'Người viết', 'nguoi-viet', '', 31, 'article', '2018-11-07 07:23:58', '2018-11-07 07:42:05'),
(198, 'Banner', 'banner', '', 32, 'article', '2018-11-07 07:24:57', '2018-11-07 07:41:54'),
(199, 'Người viết', 'nguoi-viet', '', 32, 'article', '2018-11-07 07:24:57', '2018-11-07 07:41:54'),
(200, 'Chất liệu', 'chat-lieu', '[]', 15, 'product_attribute', '2018-11-07 08:58:31', '2018-11-07 09:08:25'),
(201, 'Thương hiệu', 'thuong-hieu', '[]', 15, 'product_attribute', '2018-11-07 08:58:31', '2018-11-07 09:08:25'),
(202, 'Chất liệu', 'chat-lieu', '[]', 16, 'product_attribute', '2018-11-07 08:58:48', '2018-11-07 09:03:23'),
(203, 'Thương hiệu', 'thuong-hieu', '[]', 16, 'product_attribute', '2018-11-07 08:58:48', '2018-11-07 09:03:23'),
(204, 'Chất liệu', 'chat-lieu', '[]', 17, 'product_attribute', '2018-11-07 08:59:49', '2018-11-07 09:04:00'),
(205, 'Thương hiệu', 'thuong-hieu', '[]', 17, 'product_attribute', '2018-11-07 08:59:49', '2018-11-07 09:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `metafield_translations`
--

CREATE TABLE `metafield_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `metafield_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `shipping_price` int(11) NOT NULL DEFAULT '0',
  `shipping_status` int(11) NOT NULL DEFAULT '0',
  `order_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'new',
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `coupon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coupon_discount` int(11) NOT NULL DEFAULT '0',
  `sale` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sale_discount` int(11) NOT NULL DEFAULT '0',
  `order_discount` int(11) NOT NULL DEFAULT '0',
  `subtotal` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  `reason_cancel` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `customer_id`, `payment_method`, `shipping_price`, `shipping_status`, `order_status`, `payment_status`, `notes`, `coupon`, `coupon_discount`, `sale`, `sale_discount`, `order_discount`, `subtotal`, `total`, `reason_cancel`, `created_at`, `updated_at`) VALUES
(1, 1, 'COD', 0, 2, 'done', 1, 'Company: </br>Nội dung: ', '#123', 10, '[]', 0, 0, 365000, 364990, '', '2018-08-20 03:57:57', '2018-08-20 03:57:57'),
(2, 2, 'COD', 0, 2, 'done', 1, 'Company: </br>Nội dung: Pls check it', '', 0, '', 0, 0, 65000, 65000, '', '2018-08-21 11:51:49', '2018-08-21 11:51:49'),
(3, 2, 'COD', 0, 2, 'done', 1, 'Company: </br>Nội dung: Pls check it', '', 0, '', 0, 0, 65000, 65000, '', '2018-08-21 11:52:02', '2018-08-21 11:52:02'),
(4, 2, 'COD', 0, 2, 'done', 1, 'Company: </br>Nội dung: Pls check it', '', 0, '', 0, 0, 65000, 65000, '', '2018-08-21 11:52:03', '2018-08-21 11:52:03'),
(5, 2, 'COD', 0, 2, 'done', 1, 'Company: </br>Nội dung: Pls check it', '', 0, '', 0, 0, 65000, 65000, '', '2018-08-21 11:52:04', '2018-08-21 11:52:04'),
(6, 3, 'BANK', 0, 2, 'done', 1, 'Company: </br>Nội dung: ', '', 0, '[]', 0, 0, 65000, 65000, '', '2018-08-22 02:15:44', '2018-08-22 02:15:44'),
(7, 3, 'COD', 0, 2, 'done', 1, 'Company: </br>Nội dung: ', '', 0, '[]', 0, 0, 1100000, 1100000, '', '2018-08-22 02:42:32', '2018-08-22 02:43:51'),
(8, 3, 'COD', 0, 2, 'done', 1, 'Company: laziweb</br>Nội dung: note abv', '#123', 15000, '[]', 0, 0, 150000, 135000, '', '2018-08-22 06:52:30', '2018-08-22 06:52:30'),
(9, 3, 'BANK', 0, 2, 'done', 1, 'Company: lazi</br>Nội dung: dagfhj', '', 0, '[]', 0, 0, 65000, 65000, '', '2018-08-22 06:57:22', '2018-08-22 06:57:22'),
(10, 2, 'BANK', 0, 2, 'done', 1, 'Công ty: REAL TRADING</br>Nội dung: Thanh toán đơn hàng- Test', '', 0, '[]', 0, 0, 1550000, 1550000, '', '2018-08-28 14:09:04', '2018-08-28 14:09:04'),
(11, 2, 'COD', 0, 2, 'done', 1, 'Công ty: </br>Nội dung: ', '', 0, '[]', 0, 0, 375000, 375000, '', '2018-08-28 14:13:45', '2018-08-28 14:13:45'),
(12, 2, 'BANK', 0, 2, 'done', 1, 'Công ty: E</br>Nội dung: \n', '', 0, '[]', 0, 0, 1100000, 1100000, '', '2018-08-28 14:21:58', '2018-08-28 14:21:58'),
(13, 4, 'COD', 0, 0, 'new', 0, 'Công ty: laziweb</br>Nội dung: ', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-09-04 10:27:15', '2018-09-04 10:27:15'),
(14, 5, 'BANK', 0, 0, 'new', 0, 'Công ty: </br>Nội dung: ', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-09-05 08:16:03', '2018-09-05 08:16:03'),
(15, 3, 'BANK', 0, 0, 'new', 0, 'Công ty: </br>Nội dung: ', '#123', 60000, '[]', 0, 0, 600000, 540000, '', '2018-09-06 02:43:15', '2018-09-06 02:43:15'),
(16, 3, 'BANK', 0, 0, 'new', 0, 'Công ty: </br>Nội dung: ', '', 0, '[]', 0, 0, 650000, 650000, '', '2018-09-06 02:44:56', '2018-09-06 02:44:56'),
(17, 3, 'BANK', 0, 0, 'new', 0, 'Công ty: laziweb</br>Nội dung: ', '', 0, '[]', 0, 0, 350000, 350000, '', '2018-09-06 02:49:19', '2018-09-06 02:49:19'),
(18, 3, 'COD', 0, 0, 'new', 0, 'Công ty: </br>Nội dung: ', '', 0, '[]', 0, 0, 300000, 300000, '', '2018-09-06 02:50:50', '2018-09-06 02:50:50'),
(19, 3, 'COD', 0, 0, 'new', 0, 'Công ty: </br>Nội dung: test ghi chu ', '', 0, '[]', 0, 0, 1300000, 1300000, '', '2018-09-07 09:22:05', '2018-09-07 09:22:05'),
(20, 3, 'BANK', 0, 0, 'new', 0, 'Công ty: </br>Nội dung: ', '#123', 16000, '[]', 0, 0, 160000, 144000, '', '2018-09-07 09:23:25', '2018-09-07 09:23:25'),
(21, 6, 'BANK', 0, 0, 'new', 0, 'Công ty: cong ty 1</br>Nội dung: ghi chú 1', '', 0, '[]', 0, 0, 160000, 160000, '', '2018-09-07 09:33:19', '2018-09-07 09:33:19'),
(22, 7, 'BANK', 0, 0, 'new', 0, 'Công ty: Lazi</br>Nội dung: ', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-09-26 07:56:51', '2018-09-26 07:56:51'),
(23, 7, 'BANK', 0, 0, 'new', 0, 'Công ty: Lazi</br>Nội dung: ', '', 0, '[]', 0, 0, 350000, 350000, '', '2018-09-26 07:57:59', '2018-09-26 07:57:59'),
(24, 8, 'BANK', 0, 0, 'new', 0, 'Công ty: 1241414</br>Nội dung: Ghi chu', '', 0, '[]', 0, 0, 300000, 300000, '', '2018-09-27 09:40:16', '2018-09-27 09:40:16'),
(25, 9, 'BANK', 0, 0, 'new', 0, 'Công ty: cuonto</br>Nội dung: Ghi chú', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-09-27 10:06:15', '2018-09-27 10:06:15'),
(26, 3, 'COD', 0, 0, 'new', 0, 'Công ty: </br>Nội dung: ', '', 0, '[]', 0, 0, 510000, 510000, '', '2018-09-28 01:06:04', '2018-09-28 01:06:04'),
(27, 9, 'BANK', 0, 0, 'new', 0, 'Công ty: cuong</br>Nội dung: Ghi chú', '', 0, '[]', 0, 0, 375000, 375000, '', '2018-09-28 02:17:40', '2018-09-28 02:17:40'),
(28, 9, 'BANK', 0, 0, 'new', 0, 'Ghi chu', '', 0, '[]', 0, 0, 600000, 600000, '', '2018-09-28 02:30:06', '2018-09-28 02:30:06'),
(29, 9, 'BANK', 0, 0, 'new', 0, '', '', 0, '[]', 0, 0, 300000, 300000, '', '2018-09-28 02:48:45', '2018-09-28 02:48:45'),
(30, 3, 'COD', 0, 0, 'new', 0, '', '#123', 100500, '[]', 0, 0, 1005000, 904500, '', '2018-09-28 06:29:25', '2018-09-28 06:29:25'),
(31, 3, 'BANK', 0, 0, 'new', 0, '', '', 0, '[]', 0, 0, 415000, 415000, '', '2018-09-28 06:32:30', '2018-09-28 06:32:30'),
(32, 3, 'COD', 0, 0, 'new', 0, '', '', 0, '[]', 0, 0, 900000, 900000, '', '2018-10-05 04:27:23', '2018-10-05 04:27:23'),
(33, 7, 'COD', 0, 0, 'new', 0, 'Test website', '', 0, '[]', 0, 0, 0, 0, '', '2018-10-08 04:50:32', '2018-10-08 04:50:32'),
(34, 10, 'cod', 0, 0, 'new', 0, '', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-11-06 07:40:43', '2018-11-06 07:40:43'),
(35, 11, 'cod', 0, 0, 'new', 0, '', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-11-07 07:36:08', '2018-11-07 07:36:08'),
(36, 12, 'cod', 50000, 2, 'new', 0, '', '', 0, '[]', 0, 0, 910000, 960000, '', '2018-11-07 10:37:12', '2018-11-07 10:38:33'),
(37, 13, 'cod', 50000, 0, 'new', 0, '', '#1', -2, '[]', 0, 0, 780000, 830002, '', '2018-11-08 02:15:13', '2018-11-08 02:15:13'),
(38, 15, 'cod', 0, 0, 'new', 0, '', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-11-08 08:46:58', '2018-11-08 08:46:58'),
(39, 15, 'cod', 0, 0, 'new', 0, '', '', 0, '[]', 0, 0, 150000, 150000, '', '2018-11-08 08:49:39', '2018-11-08 08:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `image`, `description`, `content`, `tags`, `view`, `status`, `template`, `created_at`, `updated_at`) VALUES
(1, 'CONTACT US', '1535903729_banner_contact_1903_x447.png', 'It is a long established fact that a reader will be distracted', '<h2 class=\"sub-heading\"><span style=\"color: #000000;\"><span class=\"f2\">Feel free to</span>&nbsp;<em><span class=\"f1\">keep in touch</span></em></span></h2>', '', 200, 'delete', 'contact', '2018-08-11 03:52:31', '2018-09-17 08:15:25'),
(2, 'GIỚI THIỆU', '1533961058_bg_nito_2.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit', '<p class=\"mb-3\">Nito is working with the conviction to create exceptional experiences through the inventive and skillful application of design and technology. This is our art and our craft.</p>\n<ul class=\"bullet-list mb-5\">\n<li>Effective digital solutions</li>\n<li>Fresh &amp; creative ideas</li>\n<li>Clean and modern designs</li>\n</ul>', '', 469, 'active', 'about', '2018-08-11 04:53:05', '2018-11-07 07:52:06'),
(3, 'Spotlight on Geordie Willis', '', '', '<p><img src=\"http://realtrading.demo3.laziweb.com/uploads/1533981138_blog_detail.jpg\" alt=\"\" width=\"870\" height=\"450\" />Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd in St. James&rsquo;s Street, Pall Mall. Founded in 1698, it is the UK&rsquo;s oldest family-run wine merchant and supplier of wines to the royal family. As the eighth generation of the Berry family to work in the company, Willis looks after all aspects of design, from print and publishing to photography, labels and interior design. He previously worked in magazines and for a design agency, as well as doing a stint in Asia working out of Berry Bros. &amp; Rudd&rsquo;s Hong Kong office.</p>\n<blockquote>\n<p>The world of wine is very much about story telling, it has been a wonderful opportunity to apply my background in design and branding to a firm that is so rich in history.</p>\n</blockquote>\n<p>It was his ancestor, Charles Walter Berry, who cemented his love of wine, imparting to a young Willis the view that the role of the wine merchant is to be &ldquo;the closest link between those who make the wine and those who drink the wine&rdquo;. Says Willis: &ldquo;I&rsquo;ve always loved the stories behind each individual wine and the people that make them. There is so much more to wine than the bottle and the label. My grandfather Anthony Berry was also a great influence, he helped demystify wine and make it accessible to me.&rdquo;</p>\n<p>A look at the company&rsquo;s Instagram account demonstrates Willis&rsquo;s talent in bringing these elements together and keeping Berry Bros. relevant to all ages, with pictures of the newest restaurant hotspot, comfortably sitting alongside a 1996 Chateau Haut-Brion and 1970 Taylor&rsquo;s Vintage Port; from queuing for an hour to squeeze into the former and patiently waiting 46 years to crack open the latter, wine lovers of all ages &ndash; and all budgets &ndash; flock to the treasure trove in Pall Mall.</p>\n<p>&nbsp;</p>\n<ul class=\"bullet-list ml-0\">\n<li>I don&rsquo;t think that it&rsquo;s possible to separate wine and food, it&rsquo;s such a symbiotic relationship. I&rsquo;m very fickle, I tend to fall in love with the wines of a region that I&rsquo;ve recently visited. The simplest food matches are often the best, a wine from the Jura with a piece of Comt<em>&eacute;</em>&nbsp;or oysters with a young Chablis.</li>\n<li>My grandfather&rsquo;s advice was always &ldquo;everything in moderation&rdquo;. Drinking wine with the right people is often as important as what&rsquo;s in the bottle.</li>\n<li>I&rsquo;m partial to a Negroni and the occasional Martini. Particularly when they&rsquo;re made with our No3 London Dry Gin by Alessandro Palazzi at Dukes Hotel opposite our St James&rsquo;s Street shop.</li>\n</ul>', '', 1, 'active', 'policy', '2018-08-17 03:28:37', '2018-08-17 03:28:37');

-- --------------------------------------------------------

--
-- Table structure for table `page_translations`
--

CREATE TABLE `page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endpoint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `role_id`, `group`, `method`, `endpoint`, `created_at`, `updated_at`) VALUES
(140, 2, 'product', 'GET', '/product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(141, 2, 'product', 'GET', '/product/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(142, 2, 'product', 'GET', '/product/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(143, 2, 'product', 'POST', '/product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(144, 2, 'product', 'PUT', '/product/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(145, 2, 'product', 'DELETE', '/product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(146, 2, 'product', 'POST', '/product/duplicate/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(147, 2, 'collection', 'GET', '/collection', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(148, 2, 'collection', 'GET', '/collection/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(149, 2, 'collection', 'GET', '/collection/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(150, 2, 'collection', 'POST', '/collection', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(151, 2, 'collection', 'PUT', '/collection/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(152, 2, 'collection', 'DELETE', '/collection', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(153, 2, 'product_buy_together', 'GET', '/product_buy_together', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(154, 2, 'product_buy_together', 'GET', '/product_buy_together/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(155, 2, 'product_buy_together', 'GET', '/product_buy_together/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(156, 2, 'product_buy_together', 'POST', '/product_buy_together', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(157, 2, 'product_buy_together', 'PUT', '/product_buy_together/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(158, 2, 'product_buy_together', 'POST', '/product_buy_together/one', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(159, 2, 'product_buy_together', 'DELETE', '/product_buy_together/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(160, 2, 'attribute', 'GET', '/attribute', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(161, 2, 'attribute', 'GET', '/attribute/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(162, 2, 'attribute', 'GET', '/attribute/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(163, 2, 'attribute', 'POST', '/attribute', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(164, 2, 'attribute', 'PUT', '/attribute/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(165, 2, 'attribute', 'DELETE', '/attribute/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(166, 2, 'coupon', 'GET', '/coupon', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(167, 2, 'coupon', 'GET', '/coupon/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(168, 2, 'coupon', 'GET', '/coupon/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(169, 2, 'coupon', 'POST', '/coupon', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(170, 2, 'coupon', 'PUT', '/coupon/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(171, 2, 'coupon', 'DELETE', '/coupon', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(172, 2, 'sale', 'GET', '/sale', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(173, 2, 'sale', 'GET', '/sale/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(174, 2, 'sale', 'GET', '/sale/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(175, 2, 'sale', 'POST', '/sale', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(176, 2, 'sale', 'PUT', '/sale/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(177, 2, 'sale', 'DELETE', '/sale', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(178, 2, 'customer', 'GET', '/customer', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(179, 2, 'customer', 'GET', '/customer/order/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(180, 2, 'customer', 'DELETE', '/customer', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(181, 2, 'article', 'GET', '/article', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(182, 2, 'article', 'GET', '/article/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(183, 2, 'article', 'GET', '/article/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(184, 2, 'article', 'POST', '/article', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(185, 2, 'article', 'PUT', '/article/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(186, 2, 'article', 'DELETE', '/article', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(187, 2, 'article', 'POST', '/article/duplicate/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(188, 2, 'blog', 'GET', '/blog', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(189, 2, 'blog', 'GET', '/blog/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(190, 2, 'blog', 'GET', '/blog/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(191, 2, 'blog', 'POST', '/blog', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(192, 2, 'blog', 'PUT', '/blog/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(193, 2, 'blog', 'DELETE', '/blog', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(194, 2, 'page', 'GET', '/page', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(195, 2, 'page', 'GET', '/page/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(196, 2, 'page', 'GET', '/page/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(197, 2, 'page', 'POST', '/page', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(198, 2, 'page', 'PUT', '/page/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(199, 2, 'page', 'DELETE', '/page', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(200, 2, 'gallery', 'GET', '/gallery', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(201, 2, 'gallery', 'GET', '/gallery/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(202, 2, 'gallery', 'GET', '/gallery/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(203, 2, 'gallery', 'POST', '/gallery', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(204, 2, 'gallery', 'PUT', '/gallery/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(205, 2, 'gallery', 'DELETE', '/gallery', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(206, 2, 'photo', 'GET', '/photo/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(207, 2, 'photo', 'GET', '/photo/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(208, 2, 'photo', 'POST', '/photo', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(209, 2, 'photo', 'PUT', '/photo/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(210, 2, 'photo', 'DELETE', '/photo/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(211, 2, 'review', 'GET', '/review', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(212, 2, 'subscriber', 'GET', '/subscriber', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(213, 2, 'comment', 'GET', '/comment', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(214, 2, 'comment', 'GET', '/comment/product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(215, 2, 'comment', 'GET', '/comment/article', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(216, 2, 'comment', 'PUT', '/comment/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(217, 2, 'comment', 'DELETE', '/comment', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(218, 2, 'menu', 'GET', '/menu', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(219, 2, 'menu', 'GET', '/menu/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(220, 2, 'menu', 'GET', '/menu/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(221, 2, 'menu', 'PUT', '/menu/updatePriority', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(222, 2, 'menu', 'POST', '/menu', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(223, 2, 'menu', 'PUT', '/menu/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(224, 2, 'menu', 'DELETE', '/menu/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(225, 2, 'menu', 'DELETE', '/menu', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(226, 2, 'setting', 'GET', '/setting', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(227, 2, 'setting', 'PUT', '/setting', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(228, 2, 'library', 'GET', '/library', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(229, 2, 'testimonial', 'GET', '/testimonial', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(230, 2, 'testimonial', 'GET', '/testimonial/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(231, 2, 'testimonial', 'GET', '/testimonial/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(232, 2, 'testimonial', 'POST', '/testimonial', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(233, 2, 'testimonial', 'PUT', '/testimonial/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(234, 2, 'testimonial', 'DELETE', '/testimonial', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(235, 2, 'client', 'GET', '/client', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(236, 2, 'client', 'GET', '/client/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(237, 2, 'client', 'GET', '/client/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(238, 2, 'client', 'POST', '/client', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(239, 2, 'client', 'PUT', '/client/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(240, 2, 'client', 'DELETE', '/client', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(241, 2, 'shipping_fee', 'GET', '/shipping_fee', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(242, 2, 'shipping_fee', 'GET', '/shipping_fee/edit/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(243, 2, 'shipping_fee', 'GET', '/shipping_fee/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(244, 2, 'shipping_fee', 'POST', '/shipping_fee', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(245, 2, 'shipping_fee', 'PUT', '/shipping_fee/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(246, 2, 'shipping_fee', 'DELETE', '/shipping_fee/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(247, 3, 'order', 'GET', '/order', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(248, 3, 'order', 'GET', '/order/search', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(249, 3, 'order', 'GET', '/order/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(250, 3, 'order', 'GET', '/order?order_status=new', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(251, 3, 'order', 'GET', '/order?order_status=confirm', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(252, 3, 'order', 'GET', '/order?order_status=confirm&payment_status=0', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(253, 3, 'order', 'GET', '/order?order_status=done', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(254, 3, 'order', 'GET', '/order?order_status=return', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(255, 3, 'order', 'GET', '/order?order_status=cancel', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(256, 3, 'order', 'PUT', '/order/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(257, 3, 'order', 'DELETE', '/order', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(258, 3, 'contact', 'GET', '/contact', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(259, 3, 'contact', 'GET', '/contact/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(260, 3, 'contact', 'GET', '/contact?read=1', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(261, 3, 'contact', 'GET', '/contact?reply=1', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(262, 3, 'contact', 'PUT', '/contact/updateStatus', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(263, 3, 'contact', 'DELETE', '/contact/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(264, 3, 'user', 'GET', '/user/email/order', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(265, 3, 'user', 'GET', '/user/email/contact', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(266, 4, 'article', 'GET', '/article', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(267, 4, 'article', 'GET', '/article/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(268, 4, 'article', 'GET', '/article/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(269, 4, 'article', 'POST', '/article', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(270, 4, 'article', 'PUT', '/article/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(271, 4, 'article', 'DELETE', '/article', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(272, 4, 'article', 'POST', '/article/duplicate/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(273, 4, 'blog', 'GET', '/blog', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(274, 4, 'blog', 'GET', '/blog/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(275, 4, 'blog', 'GET', '/blog/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(276, 4, 'blog', 'POST', '/blog', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(277, 4, 'blog', 'PUT', '/blog/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(278, 4, 'blog', 'DELETE', '/blog', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(279, 4, 'page', 'GET', '/page', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(280, 4, 'page', 'GET', '/page/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(281, 4, 'page', 'GET', '/page/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(282, 4, 'page', 'POST', '/page', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(283, 4, 'page', 'PUT', '/page/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(284, 4, 'page', 'DELETE', '/page', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(285, 5, 'product', 'GET', '/product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(286, 5, 'product', 'GET', '/product/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(287, 5, 'product', 'GET', '/product/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(288, 5, 'product', 'POST', '/product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(289, 5, 'product', 'PUT', '/product/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(290, 5, 'product', 'DELETE', '/product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(291, 5, 'product', 'POST', '/product/duplicate/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(292, 5, 'import_product', 'GET', '/import_product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(293, 5, 'import_product', 'GET', '/import_product/template', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(294, 5, 'import_product', 'POST', '/import_product', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(295, 5, 'collection', 'GET', '/collection', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(296, 5, 'collection', 'GET', '/collection/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(297, 5, 'collection', 'GET', '/collection/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(298, 5, 'collection', 'POST', '/collection', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(299, 5, 'collection', 'PUT', '/collection/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(300, 5, 'collection', 'DELETE', '/collection', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(301, 5, 'product_buy_together', 'GET', '/product_buy_together', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(302, 5, 'product_buy_together', 'GET', '/product_buy_together/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(303, 5, 'product_buy_together', 'GET', '/product_buy_together/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(304, 5, 'product_buy_together', 'POST', '/product_buy_together', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(305, 5, 'product_buy_together', 'PUT', '/product_buy_together/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(306, 5, 'product_buy_together', 'POST', '/product_buy_together/one', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(307, 5, 'product_buy_together', 'DELETE', '/product_buy_together/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(308, 5, 'attribute', 'GET', '/attribute', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(309, 5, 'attribute', 'GET', '/attribute/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(310, 5, 'attribute', 'GET', '/attribute/create', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(311, 5, 'attribute', 'POST', '/attribute', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(312, 5, 'attribute', 'PUT', '/attribute/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(313, 5, 'attribute', 'DELETE', '/attribute/{id}', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(441, 1, 'order', 'GET', '/order', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(442, 1, 'order', 'GET', '/order/search', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(443, 1, 'order', 'GET', '/order/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(444, 1, 'order', 'GET', '/order?order_status=new', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(445, 1, 'order', 'GET', '/order?order_status=confirm', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(446, 1, 'order', 'GET', '/order?order_status=done', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(447, 1, 'order', 'GET', '/order?order_status=return', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(448, 1, 'order', 'GET', '/order?order_status=cancel', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(449, 1, 'order', 'PUT', '/order/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(450, 1, 'order', 'GET', '/order/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(451, 1, 'product', 'GET', '/product', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(452, 1, 'product', 'GET', '/product/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(453, 1, 'product', 'GET', '/product/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(454, 1, 'product', 'POST', '/product', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(455, 1, 'product', 'PUT', '/product/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(456, 1, 'product', 'DELETE', '/product', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(457, 1, 'product', 'POST', '/product/duplicate/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(458, 1, 'collection', 'GET', '/collection', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(459, 1, 'collection', 'GET', '/collection/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(460, 1, 'collection', 'GET', '/collection/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(461, 1, 'collection', 'POST', '/collection', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(462, 1, 'collection', 'PUT', '/collection/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(463, 1, 'collection', 'DELETE', '/collection', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(464, 1, 'product_buy_together', 'GET', '/product_buy_together', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(465, 1, 'product_buy_together', 'GET', '/product_buy_together/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(466, 1, 'product_buy_together', 'GET', '/product_buy_together/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(467, 1, 'product_buy_together', 'POST', '/product_buy_together', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(468, 1, 'product_buy_together', 'POST', '/product_buy_together/one', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(469, 1, 'product_buy_together', 'PUT', '/product_buy_together/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(470, 1, 'product_buy_together', 'DELETE', '/product_buy_together/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(471, 1, 'attribute', 'GET', '/attribute', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(472, 1, 'attribute', 'GET', '/attribute/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(473, 1, 'attribute', 'GET', '/attribute/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(474, 1, 'attribute', 'POST', '/attribute', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(475, 1, 'attribute', 'PUT', '/attribute/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(476, 1, 'attribute', 'DELETE', '/attribute/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(477, 1, 'coupon', 'GET', '/coupon', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(478, 1, 'coupon', 'GET', '/coupon/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(479, 1, 'coupon', 'GET', '/coupon/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(480, 1, 'coupon', 'POST', '/coupon', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(481, 1, 'coupon', 'PUT', '/coupon/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(482, 1, 'coupon', 'DELETE', '/coupon', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(483, 1, 'sale', 'GET', '/sale', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(484, 1, 'sale', 'GET', '/sale/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(485, 1, 'sale', 'GET', '/sale/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(486, 1, 'sale', 'POST', '/sale', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(487, 1, 'sale', 'PUT', '/sale/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(488, 1, 'sale', 'DELETE', '/sale', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(489, 1, 'customer', 'GET', '/customer', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(490, 1, 'customer', 'GET', '/customer/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(491, 1, 'customer', 'PUT', '/customer/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(492, 1, 'customer', 'GET', '/customer/export', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(493, 1, 'customer', 'GET', '/customer/order/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(494, 1, 'customer', 'DELETE', '/customer', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(495, 1, 'article', 'GET', '/article', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(496, 1, 'article', 'GET', '/article/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(497, 1, 'article', 'GET', '/article/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(498, 1, 'article', 'POST', '/article', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(499, 1, 'article', 'PUT', '/article/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(500, 1, 'article', 'DELETE', '/article', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(501, 1, 'article', 'POST', '/article/duplicate/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(502, 1, 'blog', 'GET', '/blog', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(503, 1, 'blog', 'GET', '/blog/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(504, 1, 'blog', 'GET', '/blog/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(505, 1, 'blog', 'POST', '/blog', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(506, 1, 'blog', 'PUT', '/blog/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(507, 1, 'blog', 'DELETE', '/blog', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(508, 1, 'page', 'GET', '/page', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(509, 1, 'page', 'GET', '/page/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(510, 1, 'page', 'GET', '/page/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(511, 1, 'page', 'POST', '/page', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(512, 1, 'page', 'PUT', '/page/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(513, 1, 'page', 'DELETE', '/page', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(514, 1, 'gallery', 'GET', '/gallery', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(515, 1, 'gallery', 'GET', '/gallery/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(516, 1, 'gallery', 'GET', '/gallery/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(517, 1, 'gallery', 'POST', '/gallery', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(518, 1, 'gallery', 'PUT', '/gallery/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(519, 1, 'gallery', 'DELETE', '/gallery', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(520, 1, 'photo', 'GET', '/photo/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(521, 1, 'photo', 'POST', '/photo', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(522, 1, 'photo', 'GET', '/photo/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(523, 1, 'photo', 'PUT', '/photo/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(524, 1, 'photo', 'DELETE', '/photo/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(525, 1, 'contact', 'GET', '/contact', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(526, 1, 'contact', 'GET', '/contact/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(527, 1, 'contact', 'PUT', '/contact/updateStatus', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(528, 1, 'contact', 'DELETE', '/contact/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(529, 1, 'subscriber', 'GET', '/subscriber', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(530, 1, 'menu', 'GET', '/menu', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(531, 1, 'menu', 'GET', '/menu/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(532, 1, 'menu', 'POST', '/menu', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(533, 1, 'menu', 'GET', '/menu/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(534, 1, 'menu', 'PUT', '/menu/updatePriority', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(535, 1, 'menu', 'PUT', '/menu/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(536, 1, 'menu', 'DELETE', '/menu/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(537, 1, 'menu', 'DELETE', '/menu', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(538, 1, 'setting', 'GET', '/setting', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(539, 1, 'setting', 'PUT', '/setting', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(540, 1, 'library', 'GET', '/library', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(541, 1, 'shipping_fee', 'GET', '/shipping_fee', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(542, 1, 'shipping_fee', 'GET', '/shipping_fee/edit/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(543, 1, 'shipping_fee', 'GET', '/shipping_fee/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(544, 1, 'shipping_fee', 'POST', '/shipping_fee', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(545, 1, 'shipping_fee', 'PUT', '/shipping_fee/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(546, 1, 'shipping_fee', 'DELETE', '/shipping_fee/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(547, 1, 'user', 'GET', '/user', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(548, 1, 'user', 'GET', '/user/history', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(549, 1, 'user', 'GET', '/user/create', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(550, 1, 'user', 'POST', '/user', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(551, 1, 'user', 'GET', '/user/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(552, 1, 'user', 'PUT', '/user/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(553, 1, 'user', 'GET', '/user/email/order', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(554, 1, 'user', 'GET', '/user/email/contact', '2018-11-08 08:42:11', '2018-11-08 08:42:11'),
(555, 1, 'user', 'DELETE', '/user/{id}', '2018-11-08 08:42:11', '2018-11-08 08:42:11');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `gallery_id`, `title`, `description`, `image`, `link`, `link_type`, `link_title`, `status`, `created_at`, `updated_at`, `priority`) VALUES
(2, 1, 'Linking Up ', '<p>Quality Products</p>\n<p>From Prestigious Brands</p>', '1533886582_slider_8_2048-min_1541558315.jpg', 'http://realtrading.demo3.laziweb.com/collection/all', 'custom', '', 'active', '2018-08-10 07:38:07', '2018-11-07 02:39:13', 0),
(3, 1, 'Quality Products', '<p>Linking Up&nbsp;</p>\n<p>With Right Supplies</p>', '1533886657_slider_9_2048-min_1541558315.jpg', 'http://realtrading.demo3.laziweb.com/collection/all', 'custom', '', 'active', '2018-08-10 07:38:58', '2018-11-07 02:39:32', 0),
(4, 1, 'Being Sincere & Enthusiastic', '<p>Quality Products</p>\n<p>From Prestigious Brands</p>', '1533886582_slider_8_2048-min_1541558315.jpg', 'http://realtrading.demo3.laziweb.com/collection/all', 'custom', '', 'active', '2018-08-10 07:39:24', '2018-11-07 02:39:51', 0),
(5, 2, '1', '', '1533889645_client_1.png', 'http://realtrading.demo3.laziweb.com/', 'custom', '', 'active', '2018-08-10 08:27:28', '2018-09-25 09:17:49', 0),
(6, 2, '2', '', '1533889671_client_2.png', 'http://realtrading.demo3.laziweb.com/', 'custom', '', 'active', '2018-08-10 08:27:54', '2018-09-25 09:18:00', 0),
(7, 2, '3', '', '1533889688_client_3.png', 'http://realtrading.demo3.laziweb.com/', 'custom', '', 'active', '2018-08-10 08:28:11', '2018-09-25 09:18:09', 0),
(8, 2, '4', '', '1533889702_client_4.png', 'http://realtrading.demo3.laziweb.com/', 'custom', '', 'active', '2018-08-10 08:28:25', '2018-09-25 09:18:17', 0),
(9, 2, '5', '', '1533889715_client_5.png', 'http://realtrading.demo3.laziweb.com/', 'custom', '', 'active', '2018-08-10 08:28:38', '2018-09-25 09:18:23', 0),
(10, 2, '6', '', '1533889671_client_2.png', 'http://realtrading.demo3.laziweb.com/', 'custom', '', 'active', '2018-08-10 08:28:57', '2018-09-25 09:18:33', 0),
(11, 2, '7', '', '1533889688_client_3.png', 'http://realtrading.demo3.laziweb.com/', 'custom', '', 'active', '2018-08-10 08:29:10', '2018-09-25 09:18:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `photo_translations`
--

CREATE TABLE `photo_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `sell` int(11) NOT NULL DEFAULT '0',
  `view` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `priority` int(11) NOT NULL DEFAULT '1000',
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `stock_manage` tinyint(1) NOT NULL DEFAULT '1',
  `stock_quant` int(11) NOT NULL DEFAULT '1',
  `option_1` int(11) NOT NULL DEFAULT '0',
  `option_2` int(11) NOT NULL DEFAULT '0',
  `option_3` int(11) NOT NULL DEFAULT '0',
  `option_4` int(11) NOT NULL DEFAULT '0',
  `option_5` int(11) NOT NULL DEFAULT '0',
  `option_6` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` float DEFAULT '0',
  `stop_selling` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `image`, `description`, `content`, `sell`, `view`, `status`, `priority`, `tags`, `template`, `stock_manage`, `stock_quant`, `option_1`, `option_2`, `option_3`, `option_4`, `option_5`, `option_6`, `created_at`, `updated_at`, `rating`, `stop_selling`) VALUES
(1, 'Fort Collins Men’s Cotton Coat', '1533892138_shop_11.jpg', '', '', 19, 80, 'active', 1000, '', '', 1, -18, 0, 0, 0, 0, 0, 0, '2018-08-10 09:09:06', '2018-10-04 07:30:37', 0, 0),
(2, 'Fort Collins Men’s Cotton Coat', '1533892182_shop_13-min_1541558316.jpg', '', '', 21, 35, 'active', 1000, '', '', 1, -20, 0, 0, 0, 0, 0, 0, '2018-08-10 09:09:31', '2018-11-07 02:43:57', 0, 0),
(3, 'Black Tote', '1533892336_shop_10-min_1541558316.jpg', 'Mô tả sản phẩm', '<p>nội dung sản phẩm</p>', 12, 44, 'active', 1000, '', '', 1, -11, 0, 0, 0, 0, 0, 0, '2018-08-10 09:12:26', '2018-11-07 02:44:25', 0, 0),
(4, 'Double Breasted Coat', '1533892437_shop_12-min_1541558316.jpg', '', '', 9, 19, 'active', 1000, '', '', 1, -8, 0, 0, 0, 0, 0, 0, '2018-08-10 09:14:11', '2018-11-07 02:44:39', 0, 0),
(5, 'Utsukushii Women’s Handbag', '1533892472_shop_17.jpg', '', '', 4, 21, 'active', 1000, '', '', 1, -3, 0, 0, 0, 0, 0, 0, '2018-08-10 09:14:48', '2018-08-10 09:14:48', 0, 0),
(6, 'Women’s Handbag', '1533892526_shop_14.jpg', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.&nbsp;<br />Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum nostrum fugiat repellat, libero aperiam. Magnam excepturi voluptatum sit, optio, dicta esse, recusandae accusantium quas dignissimos ipsum ex, maiores impedit voluptatem!</p>', 3, 20, 'active', 1000, '', '', 1, -2, 0, 0, 0, 0, 0, 0, '2018-08-10 09:15:39', '2018-09-25 09:13:09', 0, 0),
(7, 'Handbag White', '1533892590_shop_15.jpg', '', '', 3, 4, 'active', 1000, '', '', 1, -2, 0, 0, 0, 0, 0, 0, '2018-08-10 09:16:44', '2018-08-10 09:16:44', 0, 0),
(8, 'Daphne Women’s Handbag', '1537430919_product-try.png', '', '<p>Check photo</p>', 3, 7, 'active', 1000, '', '', 1, -2, 0, 0, 0, 0, 0, 0, '2018-08-10 09:18:38', '2018-09-20 08:50:24', 0, 0),
(9, 'Locker', '1535298407_abs-mseries04.jpg', 'Mô tả sản phẩm 1', '<p>Nội dung sản phẩm 1</p>', 1, 47, 'active', 1000, '#bag#', '', 1, 0, 0, 0, 0, 0, 0, 0, '2018-08-21 16:46:06', '2018-09-20 03:20:07', 0, 0),
(10, 'Ghế Sofa 3 seater', '1535295014_3_seater_sofa.png', 'Ghế Sofa 3 Seater, êm ái và sang trọng', '<p>Ghế Sofa 3 seater.&nbsp;</p>\n<p>M&agrave;u sắc trang nh&atilde;, chất liệu vải bố cao cấp. Đem đến cảm gi&aacute;c &ecirc;m &aacute;i khi ngồi</p>', 6, 49, 'active', 1000, '#Sofa#', '', 1, -5, 0, 0, 0, 0, 0, 0, '2018-08-26 14:51:40', '2018-08-26 15:09:40', 0, 0),
(11, 'ABS Locker M series', '1535298407_abs-mseries04.jpg', 'Tủ ABS Locker, M Series của thương hiệu Locker& Lock Singapore', '<p class=\"fw_light m_bottom_14\">ABS M series is made up of engineering grade ABS (Acrylonitrile Butadiene Styrene) plastic material with coloured doors. Multiple locking options with keyless lock, key lock, coin return lock, RFID (digital lock) and hasp (for padlock)</p>\n<p class=\"fw_light m_bottom_14\">ABS M series is available in 2 models and sizes..The smallest size can be stack up to 9 compartments and the other one up to 5 comparment height.</p>\n<p>Internal Dimensions (mm) (H x W x D):<br />Model M400 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;370 x 274 x 348<br />Model M200 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp; 168 x 274 x 348</p>', 1, 32, 'active', 1000, '', '', 1, 0, 0, 0, 0, 0, 0, 0, '2018-08-26 15:54:49', '2018-09-20 08:51:22', 0, 0),
(12, 'RL-10347 Real Keyless Lock', '1537434467_rl10347-02_trang.jpg', 'Khóa mật mã 4 số: RL-10347', '<p><span style=\"color: #000000;\">-&nbsp;Lắp vừa c&aacute;c lỗ kh&oacute;a cam c&oacute; đường k&iacute;nh 16mm/ 19mm, rất dễ lắp đặt v&agrave; thay thế&nbsp; cho những kh&oacute;a cam ti&ecirc;u chuẩn.</span></p>\n<p><span style=\"color: #000000;\">-&nbsp;&nbsp;Cơ chế k&eacute;p: Ch&igrave;a kh&oacute;a vặn để mở kh&oacute;a l&uacute;c cần gấp, xoay một v&ograve;ng để thiết lập mật m&atilde; mới</span></p>\n<p><span style=\"color: #000000;\">-&nbsp;&nbsp;C&oacute; nhiều m&agrave;u lựa chọn</span></p>\n<p><span style=\"color: #000000;\">-&nbsp; Người d&ugrave;ng c&oacute; thể thiết lập 10,000 m&atilde; biến đổi.</span></p>\n<p><span style=\"color: #000000;\">-&nbsp; &nbsp;Th&iacute;ch hợp cho tủ Locker, tủ gỗ, hộp dụng cụ, hộp mail, hay hộp lưu trữ.</span></p>\n<p><span style=\"color: #000000;\">-&nbsp; Kh&ocirc;ng cần tay cầm, hay n&uacute;m cửa, kh&oacute;a được d&ugrave;ng như n&uacute;m vặn để đ&oacute;ng/ mở tủ</span></p>', 0, 6, 'active', 1000, '', '', 1, 1, 0, 0, 0, 0, 0, 0, '2018-09-20 09:08:30', '2018-09-20 09:14:04', 0, 0),
(13, 'RL-10347 Real Keyless Lock ( Black)', '1537436424_rl-10347_combination_locks_06.jpg', 'Khóa mật mã 4 số: RL-10347 ( Màu đen)', '<p><span style=\"color: #000000;\">-&nbsp;Lắp vừa c&aacute;c lỗ kh&oacute;a cam c&oacute; đường k&iacute;nh 16mm/ 19mm, rất dễ lắp đặt v&agrave; thay thế&nbsp; cho những kh&oacute;a cam ti&ecirc;u chuẩn.</span></p>\n<p><span style=\"color: #000000;\">-&nbsp;&nbsp;Cơ chế k&eacute;p: Ch&igrave;a kh&oacute;a vặn để mở kh&oacute;a l&uacute;c cần gấp, xoay một v&ograve;ng để thiết lập mật m&atilde; mới</span></p>\n<p><span style=\"color: #000000;\">-&nbsp;&nbsp;M&agrave;u sắc: Đen</span></p>\n<p><span style=\"color: #000000;\">-&nbsp; Người d&ugrave;ng c&oacute; thể thiết lập 10,000 m&atilde; biến đổi.</span></p>\n<p><span style=\"color: #000000;\">-&nbsp; &nbsp;Th&iacute;ch hợp cho tủ Locker, tủ gỗ, hộp dụng cụ, hộp mail, hay hộp lưu trữ.</span></p>\n<p><span style=\"color: #000000;\">-&nbsp; Kh&ocirc;ng cần tay cầm, hay n&uacute;m cửa, kh&oacute;a được d&ugrave;ng như n&uacute;m vặn để đ&oacute;ng/ mở tủ</span></p>\n<p>&nbsp;</p>', 0, 5, 'active', 1000, '', '', 1, 1, 0, 0, 0, 0, 0, 0, '2018-09-20 09:21:42', '2018-09-20 09:44:00', 0, 0),
(14, 'RL-9041 Real Keyless Lock', '1537435576_rl9041.jpg', 'Khóa mật mã 4 số RL9041', '<ul>\n<li><span style=\"color: #000000;\">Người dùng có th&ecirc;̉ tự thi&ecirc;́t l&acirc;̣p m&acirc;̣t mã, có đ&ecirc;́n 10,000 mã đ&ecirc;̉ thay đ&ocirc;̉i</span></li>\n<li><span style=\"color: #000000;\">&nbsp;Ch&acirc;́t li&ecirc;̣u: vỏ khóa Hợp Kim Kẽm, b&ecirc;̀n, đẹp, lõi khóa bằng thau, nhíp dày, ch&ocirc;́ng chìa vạn năng</span></li>\n<li><span style=\"color: #000000;\">Có sẵn loại đứng và nằm ngang</span></li>\n<li><span style=\"color: #000000;\">Có sẵn loại mở trái và mở phải</span></li>\n<li><span style=\"color: #000000;\">Thích hợp cho cửa g&ocirc;̃, sắt, nh&ocirc;m dày từ 1~20mm</span></li>\n<li><span style=\"color: #000000;\">Kh&ocirc;ng c&acirc;̀n thẻ, d&acirc;y đi&ecirc;̣n, pin, chìa và kh&ocirc;ng c&acirc;̀n phải đau đ&acirc;̀u khi quản lý</span></li>\n<li><span style=\"color: #000000;\">Có hai cơ ch&ecirc;́ mở: mở bằng m&acirc;̣t mã hoặc bằng chìa chủ.</span></li>\n<li><span style=\"color: #000000;\">Nhà quản lý có th&ecirc;̉ giữ chìa chủ phòng khi qu&ecirc;n m&acirc;̣t mã.</span></li>\n<li><span style=\"color: #000000;\">Núm vặn vừa là nơi mở chìa, vừa dùng như tay nắm ti&ecirc;̣n lợi.</span></li>\n<li><span style=\"color: #000000;\">M&acirc;̣t mã d&ecirc;̃ dàng thay đ&ocirc;̉i trong tình trạng khóa mở<img src=\"http://realtrading.demo3.laziweb.com/uploads/1537436737_rl9041_school_locker.jpg\" alt=\"\" width=\"3008\" height=\"2000\" /></span></li>\n</ul>\n<p>&nbsp;</p>', 0, 17, 'active', 1000, '', '', 1, 1, 0, 0, 0, 0, 0, 0, '2018-09-20 09:27:40', '2018-09-25 10:58:42', 0, 0),
(15, 'Product 1', '', '', '', 0, 0, 'active', 1000, '', '', 1, 1, 0, 0, 0, 0, 0, 0, '2018-11-07 08:58:31', '2018-11-07 09:08:25', 0, 0),
(16, 'Product 2', '', '', '', 0, 0, 'active', 1000, '', '', 1, 1, 0, 0, 0, 0, 0, 0, '2018-11-07 08:58:47', '2018-11-07 09:03:23', 0, 0),
(17, 'Product 3', '', '', '', 0, 2, 'active', 1000, '', '', 1, 1, 0, 0, 0, 0, 0, 0, '2018-11-07 08:59:49', '2018-11-07 09:04:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_buy_together`
--

CREATE TABLE `product_buy_together` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_buy_together_id` int(11) NOT NULL DEFAULT '0',
  `product_buy_together_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price_sale` int(11) NOT NULL,
  `promotion` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_translations`
--

CREATE TABLE `product_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `name`, `region_code`) VALUES
(123, 'Hà Nội', 1),
(124, 'Hà Giang', 1),
(125, 'Cao Bằng', 1),
(126, 'Bắc Kạn', 1),
(127, 'Tuyên Quang', 1),
(128, 'Lào Cai', 1),
(129, 'Điện Biên', 1),
(130, 'Lai Châu', 1),
(131, 'Sơn La', 1),
(132, 'Yên Bái', 1),
(133, 'Hoà Bình', 1),
(134, 'Thái Nguyên', 1),
(135, 'Lạng Sơn', 1),
(136, 'Quảng Ninh', 1),
(137, 'Bắc Giang', 1),
(138, 'Phú Thọ', 1),
(139, 'Vĩnh Phúc', 1),
(140, 'Bắc Ninh', 1),
(141, 'Hải Dương', 1),
(142, 'Hải Phòng', 1),
(143, 'Hưng Yên', 1),
(144, 'Thái Bình', 1),
(145, 'Hà Nam', 1),
(146, 'Nam Định', 1),
(147, 'Ninh Bình', 1),
(148, 'Thanh Hóa', 2),
(149, 'Nghệ An', 2),
(150, 'Hà Tĩnh', 2),
(151, 'Quảng Bình', 2),
(152, 'Quảng Trị', 2),
(153, 'Thừa Thiên Huế', 2),
(154, 'Đà Nẵng', 2),
(155, 'Quảng Nam', 2),
(156, 'Quảng Ngãi', 2),
(157, 'Bình Định', 2),
(158, 'Phú Yên', 2),
(159, 'Khánh Hoà', 2),
(160, 'Ninh Thuận', 2),
(161, 'Bình Thuận', 2),
(162, 'Kon Tum', 2),
(163, 'Gia Lai', 2),
(164, 'Đăk Lăk', 2),
(165, 'Đăk Nông', 2),
(166, 'Lâm Đồng', 2),
(167, 'Bình Phước', 3),
(168, 'Tây Ninh', 3),
(169, 'Bình Dương', 3),
(170, 'Đồng Nai', 3),
(171, 'Bà Rịa - Vũng Tàu', 3),
(172, 'Hồ Chí Minh', 3),
(173, 'Long An', 3),
(174, 'Tiền Giang', 3),
(175, 'Bến Tre', 3),
(176, 'Trà Vinh', 3),
(177, 'Vĩnh Long', 3),
(178, 'Đồng Tháp', 3),
(179, 'An Giang', 3),
(180, 'Kiên Giang', 3),
(181, 'Cần Thơ', 3),
(182, 'Hậu Giang', 3),
(183, 'Sóc Trăng', 3),
(184, 'Bạc Liêu', 3),
(185, 'Cà Mau', 3);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '-1',
  `customer_id` int(11) NOT NULL DEFAULT '-1',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` double(8,2) NOT NULL DEFAULT '5.00',
  `post_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'product',
  `post_id` int(11) NOT NULL DEFAULT '-1',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `like` int(11) NOT NULL DEFAULT '0',
  `dislike` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Quản trị viên cấp cao', '2018-08-07 02:47:21', '2018-11-08 08:42:11'),
(2, 'Quản trị viên nội dung', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(3, 'Quản trị viên đơn hàng', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(4, 'Quản trị viên bài viết', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(5, 'Quản trị viên sản phẩm', '2018-08-07 02:47:21', '2018-08-07 02:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE `sale` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '2018-08-07',
  `end_date` date NOT NULL DEFAULT '2018-08-07',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `type_relation` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'product',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sale_product`
--

CREATE TABLE `sale_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `sale_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_robots` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_image` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `meta_title`, `meta_description`, `meta_keyword`, `meta_robots`, `meta_image`, `type`, `type_id`, `created_at`, `updated_at`) VALUES
(1, 'BUSINESS', '', '', 'index, follow', '', 'blog', 1, '2018-08-10 07:49:10', '2018-08-21 07:44:52'),
(2, 'Chính sách đại lý', '', '', 'index, follow', '', 'article', 1, '2018-08-10 07:50:42', '2018-08-26 19:42:07'),
(3, 'Spotlight on Geordie Willis', 'Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd [...]', '', 'index, follow', '', 'article', 2, '2018-08-10 07:51:00', '2018-11-07 02:41:32'),
(4, 'Chính sách bảo hành sản phẩm Real Trading', 'Chính sách bảo hành sản phẩm nội thất', '', 'index, follow', '', 'article', 3, '2018-08-10 07:51:26', '2018-08-26 19:37:39'),
(5, 'Spotlight on Geordie Willis', 'Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd [...]', '', 'index, follow', '', 'article', 4, '2018-08-10 07:51:38', '2018-08-10 07:51:41'),
(6, 'Spotlight on Geordie Willis', 'Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd [...]', '', 'index, follow', '', 'article', 5, '2018-08-10 07:51:46', '2018-11-07 02:41:43'),
(7, 'Spotlight on Geordie Willis', 'Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. &amp; Rudd [...]', '', 'index, follow', '', 'article', 6, '2018-08-10 07:52:38', '2018-09-11 04:36:04'),
(8, 'LIFE STYLE', '', '', 'index, follow', '', 'blog', 2, '2018-08-10 08:32:50', '2018-08-14 02:47:03'),
(9, 'Spotlight on Geordie Willis', 'Geordie Willis is creative director at renowned fine wine and spirits merchants Berry Bros. & Rudd in St. James’s Street, Pall Mall. Founded in 1698, it is the UK’s oldest family-run wine merchant and supplier of wines', '', 'index, follow', '', 'article', 7, '2018-08-10 08:40:39', '2018-11-07 07:40:37'),
(10, 'Hướng dẫn mua hàng', 'Chúng tôi sẽ sớm cập nhật các hướng dẫn mua hàng chi tiết cho Quý khách', '', 'index, follow', '', 'article', 8, '2018-08-10 08:40:54', '2018-08-26 19:48:11'),
(11, 'ABS Locker', 'Tủ Locker ABS của thương hiệu Locker & Lock Singapore. \nTủ được làm từ vật liệu nhựa ABS cấp kỹ thuật, \nDòng ABS M được tạo thành từ vật liệu nhựa ABS kỹ thuật có cửa màu, mạnh mẽ, bền bỉ và chống gỉ. ', 'ABS Locker, Plastic locker, tủ nhựa ABS, ', 'index, follow', '1535298407_abs-mseries04.jpg', 'collection', 1, '2018-08-10 09:05:39', '2018-09-04 08:54:25'),
(12, 'Khóa Tủ', '', '', 'index, follow', '', 'collection', 2, '2018-08-10 09:06:40', '2018-08-26 16:20:11'),
(13, 'Ghế nhân viên', '', '', 'index, follow', '', 'collection', 3, '2018-08-10 09:06:56', '2018-08-26 16:33:55'),
(14, 'Bàn văn phòng', '', '', 'index, follow', '', 'collection', 4, '2018-08-10 09:07:12', '2018-08-26 16:47:19'),
(15, 'Fort Collins Men’s Cotton Coat', '', '', 'index, follow', '', 'product', 1, '2018-08-10 09:09:07', '2018-10-04 07:30:37'),
(16, 'Fort Collins Men’s Cotton Coat', '', '', 'index, follow', '', 'product', 2, '2018-08-10 09:09:31', '2018-11-07 02:43:57'),
(17, 'Black Tote', '', '', 'index, follow', '', 'product', 3, '2018-08-10 09:12:26', '2018-11-07 02:44:25'),
(18, 'Double Breasted Coat', '', '', 'index, follow', '', 'product', 4, '2018-08-10 09:14:12', '2018-11-07 02:44:39'),
(19, 'Utsukushii Women’s Handbag', '', '', 'index, follow', '', 'product', 5, '2018-08-10 09:14:48', '2018-08-10 09:14:48'),
(20, 'Women’s Handbag', '', '', 'index, follow', '', 'product', 6, '2018-08-10 09:15:39', '2018-09-25 09:13:09'),
(21, 'Handbag White', '', '', 'index, follow', '', 'product', 7, '2018-08-10 09:16:44', '2018-08-10 09:16:44'),
(22, 'Daphne Women’s Handbag', '', '', 'index, follow', '', 'product', 8, '2018-08-10 09:18:39', '2018-09-20 08:50:24'),
(23, 'CONTACT', '', '', 'index, follow', '1535898047_customer_support_793_x826_(1).png', 'page', 1, '2018-08-11 03:52:31', '2018-09-12 06:00:58'),
(24, 'Giới thiệu', '', '', 'index, follow', '', 'page', 2, '2018-08-11 04:53:05', '2018-11-07 07:52:06'),
(25, 'What we do', '', '', 'index, follow', '', 'blog', 3, '2018-08-11 07:57:11', '2018-08-14 08:03:40'),
(26, 'Supersonic import', 'No more time wasting in data import. We rocket its speed by improving the progress which strikingly reduces your waiting time.', '', 'index, follow', '', 'article', 9, '2018-08-11 07:58:29', '2018-09-04 04:21:35'),
(27, 'Supersonic import', 'No more time wasting in data import. We rocket its speed by improving the progress which strikingly reduces your waiting time.', '', 'index, follow', '', 'article', 10, '2018-08-11 07:58:55', '2018-11-07 07:41:09'),
(28, 'Fast loading speed', 'The bad news is time flies. The good news is you\'re the pilot. Be a wise pilot to choose Nito\'s fast loading speed and be outstanding.', '', 'index, follow', '', 'article', 11, '2018-08-11 08:02:05', '2018-11-07 08:19:20'),
(29, 'Easy page builder', 'You don\'t need to any code to build your page. Visual Composer will do it for you. Just drag & crop elements & enjoy the process.', '', 'index, follow', '1533973396_about3.png', 'article', 12, '2018-08-11 08:02:57', '2018-11-07 08:09:52'),
(30, 'Fully Responsive', 'We cannot deny the importance of mobile in interacting with customer. Responding to that requirement, Nito is fully responsive.', '', 'index, follow', '', 'article', 13, '2018-08-11 08:04:39', '2018-11-07 08:17:34'),
(31, 'Powerful Shortcodes', 'There may be some complicated establishing tasks. But no worry as all of those have been solved by Nito\'s effective shortcodes system.', '', 'index, follow', '', 'article', 14, '2018-08-11 08:05:23', '2018-11-07 08:05:32'),
(32, 'Post Format', '', '', 'index, follow', '', 'blog', 4, '2018-08-11 09:13:03', '2018-11-07 07:37:10'),
(33, 'A perfect day in New York', 'Creative strategy', '', 'index, follow', '', 'article', 15, '2018-08-11 09:14:05', '2018-08-11 09:17:34'),
(34, 'A perfect day in New York', 'Creative strategy', '', 'index, follow', '', 'article', 16, '2018-08-11 09:14:22', '2018-08-11 09:17:41'),
(35, 'A perfect day in New York', 'Creative strategy', '', 'index, follow', '', 'article', 17, '2018-08-11 09:15:00', '2018-08-11 09:17:49'),
(36, 'A perfect day in New York', 'Creative strategy', '', 'index, follow', '', 'article', 18, '2018-08-11 09:15:52', '2018-08-16 03:02:35'),
(37, 'A perfect day in New York', 'Creative strategy', '', 'index, follow', '', 'article', 19, '2018-08-11 09:16:31', '2018-08-11 09:16:55'),
(38, 'A perfect day in New York', 'Creative strategy', '', 'index, follow', '', 'article', 20, '2018-08-11 09:17:03', '2018-08-11 09:17:23'),
(39, 'sidebar', '', '', 'index, follow', '', 'blog', 5, '2018-08-14 09:31:00', '2018-08-14 09:31:13'),
(40, 'sidebar', 'aaaaaaaaaaaaaaaaaaaaaaaa', '', 'index, follow', '', 'blog', 6, '2018-08-14 09:37:14', '2018-08-14 09:37:14'),
(41, 'test', 'test', 'test', 'index, follow', '1533979002_project_5.jpg', 'article', 21, '2018-08-16 02:21:14', '2018-08-18 08:15:04'),
(42, 'test', 'test', '', 'index, follow', '', 'article', 22, '2018-08-16 02:35:44', '2018-08-16 02:42:15'),
(43, 'What we do', 'Spotlight on Geordie Willis', '', 'index, follow', '', 'blog', 7, '2018-08-16 02:40:18', '2018-11-07 08:18:04'),
(44, 'what we do', 'test', '', 'index, follow', '', 'article', 23, '2018-08-16 02:54:17', '2018-08-16 02:54:17'),
(45, 'test', '1111', '', 'index, follow', '', 'article', 24, '2018-08-16 02:55:10', '2018-08-16 02:55:10'),
(46, 'Q&A', 'Thông tin khách hàng cần biết', '', 'index, follow', '', 'blog', 8, '2018-08-16 03:04:09', '2018-08-26 18:46:18'),
(47, 'Hỗ Trợ Khách Hàng', '', '', 'index, follow', '', 'blog', 9, '2018-08-16 12:08:03', '2018-08-16 12:08:03'),
(48, 'Brunch on Saturday', '', '', 'index, follow', '', 'article', 25, '2018-08-16 12:08:24', '2018-11-07 08:53:19'),
(49, 'Spotlight on Geordie Willis', '', '', 'index, follow', '', 'page', 3, '2018-08-17 03:28:37', '2018-08-17 03:28:37'),
(50, 'New Trend', '', '', 'index, follow', '', 'blog', 10, '2018-08-21 07:36:15', '2018-11-07 07:35:40'),
(51, 'Spirit', '', '', 'index, follow', '', 'blog', 11, '2018-08-21 07:36:35', '2018-11-07 07:38:30'),
(52, 'Human', '', '', 'index, follow', '', 'blog', 12, '2018-08-21 07:43:20', '2018-11-07 07:38:03'),
(53, 'ABS Locker', '', '', 'index, follow', '', 'collection', 5, '2018-08-21 07:49:28', '2018-10-05 03:16:07'),
(54, 'PVC Locker', '', '', 'index, follow', '', 'collection', 6, '2018-08-21 07:50:02', '2018-08-26 16:16:35'),
(55, 'Metal Locker', '', '', 'index, follow', '', 'collection', 7, '2018-08-21 07:50:19', '2018-08-26 16:17:15'),
(56, 'SUS Locker', '', '', 'index, follow', '', 'collection', 8, '2018-08-21 07:50:33', '2018-08-26 16:18:42'),
(57, 'Khóa mật mã', '', '', 'index, follow', '', 'collection', 9, '2018-08-21 07:54:27', '2018-08-26 16:24:17'),
(58, 'Khóa tủ gỗ', '', '', 'index, follow', '', 'collection', 10, '2018-08-21 07:55:10', '2018-08-26 16:23:41'),
(59, 'Khóa tủ sắt, nhựa', '', '', 'index, follow', '', 'collection', 11, '2018-08-21 07:55:21', '2018-08-26 16:25:15'),
(60, 'Ghế nhân viên', '', '', 'index, follow', '', 'collection', 12, '2018-08-21 07:56:13', '2018-08-26 16:34:32'),
(61, 'Ghế lãnh đạo', '', '', 'index, follow', '', 'collection', 13, '2018-08-21 07:56:27', '2018-08-26 16:35:36'),
(62, 'Ghế phòng họp', '', '', 'index, follow', '', 'collection', 14, '2018-08-21 07:56:39', '2018-08-26 16:37:18'),
(63, 'Bàn Lãnh đạo', '', '', 'index, follow', '', 'collection', 15, '2018-08-21 07:57:15', '2018-08-26 16:55:36'),
(64, 'Bàn nhân viên', '', '', 'index, follow', '', 'collection', 16, '2018-08-21 07:58:50', '2018-08-26 17:11:53'),
(65, 'Bàn phòng họp', '', '', 'index, follow', '', 'collection', 17, '2018-08-21 07:59:09', '2018-08-26 17:10:17'),
(66, 'ABS Locker', 'ABS Locker- Endurable and strong with color doors', '', 'index, follow', '', 'product', 9, '2018-08-21 16:46:06', '2018-09-20 03:20:07'),
(67, 'Test nhóm sp1', 'mo ta nhom sp 1', '', 'index, follow', '', 'collection', 18, '2018-08-22 03:42:15', '2018-08-22 03:45:58'),
(68, 'con nhom 1', 'mo ta con nhom 1', '', 'index, follow', '', 'collection', 19, '2018-08-22 03:43:36', '2018-08-22 03:46:20'),
(69, 'Câu hỏi thường gặp', 'Những câu hỏi thường gặp sẽ được chúng tôi giải đáp tại đây. Nếu có câu hỏi Quý khách vui lòng liên hệ để được giải đáp trong thời gian sớm nhất.', 'Q&A', 'index, follow', '1533981138_blog_detail.jpg', 'article', 26, '2018-08-22 03:52:30', '2018-08-26 19:45:27'),
(70, 'test bai viet 2', '', '', 'index, follow', '', 'article', 27, '2018-08-22 04:54:45', '2018-08-22 04:54:49'),
(71, 'Ghế Sofa 3 seater', 'Ghế Sofa 3 Seater, êm ái và sang trọng', 'Sofa', 'index, follow', '', 'product', 10, '2018-08-26 14:51:40', '2018-08-26 15:09:41'),
(72, 'ABS Locker M series', 'Tủ ABS Locker, M Series của thương hiệu Locker& Lock Singapore', 'ABS locker', 'index, follow', '1535298407_abs-mseries04.jpg', 'product', 11, '2018-08-26 15:54:49', '2018-09-20 08:51:22'),
(73, 'Ghế training', '', '', 'index, follow', '', 'collection', 20, '2018-08-26 16:36:33', '2018-08-26 16:36:38'),
(74, 'Ghế sofa', '', '', 'index, follow', '', 'collection', 21, '2018-08-26 16:38:30', '2018-08-26 16:38:37'),
(75, 'Bàn Sofa', '', '', 'index, follow', '', 'collection', 22, '2018-08-26 17:10:49', '2018-08-26 17:10:49'),
(76, 'Văn phòng phẩm', '', '', 'index, follow', '', 'collection', 23, '2018-08-26 17:30:40', '2018-08-26 17:32:09'),
(77, 'News & Event', '', '', 'index, follow', '', 'blog', 13, '2018-09-05 08:30:31', '2018-11-07 08:54:49'),
(78, 'Spotlight on Geordie Willis', '', '', 'index, follow', '1533887438_blog_1.jpg', 'article', 28, '2018-09-05 08:33:51', '2018-11-07 07:17:29'),
(79, 'Blackpool Lancashire', '', '', 'index, follow', 'blog_2_1541575200.jpg', 'article', 29, '2018-09-05 08:34:45', '2018-11-07 08:50:28'),
(80, 'Brunch on Saturday', '', '', 'index, follow', '1533887543_blog_3.jpg', 'article', 30, '2018-09-05 08:35:23', '2018-11-07 07:21:45'),
(81, 'RL-10347 Real Keyless Lock', 'Khóa mật mã 4 số: RL-10347', '', 'index, follow', '', 'product', 12, '2018-09-20 09:08:30', '2018-09-20 09:14:04'),
(82, 'RL-10347 Real Keyless Lock ( Black)', 'Khóa mật mã 4 số: RL-10347 ( Màu đen)', '', 'index, follow', '1537435264_rl10347-bk.png', 'product', 13, '2018-09-20 09:21:42', '2018-09-20 09:44:00'),
(83, 'RL-9041 Real Keyless Lock', 'Khóa mật mã 4 số RL9041', '', 'index, follow', '', 'product', 14, '2018-09-20 09:27:40', '2018-09-25 10:58:42'),
(84, 'Banksy identity', 'In 2008, a report claimed to know the identity of notorious street artist Banksy (spoilers: his name is reportedly Robin Gunningham). Earlier this year, a scientific study analysed everything publically known', '', 'index, follow', 'blog_4_1541575421.jpg', 'article', 31, '2018-11-07 07:23:58', '2018-11-07 07:42:05'),
(85, 'Nick Cave And The Bad Seeds', 'There’s a satisfying continuity of texture, temperament and themes between Skeleton Tree and its predecessor, 2013’s Push The Sky Away – despite the awful intercession, in the intervening years', '', 'index, follow', 'blog_5_1541575428.jpg', 'article', 32, '2018-11-07 07:24:57', '2018-11-07 07:41:54'),
(86, 'Product 1', '', '', 'index, follow', '', 'product', 15, '2018-11-07 08:58:31', '2018-11-07 09:08:25'),
(87, 'Product 2', '', '', 'index, follow', '', 'product', 16, '2018-11-07 08:58:48', '2018-11-07 09:03:23'),
(88, 'Product 3', '', '', 'index, follow', '', 'product', 17, '2018-11-07 08:59:49', '2018-11-07 09:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `seo_translations`
--

CREATE TABLE `seo_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `seo_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `meta_title` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address`
--

CREATE TABLE `shipping_address` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `region` int(11) NOT NULL,
  `subregion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_address`
--

INSERT INTO `shipping_address` (`id`, `order_id`, `name`, `email`, `phone`, `address`, `region`, `subregion`, `created_at`, `updated_at`) VALUES
(1, 1, 'Huy Phạm', 'pnqh67@gmail.com', '0123456789', '', -1, -1, '2018-08-20 03:57:57', '2018-08-20 03:57:57'),
(2, 2, 'Dinh Ngoc', 'ruby@realtrading.vn', '0903688183', '184/30/13 Bùi Văn Ngữ- KP 7 Phường Hiệp Thành', -1, -1, '2018-08-21 11:51:49', '2018-08-21 11:51:49'),
(3, 3, 'Dinh Ngoc', 'ruby@realtrading.vn', '0903688183', '184/30/13 Bùi Văn Ngữ- KP 7 Phường Hiệp Thành', -1, -1, '2018-08-21 11:52:02', '2018-08-21 11:52:02'),
(4, 4, 'Dinh Ngoc', 'ruby@realtrading.vn', '0903688183', '184/30/13 Bùi Văn Ngữ- KP 7 Phường Hiệp Thành', -1, -1, '2018-08-21 11:52:03', '2018-08-21 11:52:03'),
(5, 5, 'Dinh Ngoc', 'ruby@realtrading.vn', '0903688183', '184/30/13 Bùi Văn Ngữ- KP 7 Phường Hiệp Thành', -1, -1, '2018-08-21 11:52:04', '2018-08-21 11:52:04'),
(6, 6, 'ưqer  ádf', 'vubatientien@gmail.com', '0909090502', '', -1, -1, '2018-08-22 02:15:44', '2018-08-22 02:15:44'),
(7, 7, 'qưer ưqer', 'vubatientien@gmail.com', '01698855858', '42/9/12r qưerwe', -1, -1, '2018-08-22 02:42:32', '2018-08-22 02:42:32'),
(8, 8, 'Vũ Tiến', 'vubatientien@gmail.com', '0909876451', '42/9/12', -1, -1, '2018-08-22 06:52:30', '2018-08-22 06:52:30'),
(9, 9, 'Vũ Tiến', 'vubatientien@gmail.com', '0988888888', '42/9/12 khu', -1, -1, '2018-08-22 06:57:22', '2018-08-22 06:57:22'),
(10, 10, 'Dinh Ngoc', 'ruby@realtrading.vn', '0903688183', '184/30/13 Bùi Văn Ngữ- KP 7 Phường Hiệp Thành', -1, -1, '2018-08-28 14:09:04', '2018-08-28 14:09:04'),
(11, 11, 'Đinh Kim', 'ruby@realtrading.vn', '0909318163', '132/12 Đường TA19', -1, -1, '2018-08-28 14:13:45', '2018-08-28 14:13:45'),
(12, 12, 'E E', 'ruby@realtrading.vn', '0909318163', '132/12 Đường TA19', -1, -1, '2018-08-28 14:21:58', '2018-08-28 14:21:58'),
(13, 13, 'Test gmail', 'laziweb.customercare2@gmail.com', '0909999999', 'an giang', -1, -1, '2018-09-04 10:27:15', '2018-09-04 10:27:15'),
(14, 14, 'eww ee', 'admin@gmail.com', '01657977672', '', -1, -1, '2018-09-05 08:16:03', '2018-09-05 08:16:03'),
(15, 15, 'Vũ Tiến', 'vubatientien@gmail.com', '01698855226', '42/9/12', -1, -1, '2018-09-06 02:43:15', '2018-09-06 02:43:15'),
(16, 16, 'Vũ Tiến', 'vubatientien@gmail.com', '01658852475', '42/9', -1, -1, '2018-09-06 02:44:56', '2018-09-06 02:44:56'),
(17, 17, 'Vũ Tiến', 'vubatientien@gmail.com', '016542255412', '42/9/12', -1, -1, '2018-09-06 02:49:19', '2018-09-06 02:49:19'),
(18, 18, 'Vũ Bá Tiến', 'vubatientien@gmail.com', '0164885574', '42/9', -1, -1, '2018-09-06 02:50:50', '2018-09-06 02:50:50'),
(19, 19, 'Vũ Tiến', 'vubatientien@gmail.com', '01654224451', '42', -1, -1, '2018-09-07 09:22:05', '2018-09-07 09:22:05'),
(20, 20, 'Vũ Tiến', 'vubatientien@gmail.com', '01658852654', '42/9', -1, -1, '2018-09-07 09:23:26', '2018-09-07 09:23:26'),
(21, 21, 'ho 1 ten 1', 'mail1@gmail.com', '01699999999', 'dia chi 1', -1, -1, '2018-09-07 09:33:19', '2018-09-07 09:33:19'),
(22, 22, 'Nguyễn Đức Mạnh', 'ducmanh0323@gmail.com', '0938774611', '22 Thạch Lam', -1, -1, '2018-09-26 07:56:51', '2018-09-26 07:56:51'),
(23, 23, 'Nguyễn Đức Mạnh', 'ducmanh0323@gmail.com', '0938774611', '22 Thạch Lam', -1, -1, '2018-09-26 07:57:59', '2018-09-26 07:57:59'),
(24, 24, 'Doãn Cường', 'superadmin@gmail.com', '0987654321', '121414', -1, -1, '2018-09-27 09:40:16', '2018-09-27 09:40:16'),
(25, 25, 'Doãn Cường', 'cuongmt01@gmail.com', '09887090906', '235235', -1, -1, '2018-09-27 10:06:15', '2018-09-27 10:06:15'),
(26, 26, 'Vũ Tiến', 'vubatientien@gmail.com', '01654789789', '42/9/12 ', -1, -1, '2018-09-28 01:06:04', '2018-09-28 01:06:04'),
(27, 27, 'Doãn Cường', 'cuongmt01@gmail.com', '0987654321', '49 vo thanh trang', -1, -1, '2018-09-28 02:17:40', '2018-09-28 02:17:40'),
(28, 28, 'Doãn Cường', 'cuongmt01@gmail.com', '0987654321', '49 vo thanh trang', -1, -1, '2018-09-28 02:30:06', '2018-09-28 02:30:06'),
(29, 29, 'Doãn Cường', 'cuongmt01@gmail.com', '0987654321', '49 vo thanh trang', -1, -1, '2018-09-28 02:48:45', '2018-09-28 02:48:45'),
(30, 30, 'Vũ Tiến', 'vubatientien@gmail.com', '01654789789', '42/9/12', -1, -1, '2018-09-28 06:29:25', '2018-09-28 06:29:25'),
(31, 31, 'Vũ Tiến', 'vubatientien@gmail.com', '01654789789', 'q123', -1, -1, '2018-09-28 06:32:30', '2018-09-28 06:32:30'),
(32, 32, 'Vũ Tiến', 'vubatientien@gmail.com', '01654789789', '42/9/1', -1, -1, '2018-10-05 04:27:23', '2018-10-05 04:27:23'),
(33, 33, 'Nguyễn Đức Mạnh', 'ducmanh0323@gmail.com', '0938774611', '22 Thạch Lam', -1, -1, '2018-10-08 04:50:32', '2018-10-08 04:50:32'),
(34, 34, 'Vĩ Hà', 'viha.tran764@gmail.com', '0912345678', '123 tô hiến thành tp.hcm', 137, 140, '2018-11-06 07:40:43', '2018-11-06 07:40:43'),
(35, 35, 'QA Team ', 'la@gmail.com', '0912345678', '123 tô hiến thành tp.hcm', 126, 38, '2018-11-07 07:36:08', '2018-11-07 07:36:08'),
(36, 36, 'Quỳnh', 'laziweb.customercare@gmail.com', '01664922268', 'Tân Phú', 126, 41, '2018-11-07 10:37:12', '2018-11-07 10:37:12'),
(37, 37, 'aa', 'admin@gmail.co', '01654789789', '42/9/12', 179, 599, '2018-11-08 02:15:13', '2018-11-08 02:15:13'),
(38, 38, 'danh', 'a@gmail.com', '0932720274', '163 tô hiến thành', 172, 526, '2018-11-08 08:46:58', '2018-11-08 08:46:58'),
(39, 39, 'danh', 'a@gmail.com', '0932720274', '163 tô hiến thành', 172, 526, '2018-11-08 08:49:39', '2018-11-08 08:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_fee_region`
--

CREATE TABLE `shipping_fee_region` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_fee_region`
--

INSERT INTO `shipping_fee_region` (`id`, `title`, `type`, `price`, `from`, `to`, `region_id`, `created_at`, `updated_at`) VALUES
(1, 'COD', 'price', 30000, 10000, 500000, 179, '2018-08-21 09:02:33', '2018-08-21 09:03:34'),
(2, 'Ship COD', 'price', 50000, 300000, 1000000, 100, '2018-08-26 18:02:03', '2018-08-26 18:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_fee_subregion`
--

CREATE TABLE `shipping_fee_subregion` (
  `id` int(10) UNSIGNED NOT NULL,
  `subregion_id` int(11) NOT NULL,
  `shipping_fee_region_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_fee_subregion`
--

INSERT INTO `shipping_fee_subregion` (`id`, `subregion_id`, `shipping_fee_region_id`, `price`, `created_at`, `updated_at`) VALUES
(3, 599, 1, 10000, '2018-08-21 09:03:34', '2018-08-21 09:03:34'),
(4, 600, 1, 15000, '2018-08-21 09:03:34', '2018-08-21 09:03:34'),
(5, 601, 1, 18000, '2018-08-21 09:03:34', '2018-08-21 09:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_order`
--

CREATE TABLE `shipping_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `shipping_method` text COLLATE utf8_unicode_ci NOT NULL,
  `label_id` text COLLATE utf8_unicode_ci NOT NULL,
  `reason_code` int(11) NOT NULL,
  `weight` text COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `fee` int(11) NOT NULL,
  `pick_time` text COLLATE utf8_unicode_ci NOT NULL,
  `deliver_time` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slug`
--

CREATE TABLE `slug` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `post_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slug`
--

INSERT INTO `slug` (`id`, `post_id`, `post_type`, `lang`, `handle`, `created_at`, `updated_at`) VALUES
(1, 1, 'gallery', 'vi', 'slider', '2018-08-10 07:36:03', '2018-08-10 07:36:03'),
(3, 1, 'article', 'vi', 'chinh-sach-dai-ly', '2018-08-10 07:50:42', '2018-08-26 19:42:07'),
(4, 2, 'article', 'vi', 'spotlight-on-geordie-willis', '2018-08-10 07:51:00', '2018-11-07 02:41:32'),
(5, 3, 'article', 'vi', 'chinh-sach-bao-hanh', '2018-08-10 07:51:26', '2018-08-26 19:37:39'),
(6, 4, 'article', 'vi', 'spotlight-on-geordie-willis-3', '2018-08-10 07:51:38', '2018-08-10 07:51:41'),
(7, 5, 'article', 'vi', 'brunch-on-saturday', '2018-08-10 07:51:46', '2018-11-07 02:41:43'),
(8, 6, 'article', 'vi', 'brunch-on-saturday-1', '2018-08-10 07:52:38', '2018-09-11 04:36:04'),
(9, 2, 'gallery', 'vi', 'company', '2018-08-10 08:27:00', '2018-09-25 09:49:43'),
(11, 7, 'article', 'vi', 'spotlight-on-geordie-willis-4', '2018-08-10 08:40:39', '2018-11-07 07:40:37'),
(12, 8, 'article', 'vi', 'huong-dan-mua-hang', '2018-08-10 08:40:54', '2018-08-26 19:48:10'),
(13, 1, 'collection', 'vi', 'locker', '2018-08-10 09:05:39', '2018-09-04 08:54:25'),
(14, 2, 'collection', 'vi', 'khoa-tu', '2018-08-10 09:06:40', '2018-08-26 16:20:11'),
(15, 3, 'collection', 'vi', 'chair-1', '2018-08-10 09:06:56', '2018-08-26 16:33:55'),
(16, 4, 'collection', 'vi', 'office-table', '2018-08-10 09:07:12', '2018-08-26 16:47:19'),
(17, 1, 'product', 'vi', 'fort-collins-mens-cotton-coat', '2018-08-10 09:09:07', '2018-10-04 07:30:37'),
(18, 2, 'product', 'vi', 'fort-collins-men-s-cotton-coat', '2018-08-10 09:09:31', '2018-11-07 02:43:57'),
(19, 3, 'product', 'vi', 'black-tote', '2018-08-10 09:12:26', '2018-11-07 02:44:25'),
(20, 4, 'product', 'vi', 'double-breasted-coat', '2018-08-10 09:14:11', '2018-11-07 02:44:39'),
(21, 5, 'product', 'vi', 'utsukushii-womens-handbag', '2018-08-10 09:14:48', '2018-08-10 09:14:48'),
(22, 6, 'product', 'vi', 'womens-handbag', '2018-08-10 09:15:39', '2018-09-25 09:13:09'),
(23, 7, 'product', 'vi', 'handbag-white', '2018-08-10 09:16:44', '2018-08-10 09:16:44'),
(24, 8, 'product', 'vi', 'daphne-womens-handbag', '2018-08-10 09:18:38', '2018-09-20 08:50:24'),
(26, 2, 'page', 'vi', 'gioi-thieu', '2018-08-11 04:53:05', '2018-11-07 07:52:06'),
(31, 12, 'article', 'vi', 'factory-supplies', '2018-08-11 08:02:57', '2018-11-07 08:09:52'),
(32, 13, 'article', 'vi', 'fully-responsive', '2018-08-11 08:04:39', '2018-11-07 08:17:34'),
(33, 14, 'article', 'vi', 'office-supplies', '2018-08-11 08:05:23', '2018-11-07 08:05:32'),
(34, 4, 'blog', 'vi', 'post-format', '2018-08-11 09:13:03', '2018-11-07 07:37:09'),
(35, 15, 'article', 'vi', 'a-perfect-day-in-new-york', '2018-08-11 09:14:05', '2018-08-11 09:17:34'),
(36, 16, 'article', 'vi', 'giving-back-to-your-fans', '2018-08-11 09:14:22', '2018-08-11 09:17:41'),
(37, 17, 'article', 'vi', 'reintroducing-a-key-tech-media-publisher', '2018-08-11 09:15:00', '2018-08-11 09:17:49'),
(38, 18, 'article', 'vi', 'unifying-a-brand-identity-and-experience', '2018-08-11 09:15:52', '2018-08-16 03:02:35'),
(39, 19, 'article', 'vi', 'explore-nature', '2018-08-11 09:16:31', '2018-08-11 09:16:55'),
(40, 20, 'article', 'vi', 'the-end-of-social-media', '2018-08-11 09:17:03', '2018-08-11 09:17:22'),
(45, 7, 'blog', 'vi', 'what-we-do', '2018-08-16 02:40:18', '2018-11-07 08:18:04'),
(48, 8, 'blog', 'vi', 'ho-tro-khach-hang', '2018-08-16 03:04:09', '2018-08-26 18:46:18'),
(50, 25, 'article', 'vi', 'brunch-on-saturday-2', '2018-08-16 12:08:24', '2018-11-07 08:53:19'),
(52, 3, 'page', 'vi', 'spotlight-on-geordie-willis-5', '2018-08-17 03:28:37', '2018-08-17 03:28:37'),
(53, 10, 'blog', 'vi', 'new-trend', '2018-08-21 07:36:15', '2018-11-07 07:35:40'),
(54, 11, 'blog', 'vi', 'spirit', '2018-08-21 07:36:35', '2018-11-07 07:38:30'),
(55, 12, 'blog', 'vi', 'human', '2018-08-21 07:43:20', '2018-11-07 07:38:03'),
(56, 5, 'collection', 'vi', 'abs-locker', '2018-08-21 07:49:28', '2018-10-05 03:16:07'),
(57, 6, 'collection', 'vi', 'pvc-locker', '2018-08-21 07:50:02', '2018-08-26 16:16:35'),
(58, 7, 'collection', 'vi', 'metal-locker', '2018-08-21 07:50:19', '2018-08-26 16:17:15'),
(59, 8, 'collection', 'vi', 'stainless-steel-locker', '2018-08-21 07:50:33', '2018-08-26 16:18:42'),
(60, 9, 'collection', 'vi', 'keyless-lock', '2018-08-21 07:54:26', '2018-08-26 16:24:17'),
(61, 10, 'collection', 'vi', 'khoa-tu-go', '2018-08-21 07:55:10', '2018-08-26 16:23:40'),
(62, 11, 'collection', 'vi', 'khoa-tu-sat-nhua', '2018-08-21 07:55:21', '2018-08-26 16:25:15'),
(63, 12, 'collection', 'vi', 'chair-for-staff', '2018-08-21 07:56:13', '2018-08-26 16:34:32'),
(64, 13, 'collection', 'vi', 'chair-for-manager', '2018-08-21 07:56:27', '2018-08-26 16:35:36'),
(65, 14, 'collection', 'vi', 'meeting-chair', '2018-08-21 07:56:39', '2018-08-26 16:37:18'),
(66, 15, 'collection', 'vi', 'manager-chair', '2018-08-21 07:57:15', '2018-08-26 16:55:36'),
(67, 16, 'collection', 'vi', 'ban-nhan-vien', '2018-08-21 07:58:50', '2018-08-26 17:11:53'),
(68, 17, 'collection', 'vi', 'meeting-desk', '2018-08-21 07:59:09', '2018-08-26 17:10:17'),
(69, 9, 'product', 'vi', 'locker-1', '2018-08-21 16:46:06', '2018-09-20 03:20:07'),
(72, 26, 'article', 'vi', 'cau-hoi-thuong-gap', '2018-08-22 03:52:30', '2018-08-26 19:45:27'),
(74, 10, 'product', 'vi', 'ghe-sofa-3-seater', '2018-08-26 14:51:40', '2018-08-26 15:09:40'),
(75, 11, 'product', 'vi', 'abs-locker-m-series', '2018-08-26 15:54:49', '2018-09-20 08:51:22'),
(76, 20, 'collection', 'vi', 'ghe-training', '2018-08-26 16:36:33', '2018-08-26 16:36:38'),
(77, 21, 'collection', 'vi', 'ghe-sofa', '2018-08-26 16:38:30', '2018-08-26 16:38:37'),
(78, 22, 'collection', 'vi', 'ban-sofa', '2018-08-26 17:10:49', '2018-08-26 17:10:49'),
(79, 23, 'collection', 'vi', 'van-phong-pham', '2018-08-26 17:30:40', '2018-08-26 17:32:09'),
(80, 9, 'article', 'vi', 'supersonic-import', '2018-09-04 04:21:35', '2018-09-04 04:21:35'),
(81, 10, 'article', 'vi', 'effortless-customization', '2018-09-04 04:22:02', '2018-11-07 07:41:09'),
(82, 13, 'blog', 'vi', 'life-style', '2018-09-05 08:30:31', '2018-11-07 08:54:49'),
(83, 28, 'article', 'vi', 'spotlight-on-geordie-willis-1', '2018-09-05 08:33:51', '2018-11-07 07:17:29'),
(84, 29, 'article', 'vi', 'hoat-dong-su-kien', '2018-09-05 08:34:45', '2018-11-07 08:50:28'),
(85, 30, 'article', 'vi', 'tin-chuyen-nganh', '2018-09-05 08:35:23', '2018-11-07 07:21:45'),
(86, 12, 'product', 'vi', 'rl-10347-real-keyless-lock', '2018-09-20 09:08:30', '2018-09-20 09:14:04'),
(87, 13, 'product', 'vi', 'rl-10347-real-keyless-lock-1', '2018-09-20 09:21:42', '2018-09-20 09:44:00'),
(88, 14, 'product', 'vi', 'rl-9041-real-keyless-lock', '2018-09-20 09:27:40', '2018-09-25 10:58:42'),
(89, 31, 'article', 'vi', 'banksy-identity', '2018-11-07 07:23:58', '2018-11-07 07:42:04'),
(90, 32, 'article', 'vi', 'nick-cave-and-the-bad-seeds', '2018-11-07 07:24:57', '2018-11-07 07:41:54'),
(91, 11, 'article', 'vi', 'fast-loading-speed', '2018-11-07 08:13:19', '2018-11-07 08:19:19'),
(92, 15, 'product', 'vi', 'product-1', '2018-11-07 08:58:31', '2018-11-07 09:08:25'),
(93, 16, 'product', 'vi', 'product-2', '2018-11-07 08:58:47', '2018-11-07 09:03:23'),
(94, 17, 'product', 'vi', 'product-3', '2018-11-07 08:59:49', '2018-11-07 09:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_account`
--

CREATE TABLE `social_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statistic`
--

CREATE TABLE `statistic` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `statistic`
--

INSERT INTO `statistic` (`id`, `user_agent`, `count`, `created_at`, `updated_at`) VALUES
(1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 2, '2018-08-07 02:49:32', '2018-08-07 02:49:32'),
(2, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 8, '2018-08-09 03:29:36', '2018-08-09 03:29:36'),
(3, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 6, '2018-08-10 02:14:52', '2018-08-10 02:14:52'),
(4, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Mobile Safari/537.36', 10, '2018-08-10 02:27:38', '2018-08-10 02:27:38'),
(5, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 52, '2018-08-10 07:21:52', '2018-08-10 07:21:52'),
(6, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 4, '2018-08-10 08:56:57', '2018-08-10 08:56:57'),
(7, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 47, '2018-08-11 02:23:13', '2018-08-11 02:23:13'),
(8, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 20, '2018-08-11 03:54:15', '2018-08-11 03:54:15'),
(9, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 217, '2018-08-14 02:06:47', '2018-08-14 02:06:47'),
(10, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 1, '2018-08-14 02:22:20', '2018-08-14 02:22:20'),
(11, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 1, '2018-08-14 08:45:35', '2018-08-14 08:45:35'),
(12, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 11, '2018-08-15 03:30:29', '2018-08-15 03:30:29'),
(13, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 107, '2018-08-16 01:49:26', '2018-08-16 01:49:26'),
(14, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 82, '2018-08-16 11:26:24', '2018-08-16 11:26:24'),
(15, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 113, '2018-08-17 01:48:30', '2018-08-17 01:48:30'),
(16, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 20, '2018-08-17 02:13:08', '2018-08-17 02:13:08'),
(17, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 1, '2018-08-17 03:51:16', '2018-08-17 03:51:16'),
(18, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 154, '2018-08-18 01:21:59', '2018-08-18 01:21:59'),
(19, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 26, '2018-08-18 02:12:14', '2018-08-18 02:12:14'),
(20, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 69, '2018-08-18 03:35:09', '2018-08-18 03:35:09'),
(21, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 15, '2018-08-20 01:49:02', '2018-08-20 01:49:02'),
(22, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 13, '2018-08-20 02:32:46', '2018-08-20 02:32:46'),
(23, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 9, '2018-08-20 08:40:40', '2018-08-20 08:40:40'),
(24, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 259, '2018-08-21 02:00:12', '2018-08-21 02:00:12'),
(25, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 7, '2018-08-21 02:59:10', '2018-08-21 02:59:10'),
(26, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 47, '2018-08-21 04:32:29', '2018-08-21 04:32:29'),
(27, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 32, '2018-08-21 07:18:17', '2018-08-21 07:18:17'),
(28, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 44, '2018-08-21 11:41:44', '2018-08-21 11:41:44'),
(29, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/72.4.208 Chrome/66.4.3359.208 Safari/537.36', 2, '2018-08-21 15:14:55', '2018-08-21 15:14:55'),
(30, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 202, '2018-08-22 00:57:58', '2018-08-22 00:57:58'),
(31, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 22, '2018-08-22 02:15:59', '2018-08-22 02:15:59'),
(32, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 23, '2018-08-22 04:30:45', '2018-08-22 04:30:45'),
(33, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 32, '2018-08-22 08:42:12', '2018-08-22 08:42:12'),
(34, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 30, '2018-08-23 01:09:00', '2018-08-23 01:09:00'),
(35, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 12, '2018-08-23 01:29:31', '2018-08-23 01:29:31'),
(36, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 24, '2018-08-25 04:39:05', '2018-08-25 04:39:05'),
(37, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 3, '2018-08-25 05:00:47', '2018-08-25 05:00:47'),
(38, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 34, '2018-08-25 14:58:36', '2018-08-25 14:58:36'),
(39, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 99, '2018-08-26 01:02:50', '2018-08-26 01:02:50'),
(40, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 140, '2018-08-26 17:09:32', '2018-08-26 17:09:32'),
(41, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 7, '2018-08-27 02:13:30', '2018-08-27 02:13:30'),
(42, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 84, '2018-08-27 02:25:28', '2018-08-27 02:25:28'),
(43, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 5, '2018-08-27 03:29:04', '2018-08-27 03:29:04'),
(44, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/72.4.208 Chrome/66.4.3359.208 Safari/537.36', 11, '2018-08-27 04:46:42', '2018-08-27 04:46:42'),
(45, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 10, '2018-08-27 07:43:45', '2018-08-27 07:43:45'),
(46, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Mobile Safari/537.36', 2, '2018-08-27 10:02:17', '2018-08-27 10:02:17'),
(47, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36 Zalo/1.0', 2, '2018-08-27 12:05:40', '2018-08-27 12:05:40'),
(48, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 170, '2018-08-27 17:35:41', '2018-08-27 17:35:41'),
(49, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 15, '2018-08-28 04:49:06', '2018-08-28 04:49:06'),
(50, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-08-28 08:32:58', '2018-08-28 08:32:58'),
(51, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/72.4.208 Chrome/66.4.3359.208 Safari/537.36', 2, '2018-08-28 10:39:48', '2018-08-28 10:39:48'),
(52, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 19, '2018-08-28 19:30:13', '2018-08-28 19:30:13'),
(53, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 4, '2018-08-29 01:21:39', '2018-08-29 01:21:39'),
(54, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 73, '2018-08-29 17:42:50', '2018-08-29 17:42:50'),
(55, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 66, '2018-08-30 06:34:03', '2018-08-30 06:34:03'),
(56, 'Mozilla/5.0 (Linux; U; Android 4.4.4; en-gb; SM-G316HU Build/KTU84P) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30', 9, '2018-08-30 17:40:24', '2018-08-30 17:40:24'),
(57, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 5, '2018-08-31 01:30:26', '2018-08-31 01:30:26'),
(58, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 29, '2018-08-31 06:58:10', '2018-08-31 06:58:10'),
(59, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 57, '2018-08-31 20:58:08', '2018-08-31 20:58:08'),
(60, 'Mozilla/5.0 (Linux; U; Android 4.4.4; en-gb; SM-G316HU Build/KTU84P) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30', 1, '2018-09-02 01:12:29', '2018-09-02 01:12:29'),
(61, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 66, '2018-09-02 04:36:03', '2018-09-02 04:36:03'),
(62, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 2, '2018-09-02 18:06:56', '2018-09-02 18:06:56'),
(63, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 28, '2018-09-04 00:14:21', '2018-09-04 00:14:21'),
(64, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 20, '2018-09-04 01:03:24', '2018-09-04 01:03:24'),
(65, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 81, '2018-09-04 03:20:52', '2018-09-04 03:20:52'),
(66, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/72.4.208 Chrome/66.4.3359.208 Safari/537.36', 4, '2018-09-04 08:25:42', '2018-09-04 08:25:42'),
(67, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 3, '2018-09-04 18:49:16', '2018-09-04 18:49:16'),
(68, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 12, '2018-09-05 07:06:27', '2018-09-05 07:06:27'),
(69, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 26, '2018-09-05 07:31:36', '2018-09-05 07:31:36'),
(70, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 8, '2018-09-05 07:35:36', '2018-09-05 07:35:36'),
(71, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 69, '2018-09-06 02:21:41', '2018-09-06 02:21:41'),
(72, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15', 6, '2018-09-06 07:57:49', '2018-09-06 07:57:49'),
(73, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36 Zalo/1.0', 4, '2018-09-06 07:59:36', '2018-09-06 07:59:36'),
(74, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 7, '2018-09-06 08:05:56', '2018-09-06 08:05:56'),
(75, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 68, '2018-09-06 09:32:34', '2018-09-06 09:32:34'),
(76, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 46, '2018-09-07 01:23:44', '2018-09-07 01:23:44'),
(77, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 164, '2018-09-07 01:56:18', '2018-09-07 01:56:18'),
(78, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 60, '2018-09-07 02:29:59', '2018-09-07 02:29:59'),
(79, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; GTB7.5; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)', 1, '2018-09-07 03:49:07', '2018-09-07 03:49:07'),
(80, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 35, '2018-09-07 10:12:57', '2018-09-07 10:12:57'),
(81, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-09-08 02:41:20', '2018-09-08 02:41:20'),
(82, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/72.4.208 Chrome/66.4.3359.208 Safari/537.36', 3, '2018-09-08 02:45:14', '2018-09-08 02:45:14'),
(83, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-09-09 01:15:46', '2018-09-09 01:15:46'),
(84, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 23, '2018-09-10 02:03:27', '2018-09-10 02:03:27'),
(85, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-09-10 06:18:39', '2018-09-10 06:18:39'),
(86, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 2, '2018-09-10 10:30:29', '2018-09-10 10:30:29'),
(87, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 13, '2018-09-11 02:42:29', '2018-09-11 02:42:29'),
(88, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 99, '2018-09-11 03:10:46', '2018-09-11 03:10:46'),
(89, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Mobile Safari/537.36', 10, '2018-09-11 03:49:55', '2018-09-11 03:49:55'),
(90, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 2, '2018-09-11 04:32:31', '2018-09-11 04:32:31'),
(91, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 1, '2018-09-11 10:19:04', '2018-09-11 10:19:04'),
(92, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 71, '2018-09-12 02:02:14', '2018-09-12 02:02:14'),
(93, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 4, '2018-09-12 02:26:08', '2018-09-12 02:26:08'),
(94, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Mobile Safari/537.36', 3, '2018-09-12 03:12:48', '2018-09-12 03:12:48'),
(95, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-09-13 01:42:57', '2018-09-13 01:42:57'),
(96, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36 Zalo/1.0', 1, '2018-09-13 04:44:33', '2018-09-13 04:44:33'),
(97, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-09-13 17:18:46', '2018-09-13 17:18:46'),
(98, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 1, '2018-09-14 02:28:26', '2018-09-14 02:28:26'),
(99, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 27, '2018-09-14 08:00:36', '2018-09-14 08:00:36'),
(100, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 1, '2018-09-14 09:24:14', '2018-09-14 09:24:14'),
(101, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 15, '2018-09-14 09:52:33', '2018-09-14 09:52:33'),
(102, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 102, '2018-09-14 20:23:15', '2018-09-14 20:23:15'),
(103, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/42.0.2311.138 Mobile Safari/537.36 Zalo/1.0', 2, '2018-09-14 22:50:20', '2018-09-14 22:50:20'),
(104, 'Mozilla/5.0 (Linux; Android 5.1.1; F1w Build/LMY47V; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.158 Mobile Safari/537.36 Zalo/1.0', 3, '2018-09-14 23:05:17', '2018-09-14 23:05:17'),
(105, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 17, '2018-09-15 07:14:30', '2018-09-15 07:14:30'),
(106, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 24, '2018-09-17 04:24:58', '2018-09-17 04:24:58'),
(107, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 12, '2018-09-17 08:06:47', '2018-09-17 08:06:47'),
(108, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-09-17 10:21:33', '2018-09-17 10:21:33'),
(109, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, '2018-09-17 23:54:12', '2018-09-17 23:54:12'),
(110, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/72.4.208 Chrome/66.4.3359.208 Safari/537.36', 1, '2018-09-18 14:11:29', '2018-09-18 14:11:29'),
(111, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 69, '2018-09-20 03:10:19', '2018-09-20 03:10:19'),
(112, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 18, '2018-09-20 04:39:49', '2018-09-20 04:39:49'),
(113, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 4, '2018-09-20 04:58:30', '2018-09-20 04:58:30'),
(114, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', 1, '2018-09-20 06:04:27', '2018-09-20 06:04:27'),
(115, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-09-20 07:47:00', '2018-09-20 07:47:00'),
(116, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-09-21 06:22:49', '2018-09-21 06:22:49'),
(117, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 6, '2018-09-21 06:58:39', '2018-09-21 06:58:39'),
(118, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 9, '2018-09-21 09:19:40', '2018-09-21 09:19:40'),
(119, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 2, '2018-09-22 15:46:27', '2018-09-22 15:46:27'),
(120, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/42.0.2311.138 Mobile Safari/537.36 Zalo/1.0', 8, '2018-09-22 16:10:14', '2018-09-22 16:10:14'),
(121, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/42.0.2311.138 Mobile Safari/537.36 Zalo/1.0', 13, '2018-09-22 17:22:20', '2018-09-22 17:22:20'),
(122, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 10, '2018-09-23 19:18:04', '2018-09-23 19:18:04'),
(123, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 71, '2018-09-24 02:06:49', '2018-09-24 02:06:49'),
(124, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 4, '2018-09-24 02:24:09', '2018-09-24 02:24:09'),
(125, 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15G77', 1, '2018-09-24 04:20:02', '2018-09-24 04:20:02'),
(126, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36', 3, '2018-09-24 09:44:29', '2018-09-24 09:44:29'),
(127, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 85, '2018-09-25 04:48:05', '2018-09-25 04:48:05'),
(128, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 4, '2018-09-25 07:39:21', '2018-09-25 07:39:21'),
(129, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 38, '2018-09-26 01:00:58', '2018-09-26 01:00:58'),
(130, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 6, '2018-09-26 12:10:18', '2018-09-26 12:10:18'),
(131, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 3, '2018-09-26 20:05:22', '2018-09-26 20:05:22'),
(132, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 11, '2018-09-27 09:38:58', '2018-09-27 09:38:58'),
(133, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 7, '2018-09-27 10:22:02', '2018-09-27 10:22:02'),
(134, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 6, '2018-09-28 00:56:04', '2018-09-28 00:56:04'),
(135, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 25, '2018-09-28 01:05:29', '2018-09-28 01:05:29'),
(136, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 27, '2018-09-28 02:16:57', '2018-09-28 02:16:57'),
(137, 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', 5, '2018-09-28 07:14:09', '2018-09-28 07:14:09'),
(138, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-02 01:53:21', '2018-10-02 01:53:21'),
(139, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 3, '2018-10-02 10:28:38', '2018-10-02 10:28:38'),
(140, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 15, '2018-10-03 05:22:57', '2018-10-03 05:22:57'),
(141, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-03 06:11:07', '2018-10-03 06:11:07'),
(142, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 31, '2018-10-04 01:27:24', '2018-10-04 01:27:24'),
(143, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/42.0.2311.138 Mobile Safari/537.36 Zalo/1.0', 6, '2018-10-04 01:27:59', '2018-10-04 01:27:59'),
(144, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 40, '2018-10-04 02:18:45', '2018-10-04 02:18:45'),
(145, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 29, '2018-10-04 04:56:23', '2018-10-04 04:56:23'),
(146, 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', 2, '2018-10-04 04:59:36', '2018-10-04 04:59:36'),
(147, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/72.4.208 Chrome/66.4.3359.208 Safari/537.36', 3, '2018-10-04 07:14:34', '2018-10-04 07:14:34'),
(148, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 260, '2018-10-05 01:10:11', '2018-10-05 01:10:11'),
(149, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 45, '2018-10-05 02:35:41', '2018-10-05 02:35:41'),
(150, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 56, '2018-10-05 03:18:30', '2018-10-05 03:18:30'),
(151, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 34, '2018-10-05 03:29:47', '2018-10-05 03:29:47'),
(152, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 OPR/55.0.2994.61', 2, '2018-10-05 04:21:01', '2018-10-05 04:21:01'),
(153, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 5, '2018-10-05 07:36:42', '2018-10-05 07:36:42'),
(154, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 4, '2018-10-05 12:35:59', '2018-10-05 12:35:59'),
(155, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 24, '2018-10-06 21:11:24', '2018-10-06 21:11:24'),
(156, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 14, '2018-10-08 04:44:38', '2018-10-08 04:44:38'),
(157, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 1, '2018-10-09 04:44:48', '2018-10-09 04:44:48'),
(158, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 3, '2018-10-12 04:08:25', '2018-10-12 04:08:25'),
(159, 'Mozilla/5.0 (Windows NT 6.1; rv:62.0) Gecko/20100101 Firefox/62.0', 12, '2018-10-12 04:08:46', '2018-10-12 04:08:46'),
(160, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-13 04:48:56', '2018-10-13 04:48:56'),
(161, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 2, '2018-10-16 06:33:24', '2018-10-16 06:33:24'),
(162, 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', 5, '2018-10-16 06:34:57', '2018-10-16 06:34:57'),
(163, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-16 06:36:14', '2018-10-16 06:36:14'),
(164, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 5, '2018-10-16 06:36:20', '2018-10-16 06:36:20'),
(165, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 12, '2018-10-19 02:48:15', '2018-10-19 02:48:15'),
(166, 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36', 1, '2018-10-19 10:41:37', '2018-10-19 10:41:37'),
(167, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-25 08:49:21', '2018-10-25 08:49:21'),
(168, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-25 08:54:05', '2018-10-25 08:54:05'),
(169, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-26 08:57:17', '2018-10-26 08:57:17'),
(170, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 2, '2018-10-27 09:41:40', '2018-10-27 09:41:40'),
(171, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 8, '2018-10-27 18:22:09', '2018-10-27 18:22:09'),
(172, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/42.0.2311.138 Mobile Safari/537.36 Zalo/1.0', 3, '2018-10-29 04:46:59', '2018-10-29 04:46:59'),
(173, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-29 07:28:20', '2018-10-29 07:28:20'),
(174, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-29 10:41:55', '2018-10-29 10:41:55'),
(175, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1, '2018-10-29 13:34:33', '2018-10-29 13:34:33'),
(176, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, '2018-10-30 09:24:36', '2018-10-30 09:24:36'),
(177, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 3, '2018-10-31 17:09:32', '2018-10-31 17:09:32'),
(178, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 3, '2018-11-01 03:46:45', '2018-11-01 03:46:45'),
(179, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 2, '2018-11-01 03:59:20', '2018-11-01 03:59:20'),
(180, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 2, '2018-11-02 07:08:46', '2018-11-02 07:08:46'),
(181, 'Mozilla/5.0 (Linux; Android 5.0; E2312 Build/26.1.B.2.147) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Mobile Safari/537.36', 1, '2018-11-02 07:15:30', '2018-11-02 07:15:30'),
(182, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1, '2018-11-03 02:27:00', '2018-11-03 02:27:00'),
(183, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 1, '2018-11-03 04:00:05', '2018-11-03 04:00:05'),
(184, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 5, '2018-11-03 15:08:31', '2018-11-03 15:08:31'),
(185, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 6, '2018-11-04 19:47:37', '2018-11-04 19:47:37'),
(186, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1, '2018-11-05 09:17:38', '2018-11-05 09:17:38'),
(187, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1, '2018-11-05 10:32:50', '2018-11-05 10:32:50'),
(188, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0', 6, '2018-11-06 02:33:42', '2018-11-06 02:33:42'),
(189, 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko; Google Page Speed Insights) Chrome/41.0.2272.118 Mobile Safari/537.36', 14, '2018-11-06 02:33:56', '2018-11-06 02:33:56'),
(190, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Page Speed Insights) Chrome/41.0.2272.118 Safari/537.36', 12, '2018-11-06 02:33:56', '2018-11-06 02:33:56'),
(191, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36(KHTML, like Gecko) Chrome/69.0.3464.0 Safari/537.36 Chrome-Lighthouse', 8, '2018-11-06 02:34:05', '2018-11-06 02:34:05'),
(192, 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5 Build/MRA58N) AppleWebKit/537.36(KHTML, like Gecko) Chrome/69.0.3464.0 Mobile Safari/537.36 Chrome-Lighthouse', 10, '2018-11-06 02:34:05', '2018-11-06 02:34:05'),
(193, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 21, '2018-11-06 02:54:03', '2018-11-06 02:54:03'),
(194, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 15, '2018-11-06 04:02:44', '2018-11-06 04:02:44'),
(195, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 106, '2018-11-07 02:12:45', '2018-11-07 02:12:45'),
(196, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 231, '2018-11-07 02:14:08', '2018-11-07 02:14:08'),
(197, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Page Speed Insights) Chrome/41.0.2272.118 Safari/537.36', 15, '2018-11-07 02:28:17', '2018-11-07 02:28:17'),
(198, 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko; Google Page Speed Insights) Chrome/41.0.2272.118 Mobile Safari/537.36', 15, '2018-11-07 02:28:17', '2018-11-07 02:28:17'),
(199, 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5 Build/MRA58N) AppleWebKit/537.36(KHTML, like Gecko) Chrome/69.0.3464.0 Mobile Safari/537.36 Chrome-Lighthouse', 8, '2018-11-07 02:28:23', '2018-11-07 02:28:23'),
(200, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36(KHTML, like Gecko) Chrome/69.0.3464.0 Safari/537.36 Chrome-Lighthouse', 8, '2018-11-07 02:28:25', '2018-11-07 02:28:25'),
(201, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0', 17, '2018-11-07 04:00:49', '2018-11-07 04:00:49'),
(202, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 6, '2018-11-07 07:30:52', '2018-11-07 07:30:52'),
(203, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 119, '2018-11-08 01:59:15', '2018-11-08 01:59:15'),
(204, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Page Speed Insights) Chrome/41.0.2272.118 Safari/537.36', 3, '2018-11-08 02:29:42', '2018-11-08 02:29:42'),
(205, 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko; Google Page Speed Insights) Chrome/41.0.2272.118 Mobile Safari/537.36', 5, '2018-11-08 02:29:42', '2018-11-08 02:29:42'),
(206, 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5 Build/MRA58N) AppleWebKit/537.36(KHTML, like Gecko) Chrome/69.0.3464.0 Mobile Safari/537.36 Chrome-Lighthouse', 3, '2018-11-08 02:29:47', '2018-11-08 02:29:47'),
(207, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36(KHTML, like Gecko) Chrome/69.0.3464.0 Safari/537.36 Chrome-Lighthouse', 3, '2018-11-08 02:29:49', '2018-11-08 02:29:49'),
(208, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 10, '2018-11-08 02:30:17', '2018-11-08 02:30:17'),
(209, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 2, '2018-11-08 07:00:01', '2018-11-08 07:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `subregion`
--

CREATE TABLE `subregion` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subregion`
--

INSERT INTO `subregion` (`id`, `name`, `region_id`) VALUES
(1, 'Quận Hoàn Kiếm', '123'),
(2, 'Quận Tây Hồ', '123'),
(3, 'Quận Long Biên', '123'),
(4, 'Quận Cầu Giấy', '123'),
(5, 'Quận Đống Đa', '123'),
(6, 'Quận Hai Bà Trưng', '123'),
(7, 'Quận Hoàng Mai', '123'),
(8, 'Quận Thanh Xuân', '123'),
(9, 'Huyện Sóc Sơn', '123'),
(10, 'Huyện Đông Anh', '123'),
(11, 'Huyện Gia Lâm', '123'),
(12, 'Quận Nam Từ Liêm', '123'),
(13, 'Huyện Thanh Trì', '123'),
(14, 'Thị xã Hà Giang', '124'),
(15, 'Huyện Đồng Văn', '124'),
(16, 'Huyện Mèo Vạc', '124'),
(17, 'Huyện Yên Minh', '124'),
(18, 'Huyện Quản Bạ', '124'),
(19, 'Huyện Vị Xuyên', '124'),
(20, 'Huyện Bắc Mê', '124'),
(21, 'Huyện Hoàng Su Phì', '124'),
(22, 'Huyện Xín Mần', '124'),
(23, 'Huyện Bắc Quang', '124'),
(24, 'Huyện Quang Bình', '124'),
(25, 'Thành phố Cao Bằng', '125'),
(26, 'Huyện Bảo Lâm', '125'),
(27, 'Huyện Bảo Lạc', '125'),
(28, 'Huyện Thông Nông', '125'),
(29, 'Huyện Hà Quảng', '125'),
(30, 'Huyện Trà Lĩnh', '125'),
(31, 'Huyện Trùng Khánh', '125'),
(32, 'Huyện Hạ Lang', '125'),
(33, 'Huyện Quảng Uyên', '125'),
(34, 'Huyện Phục Hoà', '125'),
(35, 'Huyện Hoà An', '125'),
(36, 'Huyện Nguyên Bình', '125'),
(37, 'Huyện Thạch An', '125'),
(38, 'Thành phố Bắc Kạn', '126'),
(39, 'Huyện Pác Nặm', '126'),
(40, 'Huyện Ba Bể', '126'),
(41, 'Huyện Ngân Sơn', '126'),
(42, 'Huyện Bạch Thông', '126'),
(43, 'Huyện Chợ Đồn', '126'),
(44, 'Huyện Chợ Mới', '126'),
(45, 'Huyện Na Rì', '126'),
(46, 'Thành phố Tuyên Quang', '127'),
(47, 'Huyện Na Hang', '127'),
(48, 'Huyện Chiêm Hóa', '127'),
(49, 'Huyện Hàm Yên', '127'),
(50, 'Huyện Yên Sơn', '127'),
(51, 'Huyện Sơn Dương', '127'),
(52, 'Thành phố Lào Cai', '128'),
(53, 'Huyện Bát Xát', '128'),
(54, 'Huyện Mường Khương', '128'),
(55, 'Huyện Si Ma Cai', '128'),
(56, 'Huyện Bắc Hà', '128'),
(57, 'Huyện Bảo Thắng', '128'),
(58, 'Huyện Bảo Yên', '128'),
(59, 'Huyện Sa Pa', '128'),
(60, 'Huyện Văn Bàn', '128'),
(61, 'Thành phố Điện Biên Phủ', '129'),
(62, 'Thị xã Mường Lay', '129'),
(63, 'Huyện Mường Nhé', '129'),
(64, 'Huyện Mường Chà', '129'),
(65, 'Huyện Tủa Chùa', '129'),
(66, 'Huyện Tuần Giáo', '129'),
(67, 'Huyện Điện Biên', '129'),
(68, 'Huyện Điện Biên Đông', '129'),
(69, 'Huyện Tam Đường', '130'),
(70, 'Huyện Mường Tè', '130'),
(71, 'Huyện Sìn Hồ', '130'),
(72, 'Huyện Phong Thổ', '130'),
(73, 'Huyện Than Uyên', '130'),
(74, 'Thị xã Lai châu', '130'),
(75, 'Thành phố Sơn La', '131'),
(76, 'Huyện Quỳnh Nhai', '131'),
(77, 'Huyện Thuận Châu', '131'),
(78, 'Huyện Mường La', '131'),
(79, 'Huyện Bắc Yên', '131'),
(80, 'Huyện Phù Yên', '131'),
(81, 'Huyện Mộc Châu', '131'),
(82, 'Huyện Yên Châu', '131'),
(83, 'Huyện Mai Sơn', '131'),
(84, 'Huyện Sông Mã', '131'),
(85, 'Huyện Sốp Cộp', '131'),
(86, 'Thành phố Yên Bái', '132'),
(87, 'Thị xã Nghĩa Lộ', '132'),
(88, 'Huyện Lục Yên', '132'),
(89, 'Huyện Văn Yên', '132'),
(90, 'Huyện Mù Căng Chải', '132'),
(91, 'Huyện Trấn Yên', '132'),
(92, 'Huyện Trạm Tấu', '132'),
(93, 'Huyện Văn Chấn', '132'),
(94, 'Huyện Yên Bình', '132'),
(95, 'Thị xã Hòa Bình', '133'),
(96, 'Huyện Đà Bắc', '133'),
(97, 'Huyện Kỳ Sơn', '133'),
(98, 'Huyện Lương Sơn', '133'),
(99, 'Huyện Kim Bôi', '133'),
(100, 'Huyện Cao Phong', '133'),
(101, 'Huyện Tân Lạc', '133'),
(102, 'Huyện Mai Châu', '133'),
(103, 'Huyện Lạc Sơn', '133'),
(104, 'Huyện Yên Thủy', '133'),
(105, 'Huyện Lạc Thủy', '133'),
(106, 'Thành phố Thái Nguyên', '134'),
(107, 'Thành phố Sông Công', '134'),
(108, 'Huyện Định Hóa', '134'),
(109, 'Huyện Phú Lương', '134'),
(110, 'Huyện Đồng Hỷ', '134'),
(111, 'Huyện Võ Nhai', '134'),
(112, 'Huyện Đại Từ', '134'),
(113, 'Thị xã Phổ Yên', '134'),
(114, 'Huyện Phú Bình', '134'),
(115, 'Thành phố Lạng Sơn', '135'),
(116, 'Huyện Tràng Định', '135'),
(117, 'Huyện Bình Gia', '135'),
(118, 'Huyện Văn Lãng', '135'),
(119, 'Huyện Cao Lộc', '135'),
(120, 'Huyện Văn Quan', '135'),
(121, 'Huyện Bắc Sơn', '135'),
(122, 'Huyện Hữu Lũng', '135'),
(123, 'Huyện Chi Lăng', '135'),
(124, 'Huyện Lộc Bình', '135'),
(125, 'Huyện Đình Lập', '135'),
(126, 'Thành phố Hạ Long', '136'),
(127, 'Thành phố Móng Cái', '136'),
(128, 'Thành phố Cẩm Phả', '136'),
(129, 'Thành phố Uông Bí', '136'),
(130, 'Huyện Bình Liêu', '136'),
(131, 'Huyện Tiên Yên', '136'),
(132, 'Huyện Đầm Hà', '136'),
(133, 'Huyện Hải Hà', '136'),
(134, 'Huyện Ba Chẽ', '136'),
(135, 'Huyện Vân Đồn', '136'),
(136, 'Huyện Hoành Bồ', '136'),
(137, 'Thị xã Đông Triều', '136'),
(138, 'Thị xã Quảng Yên', '136'),
(139, 'Huyện Cô Tô', '136'),
(140, 'Thành phố Bắc Giang', '137'),
(141, 'Huyện Yên Thế', '137'),
(142, 'Huyện Tân Yên', '137'),
(143, 'Huyện Lạng Giang', '137'),
(144, 'Huyện Lục Nam', '137'),
(145, 'Huyện Lục Ngạn', '137'),
(146, 'Huyện Sơn Động', '137'),
(147, 'Huyện Yên Dũng', '137'),
(148, 'Huyện Việt Yên', '137'),
(149, 'Huyện Hiệp Hòa', '137'),
(150, 'Thành phố Việt Trì', '138'),
(151, 'Thị xã Phú Thọ', '138'),
(152, 'Huyện Đoan Hùng', '138'),
(153, 'Huyện Hạ Hoà', '138'),
(154, 'Huyện Thanh Ba', '138'),
(155, 'Huyện Phù Ninh', '138'),
(156, 'Huyện Yên Lập', '138'),
(157, 'Huyện Cẩm Khê', '138'),
(158, 'Huyện Tam Nông', '138'),
(159, 'Huyện Lâm Thao', '138'),
(160, 'Huyện Thanh Sơn', '138'),
(161, 'Huyện Thanh Thuỷ', '138'),
(162, 'Thành phố Vĩnh Yên', '139'),
(163, 'Thị xã Phúc Yên', '139'),
(164, 'Huyện Lập Thạch', '139'),
(165, 'Huyện Tam Dương', '139'),
(166, 'Huyện Tam Đảo', '139'),
(167, 'Huyện Bình Xuyên', '139'),
(168, 'Huyện Mê Linh', '123'),
(169, 'Huyện Yên Lạc', '139'),
(170, 'Huyện Vĩnh Tường', '139'),
(171, 'Thành phố Bắc Ninh', '140'),
(172, 'Huyện Yên Phong', '140'),
(173, 'Huyện Quế Võ', '140'),
(174, 'Huyện Tiên Du', '140'),
(175, 'Thị xã Từ Sơn', '140'),
(176, 'Huyện Thuận Thành', '140'),
(177, 'Huyện Gia Bình', '140'),
(178, 'Huyện Lương Tài', '140'),
(179, 'Quận Hà Đông', '123'),
(180, 'Thị xã Sơn Tây', '123'),
(181, 'Huyện Ba Vì', '123'),
(182, 'Huyện Phúc Thọ', '123'),
(183, 'Huyện Đan Phượng', '123'),
(184, 'Huyện Hoài Đức', '123'),
(185, 'Huyện Quốc Oai', '123'),
(186, 'Huyện Thạch Thất', '123'),
(187, 'Huyện Chương Mỹ', '123'),
(188, 'Huyện Thanh Oai', '123'),
(189, 'Huyện Thường Tín', '123'),
(190, 'Huyện Phú Xuyên', '123'),
(191, 'Huyện ứng Hòa', '123'),
(192, 'Huyện Mỹ Đức', '123'),
(193, 'Thành phố Hải Dương', '141'),
(194, 'Thị xã Chí Linh', '141'),
(195, 'Huyện Nam Sách', '141'),
(196, 'Huyện Kinh Môn', '141'),
(197, 'Huyện Kim Thành', '141'),
(198, 'Huyện Thanh Hà', '141'),
(199, 'Huyện Cẩm Giàng', '141'),
(200, 'Huyện Bình Giang', '141'),
(201, 'Huyện Gia Lộc', '141'),
(202, 'Huyện Tứ Kỳ', '141'),
(203, 'Huyện Ninh Giang', '141'),
(204, 'Huyện Thanh Miện', '141'),
(205, 'Quận Hồng Bàng', '142'),
(206, 'Quận Ngô Quyền', '142'),
(207, 'Quận Lê Chân', '142'),
(208, 'Quận Hải An', '142'),
(209, 'Quận Kiến An', '142'),
(210, 'Quận Đồ Sơn', '142'),
(211, 'Huyện Thuỷ Nguyên', '142'),
(212, 'Huyện An Dương', '142'),
(213, 'Huyện An Lão', '142'),
(214, 'Huyện Kiến Thuỵ', '142'),
(215, 'Huyện Tiên Lãng', '142'),
(216, 'Huyện Vĩnh Bảo', '142'),
(217, 'Huyện Cát Hải', '142'),
(218, 'Huyện Bạch Long Vĩ', '142'),
(219, 'Thành phố Hưng Yên', '143'),
(220, 'Huyện Văn Lâm', '143'),
(221, 'Huyện Văn Giang', '143'),
(222, 'Huyện Yên Mỹ', '143'),
(223, 'Huyện Mỹ Hào', '143'),
(224, 'Huyện Ân Thi', '143'),
(225, 'Huyện Khoái Châu', '143'),
(226, 'Huyện Kim Động', '143'),
(227, 'Huyện Tiên Lữ', '143'),
(228, 'Huyện Phù Cừ', '143'),
(229, 'Thành phố Thái Bình', '144'),
(230, 'Huyện Quỳnh Phụ', '144'),
(231, 'Huyện Hưng Hà', '144'),
(232, 'Huyện Đông Hưng', '144'),
(233, 'Huyện Thái Thụy', '144'),
(234, 'Huyện Tiền Hải', '144'),
(235, 'Huyện Kiến Xương', '144'),
(236, 'Huyện Vũ Thư', '144'),
(237, 'Thành phố Phủ Lý', '145'),
(238, 'Huyện Duy Tiên', '145'),
(239, 'Huyện Kim Bảng', '145'),
(240, 'Huyện Thanh Liêm', '145'),
(241, 'Huyện Bình Lục', '145'),
(242, 'Huyện Lý Nhân', '145'),
(243, 'Thành phố Nam Định', '146'),
(244, 'Huyện Mỹ Lộc', '146'),
(245, 'Huyện Vụ Bản', '146'),
(246, 'Huyện ý Yên', '146'),
(247, 'Huyện Nghĩa Hưng', '146'),
(248, 'Huyện Nam Trực', '146'),
(249, 'Huyện Trực Ninh', '146'),
(250, 'Huyện Xuân Trường', '146'),
(251, 'Huyện Giao Thủy', '146'),
(252, 'Huyện Hải Hậu', '146'),
(253, 'Thành phố Ninh Bình', '147'),
(254, 'Thành phố Tam Điệp', '147'),
(255, 'Huyện Nho Quan', '147'),
(256, 'Huyện Gia Viễn', '147'),
(257, 'Huyện Hoa Lư', '147'),
(258, 'Huyện Yên Khánh', '147'),
(259, 'Huyện Kim Sơn', '147'),
(260, 'Huyện Yên Mô', '147'),
(261, 'Thành phố Thanh Hóa', '148'),
(262, 'Thị xã Bỉm Sơn', '148'),
(263, 'Thị xã Sầm Sơn', '148'),
(264, 'Huyện Mường Lát', '148'),
(265, 'Huyện Quan Hóa', '148'),
(266, 'Huyện Bá Thước', '148'),
(267, 'Huyện Quan Sơn', '148'),
(268, 'Huyện Lang Chánh', '148'),
(269, 'Huyện Ngọc Lặc', '148'),
(270, 'Huyện Cẩm Thủy', '148'),
(271, 'Huyện Thạch Thành', '148'),
(272, 'Huyện Hà Trung', '148'),
(273, 'Huyện Vĩnh Lộc', '148'),
(274, 'Huyện Yên Định', '148'),
(275, 'Huyện Thọ Xuân', '148'),
(276, 'Huyện Thường Xuân', '148'),
(277, 'Huyện Triệu Sơn', '148'),
(278, 'Huyện Thiệu Hoá', '148'),
(279, 'Huyện Hoằng Hóa', '148'),
(280, 'Huyện Hậu Lộc', '148'),
(281, 'Huyện Nga Sơn', '148'),
(282, 'Huyện Như Xuân', '148'),
(283, 'Huyện Như Thanh', '148'),
(284, 'Huyện Nông Cống', '148'),
(285, 'Huyện Đông Sơn', '148'),
(286, 'Huyện Quảng Xương', '148'),
(287, 'Huyện Tĩnh Gia', '148'),
(288, 'Thành phố Vinh', '149'),
(289, 'Thị xã Cửa Lò', '149'),
(290, 'Huyện Quế Phong', '149'),
(291, 'Huyện Quỳ Châu', '149'),
(292, 'Huyện Tương Dương', '149'),
(293, 'Huyện Nghĩa Đàn', '149'),
(294, 'Huyện Quỳ Hợp', '149'),
(295, 'Huyện Quỳnh Lưu', '149'),
(296, 'Huyện Con Cuông', '149'),
(297, 'Huyện Tân Kỳ', '149'),
(298, 'Huyện Anh Sơn', '149'),
(299, 'Huyện Diễn Châu', '149'),
(300, 'Huyện Yên Thành', '149'),
(301, 'Huyện Đô Lương', '149'),
(302, 'Huyện Thanh Chương', '149'),
(303, 'Huyện Nghi Lộc', '149'),
(304, 'Huyện Nam Đàn', '149'),
(305, 'Huyện Hưng Nguyên', '149'),
(306, 'Thành phố Hà Tĩnh', '150'),
(307, 'Thị xã Hồng Lĩnh', '150'),
(308, 'Huyện Hương Sơn', '150'),
(309, 'Huyện Đức Thọ', '150'),
(310, 'Huyện Vũ Quang', '150'),
(311, 'Huyện Nghi Xuân', '150'),
(312, 'Huyện Can Lộc', '150'),
(313, 'Huyện Hương Khê', '150'),
(314, 'Huyện Thạch Hà', '150'),
(315, 'Huyện Cẩm Xuyên', '150'),
(316, 'Huyện Kỳ Anh', '150'),
(317, 'Thành phố Đồng Hới', '151'),
(318, 'Huyện Minh Hóa', '151'),
(319, 'Huyện Tuyên Hóa', '151'),
(320, 'Huyện Quảng Trạch', '151'),
(321, 'Huyện Bố Trạch', '151'),
(322, 'Huyện Quảng Ninh', '151'),
(323, 'Huyện Lệ Thủy', '151'),
(324, 'Thị xã Đông Hà', '152'),
(325, 'Thị xã Quảng Trị', '152'),
(326, 'Huyện Vĩnh Linh', '152'),
(327, 'Huyện Hướng Hóa', '152'),
(328, 'Huyện Gio Linh', '152'),
(329, 'Huyện Đa Krông', '152'),
(330, 'Huyện Cam Lộ', '152'),
(331, 'Huyện Triệu Phong', '152'),
(332, 'Huyện Hải Lăng', '152'),
(333, 'Huyện Đảo Cồn Cỏ', '152'),
(334, 'Thành phố Huế', '153'),
(335, 'Huyện Phong Điền', '153'),
(336, 'Huyện Quảng Điền', '153'),
(337, 'Huyện Phú Vang', '153'),
(338, 'Thị xã Hương Thủy', '153'),
(339, 'Huyện Hương Trà', '153'),
(340, 'Huyện A Lưới', '153'),
(341, 'Huyện Phú Lộc', '153'),
(342, 'Huyện Nam Đông', '153'),
(343, 'Quận Liên Chiểu', '154'),
(344, 'Quận Thanh Khê', '154'),
(345, 'Quận Hải Châu', '154'),
(346, 'Quận Sơn Trà', '154'),
(347, 'Quận Ngũ Hành Sơn', '154'),
(348, 'Huyện Hoà Vang', '154'),
(349, 'Huyện Hoàng Sa', '154'),
(350, 'Quận Cẩm Lệ', '154'),
(351, 'Thành phố Tam Kỳ', '155'),
(352, 'Thành phố Hội An', '155'),
(353, 'Huyện Tây Giang', '155'),
(354, 'Huyện Đông Giang', '155'),
(355, 'Huyện Đại Lộc', '155'),
(356, 'Thị xã Điện Bàn', '155'),
(357, 'Huyện Duy Xuyên', '155'),
(358, 'Huyện Quế Sơn', '155'),
(359, 'Huyện Nam Giang', '155'),
(360, 'Huyện Phước Sơn', '155'),
(361, 'Huyện Hiệp Đức', '155'),
(362, 'Huyện Thăng Bình', '155'),
(363, 'Huyện Tiên Phước', '155'),
(364, 'Huyện Bắc Trà My', '155'),
(365, 'Huyện Nam Trà My', '155'),
(366, 'Huyện Núi Thành', '155'),
(367, 'Huyện Phú Ninh', '155'),
(368, 'Thành phố Quảng Ngãi', '156'),
(369, 'Huyện Bình Sơn', '156'),
(370, 'Huyện Trà Bồng', '156'),
(371, 'Huyên Tây Trà', '156'),
(372, 'Huyện Sơn Tịnh', '156'),
(373, 'Huyện Tư Nghĩa', '156'),
(374, 'Huyện Sơn Hà', '156'),
(375, 'Huyện Sơn Tây', '156'),
(376, 'Huyện Minh Long', '156'),
(377, 'Huyện Nghĩa Hành', '156'),
(378, 'Huyện Mộ Đức', '156'),
(379, 'Huyện Đức Phổ', '156'),
(380, 'Huyện Ba Tơ', '156'),
(381, 'Huyện Lý Sơn', '156'),
(382, 'Thành phố Qui Nhơn', '157'),
(383, 'Huyện Hoài Nhơn', '157'),
(384, 'Huyện Hoài Ân', '157'),
(385, 'Huyện Phù Mỹ', '157'),
(386, 'Huyện Vĩnh Thạnh', '157'),
(387, 'Huyện Tây Sơn', '157'),
(388, 'Huyện Phù Cát', '157'),
(389, 'Huyện An Nhơn', '157'),
(390, 'Huyện Tuy Phước', '157'),
(391, 'Huyện Vân Canh', '157'),
(392, 'Thành Phố Tuy Hòa', '158'),
(393, 'Thị xã Sông Cầu', '158'),
(394, 'Huyện Đồng Xuân', '158'),
(395, 'Huyện Tuy An', '158'),
(396, 'Huyện Sơn Hòa', '158'),
(397, 'Huyện Sông Hinh', '158'),
(398, 'Huyện Tuy Hòa', '158'),
(399, 'Huyện Phú Hoà', '158'),
(400, 'Huyện Tây Hoà', '158'),
(401, 'Huyện Đông Hoà', '158'),
(402, 'Thành phố Nha Trang', '159'),
(403, 'Thành phố Cam Ranh', '159'),
(404, 'Huyện Vạn Ninh', '159'),
(405, 'Thị xã Ninh Hòa', '159'),
(406, 'Huyện Khánh Vĩnh', '159'),
(407, 'Huyện Diên Khánh', '159'),
(408, 'Huyện Khánh Sơn', '159'),
(409, 'Huyện Trường Sa', '159'),
(410, 'Thị xã Phan Rang-Tháp Chàm', '160'),
(411, 'Huyện Bác ái', '160'),
(412, 'Huyện Ninh Sơn', '160'),
(413, 'Huyện Ninh Hải', '160'),
(414, 'Huyện Ninh Phước', '160'),
(415, 'Huyện Thuận Bắc', '160'),
(416, 'Thành phố Phan Thiết', '161'),
(417, 'Huyện Tuy Phong', '161'),
(418, 'Huyện Bắc Bình', '161'),
(419, 'Huyện Hàm Thuận Bắc', '161'),
(420, 'Huyện Hàm Thuận Nam', '161'),
(421, 'Huyện Tánh Linh', '161'),
(422, 'Huyện Đức Linh', '161'),
(423, 'Huyện Hàm Tân', '161'),
(424, 'Huyện Phú Quí', '161'),
(425, 'Thị xã La Gi', '161'),
(426, 'Thành phố Kon Tum', '162'),
(427, 'Huyện Đắk Glei', '162'),
(428, 'Huyện Ngọc Hồi', '162'),
(429, 'Huyện Đắk Tô', '162'),
(430, 'Huyện Kon Plông', '162'),
(431, 'Huyện Kon Rẫy', '162'),
(432, 'Huyện Đắk Hà', '162'),
(433, 'Huyện Sa Thầy', '162'),
(434, 'Huyện Tu Mơ Rông', '162'),
(435, 'Thành phố Pleiku', '163'),
(436, 'Thị Xã An Khê', '163'),
(437, 'Huyện KBang', '163'),
(438, 'Huyện Đăk Đoa', '163'),
(439, 'Huyện Chư Păh', '163'),
(440, 'Huyện Ia Grai', '163'),
(441, 'Huyện Mang Yang', '163'),
(442, 'Huyện Kông Chro', '163'),
(443, 'Huyện Đức Cơ', '163'),
(444, 'Huyện Chư Prông', '163'),
(445, 'Huyện Chư Sê', '163'),
(446, 'Huyện Đăk Pơ', '163'),
(447, 'Huyện Ia Pa', '163'),
(448, 'Thị xã Ayun Pa', '163'),
(449, 'Huyện Krông Pa', '163'),
(450, 'Thành phố Buôn Ma Thuột', '164'),
(451, 'Huyện Ea H\'leo', '164'),
(452, 'Huyện Ea Súp', '164'),
(453, 'Huyện Buôn Đôn', '164'),
(454, 'Huyện Cư M\'gar', '164'),
(455, 'Huyện Krông Búk', '164'),
(456, 'Huyện Krông Năng', '164'),
(457, 'Huyện Ea Kar', '164'),
(458, 'Huyện M\'Đrắk', '164'),
(459, 'Huyện Krông Bông', '164'),
(460, 'Huyện Krông Pắc', '164'),
(461, 'Huyện Krông A Na', '164'),
(462, 'Huyện Lắk', '164'),
(463, 'Huyện Đắk Glong', '165'),
(464, 'Huyện Cư Jút', '165'),
(465, 'Huyện Đắk Mil', '165'),
(466, 'Huyện Krông Nô', '165'),
(467, 'Huyện Đắk Song', '165'),
(468, 'Huyện Đắk R\'Lấp', '165'),
(469, 'Thị xã Gia Nghĩa', '165'),
(470, 'Thành phố Đà Lạt', '166'),
(471, 'Thành phố Bảo Lộc', '166'),
(472, 'Huyện Lạc Dương', '166'),
(473, 'Huyện Lâm Hà', '166'),
(474, 'Huyện Đơn Dương', '166'),
(475, 'Huyện Đức Trọng', '166'),
(476, 'Huyện Di Linh', '166'),
(477, 'Huyện Đạ Huoai', '166'),
(478, 'Huyện Đạ Tẻh', '166'),
(479, 'Huyện Cát Tiên', '166'),
(480, 'Huyện Đam Rông', '166'),
(481, 'Thị xã Đồng Xoài', '167'),
(482, 'Huyện Bù Gia Mập', '167'),
(483, 'Huyện Lộc Ninh', '167'),
(484, 'Huyện Bù Đốp', '167'),
(485, 'Huyện Hớn Quản', '167'),
(486, 'Huyện Đồng Phú', '167'),
(487, 'Huyện Bù Đăng', '167'),
(488, 'Huyện Chơn Thành', '167'),
(489, 'Thị xã Tây Ninh', '168'),
(490, 'Huyện Tân Biên', '168'),
(491, 'Huyện Tân Châu', '168'),
(492, 'Huyện Dương Minh Châu', '168'),
(493, 'Huyện Châu Thành', '168'),
(494, 'Huyện Hòa Thành', '168'),
(495, 'Huyện Gò Dầu', '168'),
(496, 'Huyện Bến Cầu', '168'),
(497, 'Huyện Trảng Bàng', '168'),
(498, 'Thành phố Thủ Dầu Một', '169'),
(499, 'Huyện Dầu Tiếng', '169'),
(500, 'Thị xã Bến Cát', '169'),
(501, 'Huyện Phú Giáo', '169'),
(502, 'Thị xã Tân Uyên', '169'),
(503, 'Thị xã Dĩ An', '169'),
(504, 'Thị xã Thuận An', '169'),
(505, 'Thành phố Biên Hòa', '170'),
(506, 'Thị xã Long Khánh', '170'),
(507, 'Huyện Tân Phú', '170'),
(508, 'Huyện Vĩnh Cửu', '170'),
(509, 'Huyện Định Quán', '170'),
(510, 'Huyện Trảng Bom', '170'),
(511, 'Huyện Thống Nhất', '170'),
(512, 'Huyện Cẩm Mỹ', '170'),
(513, 'Huyện Long Thành', '170'),
(514, 'Huyện Xuân Lộc', '170'),
(515, 'Huyện Nhơn Trạch', '170'),
(516, 'Thành phố Vũng Tàu', '171'),
(517, 'Thị xã Bà Rịa', '171'),
(518, 'Huyện Châu Đức', '171'),
(519, 'Huyện Xuyên Mộc', '171'),
(520, 'Huyện Long Điền', '171'),
(521, 'Huyện Đất Đỏ', '171'),
(522, 'Huyện Tân Thành', '171'),
(523, 'Huyện Côn Đảo', '171'),
(524, 'Quận 1', '172'),
(525, 'Quận 12', '172'),
(526, 'Quận Thủ Đức', '172'),
(527, 'Quận 9', '172'),
(528, 'Quận Gò Vấp', '172'),
(529, 'Quận Bình Thạnh', '172'),
(530, 'Quận Tân Bình', '172'),
(531, 'Quận Tân Phú', '172'),
(532, 'Quận Phú Nhuận', '172'),
(533, 'Quận 2', '172'),
(534, 'Quận 3', '172'),
(535, 'Quận 10', '172'),
(536, 'Quận 11', '172'),
(537, 'Quận 4', '172'),
(538, 'Quận 5', '172'),
(539, 'Quận 6', '172'),
(540, 'Quận 8', '172'),
(541, 'Quận Bình Tân', '172'),
(542, 'Quận 7', '172'),
(543, 'Huyện Củ Chi', '172'),
(544, 'Huyện Hóc Môn', '172'),
(545, 'Huyện Bình Chánh', '172'),
(546, 'Huyện Nhà Bè', '172'),
(547, 'Huyện Cần Giờ', '172'),
(548, 'Thành phố Tân An', '173'),
(549, 'Huyện Tân Hưng', '173'),
(550, 'Huyện Vĩnh Hưng', '173'),
(551, 'Huyện Mộc Hóa', '173'),
(552, 'Huyện Tân Thạnh', '173'),
(553, 'Huyện Thạnh Hóa', '173'),
(554, 'Huyện Đức Huệ', '173'),
(555, 'Huyện Đức Hòa', '173'),
(556, 'Huyện Bến Lức', '173'),
(557, 'Huyện Thủ Thừa', '173'),
(558, 'Huyện Tân Trụ', '173'),
(559, 'Huyện Cần Đước', '173'),
(560, 'Huyện Cần Giuộc', '173'),
(561, 'Thành phố Mỹ Tho', '174'),
(562, 'Thị xã Gò Công', '174'),
(563, 'Huyện Tân Phước', '174'),
(564, 'Huyện Cái Bè', '174'),
(565, 'Huyện Cai Lậy', '174'),
(566, 'Huyện Chợ Gạo', '174'),
(567, 'Huyện Gò Công Tây', '174'),
(568, 'Huyện Gò Công Đông', '174'),
(569, 'Thành phố Bến Tre', '175'),
(570, 'Huyện Chợ Lách', '175'),
(571, 'Huyện Mỏ Cày Nam', '175'),
(572, 'Huyện Giồng Trôm', '175'),
(573, 'Huyện Bình Đại', '175'),
(574, 'Huyện Ba Tri', '175'),
(575, 'Huyện Thạnh Phú', '175'),
(576, 'Thành phố Trà Vinh', '176'),
(577, 'Huyện Càng Long', '176'),
(578, 'Huyện Cầu Kè', '176'),
(579, 'Huyện Tiểu Cần', '176'),
(580, 'Huyện Cầu Ngang', '176'),
(581, 'Huyện Trà Cú', '176'),
(582, 'Huyện Duyên Hải', '176'),
(583, 'Thành phố Vĩnh Long', '177'),
(584, 'Huyện Long Hồ', '177'),
(585, 'Huyện Mang Thít', '177'),
(586, 'Huyện Vũng Liêm', '177'),
(587, 'Huyện Tam Bình', '177'),
(588, 'Thị xã Bình Minh', '177'),
(589, 'Huyện Trà Ôn', '177'),
(590, 'Thành phố Cao Lãnh', '178'),
(591, 'Thành phố Sa Đéc', '178'),
(592, 'Huyện Tân Hồng', '178'),
(593, 'Huyện Hồng Ngự', '178'),
(594, 'Huyện Tháp Mười', '178'),
(595, 'Huyện Cao Lãnh', '178'),
(596, 'Huyện Thanh Bình', '178'),
(597, 'Huyện Lấp Vò', '178'),
(598, 'Huyện Lai Vung', '178'),
(599, 'Thành phố Long Xuyên', '179'),
(600, 'Thành phố Châu Đốc', '179'),
(601, 'Huyện An Phú', '179'),
(602, 'Thị xã Tân Châu', '179'),
(603, 'Huyện Phú Tân', '179'),
(604, 'Huyện Châu Phú', '179'),
(605, 'Huyện Tịnh Biên', '179'),
(606, 'Huyện Tri Tôn', '179'),
(607, 'Huyện Thoại Sơn', '179'),
(608, 'Thành phố Rạch Giá', '180'),
(609, 'Thị xã Hà Tiên', '180'),
(610, 'Huyện Kiên Lương', '180'),
(611, 'Huyện Hòn Đất', '180'),
(612, 'Huyện Tân Hiệp', '180'),
(613, 'Huyện Giồng Riềng', '180'),
(614, 'Huyện Gò Quao', '180'),
(615, 'Huyện An Biên', '180'),
(616, 'Huyện An Minh', '180'),
(617, 'Huyện Vĩnh Thuận', '180'),
(618, 'Huyện Phú Quốc', '180'),
(619, 'Huyện Kiên Hải', '180'),
(620, 'Quận Ninh Kiều', '181'),
(621, 'Quận Ô Môn', '181'),
(622, 'Quận Bình Thuỷ', '181'),
(623, 'Quận Cái Răng', '181'),
(624, 'Quận Thốt Nốt', '181'),
(625, 'Huyện Cờ Đỏ', '181'),
(626, 'Thành phố Vị Thanh', '182'),
(627, 'Huyện Châu Thành A', '182'),
(628, 'Huyện Phụng Hiệp', '182'),
(629, 'Huyện Vị Thuỷ', '182'),
(630, 'Huyện Long Mỹ', '182'),
(631, 'Thị Xã Ngã Bảy', '182'),
(632, 'Thành phố Sóc Trăng', '183'),
(633, 'Huyện Kế Sách', '183'),
(634, 'Huyện Mỹ Tú', '183'),
(635, 'Huyện Cù Lao Dung', '183'),
(636, 'Huyện Long Phú', '183'),
(637, 'Huyện Mỹ Xuyên', '183'),
(638, 'Thị xã Ngã Năm', '183'),
(639, 'Huyện Thạnh Trị', '183'),
(640, 'Thị xã Vĩnh Châu', '183'),
(641, 'Thành phố Bạc Liêu', '184'),
(642, 'Huyện Hồng Dân', '184'),
(643, 'Huyện Phước Long', '184'),
(644, 'Huyện Vĩnh Lợi', '184'),
(645, 'Huyện Giá Rai', '184'),
(646, 'Huyện Đông Hải', '184'),
(647, 'Huyện Hòa Bình', '184'),
(648, 'Thành phố Cà Mau', '185'),
(649, 'Huyện U Minh', '185'),
(650, 'Huyện Thới Bình', '185'),
(651, 'Huyện Trần Văn Thời', '185'),
(652, 'Huyện Cái Nước', '185'),
(653, 'Huyện Đầm Dơi', '185'),
(654, 'Huyện Năm Căn', '185'),
(655, 'Huyện Ngọc Hiển', '185'),
(656, 'Huyện Mường ảng', '129'),
(657, 'Huyện Tuy Đức', '165'),
(658, 'Huyện Tân Sơn', '138'),
(659, 'Huyện U Minh Thượng', '180'),
(660, 'Huyện Phú Thiện', '163'),
(661, 'Huyện Lộc Hà', '150'),
(662, 'Huyện Cam Lâm', '159'),
(663, 'Huyện Cư Kuin', '164'),
(664, 'Quận Dương Kinh', '142'),
(665, 'Huyện Bình Tân', '177'),
(666, 'Tân Phú Đông', '174'),
(667, 'Huyện Nông Sơn', '155'),
(668, 'Thị xã Thái Hoà', '149'),
(669, 'Huyện Tân Uyên', '130'),
(670, 'Huyện Thới Lai', '181'),
(671, 'Thị xã Buôn Hồ', '164'),
(672, 'Huyện Mỏ Cày Bắc', '175'),
(673, 'Huyện Sông Lô', '139'),
(674, 'Thị xã Hồng Ngự', '178'),
(675, 'Huyện Giang Thành', '180'),
(676, 'Huyện Thuận Nam', '160'),
(677, 'Thị xã Bình Long', '167'),
(678, 'Thị xã Phước Long', '167'),
(679, 'Huyện Chư Pưh', '163'),
(680, 'Huyện Trần Đề', '183'),
(681, 'Huyện Lâm Bình', '127'),
(682, 'Thị xã Kiến Tường', '173'),
(683, 'Huyện Nậm Pồ', '129'),
(684, 'Huyện Nậm Nhùn', '130'),
(685, 'Thị xã Hoàng Mai', '149'),
(686, 'Huyện Vân Hồ', '131'),
(687, 'Huyện Bàu Bàng', '169'),
(688, 'Huyện Bắc Tân Uyên', '169'),
(689, 'Quận Bắc Từ Liêm', '123'),
(690, 'Thị xã Cai Lậy', '174'),
(691, 'Thị xã Ba Đồn', '151'),
(692, 'Huyện Ia H\'Drai', '162'),
(693, 'Thị xã Duyên Hải', '176'),
(694, 'Huyện Phú Riềng', '167'),
(695, 'Thị xã Long Mỹ', '182'),
(696, 'Thị xã Kỳ Anh', '150'),
(697, 'Huyện Bảo Lâm', '166'),
(698, 'Huyện Châu Thành', '179'),
(699, 'Huyện Châu Thành', '175'),
(700, 'Huyện Phú Tân', '185'),
(701, 'Huyện Vĩnh Thạnh', '181'),
(702, 'Huyện Phong Điền', '181'),
(703, 'Huyện Châu Thành', '178'),
(704, 'Huyện Tam Nông', '178'),
(705, 'Quận Ba Đình', '123'),
(706, 'Huyện Châu Thành', '182'),
(707, 'Huyện Châu Thành', '180'),
(708, 'Huyện Châu Thành', '173'),
(709, 'Huyện Kỳ Sơn', '149'),
(710, 'Huyện Châu Thành', '183'),
(711, 'Huyện Châu Thành', '174'),
(712, 'Huyện Châu Thành', '176');

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE `subscriber` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'custom', '2018-08-16 03:02:25', '2018-08-16 03:02:25'),
(2, 'anh', '2018-08-16 03:07:58', '2018-08-16 03:07:58'),
(3, 'yêu', '2018-08-16 03:07:58', '2018-08-16 03:07:58'),
(4, 'hay', '2018-08-16 03:07:58', '2018-08-16 03:07:58'),
(5, 'real', '2018-08-16 12:10:13', '2018-08-16 12:10:13'),
(6, 'test', '2018-08-17 03:03:40', '2018-08-17 03:03:40'),
(7, 'abcd', '2018-08-18 04:44:56', '2018-08-18 04:44:56'),
(8, 'business', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(9, 'corporate', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(10, 'Life tranding', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(11, 'News', '2018-08-21 07:44:52', '2018-08-21 07:44:52'),
(12, 'bag', '2018-08-21 16:46:29', '2018-08-21 16:46:29'),
(13, 'Sofa', '2018-08-26 14:54:47', '2018-08-26 14:54:47'),
(14, 'Q&A', '2018-08-26 19:45:27', '2018-08-26 19:45:27');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '1000',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonial_translations`
--

CREATE TABLE `testimonial_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `testimonial_id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `random` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role_id`, `name`, `email`, `phone`, `password`, `random`, `created_at`, `updated_at`) VALUES
(1, -1, 'Super Admin', 'superadmin@gmail.com', '0123456789', '$2y$10$ou1JPIj5juc.jmar9s2juuLzTSIZ89l3lWk0Bf3TfjPu9EKDG3CRC', '', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(2, 0, 'Admin', 'admin@gmail.com', '0123456789', '$2y$10$wWkbIazSDvN.IJdok6Ey8.Qmf6kzUcK5oZeV2kQCmdQ.7t21xM5Jm', '', '2018-08-07 02:47:21', '2018-08-07 02:47:21'),
(6, 1, 'danh', 'danhle.190996@gmail.com', '', '$2y$10$Uehrp0rxqv8eOedbuNIm9urbl6r69Qm7.vzH5CUJnLqhzh6P7hXbG', '', '2018-11-08 08:18:00', '2018-11-08 08:18:23');

-- --------------------------------------------------------

--
-- Table structure for table `variant`
--

CREATE TABLE `variant` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Default',
  `product_id` int(11) NOT NULL,
  `option_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` bigint(20) DEFAULT NULL,
  `price_compare` bigint(20) DEFAULT NULL,
  `stock_quant` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sale_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `variant`
--

INSERT INTO `variant` (`id`, `title`, `product_id`, `option_1`, `option_2`, `option_3`, `option_4`, `option_5`, `option_6`, `price`, `price_compare`, `stock_quant`, `status`, `created_at`, `updated_at`, `sale_id`) VALUES
(1, 'Default', 1, '', '', '', '', '', '', 150000, 0, -18, 'active', '2018-08-10 09:09:07', '2018-10-04 07:30:37', NULL),
(2, 'Default', 2, '', '', '', '', '', '', 65000, 0, -20, 'active', '2018-08-10 09:09:31', '2018-11-07 02:43:58', NULL),
(3, 'Default', 3, '', '', '', '', '', '', 160000, 280000, -11, 'active', '2018-08-10 09:12:26', '2018-11-07 02:44:25', NULL),
(4, 'Default', 4, '', '', '', '', '', '', 150000, 0, -8, 'active', '2018-08-10 09:14:12', '2018-11-07 02:44:40', NULL),
(5, 'Default', 5, '', '', '', '', '', '', 350000, 0, -3, 'active', '2018-08-10 09:14:48', '2018-08-10 09:14:48', NULL),
(6, 'Default', 6, '', '', '', '', '', '', 650000, 0, -2, 'active', '2018-08-10 09:15:39', '2018-09-25 09:13:10', NULL),
(7, 'Default', 7, '', '', '', '', '', '', 450000, 0, -2, 'active', '2018-08-10 09:16:44', '2018-08-10 09:16:44', NULL),
(8, 'Default', 8, '', '', '', '', '', '', 650000, 660000, -2, 'active', '2018-08-10 09:18:39', '2018-09-20 08:50:24', NULL),
(9, 'Default', 9, '', '', '', '', '', '', 0, 0, 0, 'active', '2018-08-21 16:46:06', '2018-09-20 03:20:07', NULL),
(10, 'Default', 10, '', '', '', '', '', '', 300000, 600000, -5, 'active', '2018-08-26 14:51:40', '2018-08-26 15:09:41', NULL),
(11, 'Default', 11, '', '', '', '', '', '', 0, 0, 0, 'active', '2018-08-26 15:54:50', '2018-09-20 08:51:22', NULL),
(12, 'Default', 12, '', '', '', '', '', '', 450000, 0, 1, 'active', '2018-09-20 09:08:30', '2018-09-20 09:14:04', NULL),
(13, 'Default', 13, '', '', '', '', '', '', 450000, 0, 1, 'active', '2018-09-20 09:21:42', '2018-09-20 09:44:01', NULL),
(14, 'Default', 14, '', '', '', '', '', '', 700000, 0, 1, 'active', '2018-09-20 09:27:40', '2018-09-25 10:58:42', NULL),
(15, 'Default', 15, '', '', '', '', '', '', 10000, 0, 1, 'active', '2018-11-07 08:58:31', '2018-11-07 09:08:25', NULL),
(16, 'Default', 16, '', '', '', '', '', '', 100000, 0, 1, 'active', '2018-11-07 08:58:48', '2018-11-07 09:03:23', NULL),
(17, 'Default', 17, '', '', '', '', '', '', 20000, 0, 1, 'active', '2018-11-07 08:59:49', '2018-11-07 09:04:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_translations`
--
ALTER TABLE `article_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute`
--
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_translations`
--
ALTER TABLE `attribute_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_article`
--
ALTER TABLE `blog_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_translations`
--
ALTER TABLE `blog_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collection`
--
ALTER TABLE `collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collection_product`
--
ALTER TABLE `collection_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collection_translations`
--
ALTER TABLE `collection_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_review`
--
ALTER TABLE `customer_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_translations`
--
ALTER TABLE `gallery_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_translations`
--
ALTER TABLE `menu_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metafield`
--
ALTER TABLE `metafield`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metafield_translations`
--
ALTER TABLE `metafield_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_translations`
--
ALTER TABLE `photo_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_buy_together`
--
ALTER TABLE `product_buy_together`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_translations`
--
ALTER TABLE `product_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_product`
--
ALTER TABLE `sale_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_translations`
--
ALTER TABLE `seo_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_address`
--
ALTER TABLE `shipping_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_fee_region`
--
ALTER TABLE `shipping_fee_region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_fee_subregion`
--
ALTER TABLE `shipping_fee_subregion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_order`
--
ALTER TABLE `shipping_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slug`
--
ALTER TABLE `slug`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statistic`
--
ALTER TABLE `statistic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subregion`
--
ALTER TABLE `subregion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriber`
--
ALTER TABLE `subscriber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial_translations`
--
ALTER TABLE `testimonial_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variant`
--
ALTER TABLE `variant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `article_translations`
--
ALTER TABLE `article_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute`
--
ALTER TABLE `attribute`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `attribute_translations`
--
ALTER TABLE `attribute_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `blog_article`
--
ALTER TABLE `blog_article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `blog_translations`
--
ALTER TABLE `blog_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collection`
--
ALTER TABLE `collection`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `collection_product`
--
ALTER TABLE `collection_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `collection_translations`
--
ALTER TABLE `collection_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `customer_review`
--
ALTER TABLE `customer_review`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gallery_translations`
--
ALTER TABLE `gallery_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1147;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `menu_translations`
--
ALTER TABLE `menu_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `metafield`
--
ALTER TABLE `metafield`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `metafield_translations`
--
ALTER TABLE `metafield_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=556;

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `photo_translations`
--
ALTER TABLE `photo_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product_buy_together`
--
ALTER TABLE `product_buy_together`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_translations`
--
ALTER TABLE `product_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sale_product`
--
ALTER TABLE `sale_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `seo_translations`
--
ALTER TABLE `seo_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shipping_address`
--
ALTER TABLE `shipping_address`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `shipping_fee_region`
--
ALTER TABLE `shipping_fee_region`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shipping_fee_subregion`
--
ALTER TABLE `shipping_fee_subregion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shipping_order`
--
ALTER TABLE `shipping_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slug`
--
ALTER TABLE `slug`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statistic`
--
ALTER TABLE `statistic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT for table `subregion`
--
ALTER TABLE `subregion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=713;

--
-- AUTO_INCREMENT for table `subscriber`
--
ALTER TABLE `subscriber`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonial_translations`
--
ALTER TABLE `testimonial_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `variant`
--
ALTER TABLE `variant`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `variant`
--
ALTER TABLE `variant`
  ADD CONSTRAINT `variant_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sale` (`id`),
  ADD CONSTRAINT `variant_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `sale` (`id`),
  ADD CONSTRAINT `variant_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `sale` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
