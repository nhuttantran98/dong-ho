# Base Theme for StepCMS

## How to run
- Clone `eye-themes` project for the latest version of CMS
- Edit `.env` file for your environment
- Init database (or import from a .sql file)
```bash
cd db && php create_tables.php
```
- clone `base` project into `themes` folder (`themes/base`)
```
cd eye-themes/public/themes/
git clone ssh://git@code.eyeteam.vn:30022/themes/base.git
```
- Install dependencies:
```bash
cd base
yarn
```

## Develop
- Run gulp to build and watch
```bash
gulp
```

## Build
- Run gulp build task
```bash
gulp build
```
